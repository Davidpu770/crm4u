<?php
error_reporting(E_ALL);  
    ini_set('display_errors', 1);
$page_level = 0;
$title = '404 not found';
require_once dirname(__FILE__).'/inc/layout/header.php';

?>
<div class="content-wrapper">
<!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may <a href="<?php echo SCRIPT_ROOT; ?>">return to dashboard</a> or try using the search form.
          </p>
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  </div>
<?php
require_once dirname(__FILE__).'/inc/layout/footer.php';

?>