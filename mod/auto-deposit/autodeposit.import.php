<?php
namespace Crm4u\Controller;

use Crm4u\SQL\filter;
use Crm4u\Controller\FormController as Form;

class AutoDeposit {

    /**
     * @var bool
     * only one deposit is in pending
     * default is false
     */
    public $only_one = false;

    /**
     * @var bool
     * if fetch is true data will be printed
     */
    public $fetch = true;

    /**
     * AutoDeposit constructor.
     * @param $type
     * @param $params
     */

    public $json = null;

    function __construct($type, $params)
    {

        $lastDeposit =  self::get_last_deposit_from_local(self::depositType($params[0])) + 1;

        $deposits = ApiController::init($params[0],'FILTER[id][min]', $lastDeposit,'view', self::extraFilter($params[0]));

        ($this->compareTransactionNumber($lastDeposit, $deposits)) ? self::parseResult($deposits) : print_r(json_encode(array('data' => [])));

    }

    function depositType($type){

        switch($type){

            case 'CustomerDeposits':

                return 'deposit';

            case 'Withdrawal':

                return 'withdrawal';

            default:

                return 'deposit';
        }
    }

    static function extraFilter($type){

        switch ($type){

            case 'CustomerDeposits':

                return array();

            case 'Withdrawal':

                return array('FILTER[status]' => 'approved');

            default:

                return array();
        }
    }

    //3. get last deposit from api
    function get_last_deposit_from_api($deposits){

        $lastDeposits = 0;

        //if more then one deposit
        if(isset($deposits[0])){

            $lastDeposits = end($deposits);
            $lastDeposits = $lastDeposits['id'];

        } else {

            $lastDeposits = $deposits['id'];
            $this->only_one = true;
        }


        return $lastDeposits;
    }

    static function get_last_deposit_from_local($type){

        $filter = new filter();

        $cols = array(
            'MAX(tran_id) id'
        );

        $parameters = array(
            'WHERE' => '0 = 0',
            $filter->brandFilter(1),
            $filter->depositTypeFilter($type)

        );

        $result = ImportController::import('goal',$cols, $parameters);

        return $result['data'][0]['id'];
    }

    //4. check if last deposit from api is greater then local last deposit
    function compareTransactionNumber($local, $api){

        $api = $this->get_last_deposit_from_api($api);

        if(self::isGreater($api, $local)){

            return false;
        }

        if(self::isEquable($api, $local)){

            return new SwalController('error', 'no more deposits','error');
        }

        return true;
    }

    //parse
    function parseResult($deposits){
        if($this->only_one){

            if($deposits['paymentMethod'] != 'Bonus'){
                $this->json['data'][] = [
                    'id' => $deposits['id'],
                    'cid' => "<a target='_blank' href='https://spotcrm.zeusoption.com/crm/customers/page/".$deposits['customerId']."'>".$deposits['customerId']."</a>",
                    'date' => $deposits['date'],
                    'paymentMethod' => $deposits['paymentMethod'],
                    'credit_company' => $deposits['clearedBy'],
                    'amount' => $deposits['amount'],
                    'currency' => $deposits['currency'],
                    'approve'  => "<button class='btn btn-info approve' data-target='#new-deposit' data-toggle='modal' data-deposit='".$deposits['id']."'>Approve</button>"
                ];
            }

        } else {

            foreach ($deposits as $key => $deposit){

                if($deposit['paymentMethod'] != 'Bonus'){
                    $this->json['data'][] = [
                        'id' => $deposit['id'],
                        'cid' => "<a target='_blank' href='https://spotcrm.zeusoption.com/crm/customers/page/".$deposit['customerId']."'>".$deposit['customerId']."</a>",
                        'date' => $deposit['date'],
                        'paymentMethod' => $deposit['paymentMethod'],
                        'credit_company' => $deposit['clearedBy'],
                        'amount' => $deposit['amount'],
                        'currency' => $deposit['currency'],
                        'approve'  => "<button class='btn btn-info approve' data-target='#new-deposit' data-toggle='modal' data-deposit='".$deposit['id']."'>Approve</button>"
                    ];
                }

            }

        }

        if(!is_null($this->json)) {

            if ($this->fetch) {

                print_r(json_encode($this->json));
            }
        } else {

            $this->json['data'] = [];

            print_r(json_encode($this->json));
        }


    }

    //checks
    function isGreater($apiTranNum, $localTranNum){

        if($apiTranNum < $localTranNum) {

            return true;
        }
    }

    function isEquable($apiTranNum, $localTranNum){

        if($apiTranNum == $localTranNum) {

            return true;
        }
    }

}