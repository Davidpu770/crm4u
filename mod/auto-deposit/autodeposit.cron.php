<?php
namespace Crm4u\Autodeposit;

use Crm4u\Autodeposit\import;

class cron {

    protected $new_deposit_num;

    function __construct()
    {

        $this->new_deposit_num = self::checkNewDeposit();
        $this->boot();
    }

    function checkNewDeposit(){

        $deposits = new import(FALSE);
        if(isset($deposits->json) && $deposits->json){

            return count($deposits->json);
        }
    }

    function boot(){

        if($this->new_deposit_num > 0 ) {

            //1. title
            $title = "New deposit wait for Approval";

            //2. body:
            $body  = "You have ".$this->new_deposit_num." new deposit wait for your approval";

            //3. participants:
            $participants = array('All');

            new notification(array(), $title, $body, $participants, 'deposit');
        }
    }
}

new cron();