<?php
namespace Crm4u\Mods\API;

use Crm4u\Controller\BrandController;

class optionlift{

    public $error = 0;

    function get_request($module, $id)
    {

        if($module == 'CustomerDeposits'){

            $apiRequestModule = 'ticket';
        }
        elseif($module == 'Withdrawal'){

            $apiRequestModule = 'transaction';
        }
        else{

            $apiRequestModule = 'customer';
        }

        $this->result = $this->init($apiRequestModule, $id);

    }

    /**
     * @param $module
     * @param $id
     */
    function init($module, $id, $callParse = true) {

        $brand = new brand();
        $brand->brand_import();

        $apiData = array(
            'api_username' => $brand->api_format['api_username'],
            'api_password' => $brand->api_format['api_password'],
            );

        $URL = $brand->api_format['api_address'].$module."/".$id;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $apiData['api_username'].":".$apiData['api_password']);
        curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
        $result=curl_exec($ch);

        $info = curl_getinfo($ch);

        curl_close ($ch);
         $result = json_decode($result, TRUE);


        //Init parse
        if($callParse){

            $call = $module."Parse";
            $this->$call($result);
        } else {

            return $result;
        }
    }

    /**
     * @param $result
     * @return int
     */
    function customerParse($result){

        $apiResult = array(
            'name' => $result['fname']." ".$result['lname'],
            'currency'  => $result['currency'],
            'campaign'  => self::campaignParse($result['campaign_id']),
            'country'   => $result['country'],
            'ename'     => self::employeeParse($result['broker_employee_id']),
            'first_dep' => self::getFirstDepositor($result['id'])
        );

        return print json_encode($apiResult);
    }

    /**
     * @param $result
     * @return int
     */
    function ticketParse($result){

        if($result['status'] == 1 || $result['status'] == 0){

            $method = self::methodParse($result['method']);
            $apiResult = array(
                'amount'         => $result['value'] / 10000,
                'paymentMethod'  => $method,
                'customerId'     => $result['customer_id'],
                'clearedBy'      => $result['method']
            );

            return print json_encode($apiResult);
        }

    }

    /**
     * @param $result
     * @return int
     */
    function transactionParse($result){

        //change from Wire transfer to Wire Transfer on: 12/06/2016
        $method = self::methodParse($result['method']);
        $apiResult = array(
            'amount'         => $result['value'] / 10000,
            'paymentMethod'  => $method,
            'customerId'     => $result['customer_id'],
            'clearedBy'      => $result['method']
        );

        return print json_encode($apiResult);
    }

    /**
     * @param $cid
     * @return mixed
     */
    function campaignParse($cid){

        if(!is_null($cid)) {
            $result = $this->init('campaign',$cid,false);


            $name = (!isset($result['name'])) ? $result[0]['name'] : $result['name'] ;

            $apiResult = array(
                'campaign_name' => $name
            );

            return $apiResult['campaign_name'];
        } else {

            return null;
        }


    }

    /**
     * @param $method
     * @return string*
     *
     */
    function methodParse($method){

        return ($method == "Wire Transfer") ? "Wire" : "Credit Card";
    }


    /**
     * @param $cid
     * @return mixed
     */
    function employeeParse($eid){

        if(!is_null($eid)){
            $result = $this->init('employee',$eid,false);

            $apiResult = array(
                'name' => $result['fname']." ".$result['lname']
            );

            return $apiResult['name'];
        }
    }

    function getFirstDepositor($cid){

        global $loader->mysqli;

        $sql = "SELECT CONCAT(firstname,' ',lastname) name FROM goal";

        $sql .= " WHERE customer_id = '$cid' AND ftd = 1";

        $sql .= " AND brand = '".$_SESSION['brand']."'";


        $result = $loader->mysqli->query($sql);

        if($result->num_rows > 0){

            $row = $result->fetch_assoc();
            return $row['name'];

        } else {

            return null;
        }

    }
}
