<?php

namespace Crm4u\Mods\API;

interface SpotModule {

    /**
     * Customer module
     * @param $result
     * @return array
     */
    function iCustomer($result);

    /*
     * Withdrawal module
     * @param $result
     * @return array
     */
    function iWithdrawal($result);

    /**
     * All customer Deposits module
     * @param $result
     * @return array
     */
    function iCustomerDeposits($result);

    /**
     * Campaign module
     * @param $result
     * @return array
     */
    function iCampaign($result);
}