<?php
namespace Crm4u\Mods\API;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Controller\SwalController;
use Crm4u\Deposits\charts;
use Crm4u\SQL\filter;

class SpotOption implements SpotModule {

    protected $username;

    protected $password;

    protected $apiUrl;

    protected $fetch;

    function __construct()
    {

        $apiDetails = BrandController::apiDetails();

        $this->username = $apiDetails->api_username;
        $this->password = $apiDetails->api_password;
        $this->apiUrl   = $apiDetails->api_address;
    }
    /**
     * @param $module
     * @param $id
     */
    function get_request($module, $filter, $filterVal, $command = 'view', $extra = array(), $fetch = true){

        $this->fetch = $fetch;

        return $this->init($module, $filter, $filterVal, $command, $extra);
    }

    /**
     * @param $module
     * @param $id
     */
    function init($module, $filter = "FILTER[id]", $filterVal, $command, $extra){

        $apiData = array(
            'api_username' => $this->username,
            'api_password' => $this->password,
            'MODULE'       => $module,
            'COMMAND'      => $command,
            $filter        => $filterVal,
            'jsonResponse' => 'true'
        );

        $apiData = array_merge($apiData, $extra);

        $ch = curl_init($this->apiUrl);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($apiData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);

        //close the connection
        curl_close($ch);

        //check result
        $this->CheckResult($result);

        //call to parse
        $callback = "i".$module;

        if(method_exists(self::class, $callback)){

            return $this->$callback($result);
        } else {

            new SwalController('error','Module '.$module.' is not exist',"error");
        }
    }

    function iCustomerDeposits($result){

        $response = json_decode($result, TRUE);

        self::check_customer($result, $this->fetch);

        if(count($response['status']['CustomerDeposits']) > 1) {

            foreach ($response['status']['CustomerDeposits'] as $key => $deposit) {

                $transaction[] = array(
                    'id'            => $response['status']['CustomerDeposits'][$key]['depositId'],
                    'amount'        => $response['status']['CustomerDeposits'][$key]['amount'],
                    'paymentMethod' => $response['status']['CustomerDeposits'][$key]['paymentMethod'],
                    'customerId'    => $response['status']['CustomerDeposits'][$key]['customerId'],
                    'clearedBy'     => $response['status']['CustomerDeposits'][$key]['clearedBy'],
                    'currency'      => $response['status']['CustomerDeposits'][$key]['currency'],
                    'date'          => $response['status']['CustomerDeposits'][$key]['requestTime']
                );
            }

        } else {


            self::checkBonus($response['status']['CustomerDeposits']['data_0']['paymentMethod']);
            //import tran
            $transaction = array(
                'id'            => $response['status']['CustomerDeposits']['data_0']['depositId'],
                'amount'        => $response['status']['CustomerDeposits']['data_0']['amount'],
                'paymentMethod' => $response['status']['CustomerDeposits']['data_0']['paymentMethod'],
                'customerId'    => $response['status']['CustomerDeposits']['data_0']['customerId'],
                'clearedBy'     => $response['status']['CustomerDeposits']['data_0']['clearedBy'],
                'currency'      => $response['status']['CustomerDeposits']['data_0']['currency'],
                'date'          => $response['status']['CustomerDeposits']['data_0']['requestTime']
            );

        }
            return $transaction;
    }

    function CheckResult($result) {

        $response = json_decode($result, TRUE);

        if(isset($response['status']['errors'])){

            new SwalController('You hae an error',$response['status']['errors']['error'],'error');
            die();
        }
    }

    function iCustomer($result){

        global $loader;

        $response = json_decode($result, TRUE);


        self::check_customer($result);

        if(count($response['status']['Customer']) > 1) {

            foreach ($response['status']['Customer'] as $key => $customers) {

                $customer[] = array(
                    'name'       => $response['status']['Customer'][$key]['FirstName']." ".$response['status']['Customer'][$key]['LastName'],
                    'currency'   => $response['status']['Customer'][$key]['currency'],
                    'campaign'   => $response['status']['Customer'][$key]['campaignName'],
                    'country'    => $response['status']['Customer'][$key]['countryName'],
                    'first_dep'  => self::getFirstDepositor($response['status']['Customer'][$key]['id'])
                );

            }

        } else {
                //import cid
                $customer = array(
                    'name'       => $response['status']['Customer']['data_0']['FirstName']." ".$response['status']['Customer']['data_0']['LastName'],
                    'currency'   => $response['status']['Customer']['data_0']['currency'],
                    'campaign'   => ($response['status']['Customer']['data_0']['campaignName'] == "") ? $loader->site->default_campaign : $response['status']['Customer']['data_0']['campaignName'],
                    'country'    => $response['status']['Customer']['data_0']['countryName'],
                    'first_dep'  => self::getFirstDepositor($response['status']['Customer']['data_0']['id'])
                );


        }

        return $customer;

    }

    function iWithdrawal($result){

        global $loader;

        $response = json_decode($result, TRUE);

        self::check_customer($result);

        if(count($response['status']['Withdrawal']) > 1) {

            foreach ($response['status']['Withdrawal'] as $key => $deposit) {

                $transaction[] = array(
                    'id'            => $response['status']['Withdrawal'][$key]           ['id'],
                    'amount'        => $response['status']['Withdrawal'][$key]       ['amount'],
                    'paymentMethod' => $response['status']['Withdrawal'][$key]['paymentMethod'],
                    'customerId'    => $response['status']['Withdrawal'][$key]   ['customerId'],
                    'date'          => $response['status']['Withdrawal'][$key]  ['requestTime'],
                    'clearedBy'     => $loader->site->default_clearedby,
                    'currency'      => $response['status']['Withdrawal'][$key]     ['currency'],
                );

            }

        } else {

            //import tran
            $transaction = array(
                'id'            => $response['status']['Withdrawal']['data_0']           ['id'],
                'amount'        => $response['status']['Withdrawal']['data_0']       ['amount'],
                'paymentMethod' => $response['status']['Withdrawal']['data_0']['paymentMethod'],
                'customerId'    => $response['status']['Withdrawal']['data_0']   ['customerId'],
                'date'          => $response['status']['Withdrawal']['data_0']  ['requestTime'],
                'clearedBy'     => $loader->site->default_clearedby,
                'currency'      => $response['status']['Withdrawal']['data_0']     ['currency'],
            );
        }
        return $transaction;
    }

    function iCampaign($result){

        $response = json_decode($result, TRUE);

        $charts = new charts();

        $last6month = date("Y-m-d", strtotime("-1 months"));
        if(count($response['status']['Campaign']) > 1) {

            //array code goes here...



            foreach ($response['status']['Campaign'] as $key => $deposit) {


                if($response['status']['Campaign'][$key]['customersNum'] > 0 && $response['status']['Campaign'][$key]['lastUpdateDate'] > $last6month) {

                    //$this->get_request('Customer','FILTER[campaignId]', $response['status']['Campaign'][$key]['id'], 'view');

                    $response['status']['Campaign'][$key]['name'] = ($response['status']['Campaign'][$key]['name'] == "") ? "no-campaign" : $response['status']['Campaign'][$key]['name'];

                    $campaign[] = array(
                        'name'               => $response['status']['Campaign'][$key]['name'],
                        'customersNum'       => $response['status']['Campaign'][$key]['customersNum'],
                        'customer'           => 0,
                        'totalDeposits'      => $charts->campaignMonitor('json', false, $response['status']['Campaign'][$key]['name'])
                    );
                }

            }

        } else {

            //import tran
            $campaign = array(
                'name'               => $response['status']['Campaign']['data_0']['name'],
                'customersNum'       => $response['status']['Campaign']['data_0']['customersNum'],
                'totalDeposits'      => $charts->campaignMonitor('json', false, $response['status']['Campaign']['data_0']['name'])
            );
        }

        return $campaign;
    }

    /**
     * @param $result
     */
    function check_customer($result, $fetch = true){


        if($fetch) {

            if(strpos($result, "noResult")){

                new SwalController("you have an error","The transaction or customer you are looking for are not exist","error");
                die();
            }
        } else {

            if(strpos($result, "noResult")) {
                return print_r("No deposit");
                die();
            }
        }

    }

    /**
     * @param $pm
     */
    function checkBonus($pm){

        if($pm == "Bonus"){

            new SwalController("you have an error","This is a Bonus Deposit","error");
            die();
        }
    }

    /*
     * @param $cid
     */
    function getFirstDepositor($cid){

        global $loader;

        $columns = array("CONCAT(users.firstname,' ',users.lastname) name");

        $parameters = array(
            'LEFT JOIN' => 'users',
            'ON'        => 'users.user_key_id = goal.emp_id',
            'WHERE'     => "customer_id = $cid",
            'AND'       => 'ftd = 1',
            'AND '      => 'goal.brand = '.BrandController::current_brand());

        $result = ImportController::import('goal', $columns, $parameters);

        if(count($result['data']) > 0 ){

            return $result['data'][0]['name'];

        } else {

            return null;
        }

    }
}