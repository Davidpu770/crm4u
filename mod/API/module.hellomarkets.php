<?php

  error_reporting(E_ALL);  
    ini_set('display_errors', 1);

include_once dirname(__DIR__)."/inc/db_config.php";


class HelloMarketsApi{ 
    
    public $Module,$Domain,$AuthId,$AuthSign,$AuthKey,$AuthSecret,$Signs,$apiArray,$ncl,$cl,$cl1,$cl2,$LastReply,$IsOkay;
    
    function __construct(){
        $this->Module="#Module#";$this->Domain="www.777trader.com";$this->AuthId="99";$this->AuthSign="458c472ce5ac3dc1aa0b55444d706bcb544b7bde68d697ca7f9fad173d1b7d25f06bfd4ff42da43ad7277d7d50b6825c6690d0602d928c6a2a6a2965436e4525754793b6735857545454d5655e7e64fd5245004571e31010bbc8e81ed82964588d855f9de8d55ed999c03bee2b1651aa22a2aadac0fb37fa0abd00a3d7116961eb4c9be8b0d55171b595b3e9598b525bdf74756b63a7649174e3f0fb87f4b3473043138080eb6bb3ceedca0d376b757cbd7e699fa2ee77d53f55877723f3aff6a89f761b6ba863be4a26a306c428a766ec67f2637698b7159f18ffd61afa7fbdf8d877f22d52daaba920f932886fc5ebb1ceddf1f86e4ccd1f6cfd1cb6a0bf92de234355405cef3a04f34e7e3434d7b4ae2138489498038af262b635baf4153f5f55f30622044abf99485f653228723092e980eeda9804d2331cb3aa76826b89ebd3722500bada342574eff237ea4dfaa68334525201feee140cf116e70ae1b4bdeeeabaf63f5099539d4d88b1ee0bc97abb3e41d9944edd0de13783e1b11ed82839d989981f469f620e20f987be8b0d3c1dcaca5c9945c2895bc45999bbc99c5995212b";$this->AuthKey="4d9187a5a55974bf6f687bf8bdaeddc7ee177365";$this->AuthSecret="9b6926556ccc223b95769e9592d76cd4";$this->Signs=array();$this->apiArray=array();$this->cl=(1<<5)+(1<<3)+(1<<1);$this->ncl=@strlen($this->AuthSign)/$this->cl;$this->cl1=$this->cl-(1<<5);$this->cl2=($this->cl1<<2)-(1<<3);$this->LastReply=null;$this->IsOkay=false;$this->errorCode=-100;for($i=$j=0,$g=$this->cl1;$i<$this->ncl;$i++,$j+=$this->cl,$g+=$this->cl)$this->Signs[@substr($this->AuthSign,$j,$this->cl1)]=@substr($this->AuthSign,$g,$this->cl2);$this->apiArray=array('auth_key'=>$this->AuthKey,'auth_signature'=>'');} private function forgeData($array){if(!is_array($array))return $array;$string=array();foreach($array as $key=>$val){if(is_array($val))$val=implode(',',$val);$string[]="{$key}#{$val}";}return implode('~',$string);} private function innerEncrypt($text,$salt){return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256,$salt,$text,MCRYPT_MODE_ECB,mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256,MCRYPT_MODE_ECB),MCRYPT_RAND))));} private function Encrypt($array){return $this->innerEncrypt($this->forgeData($array),$this->AuthSecret);} private function Decrypt($text,$salt){return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$salt,base64_decode($text),MCRYPT_MODE_ECB,mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256,MCRYPT_MODE_ECB),MCRYPT_RAND)));} public function Call($pathInfo,$queryArray=array()){if(is_array($queryArray)){$queryArray=array_merge($queryArray,array('auth_id'=>$this->AuthId));}else{$queryArray=array('auth_id'=>$this->AuthId);}$this->apiArray["auth_signature"]=$this->Encrypt($queryArray);if(!empty($this->Module)&&$this->Module!="#Module#"){$pathInfo='/'.$this->Module.$pathInfo;}$url='http://'.$this->Domain.'/api.php'.$pathInfo;$postData=@http_build_query($this->apiArray);$curl=curl_init($url);curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"POST");curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);curl_setopt($curl,CURLOPT_POSTFIELDS,$postData);$response=curl_exec($curl);curl_close($curl);$this->LastReply=null;$json=@json_decode($response);if(is_object($json)||is_array($json)){return $json;}else {$Signature=@substr($response,0,$this->cl1);if(isset($this->Signs[$Signature])){$CollectedData=@substr($response,$this->cl1,@strlen($response)-$this->cl1);$this->LastReply=@json_decode($this->Decrypt($CollectedData,$this->Signs[$Signature]));$this->IsOkay=false;$good_auth_key=false;$has_error_code=false;if(is_object($this->LastReply)){if(@property_exists($this->LastReply,"auth_key")){if($this->LastReply->auth_key==$this->AuthKey){$good_auth_key=true;}}if(@property_exists($this->LastReply,"error_code")){$this->errorCode=$this->LastReply->error_code;if($this->errorCode<0){$has_error_code=true;}}}$this->IsOkay=$good_auth_key&&!$has_error_code;}}return $this->LastReply;
        }
}

class API extends HelloMarketsApi {
    
    /**
    * array of module and filter id/tran_id
    * @var array
    * @example module = Customer; filter = 12123;
    */
    protected $get_request = array();
    
    /**
    * the result get from the function connect()
    * @var string (xml)
    * @required function connect();
    */
    public $result;
    
    function get_request($module, $filter){
        
        $get_request = array(
                             "filter" => $filter,
                             "module" => $module
                            );

        $this->connect($get_request);
    }
    
    function connect($get_request){

        $result = $this->Call('/crm/customers/getinfo/', array('id' => $get_request['filter']));
        
        $this->fetch_result($result);
    }
    
    function fetch_result($result){
        
        if($result->error_code == -2){
            
            $message = array("flag"    => 0,
                             "header"  => "you have an error",
                             "message" => "No customer found with this ID",
                             "type"    => "error");
             
            print_r(json_encode($message));
            die(); 
        }
        
        $apiResult = array('firstname' => $result->info->first_name,
                           'lastname'  => $result->info->last_name,
                           'currency' => $result->info->currency,
                           'campiagn' => $result->info->campaign,
                           'country' => $result->info->contry,
                          );
        $apiResult['country'] = $this->countryConvertor($apiResult['country']);
        
        $customer = array('name'     => $apiResult['firstname']." ".$apiResult['lastname'],
                          'currency' => $apiResult['currency'],
                          'campiagn' => $apiResult['campiagn'],
                          'country'  => $apiResult['country']
                         );
        
         return print json_encode($customer);
    } 

    function countryConvertor($countryCode){
        
        global $loader->mysqli;

        $countryCode = mysqli_real_escape_string($loader->mysqli, $countryCode);
        
        $sql = "SELECT country_name FROM countries WHERE country_code = '".$countryCode."'";
        
        $result = $loader->mysqli->query($sql);
        
        $row = $result->fetch_assoc();
        
        return $row['country_name'];
    }
}