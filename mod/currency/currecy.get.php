<?php

class currency {

    function __construct() {

        $currencies = array(
            'base' => array('EUR','GBP'),
            'symbols' => array('USD')
        );

        echo self::get_currencies($currencies);
    }

    function get_currencies($currencies){

        foreach ($currencies['base'] as $key => $value){

            $get = json_decode(file_get_contents("http://api.fixer.io/latest?symbols=".$currencies['symbols'][0]."&base=".$value), true);

            $updated_value[$get['base']] = round($get['rates']['USD'], 2);
        }

        $updated_value['BTC'] = $this->getBTC();


        return json_encode($updated_value);
    }

    function getBTC(){

        $get = json_decode(file_get_contents("https://blockchain.info/ticker"));

        FormController::Debug($get);
        return $get['USD']['last'];

    }
}

new currency();