<?php
namespace Crm4u\Pages\Statistic\Campaign;


use Crm4u\Import\graph;

$page_level = 3;
$title = 'Statistic | by campaign';

global $loader;
$loader->head($page_level, $title);


$graph = new graph();
$graph->import(0);

$graph->graph = array('graphs' => array($graph->graphs['graphs']['67566']));

$graph->graph['graphs'][0]['graph_arg'] = "campaign";



echo "<script type='text/javascript' src='//cdn.jsdelivr.net/momentjs/latest/moment.min.js'></script>";

echo "<script type='text/javascript' src=".SCRIPT_ROOT."/inc/js/daterangepicker.js></script>";
echo "<link rel='stylesheet' type='text/css' href=".SCRIPT_ROOT."/css/daterangepicker.css />";

echo "<script type='text/javascript' src=".SCRIPT_ROOT."/inc/js/highcharts.js></script>";
echo "<script type='text/javascript' src=".SCRIPT_ROOT."/inc/js/exporting.js></script>";

echo "<script type='text/javascript' src='".SCRIPT_ROOT."/inc/js/grid-light.js'></script>";
//-->filter

$filter = array(
    'option'  => array(
        'collapse' => ""),
    'filters' => array(
        'user',
        'tran_cid',
        'verification',
        'date',
        'shift',
        'country'
    )
);
$loader->_load('Crm4u\\Pages\\Partial\\searchBox', $filter);

$html = "<div class='col-md-12'>";
$html .= "<div class='box box-success'>";
$html .= "<div class='box-header with-border'>";
$html .= "<h3 class='box-title'>";
$html .= "<i class='fa fa-bar-chart-o fa-fw'></i> Live Campaign</h3>";

$html .= "<div class='box-tools pull-right'>";
$html .= "<button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-plus'></i>";
$html .= "</button></div></div>";
$html .= "<div class='box-body'>";
$html .= "<div id='live-campaign' style='min-width: 280px; height: 450px; margin: 0 auto'>";

$html .= "</div></div>";
$html .= "<div class=\"overlay\">
              <i class=\"fa fa-refresh fa-spin\"></i>
            </div>";
$html .= "</div></div>";

print $html;

$graph->getGraph('users', $graph->graph);
?>

<script>

    function campaignData (data) {

        var name = [];
        var num = [];
        var amount = [];
        var ftd = [];

        $.each(data, function (key, value) {

            num.push(value['customersNum']);
            name.push(value['name']);

            if(value['totalDeposits'] != null){

                amount.push(value['totalDeposits']['amount']);
                ftd.push(value['totalDeposits']['ftd']);
            } else {

                amount.push(0);
                ftd.push(0);
            }

        });
console.log(ftd);
        num.sort(function(a, b){return a-b});
        num.reverse();

        $('#live-campaign').highcharts({
            chart: {
                type: 'column',
                zoomType: 'xy'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: name
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} $',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                title: {
                    text: 'Customer Count',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Customer Amount',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                labels: {
                    format:null,
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                opposite: true
            }],
            series: [
                {
                    name: "Customer count",
                    data: num,
                    yAxis: 0
                },
                {
                    name: "Customer FTD",
                    data: ftd,
                    yAxis: 0
                },
                {
                    name: "Customer amount",
                    data: amount,
                    yAxis: 1
                }
            ]
        });
    }

    $(document).ready(function() {
        $.ajax({
            url: '/crm_api/Campaign/all',
            type: 'GET',
            async: true,
            dataType: "json",
            success: function (data) {
                campaignData(data);
                $(".overlay").css("display","none");
            }
        });
    });

</script>
<?php
//-->print row
print "<div class='row mt'></div>";



//-->include the footer
$loader->footer();