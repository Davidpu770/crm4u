<?php
namespace Crm4u\Pages\Statistic;

class UserPage{}
use Crm4u\Import\graph;

$page_level = 1;
$title = 'Statistic | by user';


global $loader;

$loader->head($page_level, $title);

$graph = new graph();
$graph->import(0);



//-->filter

$filter = array(
    'option'  => array(
        'collapse' => ""),
    'filters' => array(
        'user',
        'tran_cid',
        'verification',
        'date',
        'shift',
        'country'
    )
);
$loader->_load('Crm4u\\Pages\\Partial\\searchBox',$filter);

//-->graphs
$graph->graph = array('graphs' => array($graph->graphs['graphs']['23425']));


$loader->_load('Crm4u\\Graph\\WeeklyTarget');


//change relevant value
$graph->graph['graphs'][0]['graph_arg'] = "brand";
$graph->graph['graphs'][0]['graph_id'] = 67567;
$graph->graph['graphs'][0]['graph_sort_id'] = 5;

$graph->getGraph('users', $graph->graphs);
$graph->getGraph('users', $graph->graph);

print "<div class='row mt'></div>";

$loader->footer();