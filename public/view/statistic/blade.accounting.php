<?php
namespace Crm4u\Pages\Statistic\Accounting;


use Crm4u\Import\graph;

$page_level = 2;
$title = 'Statistic | by accounting';

global $loader;
$loader->head($page_level, $title);


$graph = new graph();
$graph->import(0);


$filter = array(
    'option'  => array(
        'collapse' => ""),
    'filters' => array(
        'user',
        'tran_cid',
        'verification',
        'date',
        'shift',
        'country'
    )
);
$loader->_load('Crm4u\\Pages\\Partial\\searchBox', $filter);

$graph->graph = array('graphs' => array($graph->graphs['graphs']['67566'],
    $graph->graphs['graphs']['75474'],
    $graph->graphs['graphs']['67566'],
    $graph->graphs['graphs']['23425'])
);


//change relevant value
$graph->graph['graphs'][0]['graph_arg'] = "campaign";

$graph->graph['graphs'][1]['graph_arg'] = "campaign";

$graph->graph['graphs'][2]['graph_arg'] = "country";
$graph->graph['graphs'][2]['graph_id'] = "545465";

$graph->graph['graphs'][3]['graph_arg'] = "country";

//-->graph
$graph->getGraph('users', $graph->graph);
//-->graphs
$graph->getGraph('accounting', $graph->graphs);

//-->print row
print "<div class='row mt'></div>";



//-->include the footer
$loader->footer();