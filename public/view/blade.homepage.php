<?php
namespace Crm4u\Pages\Homepage;



global $loader;


use Crm4u\Controller\BonusController;
use Crm4u\Import\graph;

$loader->head('1','Dashboard');



//-->include the support boxes
$loader->_load('Crm4u\\Pages\\Partial\\Support\\Boxes');


//-->include graph

/**
* the previeus month deposit
* @size col-md-6 col-sm-12;
* @filter brand, user
**/

print "<div class='col-md-4 col-sm-12 '>";

    include('inc/graph/graph.currency.php');

    print "</div>";
$graph = new graph();
$graph->import(0);
$graph->graphs = array('graphs' => array($graph->graphs['graphs']['78975']));

//change relevant value
$graph->graphs['graphs'][0]['graph_arg'] = 6;
$graph->graphs['graphs'][0]['graph_filter'] = 'index';
$graph->graphs['graphs'][0]['graph_size'] = 8;
$graph->graphs['graphs'][0]['graph_inner_size'] = 300;


$graph->getGraph('index', $graph->graphs);

/**
* the currency pie of the current month
* @size col-md-6 col-sm-12;
* @filter brand, user
**/
$graph->import(0);

$graph->graphs = array(
'graphs' => array(
$graph->graphs['graphs']['23425'],
$graph->graphs['graphs']['23425'],
$graph->graphs['graphs']['23425']
)
);



//change relevant value
$graph->graphs['graphs'][0]['graph_arg'] = "country";
$graph->graphs['graphs'][0]['graph_id'] = 234234;
$graph->graphs['graphs'][0]['graph_priv'] = 3;
$graph->graphs['graphs'][0]['graph_size'] = 4;
$graph->graphs['graphs'][0]['graph_inner_size'] = 300;
$graph->graphs['graphs'][0]['graph_addition_name'] = ' - By country';

$graph->graphs['graphs'][1]['graph_filter'] = "users";
$graph->graphs['graphs'][1]['graph_size'] = 4;
$graph->graphs['graphs'][1]['graph_priv'] = 3;
$graph->graphs['graphs'][1]['graph_inner_size'] = 300;
$graph->graphs['graphs'][1]['graph_addition_name'] = ' - By users';

$graph->graphs['graphs'][2]['graph_id'] = 232234;
$graph->graphs['graphs'][2]['graph_arg'] = "clearedby";
$graph->graphs['graphs'][2]['graph_priv'] = 3;
$graph->graphs['graphs'][2]['graph_size'] = 4;
$graph->graphs['graphs'][2]['graph_inner_size'] = 300;
$graph->graphs['graphs'][2]['graph_addition_name'] = ' - By CC';

$graph->getGraph('users', $graph->graphs);
//    echo "<pre>";
//var_dump($graph->graphs);
//echo "</pre>";

print "<div class='col-md-12 col-sm-12'>";

    $loader->_load('Crm4u\\Pages\\Partial\\mini_dTable');

    print "</div>";


//-->print row
print "<div class='row mt'></div>";



//-->include the footer
$loader->footer();
?>
