<?php

namespace Crm4u\Pages\Employee;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\Route;
use Crm4u\Import\employeeImport;
//-->set default args
$title = "Pages";
$page_level = 2;

global $loader;

$loader->head($page_level, $title);

$url = '/import/pages?is_simple=false';

if(isset($_loadArgs) && !is_null($_loadArgs)){

    foreach($_loadArgs as $key => $value){

        $parameters[] = $key."=".$value;
    }

    $url = $url . "&". implode('&',$parameters);
}


?>
<div class="col-md-12" style="min-height: 917px;">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#log" data-toggle="tab">Pages</a></li>
        </ul>
        <div class="tab-content">
            <div class="active tab-pane" id="log">

                <div class="table-responsive">

                    <table id="pages_table" class="table" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Method</th>
                            <th>Name</th>
                            <th>Page name</th>
                            <th>Active</th>
                            <th>Created at</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div>
                        <button type="button" class="btn btn-info btn-table" id="new_campaign" data-target="#new-department" data-toggle="modal"title="Adding New Desk">
                            <i class="fa fa-plus"></i> Add Page
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function() {


        $table = $("#pages_table").DataTable({

            "bLengthChange": false,
            "ajax" : {
                'url' : '<?php echo $url ?>',
                'type' : "GET"
            },
            "order" : [[0, 'desc']],
            rowId: 'id',
            "columns": [
                {"data": "id"},
                {"data": "method"},
                {"data": "name"},
                {"data": "page_name"},
                {"data": "active"},
                {
                    "class" : "hideOverflow",
                    "data": "created"},
                {
                    "class" : "hideOverflow",
                    "data": null,
                    "defaultContent": DataTableButton([$delete_btn, $edit_btn])
                }
            ]

        });

        $table.deleteRow({
            'table_name' : 'pages',
            'pk' : 'id',
            'refresh_table_name' : 'pages_table'
        });

    });
</script>

<?php
//-->print row
print "<div class='row mt'></div>";

$loader->footer();
?>
