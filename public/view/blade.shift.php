<?php

namespace Crm4u\Pages\Shift;

use Crm4u\Controller\FormController;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Mod\Shift\Box;

//-->set default args
$title = "Time Watch";
$page_level = 1;

global $loader;

$loader->head($page_level, $title);

$filter = array(
    'option'  => array(
        'collapse' => ""),
    'filters' => array(
        'user',
        'desk',
        'date'
    )
);

$loader->_load('Crm4u\\Pages\\Partial\\searchBox',$filter);
$loader->_load('Crm4u\\Mod\\Shift\\NewShift',$filter);
new Box();

if(CheckAccess::isAdmin()) {
?>

    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <span class="Title">Online Users</span>
            </div>
            <div class="box-body">
                <table width="100%" id="online_table" class="table table-responsive table-striped">
                    <thead>
                    <th>Name</th>
                    <th>Started</th>
                    <th>Options</th>
                    </thead>
                </table>
            </div>
        </div>
    </div>

<script>
    $online = $('#online_table').DataTable({
        ajax: {
            "url": 'import/Shift/online',
            "type": "GET"
        },
        rowId: 'id',
        "columns": [

            {
                "title": "Name",
                "class": "hideOverflow",

                "data": "name"
            },
            {
                "class": "hideOverflow",
                "data": "started"
            },
            {
                "class": "hideOverflow",
                "data": null,
                "defaultContent": DataTableButton([$endShift_btn])
            }


        ]
    });
    TableEndShift('#online_table');

</script>
    <?php } ?>
<div class="col-md-12">



<div class="box">
    <div class="box-header">
        <span class="Title">All Shifts</span>
        <div class="pull-right">

            <button type="button" class="btn btn-info deposit-admin-btn" id="new_shift" data-target="#new-shift" data-toggle="modal" title="Adding New Shift">
                <i class="fa fa-plus"></i><span class="hidden-xs hidden-xs-inline"> New Shift</span>
            </button>

            <button class="btn btn-success export" id="export" data-value="all" title="Export Deposit Table">
                <i class="fa fa-desktop"></i><span class="hidden-xs hidden-xs-inline"> Export</span>
            </button>
        </div>
    </div>
    <div class="box-body">


        <table width="100%" id="shift_table" class="table table-responsive table-striped">
            <thead>
            <th>Name</th>
            <th>Desk</th>
            <th>Started</th>
            <th>Ended</th>
            <th>Total hours</th>
            <th>Options</th>
            </thead>
        </table>
        <script>
            $("#searchBTN").on('click', function () {

                $form = $("#filter_form").serialize();

                $table.ajax.reload(function (json) {

                    $.each(json.Total.result, function (k, v) {

                        $("#" +k).html(v);
                    });
                });

                $("#resetFilter").css("display","");
            });

            exportTable('#export','shift',"#filter_form");
            $form = '';


            $table = $('#shift_table').DataTable({
                ajax: {
                    "url": 'import/Shift',
                    "type": "GET",
                    data: function (d) {
                        return $form
                    }
                },
                order: [[3, "desc"]],
                rowId: 'id',
                "columns": [

                    {
                        "title": "Name",
                        "class": "hideOverflow",

                        "data": "name"
                    },
                    {
                        "title": "desk",
                        "class": "hideOverflow",

                        "data": "desk_name"
                    },
                    {
                        "class": "hideOverflow",
                        "data": "started"
                    },
                    {
                        "class": "hideOverflow",
                        "data": "ended"
                    },
                    {
                        "class": "hideOverflow",
                        "data": "total_hour"
                    },
                    {
                        "class": "hideOverflow",
                        "data": null,
                        "defaultContent": DataTableButton([$edit_btn, $delete_btn])
                    }


                ],


                dom: 'Bfrtip',
                "buttons": [
                    {extend: 'colvis', text: 'Columns'}
                ],
                "initComplete": function(settings, json) {

                    $.each(json.Total.result, function (k, v) {

                        $("#" +k).html(v);
                    });
                }
            });
            deleteRow('#shift_table', 'shift', 'id');
        </script>
    </div>
</div>
</div>

    <?php
print "<div class='row mt'></div>";

$loader->footer();