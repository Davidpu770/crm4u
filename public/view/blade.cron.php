<?php

namespace Crm4u\Pages\Employee;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\Route;
use Crm4u\Import\employeeImport;
//-->set default args
$title = "Cron job";
$page_level = 2;

global $loader, $brand;

$loader->head($page_level, $title);

$url = '/import/cron?is_simple=false';

if(isset($_loadArgs) && !is_null($_loadArgs)){

    foreach($_loadArgs as $key => $value){

        $parameters[] = $key."=".$value;
    }

    $url = $url . "&". implode('&',$parameters);
}


?>
<div class="col-md-12" style="min-height: 917px;">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#jobs" data-toggle="tab">Jobs</a></li>
        </ul>
        <div class="tab-content">
            <div class="active tab-pane" id="jobs">

                <div class="table-responsive">
                    <table id="jobs_table" class="table" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Class name</th>
                            <th>Method name</th>
                            <th>Run every (ms)</th>
                            <th>Last execute</th>
                            <th>Created at</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <button type="button" class="btn btn-info"
                            data-target="#new-job" data-toggle="modal"
                            title="Adding New Job">
                        <i class="fa fa-plus"></i><spzn class="hidden-xs hidden-xs-inline"> New Job </spzn>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function() {


        var table = $("#jobs_table").DataTable({

            "bLengthChange": false,
            "ajax" : {
                'url' : '<?php echo $url ?>',
                'type' : "GET"
            },
            "order" : [[0, 'desc']],
            rowId: 'id',
            "columns": [
                {"data": "id"},
                {"data": "name"},
                {"data": "class_name"},
                {"data": "method_name"},
                {"data": "run_every"},
                {"data": "updated"},
                {
                    "class" : "hideOverflow",
                    "data": "created"},
                {
                    "class" : "hideOverflow",
                    "data": null,
                    "defaultContent": DataTableButton([$delete_btn])
                }
            ]

        });

        $("#jobs_table").deleteRow({
            'table_name' : 'cron_jobs',
            'pk' : 'id',
            'refresh_table_name' : 'jobs_table'
        });

    });
</script>

<?php
//-->print row
print "<div class='row mt'></div>";

$loader->footer();
?>
