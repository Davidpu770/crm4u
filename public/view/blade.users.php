<?php
namespace Crm4u\Pages\Users;

//-->setup page
use Crm4u\Controller\FormController;
use Crm4u\Controller\Route;
use Crm4u\Import\graph;
use Crm4u\Import\shift;
use Crm4u\Import\user;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Models\department as Department;
use Crm4u\Controller\BonusController;



global $loader;

$loader->head('1','User #'.$params[0]);


$form = new FormController();

(user::userExist($params[0])) ?  true : die();

$selected_user = User::import_user($params[0]);

$finance = User::import_finance($params[0]);

//load forms
$loader->_load('Crm4u\\Forms\\Users\\Dulicate_user');
$loader->_load('Crm4u\\Forms\\Users\\target', $selected_user);
$loader->_load('Crm4u\\Forms\\Users\\reset_password', $params);


?>

    <div class="content-wrapper">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <h3 class="profile-username text-center"><?php echo $selected_user->name ?></h3>

                    <p class="text-muted text-center"><?php echo Department::convertID($selected_user->department) ?></p>

                    <ul class="list-group list-group-unbordered">

                        <li class="list-group-item">
                            <b>Deposits this month</b> <a class="pull-right"><?php echo $finance->current_month->all_deposits ?> USD</a>
                        </li>

                        <li class="list-group-item">
                            <b>Average income</b> <a class="pull-right"><?php echo $form->fixRound($finance->all_time->ave_income, 1); ?> USD</a>
                        </li>

                        <li class="list-group-item">
                            <b>All Deposits</b> <a class="pull-right"><?php echo $finance->all_time->total_deposits  ?> USD</a>
                        </li>

                        <li class="list-group-item">
                            <b>Bonus to salary</b> <a class="pull-right"><?php echo BonusController::calculate($selected_user->user_key_id,'amount');  ?> USD</a>
                        </li>
                    </ul>
                    <div class="div-admin-btn">
                        <?php if(CheckAccess::isAdmin()) { ?>
                            <button class="btn btn-danger user-admin-btn" title="Delete User" id="delete_user"><i class="fa fa-trash-o"></i></button>
                            <button class="btn btn-warning user-admin-btn" title="Duplicate User" data-target="#duplicate" data-toggle="modal"><i class="fa fa-files-o"></i></button>
                            <button class="btn btn-success user-admin-btn" title="Set Target" data-target="#target" data-toggle="modal"><i class="fa fa-dot-circle-o"></i></button>
                        <?php } ?>
                        <button class="btn btn-primary user-admin-btn" title="Reset Password" data-target="#resetPassword" data-toggle="modal"><i class="fa fa-unlock"></i></button>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- statistic -->
            <?php $loader->_load('Crm4u\\Deposits\\getStatistic', array($selected_user,$finance)); ?>

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab">Finance</a></li>
                    <li><a href="#charts" data-toggle="tab">Charts</a></li>
                    <li><a href="#settings" data-toggle="tab">Preferences</a></li>
                    <li><a href="#bonuses" data-toggle="tab">Bonuses</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">

                        <?php $loader->_load('Crm4u\\Pages\\Partial\\mini_dTable', array('emp_id' => $selected_user->user_key_id)); ?>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="charts">

                        <?php


                        $graph = new graph();
                        $graph->import(0);

                        $graph->graphs = array(
                            'graphs' => array(
                                $graph->graphs['graphs']['12468'],
                                $graph->graphs['graphs']['12796'],
                            )
                        );
                        $graph->graphs['graphs'][0]['no-col'] = 1;
                        $graph->graphs['graphs'][0]['graph_filter'] = 'userPage';

                        $graph->graphs['graphs'][1]['no-col'] = 1;
                        $graph->graphs['graphs'][1]['graph_filter'] = 'userPage';

                        $graph->getGraph('userPage', $graph->graphs);


                        ?>
                    </div>
                    <!-- /.tab-pane -->

                    <div class = "tab-pane" id="settings">
                        <?php if($loader->user->priv > 2 ){?>

                        <form class="form-horizontal" id="edit_user_form" method="post">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">User ID</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" value="<?php echo $selected_user->user_key_id ?>" name="user_key_id" id="user_key_id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">User Ext</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo $selected_user->ext ?>" name="ext" id="user_ext">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-5">
                                    <input type="text" name="firstname" class="form-control" value="<?php echo $selected_user->firstname ?>" />
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" name="lastname" class="form-control" value="<?php echo $selected_user->lastname ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <input type="text" name="username" id="username" class="form-control" value="<?php echo $selected_user->username ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Main Brand</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="main-brand" name="main_brand">
                                        <?php
                                        Route::get('select', 'Models', 'brands', array('brand',$selected_user->main_brand));
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Desk</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="desk" name="location">

                                        <?php
                                        Route::get('select', 'Models', 'desks', array('desk',$selected_user->location));
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                                <div class="col-sm-10">
                                    <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Email" value="<?php echo $selected_user->email ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Shift</label>
                                <div class="col-sm-10">
                                    <select class="form-control" type="text" id="shift" name="shift" required>
                                        <?php
                                        $shift = new shift();
                                        print $form->selected_option($shift->shifts['shifts'],'shift_name',$selected_user->shift);
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Privilege</label>
                                <div class="col-sm-10">
                                    <select class="form-control" type="text" id="priv" name="priv" required>
                                        <?php
                                        $values = array("User" => 1,"Brand Admin" => 2,"Administrator" => 3,"Super Admin" => 4);
                                        print $form->selected_option($values, null, $selected_user->priv);
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Department</label>
                                <div class="col-sm-10">
                                    <select class="form-control" type="text" id="department" name="department" required>
                                        <?php
                                        Route::get('select', 'Models', 'department', array('department',array('selected',$selected_user->department)));
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Silent mode</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="Silent" name="silent_emp" required>
                                        <?php
                                        $silent_mode = array("silent","regular");
                                        print $form->selected_option($silent_mode,0,$selected_user->silent_emp);

                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!--                        priv-->

                            <h4>Privilege</h4>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="multiple_brand" <?php if($selected_user->multiple_brand  == 1) { echo "checked"; } ?>> Multiple brands
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="show_risk" <?php if($selected_user->show_risk  == 1) { echo "checked"; } ?>> Show risk
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="chargeback_page" <?php if($selected_user->chargeback_page  == 1) { echo "checked"; } ?> > Charge back Page
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="verify_deposits" <?php if($selected_user->verify_deposits  == 1) { echo "checked"; } ?> > Verify Deposits
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="send_mail"<?php if($selected_user->send_mail  == 1) { echo "checked"; } ?> > Allow mail
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="bonuses">

                        <?php $loader->_load('Crm4u\\Pages\\Partial\\bonuses'); ?>

                    </div>
                </div>

                <?php } ?>


                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <script>
        $(document).ready(function () {

            $('#desk').change(function (option) {

                deskParent = $('#desk option:selected').val();
                $.ajax({
                    type: "GET",
                    url: "/import/department",
                    data: 'deskParent=' + deskParent,
                    dataType:"json",
                    success: function (res) {
                        console.log(res);

                        options = [];

                        $.each(res['data'], function (k, v) {
                            option = {};

                            option['id'] = v['id'];
                            option['name'] = v['department_name'];

                            option = "<option value='"+option.id+"'>"+option.name+"</option>";

                            options.push(option);
                        });
                        $("#department").html(options);
                    }
                })
            })

            submitForm("#edit_user_form","edit", 'users/<?php echo $selected_user->user_key_id ?>', false);
            deleteForm('#delete_user','users',<?php echo $selected_user->user_key_id ?>, 'user_key_id', true);
        })

    </script>
<div class="row mt"></div>
    <?php

$loader->footer();
?>