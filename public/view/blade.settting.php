<?php
namespace Crm4u\Pages\Settings;

use Crm4u\Middleware\CheckAccess;

$title = "Settings";
$page_level = 2;

//load file

global $loader;

$loader->head($page_level, $title);

?>
<script>
    $('#setting a').click(function (e) {
        e.preventDefault();
        $(this).tab('show')
    })
</script>
<div class="modal fade" id="new-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New config</h4>
            </div>
            <form method="post" id="site_config_form">
                <div class="box-body">
                    <div class="form-group">
                        <label class="control-label">Group</label>
                        <input type="text" class="form-control new_config" name="group_set" id="cGroup" placeholder="Example: Deposit Config">
                    </div>

                    <div class="form-group">
                        <label class="control-label">Path</label>
                        <input type="text" class="form-control new_config" id="cPath" name="path" placeholder="Example: chargeback_status">
                    </div>

                    <div class="form-group">
                        <label class="control-label">Value</label>
                        <input type="text" class="form-control new_config" id="cValue" name="value" placeholder="Example: 0">
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" id="submit_config" name="submit_config">Add config</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="edit-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Config</h4>
            </div>
            <form method="post" id="site_edit_config">
                <input type="hidden" value="" id='row_id'>
                <div class="box-body">
                    <div class="form-group">
                        <label class="control-label">Group</label>
                        <input type="text" class="form-control new_config" name="group_set" id="0" placeholder="Example: Deposit Config">
                    </div>

                    <div class="form-group">
                        <label class="control-label">Path</label>
                        <input type="text" class="form-control new_config" id="1" name="path" placeholder="Example: chargeback_status">
                    </div>

                    <div class="form-group">
                        <label class="control-label">Value</label>
                        <input type="text" class="form-control new_config" id="2" name="value" placeholder="Example: 0">
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" id="edit_config" name="edit_config">Change config</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="nav-tabs-custom">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#configurations" aria-controls="home" role="tab" data-toggle="tab">Configurations</a></li>
            <li class><a href="#allowed-ip" aria-controls="home" role="tab" data-toggle="tab">Allowed-ip</a></li>
            <li class><a href="#campaigns" aria-controls="home" role="tab" data-toggle="tab">Campaigns</a></li>
            <li class><a href="#api_settings" aria-controls="home" role="tab" data-toggle="tab">API settings</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div class="tab-pane active" id="configurations">
                <div>

                </div>
                <div class="table-responsive" >
                    <?php if(CheckAccess::isSuperAdmin()){ ?>
                        <table id="site_config_table" class="table no-margin">
                            <thead>
                            <tr>
                                <th>Group</th>
                                <th>Name</th>
                                <th>Value</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <script>
                            $(document).ready(function () {




                                $("#site_config_table").DataTable({
                                    ajax : 'import/Settings',
                                    rowId: 'id',
                                    "columns": [
                                        {"data": "group_set"},
                                        {"data": "path"},
                                        {"data": "value"},
                                        {
                                            "data": null,
                                            "defaultContent": DataTableButton([$delete_btn,$edit_btn])
                                        }
                                    ]

                                });
                                editRow(site_config_table, function () {

                                    var tr = [];
                                    var td = $("#" + $row_id).find('td').each(function () {
                                        tr.push($(this).html());
                                    });

                                    $("#row_id").val($row_id);

                                    $.each(tr, function (k, v) {
                                        $("#" + k).val(v);
                                    });

                                    $("#edit-config").modal();

                                    $("#form_type").val('update');


                                });
                                deleteRow(site_config_table,'site_config_data','id');

                                $("#site_edit_config").submitForm({
                                    "action" : "update",
                                    "table_name" : "site_config_data",
                                    "row_id" : "#site_edit_config #row_id",
                                    "row_pk" : 'id',
                                    "refresh_table_name" : "site_config_table"
                                });

                                $("#site_config_form").submitForm({
                                    'table_name' : 'site_config_data',
                                    'action' : 'insert',
                                    'refresh_table_name' : "site_config_table"
                                });

                            });
                        </script>
                        <button type="button" class="btn btn-info deposit-admin-btn" id="new_config" data-target="#new-config" data-toggle="modal"title="Adding New Deposit">
                            <i class="fa fa-plus"></i><span class="hidden-xs hidden-xs-inline"> New Config</span>
                        </button>
                    <?php } ?>
                </div>


            </div>

            <div class="tab-pane" id="campaigns">
                <?php $loader->_load('Crm4u\\Pages\\Partial\\Campaigns'); ?>
            </div>

            <div class="tab-pane" id="api_settings">
                <?php $loader->_load('Crm4u\\Pages\\Partial\\api_settings'); ?>
            </div>

            <div class="tab-pane" id="allowed-ip">
                <div class="table-responsive" >

                    <?php $loader->_load('Crm4u\\Pages\\Partial\\allowed_ip'); ?>

                </div>

            </div>

        </div>
    </div>

    <?php
    print "<div class='row mt'></div>";

    $loader->footer();
    ?>

