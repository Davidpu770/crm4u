<?php
namespace Crm4u\Pages\Notification;

use Crm4u\etc\time;

global $loader;

$loader->head('1','Notifications');

?>

    <div class="col-md-12">

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Your Notifications</h3>
            </div>
            <div class="box-body">

                <div class="table" >
                    <table id="notifications" class="table no-margin">
                        <thead>
                        <tr>
                            <th><b>ID</b></th>
                            <th><b>Action</b></th>
                            <th><b>Title</b></th>
                            <th><b>Content</b></th>
                            <th><b>Created By</b></th>
                            <th><b>Created At</b></th>
                            <th><b>Brand</b></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql = "SELECT * FROM notification ORDER BY id DESC LIMIT 50";
                        $result = $loader->mysqli->query($sql);

                        $loader->_load('Crm4u\\etc\\time');

                        while($row = $result->fetch_assoc()){

                            echo "<tr>\n";
                            echo "<td>".$row['id']."</td>\n";
                            echo "<td>".$row['action']."</td>\n";
                            echo "<td>".$row['head']."</td>\n";
                            echo "<td>".$row['content']."</td>\n";
                            echo "<td>".$row['created_by']."</td>\n";
                            echo "<td";
                            echo " title='".\Crm4u\etc\time\time_elapsed_string($row['created'])."'>\n";
                            echo $row['created']."</td>\n";
                            echo "<td>".$row['brand']."</td>\n";
                            echo "</tr>\n";
                        }
                        ?>
                        </tbody>
                    </table>
                    <script>
                        table = $('#notifications').DataTable({
                            "order": [[ 0, "desc" ]],
                            fixedHeader: true
                        });
                    </script>
                </div>
            </div>
            <!--        notifications-->

        </div>

    </div>

    <?php
print "<div class='row mt'></div>";

$loader->footer();