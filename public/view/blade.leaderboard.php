<?php

namespace Crm4u\Pages;

global $loader;

$loader->head(0,'LeaderBoard', false, false);

?>
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
<style>
    @import url('https://fonts.googleapis.com/css?family=Khand|Russo+One');

    body {
        font-family: 'sans', sans-serif !important;

        background: rgba(0,129,250,1);
        background: -moz-linear-gradient(top, rgba(0,129,250,1) 0%, rgba(13,31,51,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(0,129,250,1)), color-stop(100%, rgba(13,31,51,1)));
        background: -webkit-linear-gradient(top, rgba(0,129,250,1) 0%, rgba(13,31,51,1) 100%);
        background: -o-linear-gradient(top, rgba(0,129,250,1) 0%, rgba(13,31,51,1) 100%);
        background: -ms-linear-gradient(top, rgba(0,129,250,1) 0%, rgba(13,31,51,1) 100%);
        background: linear-gradient(to bottom, rgba(0,129,250,1) 0%, rgba(13,31,51,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0081fa', endColorstr='#0d1f33', GradientType=0 );
    }

    h2 {

        font-family: 'Khand', sans-serif;
    }

    h3{

        font-family: 'Russo One', sans-serif;
    }

    h4 {
        font-family: 'Khand', sans-serif;
    }
    td {


    }


    .header-box-left
    {
        font-family: 'Varela Round', sans-serif;

        background-color : #1e2965;

        position : relative;

        display : block;

        color: #daefe3;

        height : 50px;

        width : 85%;

        text-align : center;

        margin : 0 auto;

        margin-top: 10px;

        margin-bottom: 10px;
    }

    .header-box-left::after {

        content: '';

        height: 50px;

        width: 50px;
        background-color : #1e2965;

        position: absolute;

        top: 0;

        right: 0;

        left: auto;

        transform: skewX(45deg);

        transform-origin: top;

        -webkit-transform: skewX(45deg);

        -webkit-transform-origin: 0 0;

    }

    .header-box-right
    {
        font-family: 'Varela Round', sans-serif;

        background-color : #1e2965;

        position : relative;

        display : block;

        color: #daefe3;

        height : 50px;

        width : 85%;

        text-align : center;

        margin-top: 10px;

        margin-bottom: 10px;
    }

    .header-title {

        font-size: 41px;
    }

    .header-box-right::before {

        content: '';

        height: 50px;

        width: 50px;

        background-color : #1e2965;

        position: absolute;

        top: 0;

        left: -50px;

        transform: skewX(45deg);

        transform-origin: top;

        -webkit-transform: skewX(45deg);

        -webkit-transform-origin: 0 0;

    }

    .event-box {
        background-color: #1e2965;
        color: #c7ddef;
        border: 2px solid #cddaeb;
        margin-bottom: 10px;
    }
    .table-white {

        color: #ffffff;
        font-size: 25px;
        font-family: 'Khand', sans-serif;
    }
    .info-box-icon{
        background-color: transparent;
        padding-top: 15px;
    }

</style>

<div class="col-md-12">

    <div class="row">
        <div class="col-md-8">
            <div class="header-box-left">
                <h3 class="text-center header-title">Daily Sales Leaderboard</h3>
            </div>
            <div class="col-md-4">

                <div class="col-md-12">
                    <div class="info-box event-box">
                <span class="info-box-icon">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </span>
                        <div class="info-box-content">
                            <h3>Monthly Leader</h3>
                            <h2><span id="monthly">Dan Williams</span></h2>
                        </div>
                    </div>
                    <div class="info-box event-box">
                <span class="info-box-icon">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </span>
                        <div class="info-box-content">
                            <h3>Weekly Leader</h3>
                            <h2><span id="weekly"></span></h2>
                        </div>

                    </div>
                    <?php if($loader->site->show_btc_rate){ ?>
                        <div class="info-box event-box">
                                <span class="info-box-icon">
                                    <i class="fa fa-btc" aria-hidden="true"></i>
                                </span>
                            <div class="info-box-content">
                                <h3>BTC Rate</h3>
                                <h2><span id="btc_rate"></span></h2>
                            </div>

                        </div>
                        <script>
                            function BTCRate() {
                                $.get('https://api.cryptonator.com/api/ticker/btc-usd',function (res) {
                                    $('#btc_rate').html(res.ticker.price + " USD");
                                })
                            }
                            $(document).ready(function () {

                                BTCRate();
                                setInterval(function () {
                                    $('#leaderboard_table').DataTable().ajax.reload();
                                    BTCRate();

                                }, 150000);

                            })
                        </script>

                  <?php } ?>

                </div>
            </div>
            <div class="col-md-8">

                <div class="carousel slide" data-ride="carousel" data-interval="20000">
                    <div class="carousel-inner">
                        <div class="item active">
                            <table class="table table-white" id="leaderboard_table" width="100%">
                                <thead>
                                <tr>
                                    <th>RANK</th>
                                    <th>NAME</th>
                                    <th>WEEKLY SALES</th>
                                    <th>MONTHLY SALES</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                        <div class="item">
                            <table class="table table-white" id="leaderboard_ftd_table" width="100%">
                                <thead>
                                <tr>
                                    <th>RANK</th>
                                    <th>NAME</th>
                                    <th>WEEKLY FTD</th>
                                    <th>MONTHLY FTD</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>



            </div>
        </div>
        <script>
            $(document).ready(function () {

                setInterval(function () {
                    $('#leaderboard_table').DataTable().ajax.reload();
                    getEvent();

                }, 300000);

                $('.carousel').carousel();
                $table = $('#leaderboard_table').DataTable({

                    ajax : {
                        "url": '/import/Leaderboard',
                        "type": "GET"
                    },

                    order: [[ 2, "desc" ]],
                    "columns": [
                        {
                            'data':'rank'
                        },
                        {
                            'data':'name'
                        },
                        {
                            'data':'total'
                        },
                        {
                            'data':'total_m'
                        }
                    ],
                    "initComplete": function(settings, json) {

                        $.each(json.leaders, function (k, v) {
                            $("#" +k).html(v);
                        });
                    },
                    "bFilter": false,
                    "bInfo" : false,
                    "bLengthChange": false,
                    "bPaginate": false
                });

                $('#leaderboard_ftd_table').DataTable({

                    ajax : {
                        "url": '/import/Leaderboard',
                        "type": "GET"
                    },

                    order: [[ 2, "desc" ]],
                    "columns": [
                        {
                            'data':'rank'
                        },
                        {
                            'data':'name'
                        },
                        {
                            'data':'count'
                        },
                        {
                            'data':'count_m'
                        }
                    ],
                    "initComplete": function(settings, json) {

                        $.each(json.leaders, function (k, v) {
                            $("#" +k).html(v);
                        });
                    },
                    "bFilter": false,
                    "bInfo" : false,
                    "bLengthChange": false,
                    "bPaginate": false
                });

                $table.on( 'order.dt search.dt', function () {
                    $table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );

                } ).draw();


                function getEvent(){
                    var events = [];
                    $.ajax({
                        'type': 'get',
                        'url': '/import/notification',
                        'dataType': 'json',
                        'success': function (res) {
                            $.each(res['data'], function (k, v) {
                                events.push(eventBox(v.head, v.content, v.action));
                            });
                            $(".events").html(events);
                        }
                    });
                }
                getEvent();

                function eventBox(title, content, action) {

                    var box = '<div class="info-box event-box"><span class="info-box-icon"><i class="fa fa-usd" aria-hidden="true"></i></span><div class="info-box-content">';
                    box += '<h3>' + title + '</h3>';
                    box += '<h4>' + content + '</h4>';
                    box += '</div></div>';

                    return box;
                }
            });

        </script>

        <div class="col-md-4">
            <div class="header-box-right">
                <h3  class="text-center header-title">Events</h3>
            </div>
            <div class="col-md-12 events">


            </div>

        </div>
    </div>
<div class="row">




    </div>
</div>

