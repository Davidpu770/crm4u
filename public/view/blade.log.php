<?php

namespace Crm4u\Pages\Employee;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\Route;
use Crm4u\Import\employeeImport;
//-->set default args
$title = "Security Log";
$page_level = 2;

global $loader, $brand;

$loader->head($page_level, $title);

$url = '/import/logs?is_simple=false';

if(isset($_loadArgs) && !is_null($_loadArgs)){

    foreach($_loadArgs as $key => $value){

        $parameters[] = $key."=".$value;
    }

    $url = $url . "&". implode('&',$parameters);
}


?>
<div class="col-md-12" style="min-height: 917px;">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#log" data-toggle="tab">Log</a></li>
        </ul>
        <div class="tab-content">
            <div class="active tab-pane" id="log">

                <div class="table-responsive">
                    <table id="logs_table" class="table" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Action</th>
                            <th>Employee name</th>
                            <th>Customer #</th>
                            <th>Customer name</th>
                            <th>Created at</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function() {


        $table = $("#logs_table").DataTable({

            "bLengthChange": false,
            "ajax" : {
                'url' : '<?php echo $url ?>',
                'type' : "GET"
            },
            "order" : [[0, 'desc']],
            rowId: 'id',
            "columns": [
                {"data": "id"},
                {"data": "action"},
                {"data": "employee_name"},
                {"data": "customer_id"},
                {"data": "customer_name"},
                {
                    "class" : "hideOverflow",
                    "data": "created"},
                {
                    "class" : "hideOverflow",
                    "data": null,
                    "defaultContent": DataTableButton([$delete_btn])
                }
            ]

        });

        $table.deleteRow({
            'table_name' : 'logs',
            'pk' : 'id',
            'refresh_table_name' : 'logs_table'
        });

    });
</script>

<?php
//-->print row
print "<div class='row mt'></div>";

$loader->footer();
?>
