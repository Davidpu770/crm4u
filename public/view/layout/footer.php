<?php
namespace Pages\Partial\Footer;

use Crm4u\Controller\BrandController;
use Crm4u\Middleware\CheckAccess;

global $loader;

$startyear = "2014";
$thisyear = date('y');
$copyright = "$startyear&ndash;$thisyear";
$last_update = false;

if(CheckAccess::isOnline() && $loader->brand->updated){

    $last_update = explode(" ", $loader->brand->updated);
    $last_date = date('d/m/Y',strtotime($last_update[0]));
    $last_time = date('H:i', strtotime($last_update[1]));
    $last_update = true;
}


?>
<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 4.1.2
    </div>
    <strong>
        copyright © <?php echo $copyright; ?> <a href="https://www.facebook.com/david.Pugatsch">David Pugatsch</a> & <a href="https://www.facebook.com/tom.kaplan2">Tom Kaplan</a>. All rights reserved.
    </strong>
    <p>
        <small>
            <?php if($last_update){
                echo "last update: $last_date at $last_time ";
            }
            ?>
        </small>
    </p>
</footer>
</div>
</div>

</body>
</html>