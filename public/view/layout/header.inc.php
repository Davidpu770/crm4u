<?php

namespace Crm4u\Layout;

use Crm4u\Controller\FormController;
use Crm4u\Controller\TitleController;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Security\blockAccess;
use Crm4u\Controller\BrandController;
use Crm4u\Controller\visit;

use Crm4u\Import\config;



//load file

class info {

    function __construct($_loadArgs)
    {
        global $loader;

        //@todo this class

        $this->site = $loader->site;

        //-->first check login
        $this->loginStatus = CheckAccess::isOnline(false, $_loadArgs['redirect']);


        //-->import user details
        $this->user = $loader->user;


        //-->brand check
        $this->brand = $loader->brand;
        $this->all_brand = $loader->all_brand;

        //set title
        $this->title = self::setTitle($_loadArgs);


        //-->import security
        $this->blockAccess = new blockAccess($_loadArgs['page_level'], $this->loginStatus, $this->user);

        visit::set_brand_session();

        //-->block access
        self::blockAccess();


    }

    function checkLogin($session){

        if(!isset($_SESSION)){ $this->session->session_create(); }
        return $this->session->check_user_connect();
    }

    function import_user(){

        if($this->loginStatus == "online"){

            $user = new user($_SESSION['user_id']);
            return $user;
        }
    }

    function brandController(){

        //set main brand

        $this->brand->set_main($this->brand->main_brand);

        //get brand list
        if($this->loginStatus == "online"){

            $this->brand->brand_list($this->user->ID,$this->user->priv);
        }

        //-->change brand
        if(isset($_GET['brand'])){

            $this->brand->change_brand($this->brand->brands_format);

        }


    }

    function setTitle($_loadArgs){

        //-->set title
        $brand_name = BrandController::brand_name();
        if(!isset($title)){ $title = 'Undefined'; }

        $get_title = new TitleController($brand_name, $_loadArgs['title']);
        $get_title->set_title();
    }

    function visit(){

        $visit = new visit();
        $visit->set_brand_session($this->brand);

        return $visit;

    }

    function blockAccess(){

        //-->Block access
        $this->blockAccess->before_header();
    }

}

//-->start checks
error_reporting(E_ALL);
ini_set('display_errors', 1);


//-->Define location
define('HOSTNAME', $_SERVER['SERVER_NAME']);
define('SCRIPT_ROOT', 'http://'.HOSTNAME);
define('LINK','http://'.HOSTNAME.'/');

//-->screen display
if(isset($online) && $_SESSION['firstname'] == "screen" && !strpos($_SERVER['REQUEST_URI'],'calls_statistic.php.php/')){
    header('Location: http://'.HOSTNAME.'/statistic/calls_statistic.php/');
}



//-->start html print
?>