<?php
namespace Crm4u\Pages\Partial\Header;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\FormController;
use Crm4u\Controller\gitController;
use Crm4u\Controller\ShiftController;
use function Crm4u\Import\site;
use Crm4u\Import\user;
use Crm4u\Layout\info;
use Crm4u\Middleware\CheckAccess;

global $loader;


$info = new info($_loadArgs);

?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="google-signin-client_id" content="434203009973-r5ej80t6i9fu0rl048r96o059fnir967.apps.googleusercontent.com">
        <?php

        //meta
        echo "<meta name='viewport' content='width=device-width,initial-scale=1' />\n\r";
        echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\n";
        echo "\n\n";

        echo "<link rel='shortcut icon' type='image/png' href='".SCRIPT_ROOT.$info->site->favicon_addr."' />\n";

        ?>
        <script>
            var AdminLTEOptions = {

                sidebarToggleSelector: "[data-toggle='push-menu']",

                sidebarPushMenu: true
            };


        </script>
        <?php
        //js
        foreach ($loader->vendor->js[0] as $package) {

            echo "<script src='".SCRIPT_ROOT."/vendor/{$package[0]}/{$package[1]}/{$package[2]}'></script>\n";
        }


        echo "<script src='".LINK."/public/js/main.js' async defer></script>\n";
        echo "<script src='https://cdn.onesignal.com/sdks/OneSignalSDK.js' async='async'></script>\n";
        echo "<script src=\"https://code.highcharts.com/highcharts.src.js\"></script>\n";


        //css
        echo "<link href=\"https://fonts.googleapis.com/css?family=Roboto\" rel=\"stylesheet\">\n";

        foreach ($loader->vendor->css[0] as $package) {

            echo "<link rel='stylesheet' type='text/css' href='".SCRIPT_ROOT."/vendor/{$package[0]}/{$package[1]}/{$package[2]}'>\n";
        }



        if(!isset($page_level)){

            $page_level = 4;

        }
        if(CheckAccess::isOnline(false, false)){

            echo "<link rel='stylesheet' type='text/css' href='".SCRIPT_ROOT."/vendor/almasaeed2010/adminlte/dist/css/skins/skin-".$info->brand->brand_color.".min.css'>\n";
            echo "<link rel='stylesheet' type='text/css' href='".SCRIPT_ROOT."/public/css/style.css'>\n";
            ?>
            <script>


                var OneSignal = window.OneSignal || [];

                OneSignal.sendTags({
                    'priv': '<?php echo $info->user->priv ?>',
                    'dep' : '<?php echo $info->user->department ?>',
                    'uid': '<?php echo user::current_user() ?>'
                });
                OneSignal.push(["init", {

                    appId: "<?php echo $info->site->onesignal_id ?>",
                    autoRegister: false, /* Set to true to automatically prompt visitors */
                    subdomainName: '<?php echo $info->site->subdomain_url ?>',
                    notifyButton: {
                        enable: true /* Set to false to hide */
                    },

                }]);

                OneSignal.getIdsAvailable(function (ids) {
                    $("#playerID").val(ids.userId);
                });


                $("#playerID").val()
            </script>
        <?php } ?>
    </head>

    <?php
//check if show_header is true
if($_loadArgs['show_header']) {
    ?>

<body class="<?php echo "skin-" . $info->brand->brand_color . " sidebar-mini sidebar-collapse" ?>"
      style="height: auto; min-height: 100%;">
    <script>
        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                console.log('User signed out.');
            });
        }
        function onLoad() {
            gapi.load('auth2', function () {
                gapi.auth2.init();
            });
        }
        $(document).ready(function () {

            $(function () {
                $('.content').removeClass('fade-out');
            });


        });

        $(document).scroll(function () {
            if ($(window).scrollTop() > 50) {

                $(".main-sidebar").addClass("main-sidebar-fixed");

            } else {
                $(".main-sidebar").removeClass("main-sidebar-fixed");

            }
        })
    </script>

    <?php

if ($info->loginStatus == "online") {

    ?>
<div class="wrapper" style="height: auto; min-height: 100%;">
    <header class="main-header" data-component="bootstrap">
        <a class="logo" title="Homepage" href="<?php echo LINK; ?>">
            <span class="logo-lg"><?php echo $info->site->site_name; ?></span>
            <span class="logo-mini"><?php echo $info->site->site_name_mini; ?></span>
        </a>
        <nav class="navbar navbar-static-top <?php echo $_SESSION['slideInDown'] ?> animated">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">


                    <?php

                    ShiftController::toolbar();

                    if ($info->user->multiple_brand == 1) { ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle"
                               data-toggle="dropdown"><?php echo strtoupper(BrandController::brand_name()) ?><span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php
                                //-->import check security

                                foreach ($info->all_brand['data'] as $key => $value) {
                                    if ($value['active'] == 1 && $info->user->priv >= $value['priv']) {

                                        echo "<li><a href='/brand/change/" . $value['id'] . "'>" . strtoupper($value['brand_name']) . "</a></li>";
                                    }
                                }
                                ?>

                            </ul>
                        </li>
                        <?php
                    } else {

                        echo "<li><a>" . strtoupper(BrandController::brand_name()) . "</a></li>";
                    }


                    //notification
                    $loader->_load('Crm4u\\Pages\\Partial\\Notifications');

                    //tasks
                    $loader->_load('Crm4u\\Pages\\Partial\\tasks');
                    ?>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><i class="fa fa-power-off" aria-hidden="true"></i></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <?php echo "<a class=''  onclick='signOut();' href='" . LINK . "logout' >Logout</a>"; ?>
                            </li>
                        </ul>
                    </li>

            </div><!-- /.navbar-collapse -->

        </nav>

    </header>

    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" style="height: auto;">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <div style="min-height: 40px;">
                        <i class="fa fa-user-circle-o fa-3 user-logo" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="pull-left info">
                    <p><?php echo $info->user->name; ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu tree" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-plus"></i> <span>Deposits</span>
                  <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php
                        echo "<li><a href=" . LINK . "deposits><i class=\"fa fa-dot-circle-o\"></i>Deposit History</a></li>";
                        echo "<li><a href=" . LINK . "withdrawals><i class=\"fa fa-dot-circle-o\"></i>Withdrwal History</a></li>";
                        echo "<li><a href=" . LINK . "chargeback><i class=\"fa fa-dot-circle-o\"></i>ChargeBack History</a></li>";
                        echo "<li><a href=" . LINK . "risk><i class=\"fa fa-dot-circle-o\"></i>Risk Center</a></li>";
                        ?>
                    </ul>
                </li>
                <?php if (CheckAccess::isAuthorized(2)) { ?>
                    <li><?php echo "<a href=" . LINK . "employees"; ?>><i class="fa fa-users"></i>
                        <span>Employees</span></a></li>
                    <li><?php echo "<a href=" . LINK . "support"; ?>><i class="fa fa-book"></i> <span>Support</span></a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-gears"></i> <span>Configuration</span>
                  <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <?php
                            echo "<li><a href=" . LINK . "settings><i class=\"fa fa-dot-circle-o\"></i>Site settings</a></li>";
                            echo "<li><a href=" . LINK . "pages><i class=\"fa fa-dot-circle-o\"></i>Pages</a></li>";
                            echo "<li><a href=" . LINK . "cron><i class=\"fa fa-dot-circle-o\"></i>Cron jobs</a></li>";
                            ?>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-cube"></i> <span>System</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <?php
                            echo "<li><a href=" . LINK . "log><i class=\"fa fa-dot-circle-o\"></i>Log</a></li>";
                            echo "<li><a href=" . LINK . "gitUpdates><i class=\"fa fa-dot-circle-o\"></i>Git Update</a></li>";
                            ?>
                        </ul>
                    </li>
                <?php } ?>
                <li><?php echo "<a href=" . LINK . "shift"; ?>><i class="fa fa-clock-o"></i>
                    <span>Shift Manager</span></a></li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-bar-chart-o fa-fw"></i> <span>Statistic</span>
                  <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php
                        echo "<li><a href=" . LINK . "statistic/worker>User</a></li>";
                        echo "<li><a href=" . LINK . "statistic/accounting>Accounting</a></li>";
                        echo "<li><a href=" . LINK . "statistic/campaign>Campaign</a></li>";
                        ?>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-link"></i> <span>Links</span>
                  <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php
                        if ($info->brand->platform_address != "") {
                            echo "<li><a target='_blank' href='" . $info->brand->platform_address . "' >CRM</a></li>";
                        }
                        if ($info->brand->site_address != "") {
                            echo "<li><a target='_blank' href='" . $info->brand->site_address . "' >" . strtoupper($info->brand->brand_name) . "</a></li>";
                        }
                        ?>
                    </ul>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

<?php } ?>

    <div class="content-wrapper content" style="padding-top: 40px; min-height: 916px;">
        <script>
            $('.content').addClass('fade-out');
        </script>
    <?php
}
$info->blockAccess->after_header();

?>