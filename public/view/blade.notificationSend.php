<?php

namespace Crm4u\Pages\Notification;

use Crm4u\Controller\Route;
//-->set default args
$title = "Test Notification";
$page_level = 2;

global $loader;

$loader->head($page_level, $title);

?>

<div class="container">

    <form id="send_notification">

        <div class="form-group">
            <label>Title:</label>
            <input class="form-control" name="title">
        </div>
        <div class="form-group">
            <label>Message:</label>
            <input class="form-control" name="message">
        </div>
        <div class="form-group">
            <label>Spasific user:</label>
            <select name="emp_id" class="form-control smart-select">
                <option value="false"></option>
                <?php
                Route::get('select', 'Models', 'users', array('user'));
                ?>
            </select>

        </div>
        <input type="submit" class="btn btn-success" value="Send">

    </form>
</div>
<script>submitForm('#send_notification','notification','send',false)</script>
