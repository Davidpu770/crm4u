<?php
namespace Crm4u\Pages\Settings;

use Crm4u\Controller\FormController;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Models\Settings;

$title = "Settings";
$page_level = 2;

//load file

global $loader;

$loader->head($page_level, $title);

?>
<script>
    $('#setting a').click(function (e) {
        e.preventDefault();
        $(this).tab('show')
    })
</script>
<div class="modal fade" id="new-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New config</h4>
            </div>
            <form method="post" id="site_config_form">
                <div class="box-body">
                    <div class="form-group">
                        <label class="control-label">Group</label>
                        <input type="text" class="form-control new_config" name="group_set" id="cGroup" placeholder="Example: Deposit Config">
                    </div>

                    <div class="form-group">
                        <label class="control-label">Path</label>
                        <input type="text" class="form-control new_config" id="cPath" name="path" placeholder="Example: chargeback_status">
                    </div>

                    <div class="form-group">
                        <label class="control-label">Value</label>
                        <input type="text" class="form-control new_config" id="cValue" name="value" placeholder="Example: 0">
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" id="submit_config" name="submit_config">Add config</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="nav-tabs-custom">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#configurations" aria-controls="home" role="tab" data-toggle="tab">Configurations</a></li>
            <li class><a href="#allowed-ip" aria-controls="home" role="tab" data-toggle="tab">Allowed-ip</a></li>
            <li class><a href="#campaigns" aria-controls="home" role="tab" data-toggle="tab">Campaigns</a></li>
            <li class><a href="#api_settings" aria-controls="home" role="tab" data-toggle="tab">API settings</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div class="tab-pane active" id="configurations">
                <div>

                </div>
                    <?php if(CheckAccess::isSuperAdmin()){


                        $settings = new Settings('withGroup');

                        foreach ($settings->result->group as $key => $value){

                                echo "<div class='panel panel-default'>";
                                echo "<div class='panel-heading'>";
                                echo $key;
                                echo  "</div>";
                                echo  "<div class='panel-body'>";

                                foreach ($value as $k => $v){
                                    echo "<div class='form-group'>";
                                        echo "<div class='col-md-3'>{$v['path']}</div>";
                                        echo "<div class='col-md-9'>";
                                            echo "<input type='text' class='form-control text-box single-line' name='{$v['path']}' value='{$v['value']}' />";
                                        echo "</div>";
                                    echo  "</div>";
                                }

                                echo  "</div>";

                                echo  "</div>";
                        }
                        ?>


                        <button type="button" class="btn btn-info deposit-admin-btn" id="new_config" data-target="#new-config" data-toggle="modal"title="Adding New Deposit">
                            <i class="fa fa-plus"></i><span class="hidden-xs hidden-xs-inline"> New Config</span>
                        </button>
                    <?php } ?>


            </div>

            <div class="tab-pane" id="campaigns">
                <?php $loader->_load('Crm4u\\Pages\\Partial\\Campaigns'); ?>
            </div>

            <div class="tab-pane" id="api_settings">
                <?php $loader->_load('Crm4u\\Pages\\Partial\\api_settings'); ?>
            </div>

            <div class="tab-pane" id="allowed-ip">
                <div class="table-responsive" >

                    <?php $loader->_load('Crm4u\\Pages\\Partial\\allowed_ip'); ?>

                </div>

            </div>

        </div>
    </div>

<?php
print "<div class='row mt'></div>";

$loader->footer();
?>

