<?php
namespace Crm4u\Pages\ChargeBack;

use Crm4u\Controller\BrandController;
use Crm4u\ChargeBack\ChargeBack;
use Crm4u\Middleware\CheckAccess;


$title = "Charge Back";
$page_level = 1;

global $loader;

$loader->head($page_level, $title);

if($loader->user->chargeback_page != 1){
    die();
}

//import close case form
$loader->_load('Crm4u\\Forms\\Chargeback\\close_case');

//import deposit
$deposit = new ChargeBack();

//import statistic
$deposit->statisticImport();

$statistic = array('Open cases'    => array(
    'value' => array($deposit->statistic['copen'], $deposit->statistic['sopen']),
    'color' => 'yellow',
    'icon'  => 'ion ion-flag'),

    'Won cases'     => array(
        'value' => array($deposit->statistic['cwin'], $deposit->statistic['swin']),
        'color' => 'green',
        'icon'  => 'ion ion-happy-outline'),

    'lost cases'    => array(
        'value' => array($deposit->statistic['close'], $deposit->statistic['slose']),
        'color' => 'red',
        'icon'  => 'ion ion-sad-outline'),
);

?>
<!--chargeback summery-->

        <div class="box-header with-border">

        <div class="box-body">
            <ul class="nav nav-stacked">

                <?php
                foreach($statistic as $key => $value){
                    $box = "";
                    $box .= "<div class='col-md-4 col-sm-12'>";
                    $box .= "<div class='info-box bg-".$value['color']." supportBox'>";
                    $box .= "<span class='info-box-icon'><i class='".$value['icon']."' style='line-height: 2'></i></span>";
                    $box .= "<div class='info-box-content'>";
                    $box .= "<span class='info-box-text'>".$key."</span>";
                    $box .= "<span class='info-box-number'>".$value['value'][0]." Deposits</span>";
                    $box .= "<hr>";
                    $box .= "<span class='info-box-number'>".$value['value'][1]." USD</span>";
                    $box .= "</div></div></div>";

                    print $box;
                }
                ?>
            </ul>
        </div>


<!--Filters-->
<?php
$filter = array(
    'option'  => array(
        'collapse' => ""),
    'filters' => array(
        'user',
        'tran_cid',
        'verification',
        'ftd',
        'date',
        'country'
    )
);

$loader->_load('Crm4u\\Pages\\Partial\\searchBox',$filter);

if(CheckAccess::isAuthorized(2)) {

    $loader->_load('Crm4u\\Pages\\Partial\\New_chargeback');

}
?>
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#open-chargeback" data-toggle="tab">Open Chargebacks</a></li>
                        <li><a href="#close-chargeback" data-toggle="tab">Closed Chargebacks</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="open-chargeback">

                            <?php $loader->_load('Crm4u\\Pages\\Partial\\Open_chargeback', array('closed' => 0)); ?>
                        </div>

                        <div class="tab-pane" id="close-chargeback">

                            <?php $loader->_load('Crm4u\\Pages\\Partial\\Close_chargeback', array('closed' => 1)); ?>
                        </div>
                    </div>
                </div>
            </div>

<div class="row mt"></div>

<?php

$loader->footer();
?>