<?php

namespace Crm4u\Pages;

global $loader;

use Crm4u\Controller\FormController;
use Crm4u\Controller\gitController;

$title = "Git Updates";
$page_level = 4;

$loader->head($page_level, $title);

?>

<h1>Git update Page</h1>
<p>This page is for system admin only</p>

<form id="git_form">

    <div class="form-group">

        <label>Current Git Version: </label>
        <input class="form-control" disabled value="<?php echo gitController::getVersion() ?>">
    </div>
    <div class="form-group">
        <label>Click Here to update server files</label>
        <button type="submit" class="btn btn-info">Update</button>
    </div>
</form>

<div class="form-group">
    <label for="changelog">Change Log:</label>
    <pre><?php echo gitController::checkUpdate()?></pre>
</div>



<script>


    submitForm("#git_form","update", 'git', false);
</script>

<?php $loader->footer() ?>
