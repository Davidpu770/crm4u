<?php

namespace Crm4u\Pages\Employee;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\Route;
use Crm4u\Import\employeeImport;
//-->set default args
$title = "Workers";
$page_level = 2;

global $loader, $brand;

$loader->head($page_level, $title);

$loader->_load('Crm4u\\Forms\\Users\\new_user');
?>


<div class="col-md-12" style="min-height: 917px;">

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#workers" data-toggle="tab">Workers</a></li>
            <li><a href="#departments" data-toggle="tab">Departments</a></li>
            <li><a href="#desks" data-toggle="tab">Desks</a></li>
        </ul>
        <div class="tab-content">
            <div class="active tab-pane" id="workers">
                            <table id="employee_table" class="table table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th style="width:30px;">ID</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Department</th>
                                    <th>email</th>
                                    <th>Desk</th>
                                    <th>Last Login</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody id="employee_list">
                                </tbody>
                            </table>
                <button class="btn btn-info" data-target="#newEmployee" data-toggle="modal"><i class="ion ion-person-add"></i> New employee</button>
            </div>
            <div class="tab-pane" id="departments">
                <?php $loader->_load('Crm4u\\Pages\\Partial\\Department'); ?>
            </div>
            <div class="tab-pane" id="desks">
                <?php $loader->_load('Crm4u\\Pages\\Partial\\Location'); ?>
            </div>
        </div>
    </div>
</div>


<script>

    $(document).ready(function(){

        $table = $('#employee_table').DataTable({
            "ajax" : 'import/users',
            rowId: 'id',
            "columns": [
                {"data": "id"},
                {"data": "user_name"},
                {"data": "username"},
                {"data": "department"},
                {"data": "email"},
                {"data": "desk_name"},
                {"data": "last_login"},

                {
                    "data": null,
                    "defaultContent": DataTableButton([$edit_btn,$delete_btn])
                }
            ]
        });
        submitForm(new_employee_form,'users', 'insert', false);
        editRow(employee_table, function () {
            window.location.href = "users/" + $row_id;
        });
        deleteRow(employee_table, 'users', 'user_key_id', false);
    });


</script>

<?php
//-->print row
print "<div class='row mt'></div>";

$loader->footer();
?>
