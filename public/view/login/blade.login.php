<?php
namespace Crm4u\Pages\Login;

use Crm4u\Layout\info;
use Crm4u\Middleware\CheckAccess;

global $loader;

$title = "Login page";
$page_level = 0;

$loader->head($page_level, $title);


if(!CheckAccess::isOnline()) {
    ?>
    <body class="login-page">


    <div class="login-box">
        <div class="login-logo">
            <a href="#"><?php echo $loader->site->site_name; ?></a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <form method="post" id="login-form">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="username" placeholder="Username" id="user"/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" id=pass class="form-control" name="password" placeholder="Password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="row">
                    <div class="col-xs-8">
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <input type="hidden" name="type" value="user">
                        <button type="submit" id="submit_login" class="btn btn-primary btn-flat">Sign In</button>
                    </div><!-- /.col -->
                </div>
            </form>
            <div class="social-auth-links text-center">
                <p>- OR -</p>
                <div class="g-signin2" data-width="300" data-height="50" data-longtitle="true" data-onsuccess="onSignIn"></div>
            </div>
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <script>

        $(document).ready(function () {

            $("#login-form").on("submit",function (login) {

                login.preventDefault();

                var $form = $(this);

                var serializedData = $form.serialize();

                $.ajax({
                    type:"POST",
                    url:"/login",
                    data:serializedData,
                    dataType:"text",
                    success: function(res){

                        window.location.replace("../");

                    },
                    error: function(){
                        var div = $("<div style='position: absolute;margin-left: 34.77%;margin-top: -27%;' class='col-md-4 col-md-offset-6 callout callout-danger'><p>Invalid username/password!</p></div>");
                        $("#error").hide().fadeIn("700");
                        $("#error").html(div);
                    }
                });
            })



        });
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            var mydata = "type=google&email=" + profile.getEmail();

            $.ajax({
                type:"POST",
                url:"/login",
                data:mydata,
                dataType:"text",
                success: function(response){
                    window.location.replace("../");

                },
                error: function(response){
                    var div = $("<div style='position: absolute;margin-left: 34.77%;margin-top: -8%;' class='col-md-4 col-md-offset-6 callout callout-danger'><p>Invalid username/password!</p></div>");
                    $("#error").hide().fadeIn("700");
                    $("#error").html(div);
                }
            });
        }

    </script>
    </div><!--.login_input-->
    <div id="error"></div>
    </div><!--.login-->
    </body>
    <?php
} else {

    $message  = "<div class='callout callout-danger'>";
    $message .= "<h4>You have an Error.</h4>";
    $message .= "<p>You are already connected.</p>";
    $message .= "</div>";
    print $message;
}

$loader->footer();
?>

