<?php
namespace Crm4u\Pages\Support;

global $loader;

$loader->head('2','Support');


//FILTERBOX
$filter = array(
    'option'  => array(
        'collapse' => ""),
    'filters' => array(
        'user',
        'payment',
        'tran_cid',
        'verification',
        'ftd',
        'date',
        'shift',
        'country'
    )
);

$loader->_load('Crm4u\\Pages\\Partial\\searchBox',$filter);

print "<div class='row mt'></div>";

//SUPPORT BOXES

$loader->_load('Crm4u\\Pages\\Partial\\Support\\Boxes');
$loader->_load('Crm4u\\Pages\\Partial\\Risk\\statistic');

print "<div class='row mt'></div>";

$loader->footer();

?>