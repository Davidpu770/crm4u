<?php
/**
 * Copyright (c) 2017. All copyright reserved to David Pugatsch
 */

namespace Crm4u\Pages\Desk;

//-->setup page
use Crm4u\Controller\FormController;
use Crm4u\Import\graph;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Models\desks;


global $loader;

$loader->head('1','Department #'.$params[0]);


$form = new FormController();

(FormController::value_exist($params[0],'desk')) ? false : die();

$selected_desk = Desks::import($params[0]);

$finance = Desks::import_finance($params[0]);

//load forms
$loader->_load('Crm4u\\Forms\\Users\\target', $selected_desk);


?>

    <div class="content-wrapper">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <h3 class="profile-username text-center"><?php echo $selected_desk->desk_name ?></h3>

                    <p class="text-muted text-center">Managed by: <?php echo $selected_desk->desk_manager ?></p>

                    <ul class="list-group list-group-unbordered">

                        <li class="list-group-item">
                            <b>Deposits this month</b> <a class="pull-right"><?php echo $finance->current_month->all_deposits ?> USD</a>
                        </li>

                        <li class="list-group-item">
                            <b>Average income</b> <a class="pull-right"><?php echo $form->fixRound($finance->all_time->ave_income, 1); ?> USD</a>
                        </li>

                        <li class="list-group-item">
                            <b>All Deposits</b> <a class="pull-right"><?php echo $finance->all_time->total_deposits  ?> USD</a>
                        </li>
                    </ul>
                    <div class="div-admin-btn">
                        <?php if(CheckAccess::isAdmin()) { ?>
                            <button class="btn btn-danger user-admin-btn" title="Delete Department" id="delete_department"><i class="fa fa-trash-o"></i></button>
                            <button class="btn btn-success user-admin-btn" title="Set Target" data-target="#target" data-toggle="modal"><i class="fa fa-dot-circle-o"></i></button>
                        <?php } ?>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- statistic -->
            <?php $loader->_load('Crm4u\\Deposits\\getStatistic', array($selected_desk,$finance, false)); ?>

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab">Finance</a></li>
                    <li><a href="#charts" data-toggle="tab">Charts</a></li>
                    <li><a href="#preferences" data-toggle="tab">Preferences</a></li>
                    <li><a href="#bonuses" data-toggle="tab">Bonuses</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">

                        <?php $loader->_load('Crm4u\\Pages\\Partial\\mini_dTable', array('desk' => $selected_desk->id)); ?>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="charts">

                        <?php


                        $graph = new graph();
                        $graph->import(0);

                        $graph->graphs = array(
                            'graphs' => array(
                                $graph->graphs['graphs']['12468'],
                                $graph->graphs['graphs']['12796'],
                            )
                        );
                        $graph->graphs['graphs'][0]['no-col'] = 1;
                        $graph->graphs['graphs'][0]['graph_filter'] = 'userPage';

                        $graph->graphs['graphs'][1]['no-col'] = 1;
                        $graph->graphs['graphs'][1]['graph_filter'] = 'userPage';

                        $graph->getGraph('userPage', $graph->graphs);


                        ?>
                    </div>

                    <div class="tab-pane" id="preferences">

                        <?php $loader->_load('Crm4u\\Pages\\Partial\\deskPreferences', array($selected_desk)); ?>

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="bonuses">

                        <?php $loader->_load('Crm4u\\Pages\\Partial\\bonuses'); ?>

                    </div>
                </div>


                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <script>
        $(document).ready(function () {
            submitForm("#edit_user_form","edit", 'users/<?php echo $selected_desk->user_key_id ?>', false);
            deleteForm('#delete_user','users',<?php echo $selected_desk->user_key_id ?>, 'user_key_id', true);
        })

    </script>
    <div class="row mt"></div>
    <?php

$loader->footer();
?>