<?php
namespace Crm4u\Pages\Deposits;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\visit;

global $loader, $brand;

$loader->head('1','Dashboard');

$loader->_load('Crm4u\\Pages\\Partial\\deposit');

$visit = new visit();

$loader->footer();
$visit->set_visit_time(BrandController::current_brand());
?>