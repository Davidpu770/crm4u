
$(document).ready(function () {

    function setShift(ukid, status){

        $.ajax({
            type: "POST",
            datatype: "text",
            data: "ukid="+ukid+"&status="+status,
            url: window.location,
            success: function (res) {
                disabled('#startShift');
            }
        });

    }
const attrAction = 'disabled';
    function disabled(attrName,attrAction) {

        $(attrName).attr('disabled',attrAction);

    }
    var ukid = $('#ukid').val();
    var shiftStart = $("#shiftTime").val();
    var now = new Date();

    var startAt = now - shiftStart;

    if(shiftStart > 0){
        disabled('#startShift');
        $('.watch').runner({
            autostart: true,
            startAt: startAt,
            milliseconds: false
        });

    }


    $('#startShift').click(function () {
        setShift(ukid, 'start');
        $('.watch').runner({
            milliseconds: false
        });
        $('.watch').runner('start');
    });

    $('#pauseShift').click(function () {

        if($(this).val() == "PAUSE"){

            $('#pauseShift').val("CONTINUE");

        } else {

            $('#pauseShift').val("PAUSE");
        }
        $('.watch').runner('toggle');
    });

    $('#stopShift').click(function () {
        $('.watch').runner('stop');
        $()
    });


})
