$(document).ready(function(e) { 
    
    //first hide splited row
    $("#splitEmployee").hide();
    
    //check transaction id and validate
    $(".tran_check").click(function(e){
        $res = '';
        //value
        var tran_id = $("#tran_id").val();
        try{
            
            var module_name = $("#type option:selected").attr('data-value');
        } catch(e){
            
            var module_name =  $("#type").attr('data-value');
        }

        var type = $("#type").val();
        
        //data
        var mydata = 'tran_check='+ tran_id + '&type='+type;
        
        
        $(".overlay").css("display","");
        
        $.ajax({
            type:"get",
            dataType:"json",
            url:"import/"+type+"/json/"+tran_id,
            success: function(response,status, xhr){
                status = 1;
                $("#tran_id").parent().removeClass("has-success has-error has-feedback").addClass("has-success has-feedback");
                $("#submitDeposit").removeAttr("disabled");
                console.log(response);

                try {
                    $res = $.parseJSON(response);
                } catch(e) {

                    check_tran(tran_id, module_name)

                    }
                $(".overlay").css("display","none");
                
                swal($res['header'],$res['message'],$res['type']);
                },
            error: function(response){ 
                $(".overlay").css("display","none");
                }
            });
    });

        $("#tran_id").keypress(function(e){

            if(e.which == 13){
                $(".tran_check").click();
            }
        });

    function check_tran(tran_id, module_name){
        $.ajax({
            type:"GET",
            dataType:"json",
            url:"/crm_api/"+ module_name + "/" + tran_id ,
            success: function(res){

                if(res['flag'] == 0) {
                    swal(res['header'], res['message'], res['type']);
                } else {

                    $amount = res['amount'];
                    $pm = res['paymentMethod'];
                    $cid = res['customerId'];
                    $cb = res['clearedBy'];

                    $("input[type=text]#deposit_cid").val($cid);
                    $("input[type=text]#deposit").val($amount);
                    $("select#payment_method").val($pm);
                    $("#clearedby").val($cb);
                    check_cid($cid);

                }

            }
        });

    }

    function check_cid(cid) {
        $(".overlay").css("display","");

        $.ajax({
            type: "GET",
            dataType: "json",
            url: "/crm_api/Customer/" + cid,
            success: function(res){

                $(".overlay").css("display","none");

                if(res['flag'] === 0) {

                    swal(res['header'],res['message'],res['type']);

                }  else {

                    $name = res['name'];
                    $currency = res['currency'];
                    $country = res["country"];
                    $camp = res["campaign"];
                    $dftd = res["first_dep"];

                    $("input[type=text]#cust_name").val($name);
                    $("#currencey").val($currency);
                    $("#country").val($country);
                    $("#campaign").val($camp);
                    $("#depositor_name_hidden").val($dftd);

                }
            }

        });
        $(".overlay").css("display","none");
    }
    
    $(".cid_check").click(function(){
        
        var cid = $("#deposit_cid").val();
        
        if(cid == ""){
        
            cid = $("#deposit_cid_edit").val();
        }

        $.ajax({
            type:"get",
            dataType:"json",
            url:"import/customer/json/"+cid,
            success: function(response,status, xhr){

                if(response['flag'] === 2){

                    return check_cid(cid);
                }
                if(response['flag'] === 0){

                    swal(response['header'],response['message'],response['type']);
                } else {

                    $name = response['name'];
                    $currency = response['currency'];
                    $country = response["country"];
                    $camp = response["campaign"];

                    $("input[type=text]#cust_name").val($name);
                    $("#currencey").val($currency);
                    $("select[name='country']").val($country).change();
                    $("select[name='campaign']").val($camp).change();

                }

                $(".overlay").css("display","none");


            },
            error: function(response){
                $(".overlay").css("display","none");
            }
        });
        

                    
            }); 
    
    $("#close").click(function(){
        $("#deposit_cid").val('');
    });
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
	

    

     
	
    
    
    
	

    $("#ftd_10").change(function(){
        var data = $("#deposit_cid").val();
        var mydata = 'ftd_depositor_check=' + data;
        var chk = $("#ftd_10");
		if(chk.is(":checked")){
        $.ajax({
				type:"POST",
				dataType:"text",
				data:mydata,
				url:"inc/status.php",
				success: function(){
			$("#error").fadeOut("450");
					},
				error: function(){
					$("#error").hide().fadeIn("700");
					$("#error").html("<div class='alert alert-danger' role='alert'><strong>Attention!</strong> There is no FTD in the system</div>");
					$("#submitDeposit").attr("disabled","disabled");

					}
				});
		} else {
			$("#error").fadeOut("450");
			}
        
    });
    $("#emp_name").change(function(){
        $("#emp_name_chosen").css("border","");
        $("#emp_name_chosen").css("background-color","");
    });
    $("#split").change(function(){
        
        if($("#split").is(":checked")){
            
            $("#splitEmployee").show().fadeIn("350");
            $("#split_emp_name").attr("name","split_emp_name");
            $("#percentage").attr("name","percentage");
        } else {
            
            $("#splitEmployee").hide().fadeOut("450");
            $("#split_emp_name").removeAttr("name");
            $("#percentage").removeAttr("name");
        }
    });  
    
});