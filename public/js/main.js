

/*
 * Copyright (c) 2017. All copyright reserved to David Pugatsch
 */

/**
 * DataTable
 */

/**
 *
 * DataTable Buttons
 */


//run on page load:
$(document).ready(function () {

    //convert shift to time
    function convetShiftToTime() {

        if ($('#shiftTime').val() !== '') {

             shiftTime = moment($('#shiftTime').val());
            $('.shiftWatch').html("You started working " + moment(shiftTime).fromNow());
        }
    }
    convetShiftToTime();
    setInterval(convetShiftToTime, '500');

    $(".tasks").on("show.bs.dropdown", function(){

        $(".overlay").css("display","");

        $.ajax({
            datatype: "json",
            url:"/import/task",
            success: function(res){

                res = $.parseJSON(res);

                $(".task-menu").html(res['message']);
                $(".overlay").css("display","none");

            }


        });
    });
});



//global var
$delete_btn = "<button class='btn btn-danger delete' title='Delete row'><i class='fa fa-trash' aria-hidden='true'></i>  </button>";
$edit_btn = "<button class='btn btn-info edit' title='Edit row'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>  </button>";
$verify_btn = "<button class='btn btn-success verify' title='Verify deposit'><i class='fa fa-check' aria-hidden='true'></i>  </button>";
$endShift_btn = "<button class='btn btn-warning endShift' title='End Shift'><i class='fa fa-stop' aria-hidden='true'></i>  </button>";
//Functions
function submitForm(form_id, table_name, action, is_simple, row_id, row_pk){

    $(form_id).on('submit',function (e) {

        e.preventDefault();

        var $form = $(this);

        var $inputs = $form.find("input");

        var serializedData = $form.serialize();

        var url = "/"+action+"/"+table_name;

        if(typeof row_id !== "undefined" && row_id.toString().indexOf("#") !== -1) {

            row_id = $(row_id).val();
        }
        if(action === 'update') {

            url += '/'+row_pk+"/"+row_id
        }
        $inputs.prop("disabled", true);

        $.ajax({

            url: url,
            type: "post",
            data: serializedData + "&simple=" + is_simple,
            dataType: "json",
            success: function (res) {
                try {
                    $res = $.parseJSON(res);
                } catch(e) {

                }
                if(res['type'] !== 'error' && is_simple !== false){

                    $inputs.val("");
                }

                $inputs.prop("disabled",false);

                swal(res['header'],res['message'],res['type']);

                table_name.ajax.reload();
            }
        });

    });
}

(function ( $ ) {

    var options = {};

    $.fn.deleteRow = function (options) {

        options.form_id = this['0'].id;

        $('#' + options.form_id).on('click','button.delete',function () {

            var td = $(this).parent();
            var tr = td.parent();
            options.id = $(tr).attr('id');

            deleteAJAX(options);

        });

    }

})( jQuery );

(function ( $ ) {

    var options = {};

    $.fn.changeStatus = function (options) {

        options.form_id = this['0'].id;

        $('#' + options.form_id).on('change','select.'+options.class_name, function () {

            $(this).hide();
            $(this).parent().append(loading_gif(''));

            options.new_status = $(this).val();

            var td = $(this).parent();
            var tr = td.parent();
            options.id = $(tr).attr('id');

            $.ajax({
                type:"POST",
                url: "/update/"+options.table_name+"/"+options.pk+"/"+options.id,
                data: options.status_row+'='+options.new_status+'&simple='+options.is_simple,
                dataType:"json",
                success: function(response){

                    $("select."+options.class_name).show();
                    $(".fa-refresh").remove();

                    if(typeof options.refresh_table_name !== "undefined") {

                        $("#" + options.refresh_table_name).DataTable().ajax.reload();
                    }

                    swal(response['header'],response['message'],response['type']);
                }
            });
        });



    }

})( jQuery );

(function( $ ){

    var options = {};

    $.fn.submitForm = function(options) {


        options.form_id = this['0'].id;

        $('#' + options.form_id).on('submit',function (e) {

            e.preventDefault();

            var $form = $(this);

            var $inputs = $form.find("input");

            var serializedData = $form.serialize();

            var url = "/"+options.action+"/"+options.table_name;

            if(typeof options.row_id !== "undefined" && options.row_id.toString().indexOf("#") !== -1) {

                var row = $(options.row_id).val();
            }
            if(options.action === 'update') {

                url += '/'+options.row_pk+"/"+row
            }
            $inputs.prop("disabled", true);

            $.ajax({

                url: url,
                type: "post",
                data: serializedData + "&simple=" + options.is_simple,
                dataType: "json",
                success: function (res) {
                    try {
                        $res = $.parseJSON(res);
                    } catch(e) {

                    }
                    if(res['type'] !== 'error' && options.is_simple !== false){

                        $inputs.val("");
                    }

                    $inputs.prop("disabled",false);

                    swal(res['header'],res['message'],res['type']);

                    if(typeof options.refresh_table_name !== "undefined") {

                        $("#" + options.refresh_table_name).DataTable().ajax.reload();
                    }

                    if(typeof options.reload_page !== "undefined" && options.reload_page == true) {

                        window.location.reload();
                    }


                }
            });

        });
    };
})( jQuery );


function changeStatus(table_id, status_row, table, pk, is_simple) {

    $(table_id).on('change','select.'+status_row, function () {

        $(this).hide();
        $(this).parent().append(loading_gif(''));

        var new_status = $(this).val();

        $row_id = $(this).parent().closest('tr').attr('id');
        $.ajax({
            type:"POST",
            url: "/update/"+table+"/"+pk+"/"+$row_id,
            data: status_row+'='+new_status+'&simple='+is_simple,
            dataType:"json",
            success: function(response){

                $("select."+status_row).show();
                $(".fa-refresh").remove();

                swal(response['header'],response['message'],response['type']);
            }
        });
    });

}

function loading_gif(){

    var html = "<span class='text-center'><i class='fa fa-refresh fa-spin fa-2x'></i></span>";

    return html;
}

function DataTableButton(button){

    var buttons;

    buttons = button.join(" ");

    return buttons;
}


function editRow(table_id, callback) {

    $(table_id).on('click','button.edit',function () {

        $row_id = $(this).parent().closest('tr').attr('id');

        if (typeof callback === "function") {
            // do something
            callback.call();
        }
    });

}

function editableRow(class_name, table, pk) {


    $(class_name).on('save', function (e, editable) {

        var comment = editable.newValue;
        var row_id = $(this).parent().closest('tr').attr('id');
        var mydata = 'deposit_comment='+ comment+'&deposit_comment_id='+ id;

        $.ajax({
            type:"POST",
            url: "/update/"+table+"/"+pk+"/"+row_id,
            dataType:"json",
            data:mydata,
            success: function(response){
                $(".deposit_comment").prop('disabled', false);
            }
        });
    });



}

function deleteForm(button_id, table, row_id, pk, reload) {

    $(button_id).on('click',function () {

        deleteAJAX(table, row_id, pk, reload, null);
    })


}

function deleteRow(table_id, table_name, pk, is_simple) {

    $(table_id).on('click','button.delete',function () {

        var row_id = $(this).parent().closest('tr').attr('id');
        deleteAJAX(table_name, row_id, pk, false, is_simple);

    });

}

function deleteAJAX(options) {

    swal({

            title: "Are you sure?",
            text: "You are about to delete this row",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "כן, מחק את זה!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "/delete/" + options.table_name + "/" + options.id + "/" + options.pk,
                    success: function (res) {
                        swal(res['header'], res['message'], res['type']);
                        if(typeof options.reload !== 'undefined'){
                            window.location.reload();
                        }

                        if(typeof options.refresh_table_name !== 'undefined' ){

                            $("#" + options.refresh_table_name).DataTable().ajax.reload();
                        }

                    }
                });
            }
        }
    );
}

function startShift(actioner, data) {
    $(actioner).on('click', function () {

        player = $("#playerID").val();

        $.ajax({
            type: "post",
            url: "insert/shift",
            data: data + '&playerID='+ player + '&simple=false',
            dataType: 'json',
            success: function (res) {

                swal(res['header'], res['message'], res['type']);
                window.location.reload();
            }
        })
    })

}

function TableEndShift(table_id) {

    $(table_id).on('click','button.endShift',function () {

        var shift_id = $(this).parent().closest('tr').attr('id');

        $.ajax({
            type: "post",
            url: "update/shift/id/"+shift_id,
            data: 'active=0&simple=true',
            dataType: 'json',
            success: function (res) {

                swal(res['header'], res['message'], res['type']);
            }
        })
    })
}

function stopShift(actioner, shift_id){

    $(actioner).on('click', function () {
        $.ajax({
            type: "post",
            url: "update/shift/id/"+shift_id,
            data: 'active=0&simple=true',
            dataType: 'json',
            success: function (res) {

                swal(res['header'], res['message'], res['type']);
                window.location.reload();
            }
        })
    })
}

function exportTable(export_btn_id, sql_table, parameters) {

    $(export_btn_id).on('click', function(){

        var data = $(parameters).serialize();
        $.ajax({
            type: "get",
            url: "export/"+sql_table,
            data: data,
            dataType: "text",
            success: function (res) {
                try{
                    res = $.parseJSON(res);
                    swal(res['header'], res['message'], res['type']);
                } catch(err){
                    window.location.replace("export/"+sql_table+"?"+data)
                }
            }

        });
    });

}


