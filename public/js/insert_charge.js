$(document).ready(function(){
    
    function notifyPermission(){

        if(!window.Notification){

            console.log("This browser does'nt support notification. sorry!");
        } else {

            Notification.requestPermission();

        }
    }

    $("#submitDeposit").click(function(){
        
        notifyPermission();    
        
        //customer name
        var fullcustomername = $("#cust_name").val();
        var customerfirstname = fullcustomername.split(' ').slice(0, -1).join(' ');
        var customerlastname = fullcustomername.split(' ').slice(-1).join(' ');
        
        //emp name
        var fullempname = $("#emp_name").val();
        var empname = fullempname.split(' ');
        
        var amount = $("#deposit").val();
        
        var type = $("#type").val();
        var payment_method = $("#payment_method").val();
        var tran_id = $("#tran_id").val();
        var cid = $("#deposit_cid").val();
        var currencey = $("#currencey").val();
        
        var emp_name = 
            '&emp_firstname='
                +empname[0]+
            '&emp_lastname='
                +empname[1];
        
        var mydata = 
            'customer_firstname='
                +customerfirstname+
            '&customer_lastname='
                +customerlastname+
            emp_name+
            '&tran_id='
                +tran_id+
            '&payment_method='
                +payment_method+
            '&type='
                +type+
            '&cid='
                +cid+
            '&currency='
                +currencey+
            '&amount='
                 +amount+
            '&do=charge'; 
        
        swal({
                    title: "Insert Chargeback",
                    text:  "A new Charge Back in amount of <strong>"+amount+" "+currencey+"</strong> via <strong>"+payment_method+"</strong> for user: <strong>"+empname[0]+" "+empname[1]+"</strong></br><strong>Click OK to continue.</strong>",
                    type:  "info",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    confirmButtonText: "OK",
                    html: true
                },
             function(){
                    $.ajax({
                        data: mydata,
                        url:'inc/chargeback/chargeback.inc.php',
                        type: 'POST',
                        dataType:'text',
                        success: function(res){
                            
                         try {
                                console.log(res);
                                JSON.parse(res);
                            } catch(e) {
                                swal({title: type+" Inserted!",
                                      text: type+" Inserted succesfuly.",
                                      type: "success",
                                      showCancelButton: true,
                                      confirmButtonText: "Another Deposit",
                                      cancelButtonText: "Finish",
                                     },function(res){
                                    if(res){
                                    $("input[type=text]").val("");
                                    
                                    }else{
                                        
                                        notify("You just add a new deposit!");
                                        location.reload();
                                    }
                                }); 
                            }
                            
                            $res = $.parseJSON(res);
                            
                            if($res['flag'] == 0){
                                swal($res['header'],$res['message'],$res['type']);
                            }
                        }
                })
            
            }); 
        });
    
    function notify(title){
        
        new Notification(title);
        
    }
    
    function lodaing_gif(){
        
        var html = "<i class='fa fa-refresh fa-spin fa-2x'></i>";
        
        return html;
    }
    $(".dispute").change(function(d){
        d.preventDefault();
        action = $(this);
        
        $(this).hide();
        $(this).parent().append(lodaing_gif(''));
        var sel = $(this).val();
        var id = $(this).closest("tr").attr('data-value');
        var mydata = 'dispute_sent='+ sel+'&id='+ id;

        $.ajax({
			type:"POST",
			url: "inc/chargeback/chargeback.inc.php",
			dataType:"text",
			data:mydata,
            success: function(res){
                     
                $(".fa-refresh").remove();
                $(".dispute").show();
                }
        });
        
    });
    
    $("#delete-button, #delete-button_c").click(function(){
        var m = $(".selected").attr('data-value');
            swal({
                title: "Are you sure?",
                text: "You going to delete this Deposit.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },function(result) {
            if(result){
                $.ajax({ 
                    type: "POST",
                    data: "deposit_id="+m+"&vald=1&table=chargeback",
                    dataType: "text",
                    url: "inc/Controller/delete.php",
                    success: function(res){
                        swal({title: "Deleted!",
                              text: "Deposit has been deleted.", 
                              type: "success"},function(){
                            //console.log(res);
                            location.reload(); 
                        }) 
                    } //close function
                }); 
            }
            });
		});
});