<?php
namespace Crm4u\Deposits;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Import\brand;
use Crm4u\Import\user;
use Crm4u\SQL\filter;
use Crm4u\Middleware\Fetch;


class charts extends filter{

    /**
    * the chart result 
    * @array
    * @default = array();
    **/
    public $chartResult = array();
    
    
    
    
    /**
    * var for limit the result number
    * 
    * @var int
    */
    public $limit;
    
    
    
    function __construct($limit = 12, $chartResult = array()){
    
        parent::__construct();
        $this->limit = $limit;

    }
    
    function currencyChart(){

        $filter = new filter();

        $cols = array($filter->currencyFilter());

        $parameters = array(
            'WHERE' => '0 = 0',
            $filter->userFilter(0),
            $filter->brandFilter(0),
            $filter->dateFilter(1)
        );

        $result = new Fetch('goal',$cols, $parameters);
        
        $this->chartResult = array(
                                   'USD' => $result->result->USD,
                                   'EUR' => $result->result->EUR,
                                   'GBP' => $result->result->GBP,
                                  );


    }
    
    function lastMonthChart(){

        $filter = new filter();

        $cols = array(
            'month',
            'year',
            $filter->currencyFilter()
        );

        $parameters = array(
            "WHERE" => "0 = 0",
            $filter->userFilter(1),
            $filter->brandFilter(1),
            $filter->countriesFilter(1),
            "GROUP BY" => 'month, year'
        );

        $result = ImportController::import('goal',$cols, $parameters);
        
        foreach($result['data'] as $key => $value) {
            
            $tempResult = array(
                                'USD' =>  $value['USD'] + $value['USDWithdrawals'] + $value['USDchargeBack'],
                                'EUR' =>  $value['EUR'] + $value['EURWithdrawals'] + $value['EURchargeBack'],
                                'GBP' =>  $value['GBP'] + $value['GBPWithdrawals'] + $value['GBPchargeBack'],
                               );
            
            $this->chartResult[$value['month']." ".$value['year']] = ( $tempResult['USD'] + $tempResult['EUR'] + $tempResult['GBP']);
        }
        
    } 
    
    function scoreboardDeposit($type = 'users') {

        $filter = new filter();

        $cols = array(
            'emp_id',
            $filter->currencyFilter(),
            filter::Sum(true, filter::case_when('goal.ftd', '= 1',1,'0', true), 'ftd','ftdCount'),

            ($type != 'users') ? "countries.country_name name "              : "CONCAT(users.firstname,' ',users.lastname) name",
            ($type == 'users') ? 'users.goal_ftd ftd_target' : 'NULL as ftd_target',
            ($type == 'users') ? 'users.goal_deposit target' : 'NULL as target',
        );

        $join = array(
            'INNER JOIN' => 'users',
            'ON'         => 'goal.emp_id = users.user_key_id',
            'AND'        => 'goal.brand  = users.brand'
        );


        $join1 = array(
            ' INNER JOIN' => 'countries',
            'ON '         => 'goal.country = countries.id '
        );

        $parameters = array(
            'WHERE'   => '0=0',
            $filter->dateFilter(1),
            $filter->typeFilter(1),
            $filter->shiftFilter(0),
            $filter->statusFilter(0),
            $filter->brandFilter(0),
            'GROUP BY' => 'name',
            'ORDER BY' => 'amount'
            //silent filter todo
        );

        $result = ImportController::import('goal', $cols, array_merge($join, $join1, $parameters));

        $chargeback = $result['data'];

        if(isset($chargeback['0'])){


            foreach ($chargeback as $key => $value){

                $tempResult = array(
                    'USD'     => $chargeback[$key]['USD'] + $chargeback[$key]['USDWithdrawals'] + $chargeback[$key]['USDchargeBack'],
                    'EUR'     => $chargeback[$key]['EUR'] + $chargeback[$key]['EURWithdrawals'] + $chargeback[$key]['EURchargeBack'],
                    'GBP'     => $chargeback[$key]['GBP'] + $chargeback[$key]['GBPWithdrawals'] + $chargeback[$key]['GBPchargeBack'],
                    'confUSD' =>  $chargeback[$key]['confUSDdeposits'],
                    'confEUR' =>  $chargeback[$key]['confEURdeposits'],
                    'confGBP' =>  $chargeback[$key]['confGBPdeposits'],
                );

                $this->chartResult[] = array(
                    'name'           => $chargeback[$key]['name'],
                    'amount'         => $tempResult['USD'] + $tempResult['EUR'] + $tempResult['GBP'],
                    'confirm_amount' => $tempResult['confUSD'] + $tempResult['confEUR'] + $tempResult['confGBP'],
                    'ftd'            => $chargeback[$key]['ftdCount'],
                    'target_ftd'     => $chargeback[$key]['ftd_target'],
                    'target'         => $chargeback[$key]['target'],
                );

            }

        } else {

            $tempResult = array(
                'USD'     => $chargeback->USD + $chargeback->USDWithdrawals + $chargeback->USDchargeBack,
                'EUR'     => $chargeback->EUR + $chargeback->EURWithdrawals + $chargeback->EURchargeBack,
                'GBP'     => $chargeback->GBP + $chargeback->GBPWithdrawals + $chargeback->GBPchargeBack,
                'confUSD' =>  $chargeback->confUSDdeposits,
                'confEUR' =>  $chargeback->confEURdeposits,
                'confGBP' =>  $chargeback->confGBPdeposits
            );

            $this->chartResult[] = array(
                'name'           => $chargeback->name,
                'amount'         => $tempResult['USD'] + $tempResult['EUR'] + $tempResult['GBP'],
                'confirm_amount' => $tempResult['confUSD'] + $tempResult['confEUR'] + $tempResult['confGBP'],
                'ftd'            => $chargeback->ftdCount,
                'target_ftd'     => $chargeback->ftd_target,
                'target'         => $chargeback->target
            );

        }




    }

    
    function allMonth($fmonth = 12){

        $filter = new filter();

        $cols = array(
            'goal.month',
            'goal.year',
            filter::Sum(true, filter::case_when('goal.ftd', '= 1',1,'0', true), 'ftd','ftdCount'),
            $filter->currencyFilter(),
            $filter->countCustomers()
        );

        $parameters = array(
            'WHERE'   => '0=0',
            $filter->userFilter(0),
            $filter->typeFilter(1),
            $filter->shiftFilter(0),
            $filter->statusFilter(0),
            $filter->brandFilter(0),
            $filter->countriesFilter(1),
            'GROUP BY' => 'goal.month, goal.year',
            'ORDER BY' => 'goal.year DESC, MONTH(date) DESC',
            $filter->limitResult($fmonth)
        );

        $result = ImportController::import('goal',$cols, $parameters);

        $i = 1;
        
        $this->weeklyTarget();
        
        foreach($result['data'] as $key => $value) {

            $tempResult = array(
                                'USD'     =>  $value['USD'] + $value['USDWithdrawals'] + $value['USDchargeBack'],
                                'EUR'     =>  $value['EUR'] + $value['EURWithdrawals'] + $value['EURchargeBack'],
                                'GBP'     =>  $value['GBP'] + $value['GBPWithdrawals'] + $value['GBPchargeBack'],
                                'confUSD' =>  $value['confUSDdeposits'],
                                'confEUR' =>  $value['confEURdeposits'],
                                'confGBP' =>  $value['confGBPdeposits']
                               );
            
            $this->chartResult['data'][]  =
                array(
                    'month'          => $value['month']." ".$value['year'],
                    'amount'         => $tempResult['USD'] + $tempResult['EUR'] + $tempResult['GBP'],
                    'confirm_amount' => $tempResult['confUSD'] + $tempResult['confEUR'] + $tempResult['confGBP'],
                    'ftd'            => $value['ftdCount'],
                    'target'         => $this->chartResult['average_countPM'],
                    'countCustomers' => $value['countCustomers']
                );
            $i++;
        }
        (isset($this->chartResult['data'])) ? krsort($this->chartResult['data'])  : "";

    }
    
    function weekFilter(){
    
        //set global var
        global $db; global $loader;
        
        
        //get brand list and put on temp array
        $sql  = "SELECT goal.brand FROM goal";
        $sql .= " WHERE 1 = 1";
        
        //filter
        $filters = array(
                 "userFilter"   => 0,
                 "dateFilter"   => 1,
                 "typeFilter"   => 1,
                 //"ftdFilter"    => 0,
                 "shiftFilter"  => 0,
                 "statusFilter" => 0,
                 "brandFilter"  => 0,
                 "countriesFilter" => 1
                );
        
        foreach($filters as $filter => $arg){
            
            $sql .= $this->$filter($arg);
        }
        $sql .= "GROUP BY brand";
        
        
        $result = $loader->mysqli->query($sql);

        while($row = $result->fetch_assoc()){
            
            $this->chartResult['brand_name'][] = ($row['brand']);
            
        }
        //get day with deposit at one of the brand
        $sql  = " SELECT DATE(goal.date) date";
        $sql .= " FROM goal";
        $sql .= " WHERE 1 = 1";
        
        foreach($filters as $filter => $arg){
            
            $sql .= $this->$filter($arg);
        }
        
        $sql .= " GROUP BY DATE(goal.date)";
        
        $result = $loader->mysqli->query($sql);
        
        while($row = $result->fetch_assoc()){
            
            $this->chartResult['date'][] = ($row['date']);
            
        }
        //chart data
        $sql = "SELECT goal.month, DATE(goal.date) date, goal.brand,";
        
        //select currency
        $sql .= $this->currencyFilter();
        
        //table
        $sql .= " FROM goal";
        
        $sql .= $this->innerJoin('goal','users');
        
        $sql .= " WHERE 1=1 ";
        
        foreach($filters as $filter => $arg){
            
            $sql .= $this->$filter($arg);
        }
        
        $sql .= " GROUP BY brand, DATE(date)";
        
        $sql .= "ORDER BY MONTH(date) DESC, DAY(date) ASC";
        
         
        $result = $loader->mysqli->query($sql);
        
        $weeknumber = 1;
        
        while($row = $result->fetch_assoc()) {
         
            $tempResult[$weeknumber] = array(
                                'USD' =>  $row['USD'] + $row['USDWithdrawals'],
                                'EUR' =>  $row['EUR'] + $row['EURWithdrawals'],
                                'GBP' =>  $row['GBP'] + $row['GBPWithdrawals']
                               );
            
            $date = explode(" ",$row['date']);
            $date = $date[0]; 
            
            $this->chartResult['data'][] = array(
                                'amount' => $tempResult[$weeknumber]['USD'] + $tempResult[$weeknumber]['EUR'] + $tempResult[$weeknumber]['GBP'],
                                'brand' => $row['brand'],
                                'date' => $date
                                            );
            
            //add week
            $weeknumber++;
        }
    }
    
    function explainIncome($type = "users"){

        $filter = new filter();

        switch ($type) {
            default:
                $name = "{$this->table}.{$type} name";
                break;
            case 'users':
                $name = "CONCAT(users.firstname,' ',users.lastname) name";
                break;
            case 'country':
                $name = "countries.".$type."_name name";

        }

        $cols = array(

            $name,
            'goal.month',
            'goal.year',
            filter::Sum(false,false,'goal.amount','s_amount'),
        );

        $join = array(
            'INNER JOIN' => 'users',
            'ON'         => 'goal.emp_id = users.user_key_id',
            'AND'        => 'goal.brand  = users.brand'
        );

        $join1 = array(
            ' INNER JOIN' => 'countries',
            'ON '         => 'goal.country = countries.id '
        );



        $parameters = array(
            $filter->userFilter(0),
            $filter->dateFilter(1),
            $filter->typeFilter(1),
            $filter->shiftFilter(0),
            $filter->statusFilter(0),
            $filter->brandFilter(0),
            $filter->countriesFilter(1),
            "GROUP BY"  => ($type == "users") ? "goal.emp_id" : "goal.".$type
        );

        $parameters = array_merge($join, $join1, $parameters);

        $result = ImportController::import('goal', $cols, $parameters);

        foreach($result['data'] as $key => $value) {

            if($value['s_amount'] > 1){

                $this->chartResult[] = array(
                    "name"              => $value['name'],
                    "percentage_amount" => $value['s_amount']);
            }
        }
    }
    
    function depositandcount(){


        $filter = new filter();

        $cols = array(
            'goal.date',
            $filter->currencyFilter(),
            $filter->countRow()
            );

        $parameters = array(
            'WHERE' => '0=0',
            $filter->userFilter(0),
            $filter->dateFilter(1),
            $filter->typeFilter(1),
            $filter->shiftFilter(0),
            $filter->statusFilter(0),
            $filter->brandFilter(0),
            $filter->countriesFilter(1),
            'GROUP BY' => 'MONTH(goal.date),WEEK(goal.date), YEAR(goal.date)',
            'ORDER BY' => 'goal.date ASC'
        );

        $result = ImportController::import('goal', $cols, $parameters);

        $this->weeklyTarget();
        
        $i = 0;
        $w = 1;
        foreach($result['data'] as $key => $value){

            $tempResult['data'][] = array('count'   =>  $value['sumdeposits'],
                                          'USD'     =>  $value['USD'] + $value['USDWithdrawals'] + $value['USDchargeBack'],
                                          'EUR'     =>  $value['EUR'] + $value['EURWithdrawals'] + $value['EURchargeBack'],
                                          'GBP'     =>  $value['GBP'] + $value['GBPWithdrawals'] + $value['GBPchargeBack'],
                                          'confUSD' =>  $value['confUSDdeposits'],
                                          'confEUR' =>  $value['confEURdeposits'],
                                          'confGBP' =>  $value['confGBPdeposits']
                                          );
                
            $this->chartResult['data'][] = array('count'      => $tempResult['data'][$i]['count'],
                                                'amount'      => $tempResult['data'][$i]['USD'] + $tempResult['data'][$i]['EUR'] + $tempResult['data'][$i]['GBP'],
                                                 'status'    => $tempResult['data'][$i]['confUSD'] + $tempResult['data'][$i]['confEUR'] + $tempResult['data'][$i]['confGBP'],
                                                 'date'       => $w." of ".date('m/y',strtotime($value['date'])),
                                                'countTarget' => $this->chartResult['average_countPW'] ,
                                                'target'      => $this->chartResult['target'] / 4
                                                );
            $i++;
            $w++;
            
        }
    }

    function weeklyTarget(){

        global $loader;

        //reset array:
        $this->chartResult['average_amount'] = 0;
        
        $this->chartResult['average_countPM'] = 0;
        
        $this->chartResult['average_countPW'] = 0;
        
        $this->chartResult['target'] = 0;

        $finance = brand::import_finance(BrandController::current_brand());

        $tempResult = array('depositAmount' => $finance->all_time->all_deposit_summery,
                            'depositCount' => $finance->all_time->total_deposit_count,
                            'target'       => $loader->brand->brand_target
                           );

        if($tempResult['depositAmount'] > 0) {
        
            $this->chartResult['average_amount'] = (round($tempResult['depositAmount'] / $tempResult['depositCount'],1));

            $this->chartResult['average_countPM'] = (round($tempResult['target'] / $this->chartResult['average_amount'],1));

            $this->chartResult['average_countPW'] = (round($this->chartResult['average_countPM'] / 4,1));
            
            $this->chartResult['target'] = $tempResult['target'];
            
        }
        return $this->chartResult;
    }
    
    public function campaignMonitor($format = 'chart', $displayAll = true, $campName = false){

        global $loader;

        $campName = (!$displayAll) ? $campName : 0;

        $sql = "SELECT campaign, sum(case when goal.ftd = 1 then goal.ftd else 0 end) ftd, ";

        $sql .= filter::currencyFilter();

        $sql .= " FROM goal ";
        
        $filters = array(
                 "fixWhere"     => $sql,
                 "userFilter"   => 0,
                 "dateFilter"   => 0,
                 "typeFilter"   => 1,
                 //"ftdFilter"    => 0,
                 "campaignFilter"  => $campName,
                 //"statusFilter" => 0,
                 "brandFilter"  => 0,
                 "countriesFilter" => 1
                );
        //filters
        foreach($filters as $filter => $arg){
            
            $sql .= $this->$filter($arg);
        }
        
        $sql .= " GROUP BY goal.campaign";
                    
        $sql .= " ORDER BY SUM(goal.amount) DESC";

        $result = $loader->mysqli->query($sql);
        
        while($row = $result->fetch_assoc()){
            $tempResult = array('USD'     =>  $row['USD'] + $row['USDWithdrawals'] + $row['USDchargeBack'],
                                'EUR'     =>  $row['EUR'] + $row['EURWithdrawals'] + $row['EURchargeBack'],
                                'GBP'     =>  $row['GBP'] + $row['GBPWithdrawals'] + $row['GBPchargeBack']
                                );

            $tempResult = array('campaign_name'   => $row['campaign'],
                                'amount'          => $tempResult['USD'] + $tempResult['EUR'] + $tempResult['GBP'],
                                'ftd'             => (int) $row['ftd']);

            $this->chartResult[] = $tempResult;

            switch ($format){

                case 'charts':

                case 'json':

                    return $tempResult;
            }

        }
        
    }
}