<?php
namespace Crm4u\Forms\Users\new_user;

use Crm4u\Controller\FormController;
use Crm4u\Controller\Route;

global $loader;
$form = new FormController();


?>

<div class="modal fade" id="newEmployee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New user
                </h4>
            </div>
            <div class="box-body newuser_form">
                <form id="new_employee_form" method="post">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="user_id">KEY ID *</label>
                        <input class="form-control" type="text" id="user_id" name="user_key_id" required/>
                    </div>
                </div>
                <div class="col-md-6">
                        <div class="form-group">
                            <label for="firstname">Firstname *</label>
                            <input class="form-control" type="text" id="firstname" name="firstname" required/>
                        </div>
                    </div>
                <div class="col-md-6">
                        <div class="form-group">
                            <label for="lastname">Lastname *</label>
                            <input class="form-control" type="text" id="lastname" name="lastname" required/>
                        </div>
                    </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="username">username *</label>
                        <input class="form-control" type="text" id="username" name="username" required/>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="password">Password *</label>
                        <input class="form-control" type="password" id="password" name="password" required/>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="priv">privelege *</label>
                        <select class="form-control" type="text" id="priv" name="priv" required>
                            <?php
                            $values = array("User" => 1,"Brand Admin" => 2,"Administrator" => 3,"Super Admin" => 4);
                            print $form->selected_option($values,null,'');
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="desk">Desk *</label>
                        <select class="form-control"  id="desk" name="location" required>
                            <option value=""></option>
                            <?php
                            Route::get('select', 'Models', 'desks', array('desk'));
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="department">Department *</label>
                        <select class="form-control"  id="department" name="department" required>
                        </select>
                    </div>
                </div>


            </div><!--.modal-body-->
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="submit-new">Add Employee</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </form>
            </div><!--.modal-footer-->

        </div>
    </div>
</div>
<script>

    $(document).ready(function () {

        $('#desk').change(function (option) {

            deskParent = $('#desk option:selected').val();
            $.ajax({
                type: "GET",
                url: "/import/department",
                data: 'deskParent=' + deskParent,
                dataType:"json",
                success: function (res) {
                    console.log(res);

                    options = [];

                    $.each(res['data'], function (k, v) {
                        option = {};

                        option['id'] = v['id'];
                        option['name'] = v['department_name'];

                        option = "<option value='"+option.id+"'>"+option.name+"</option>";

                        options.push(option);
                    });
                    $("#department").html(options);
                }
            })
        })
    })
</script>