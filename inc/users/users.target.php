<?php
namespace Crm4u\Forms\Users;

use Crm4u\Controller\notification;
use Crm4u\Import\user;

$selected_user = $_loadArgs;
?>
<div class="modal fade" id="target" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-success">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title" id="myModalLabel">Set Target</h4>
            </div>
            <div class="box-body">
                <form class="form-horizontal" role="form" id="edit_target" name="adduserform">
                    <div class="form-group">
                        <label for="deposit_target" class="col-sm-2 control-label">Deposit</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="deposit_target" name="goal_deposit" value="<?php echo $selected_user->goal_deposit ?>"/>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="ftd_target" class="col-sm-2 control-label">FTD</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="ftd_target" name="goal_ftd" value="<?php echo $selected_user->goal_ftd ?>"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="submit_target" name="submit">Set Target</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        submitForm('#edit_target','users','update', true,  <?php echo user::current_user() ?> ,'user_key_id');
    })
</script>

