<?php
namespace Crm4u\Deposits;

use Crm4u\Import\user;

class getStatistic {

    public $selected_user;
    /**
    * 4 args:
    * 1. @var string: type 
    * 1. @var int type_amount
    * 2. @var bool show_target (?)
    * 3. @var int target_amount
    * 4. @var string target_type
    */

    function output($type = '', $type_amount = 0, $show_target = 1, $target = 0, $target_type = null){

            if($target == 0){

                return "<p class='text-muted text-center'>$type target not set</p>";
                end();
            }

            if($type_amount == 0){

                return "<p class='text-muted text-center'>No $type yet this month</p>";
                end();
            }

            $progressBarColor = array('success','info','danger','warning');

            $per = $this->getPercetange($type_amount, $target); 
            
            $targetHeader = $this->targetHeader($target, $target_type, $show_target);

            $html  = "<strong><i class='fa fa-plus margin-r-5'></i> ".$type.$targetHeader."</strong>";

            $html .= "<p><div class='progress'>";

            $html .= "<div class='progress-bar progress-bar-".$progressBarColor[array_rand($progressBarColor)]." progress-bar-striped' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width:$per%'>".$per."%</div>";

            $html .= "</div></p>";

            return $html;
        }

        function getPercetange($type_amount, $target){

            $per = round($type_amount / $target * 100,1);
            $per = ($per > 100) ? 100 : $per;
            
            return $per;
        }
    
        function targetHeader($target, $target_type, $show_target){
            if($show_target == 1){
                return " target - ".$target." ".$target_type;
            } else {
                return "";
            }
            
        }
}
$goals = new getStatistic();
$selected_user = $_loadArgs[0];
$finance = $_loadArgs[1];
$show_extra_info = (isset($_loadArgs[2])) ? $_loadArgs[2] : true;
?>

<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Statistics</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-4">
                    
                </div>
            </div>

            <?php print $goals->output('Deposit', $finance->current_month->all_deposits,1,$selected_user->goal_deposit, 'USD'); ?>
            
                
              <hr>
                
            <?php print $goals->output('FTD', $finance->current_month->total_ftd,1, $selected_user->goal_ftd); ?>
            
              <hr>
            <?php print $goals->output('Verification Status', $finance->current_month->total_conf,0, $finance->current_month->all_deposits); ?>
           
              <hr>
                <?php
                if($show_extra_info){
                    ?>

                    <strong><i class="fa fa-pencil margin-r-5"></i> Member in</strong>

                    <p>
                        <!--                  @todo-->
                        <?php

                        $brandList = user::brandList($selected_user->user_key_id);

                        foreach($brandList['data'] as $key => $value){
                            print " <span class='brand-label label label-success'>".$brandList['data'][$key]['brand_name']."</span> ";
                        }
                        ?>
                    </p>

                    <hr>

                    <strong><i class="fa fa-sign-in"></i> Last Login</strong>

                    <?php print "<span class='brand-label label label-warning' >".$selected_user->last_login."</span>"; ?>
                <?php
                }
                ?>

           </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->