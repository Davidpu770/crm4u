<?php
namespace Crm4u\Forms\Users\Duplicate_user;

use Crm4u\Controller\BrandController;
use Crm4u\Import\user;

global $loader;

?>

<div class="modal fade" id="duplicate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title" id="myModalLabel">Duplicate User</h4>
            </div>
             <div class="box-body">
                 <div class="container-fluid">
                     <div class="form-group">
                        <label for="brand">Brand *</label></br>
                        <select class="form-control" type="text" id="brand" name="brand" multiple="multiple">
                            <?php 
                            foreach($loader->all_brand['data'] as $key => $value){
                                echo "<option value=".$value['brand_name'].">".$value['brand_name']."</option>";
                                }
                            ?> 
                        </select>
                     <script>$("#brand").multiselect({buttonWidth: '525px',});</script>
                        
                    </div>
                        </div>
                            <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success" id="submit_duplicate" name="submit">Duplicate</button>
                    </div>
                </div>
            </div>
        </div>
    </div>