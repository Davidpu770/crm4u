<?php
namespace Crm4u\Forms\Users\reset_password;

use Crm4u\Import\user;

?>
<div class="modal fade" id="resetPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title" id="myModalLabel">Reset Password</h4>
            </div>
             <div class="box-body">
                 <form class="form-horizontal" role="form" id="reset_password_form" name="adduserform">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="password" name="password" autocomplete="off" placeholder="Enter Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">r-password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="rpassword" name="repassword" autocomplete="off" placeholder="Password again">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" id="submit">Reset Password</button>
                            </div>
                     </form>

            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        submitForm('#reset_password_form','password','update', false,  <?php echo $_loadArgs[0] ?> ,'user_key_id');
    })
</script>