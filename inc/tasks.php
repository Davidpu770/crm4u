<?php
namespace Pages\Partial\Tasks;
?>
<li class="dropdown tasks-menu tasks">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <span id="task_number">0</span> tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <div class="slimScrollDiv " style="position: relative; overflow: hidden; width: auto; height: 200px;">
                    <ul class="menu task-menu" style="overflow: hidden; width: 100%; height: 200px;"></ul>
                    <div class="slimScrollBar" style="width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; background: rgb(0, 0, 0);"></div>
                    <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div></div>
              </li>
                <div class="overlay" style="display: none">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
