<?php
namespace Crm4u\Pages\Partial\Notification;
?>
<li class="dropdown notifications-menu notification">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="notification_ajax">
        <i class="fa fa-bell-o"></i>
        <span class="label label-warning notification_number"></span>
    </a>
    <ul class="dropdown-menu">
      <li class="header">You have <span class="notification_number">0</span> notifications</li>
      <li>
        <!-- inner menu: contains the actual data -->
        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
            <ul class="menu notification-menu" style="overflow: hidden; width: 100%; height: 200px;">
            
            </ul>
            <div class="slimScrollBar" style="width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 195.122px; background: rgb(0, 0, 0);"></div>
            <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div>
            <div class="overlay" style="display: none">
                <i class="fa fa-spinner fa-spin"></i>
            </div>
          </div>
          
          
      </li>

      <li class="footer"><a href="<?php echo LINK."notifications"; ?>">View all</a></li>
    </ul>
</li>
<script>
        
        $(".notification").on("show.bs.dropdown", function(){
            
            $(".overlay").css("display","");

            $.ajax({
                datatype: "json",
                url:'/import/notification',
                success: function(res){
                    res = $.parseJSON(res);
                    $.each(res['data'], function (key) {

                        var style = "background-color: #f8fdf6; box-shadow: 0 0 30px 2px white inset;";

                        var message = "<li class='notify' data-value='"+ res['data'][key]['id']+"' style='"+style+"'> " +
                            "<a href='#' data-toggle='tooltip' data-placement='bottom'>" +
                            "<i class='fa " + res['data'][key]['action'] + "_fa_icon text-light-blue'></i>" +
                            "<span class='details'>" +res['data'][key]['content']+ "</span>" +
                            "</a>" +
                            "</li>";

                        $("ul.notification-menu").html(message);
                    });


                    $(".notification-menu").html(res['data']);
                    $(".overlay").css("display","none");      
                }
                
                
            });
        });

            $("ul.notification-menu").on('click', function(event){
                

                $("li.close_notification").click(function(){
                    
                    var id = $("li.close_notification").attr("data-value"); 
                    var mydata = 'delete=' + id
                    if(!$.ajaxComplete){
                        
                    $.ajax({ 
                        type: "post",
                        datatype: "text",
                        url:"<?php echo LINK."inc/ajax/notification.php" ?>",
                        data: mydata,
                        success: function(res){
                            res = $.parseJSON(res);
                            $(".notification_number").html(res['count']['notification_count']);
                            $(".notification-menu").html(res['message']);
                            $(".overlay").css("display","none");   
                        }
                        })
                    }
            });
            event.stopPropagation();
        })
        
            
</script>