<?php
namespace Support;

use Import;

include_once dirname(__DIR__)."/import/import.shift.php";
$shift =new Import\shift();
$shift->import(0);

$col_max_size = 12;
$cols = $shift->shifts['shift_num'];

$col_size = $col_max_size / $cols;

?>
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Verifed deposits By shift's</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /.box-tools -->
    </div>
    
    <div class="row">
        <?php 
        $box = "";
        $color = array("yellow","red","green");
        $i = 0;
        foreach($shift->shifts['shifts'] as $key => $value){

            $box .= "<div class='col-md-$col_size col-sm-12'>";
            $box .= "<div class='info-box bg-".$color[array_rand($color,1)]."'>";
            $box .= "<span class='info-box-icon'><i class='fa fa-check' style='line-height: 2'></i></span>";
            $box .= "<div class='info-box-content'>";
            $box .= "<span class='info-box-text'>".$value['shift_display_name']."</span>";
            $box .= "<span class='info-box-number'>".$support->shiftCount[$i][$value['shift_name'].'Shift']." (".$support->shiftCount[$i]['conf'.ucfirst($value['shift_name'])]." VERIFED)</span>";
            $box .= "</div></div></div>";
            $i++;
        }
        print $box;
            ?>
    </div>
</div>