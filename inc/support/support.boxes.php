<?php
namespace Crm4u\Pages\Partial\Support\Boxes;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\FormController;
use Crm4u\Import\brand;


$finance = brand::import_finance(BrandController::current_brand());

?>

<script>

    $("#searchBTN").on('click', function () {

        $form = $("#filter_form").serialize();

        $.ajax({
            type: "get",
            url: "/import/Totals",
            data: $form,
            dataType: "json",
            success: function (res) {

                $.each(res.current_month,function (k, v) {
                    console.log(k + ":" + v);
                    $('#' + k).html(v);


                })


            }
        });


        $("#resetFilter").css("display","");
    });

    $("#resetFilter").on('click', function () {

        $.ajax({
            type: "get",
            url: "/import/Totals",
            dataType: "json",
            success: function (res) {

                $.each(res.current_month,function (k, v) {
                    console.log(k + ":" + v);
                    $('#' + k).html(v);


                })


            }
        });

        $(this).css("display","none");

    });
</script>


<div class=""
        <div class="row  mt">
            
            <!-- DEPOSITS -->
            <div class="col-md-3 col-sm-12 slideInUp animated">

                <div class="info-box bg-blue supportBox hvr-overline-reveal">
                    <span class="depositSinceLogin" ><?php echo number_format(round($finance->last_login->total_deposits,1)); ?>$ Since last login</span>
                    <span class="info-box-icon supportBox"><i class="ion ion-card"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Deposits</span>
                        <span class="info-box-number" ><span id="deposit_summery"><?php
                                                echo number_format(round($finance->current_month->deposit_summery,1));
                                ?></span> USD</span>
                        <span class="progress-description" >
                        <span id="all_deposits"><?php echo number_format($finance->current_month->all_deposits) ?></span>$ Before Withdrawals
                        </span>
                        <hr>
                        <span class="info-box-number"><span  id="total_deposit_count"><?php echo $finance->current_month->total_deposit_count ?></span> ( <span id="total_ftd"><?php echo $finance->current_month->total_ftd ?> </span> FTD's)</span>
                    </div>
                </div>

            </div>
            <!-- CONFIRM DEPOSIT -->
            <div class="col-md-3 col-sm-12 slideInUp animated">
                <div class="info-box bg-green supportBox hvr-overline-reveal">
                    <span class="depositSinceLogin" ><?php echo number_format(round($finance->last_login->total_conf,1)); ?>$ Since last login</span>
                    <span class="info-box-icon"><i class="ion ion-android-checkbox-outline" style="line-height: 2"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Confirm Deposits</span>
                        <span class="info-box-number" ><span id="total_conf"><?php
                                echo number_format(round($finance->current_month->total_conf,1));
                                ?></span> USD
                        </span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo $finance->current_month->par_conf; ?>%"></div>
                        </div>
                        <span class="progress-description"><span id="par_conf">
                                <?php echo $finance->current_month->par_conf; ?>
                            </span>% This month
                        </span>
                        <hr>
                        <span class="info-box-number" id="total_confirm_count">
                            <?php   echo $finance->current_month->total_confirm_count;
                            if($finance->current_month->total_confirm_count <= 0)
                            { echo "";
                            } else {
                                $par = round($finance->current_month->total_confirm_count / $finance->current_month->total_deposit_count * 100,1);
                            }
                            ?>
                        </span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo $finance->current_month->percentage_conf; ?>%"></div>
                        </div>
                        <span class="progress-description">
                            <span id="percentage_conf" ><span id="par_total_conf" > <?php echo $finance->current_month->par_total_conf; ?></span>% This month</span>
                    </div>
                </div>
            </div>
            
            <!-- CHARGE BACK -->
            <div class="col-md-3 col-sm-12 slideInUp animated">
                <div class="info-box bg-red supportBox hvr-overline-reveal">
                    <span class="depositSinceLogin" ><?php echo number_format(round($finance->last_login->total_chargebacks,1)); ?>$ Since last login</span>
                    <span class="info-box-icon"><i class="ion ion-backspace-outline" style="line-height: 2"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Charge Back</span>
                        <span class="info-box-number"><?php 
                        echo number_format($finance->current_month->all_chargeback)." USD";
                        if($finance->current_month->all_chargeback <= 0 || $finance->current_month->all_deposits == 0)
                        { 
                            echo ""; 
                            $par = 0;
                            } else { 
                                $par = round(($finance->current_month->all_chargeback* -1) / $finance->current_month->all_deposits * 100, 1);
                            } 

                    ?>
                        </span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo $par ?>%"></div>
                        </div>
                        <span class="progress-description">
                    <?php echo $par ?>% Of all deposit's
                        </span>
                        <hr>
                        <span class="info-box-number"><?php
                            echo $finance->current_month->total_chargeback_count;
                            if($finance->current_month->total_chargeback_count == 0 || $finance->current_month->total_deposit_count == 0)
                            {
                                echo "";
                                $par = 0;
                            } else {
                                $par = round(($finance->current_month->total_chargeback_count) / $finance->current_month->total_deposit_count * 100,1);
                            }
                            ?>
                        </span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo $par ?>%"></div>
                        </div>
                        <span class="progress-description">
                            <?php echo $par ?>% Of all Deposit's
                        </span>
                    </div>
                </div>
            </div>
            <!-- WITHDRAWALS -->
            <div class="col-md-3 col-sm-12 slideInUp animated">
                <div class="info-box bg-orange supportBox hvr-overline-reveal">
                    <span class="depositSinceLogin" ><?php echo number_format(round($finance->last_login->total_withdrawals,1)); ?>$ Since last login</span>
                    <span class="info-box-icon"><i class="ion ion-arrow-graph-down-right" style="line-height: 2"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Withdrawals</span>
                        <span class="info-box-number">
                            <span id="all_withdrawals"><?php
                                echo number_format($finance->current_month->all_withdrawals); ?></span> USD
                            <?php
                                            if($finance->current_month->all_withdrawals <= 0 || $finance->current_month->all_deposits == 0) {
                                                echo "";
                                            } else {
                                                $par = round(($finance->current_month->all_withdrawals * -1) / $finance->current_month->all_deposits * 100,1);
                                            }
                        ?>
                        </span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo $finance->current_month->par_withdrawals_conf ?>%"></div>
                        </div>
                        <span class="progress-description">
                            <span id="par_withdrawals_conf">

                                <?php echo $finance->current_month->par_withdrawals_conf ?></span>% Of all deposit's
                        </span>
                        <hr>
                        <span class="info-box-number"><span id="total_withdrawals_count" ><?php
                            echo $finance->current_month->total_withdrawals_count;
                            if($finance->current_month->total_withdrawals_count == 0 || $finance->current_month->total_deposit_count == 0)
                            {
                                echo "";
                                $par = 0;
                            } else {
                                $par = round($finance->current_month->total_withdrawals_count / $finance->current_month->total_deposit_count * 100,1);
                            }
                            ?>
                        </span></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo $par ?>%"></div>
                        </div>
                        <span class="progress-description">
                            <?php echo $par ?>% Of all deposit's
                        </span>
                    </div>
                </div>
            </div>

        </div><!-- /.row -->

<div class="row  mt"></div>
