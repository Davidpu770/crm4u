<?php
namespace Crm4u\Import;

use Crm4u\Controller\FormController;
use Crm4u\Middleware\Fetch;
use Crm4u\SQL\filter;

class brand extends filter {
    
    /**
    * this is the corrent brand details including color and etc.
     * @param brand_id (int) = the brand unique id
     * @param period (string) = period of time to calculate
    */

    public $brand_format = array();

    static function import_finance($brand_id, $period = 'month')
    {
        global $loader;

        $filter = new filter();

        $columns = array(
            filter::currencyFilter(array('all','confirm','with_cb','risk')),
            filter::countFTD(),
            filter::countType('Deposit'),
            filter::countType('withdrawal'),
            filter::countType('Chargeback'),
            filter::countConfirm()
        );

        $parameters = array(
            "WHERE"  => "brand = ".$brand_id,
            $filter->userFilter(1),
            $filter->ftdFilter(0),
            $filter->paymentFilter(0),
            $filter->statusFilter(0)
        );

        $parameters_extra = array(
            $filter->dateFilter(1, $period)
        );

        $current_month = new Fetch('goal', $columns, array_merge($parameters, $parameters_extra));
        $all_time      = new Fetch('goal', $columns, $parameters);


        $parameters_extra = array(
            'AND' => 'goal.date > '.FormController::convertValuesToString(user::last_login())
        );

        $last_login    = new Fetch('goal', $columns, array_merge($parameters, $parameters_extra));

        $finance  = new \stdClass();

        $finance->current_month               = new \stdClass();
        $finance->all_time                    = new \stdClass();
        $finance->last_login                  = new \stdClass();


        $finance->current_month->all_deposits              = $current_month->result->USD             + $current_month->result->EUR               + $current_month->result->GBP                + $current_month->result->BTC;
        $finance->current_month->all_withdrawals           = $current_month->result->USDWithdrawals  + $current_month->result->EURWithdrawals    + $current_month->result->GBPWithdrawals     + $current_month->result->BTCWithdrawals;;
        $finance->current_month->all_chargeback            = $current_month->result->USDchargeBack   + $current_month->result->EURchargeBack     + $current_month->result->GBPchargeBack      + $current_month->result->BTCchargeBack;

        $finance->current_month->deposit_summery           = $finance->current_month->all_deposits   + ($finance->current_month->all_withdrawals + $finance->current_month->all_chargeback);
        $finance->current_month->total_conf                = $current_month->result->confUSDdeposits + $current_month->result->confEURdeposits   + $current_month->result->confGBPdeposits    + $current_month->result->confBTCdeposits;
        $finance->current_month->all_ftd                   = 0;
        $finance->current_month->total_unconf              = $finance->current_month->all_deposits - $finance->current_month->total_conf;

        $finance->current_month->total_deposit_count       = (int) $current_month->result->countDeposit;
        $finance->current_month->total_confirm_count       = (int) $current_month->result->countConfirmFull;
        $finance->current_month->total_withdrawals_count   = (int) $current_month->result->countwithdrawal;
        $finance->current_month->total_chargeback_count    = (int) $current_month->result->countChargeback;
        $finance->current_month->total_ftd                 = (int) $current_month->result->countftd;


        $finance->current_month->par_conf                  = ($finance->current_month->total_conf <= 0)          ? 0 : round($finance->current_month->total_conf / $finance->current_month->deposit_summery * 100,1);
        $finance->current_month->par_total_conf            = ($finance->current_month->total_confirm_count <= 0) ? 0 : round($finance->current_month->total_confirm_count / $finance->current_month->total_deposit_count * 100,1);
        $finance->current_month->par_withdrawals_conf      = ($finance->current_month->all_withdrawals >= 0)     ? 0 : round($finance->current_month->all_withdrawals / $finance->current_month->all_deposits * -100,1);
        /**
         * all time
         */
        $finance->all_time->total_deposits                 = $all_time->result->USD             + $all_time->result->EUR                + $all_time->result->GBP                  + $all_time->result->BTC;
        $finance->all_time->total_withdrawals              = $all_time->result->USDWithdrawals  + $all_time->result->EURWithdrawals     + $all_time->result->GBPWithdrawals       + $all_time->result->BTCWithdrawals;
        $finance->all_time->total_chargeback               = $all_time->result->USDchargeBack   + $all_time->result->EURchargeBack      + $all_time->result->GBPchargeBack        + $all_time->result->BTCchargeBack;

        $finance->current_month->total_high_risk           = $all_time->result->high_USD        + $all_time->result->high_EUR           + $all_time->result->high_GBP             + $all_time->result->high_BTC;

        $finance->all_time->all_deposit_summery            = $finance->all_time->total_deposits + $finance->all_time->total_withdrawals + $finance->all_time->total_chargeback;

        $finance->all_time->total_deposit_count            = (int) $all_time->result->countDeposit;
        $finance->all_time->ave_income                     = $finance->all_time->total_deposits / $loader->user->exist_period;


        /**
         * last login
         */
        $finance->last_login->total_deposits               = $last_login->result->USD             + $last_login->result->EUR             + $last_login->result->GBP;
        $finance->last_login->total_conf                   = $last_login->result->confUSDdeposits + $last_login->result->confEURdeposits + $last_login->result->confGBPdeposits;
        $finance->last_login->total_withdrawals            = $last_login->result->USDWithdrawals  + $last_login->result->EURWithdrawals  + $last_login->result->GBPWithdrawals;
        $finance->last_login->total_chargebacks            = $last_login->result->USDchargeBack   + $last_login->result->EURchargeBack   + $last_login->result->GBPchargeBack;

        return $finance;
    }
    
    function getMainBrand() {} 
}