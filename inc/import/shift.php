<?php
namespace Crm4u\Import;

class shift{
    
    public $shifts;

    function __construct($type = 0)
    {
        self::import($type);
    }

    /**
    * this function  require $type
    * can be - 0 for get an array of shift or html type like option
    */
    function import($type){
        
        global $loader;
        
        $sql = "SELECT shifts.shift_name, shifts.shift_display_name FROM shifts";
        $result = $loader->mysqli->query($sql);
        
        
        
        //set
        $this->shifts['shift_num'] = 0;


        while($row = $result->fetch_assoc()){
                
                $this->shifts['shifts'][] = array("shift_name" => $row['shift_name'],
                                                  "shift_display_name" => $row['shift_display_name']);
                $this->shifts['shift_num']++;

        }
        return $this->shifts;
    }
}