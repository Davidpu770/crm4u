<?php
namespace Crm4u\Import;

class risk{
    
    public $risks;
    
    /**
    * this function  require $type
    * can be - 0 for get an array of shift or html type like option
    */
    function import($type){
        
        global $loader;
        
        $sql = "SELECT risk.risk_name, risk.risk_color, risk.risk_display_name FROM risk";
        
        $result = $loader->mysqli->query($sql);
        
        
        
        //set
        $this->risks['risk_num'] = 0;


        while($row = $result->fetch_assoc()){
                
                $this->risks['risks'][] = array("risk_name"    => $row['risk_name'],
                                                "display_name" => $row['risk_display_name'],
                                                "risk_color"   => $row['risk_color']);
                $this->risks['risk_num']++;
        }
        return $this->risks;
    }
}