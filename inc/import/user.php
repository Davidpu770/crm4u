<?php

namespace Crm4u\Import;

use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Controller\SwalController;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Middleware\Fetch;
use Crm4u\Controller\new_user;
use Crm4u\SQL\filter;


class user {

    function __construct($user_id = 0){

        $this->import_user($user_id);
    }

    static function import_user($user_id){

        global $loader;

        $user = new Fetch('users',array('*'),array("WHERE" => "user_key_id = ".$user_id,"LIMIT" => 1));
        $user = $user->result;
        //change valuee

        //additions
        $user->name = $user->firstname. " ". $user->lastname;
        $user->exist_period = self::MonthSumSinceUserExist($user->created, FormController::getDate());

        return $user;
    }

    static function import_finance($user_id, $only_me = true)
    {
        global $loader;

        $columns = array(
            filter::currencyFilter(array('all','confirm','with_cb')),
            filter::countFTD()
            );

        if(!$only_me && CheckAccess::isAuthorized(2)){


        } else {

            $parameters = array(
                "WHERE"  => "emp_id = ".$user_id,
            );
        }


        $parameters_extra = array(
            "AND"    => "month = '".FormController::getMonth()."'",
            "AND "   => "year =  '".FormController::getYear()."'"
        );


        $current_month = new Fetch('goal', $columns, array_merge($parameters, $parameters_extra));
        $all_time      = new Fetch('goal', $columns, $parameters);

        $finance  = new \stdClass();

        $finance->current_month               = new \stdClass();
        $finance->all_time                    = new \stdClass();

        $finance->current_month->all_deposits              = $current_month->result->USD             + $current_month->result->EUR               + $current_month->result->GBP                + $current_month->result->BTC;
        $finance->current_month->total_conf                = $current_month->result->confUSDdeposits + $current_month->result->confEURdeposits   + $current_month->result->confGBPdeposits    + $current_month->result->confBTCdeposits;
        $finance->current_month->total_ftd                 = $current_month->result->countftd;
        $finance->current_month->all_withdrawals           = $current_month->result->USDWithdrawals  + $current_month->result->EURWithdrawals    + $current_month->result->GBPWithdrawals     + $current_month->result->BTCWithdrawals;;


        $finance->all_time->total_deposits    = $all_time->result->USD + $all_time->result->EUR + $all_time->result->GBP;


        $finance->all_time->ave_income        = (isset($loader->user)) ? $finance->all_time->total_deposits / $loader->user->exist_period : false;

        return $finance;
    }

    static function current_user(){

        return (CheckAccess::isOnline()) ? $_SESSION['user_id'] : false;
    }

    static function last_login(){

        return (CheckAccess::isOnline() ? $_SESSION['last_login'] : false);
    }

    static function ConvertID($id){

        $result = ImportController::import('users',array('CONCAT(firstname," ",lastname) name'),array('WHERE' => "user_key_id = {$id}"));

        if(!$result){

            return "User not found";
        } else {

            return $result['data'][0]['name'];
        }
    }

    static function MonthSumSinceUserExist($registerd_date, $today){

        $num = 0;

        $registerd_date = new \DateTime($registerd_date);
        $today = new \DateTime($today);
        $today->modify('+1 month');

        $interval = \DateInterval::createFromDateString('1 month');

        $period = new \DatePeriod($registerd_date, $interval, $today);


        return iterator_count($period);
    }

    static function getEmail($uid){

        $result = ImportController::import('users',array('email'),array('WHERE' => "user_key_id = {$uid}"));

        if(!$result){

            return "User not found";

        } else {

            return $result['data'][0]['email'];
        }
    }

    static function brandList($user_id){

        return ImportController::import(
            'users',
            array('brands.brand_name'),
            array(
            'LEFT JOIN' => 'brands',
            'ON'        => 'users.brand = brands.id',
            'WHERE' => 'user_key_id = '.$user_id)
        );
    }

    static function userExist($ukid){

        $result = ImportController::import('users',array('COUNT(user_key_id) exist'),array('WHERE' => 'user_key_id = '.$ukid));

        if($result['data'][0]['exist'] == 1) {

            return true;
        }
    }
}