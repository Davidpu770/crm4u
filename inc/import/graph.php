<?php
namespace Crm4u\Import;

use Crm4u\Deposits\charts;

class graph{
    
    public $graphs;
    
    /**
    * this function  require $type
    * can be - 0 for get an array of shift or html type like option
    */
    function import($type){
        
        global $loader;
        
        $sql = "SELECT * FROM graph";
        
        $sql .= " ORDER BY graph.graph_sort_id";
        
        $result = $loader->mysqli->query($sql);

        //set
        $this->graphs['graph_num'] = 0;


        while($row = $result->fetch_assoc()){
                
                $this->graphs['graphs'][$row['graph_id']] = array("graph_id"            => $row['graph_id'],
                                                  "graph_display_name"  => $row['graph_name'],
                                                  "graph_color"         => $row['graph_box_color'],
                                                  "graph_size"          => $row['graph_size'],
                                                  "graph_mobile_size"   => $row['graph_mobile_size'],
                                                  "graph_filter"        => $row['graph_filter'],
                                                  "graph_location"      => $row['graph_location'],
                                                  "graph_function_name" => $row['graph_function_name'],
                                                  "graph_arg"           => $row['graph_args'],
                                                  "graph_priv"          => $row['graph_priv'],
                                                  "graph_inner_size"    => "500",
                                                  "graph_addition_name" => ""
                                                 );
                $this->graphs['graph_num']++;
        }
        return $this->graphs;
    }
    
    function getGraph($type, $graphs, $html = ''){

        global $loader;
        $chart = new charts();
        
        

        foreach($graphs['graphs'] as $key => $graph){

            if($graph['graph_priv'] <= $loader->user->priv){

                $chart->chartResult = array();

                if(!is_null($graph['graph_filter']) && $graph['graph_filter'] == $type){

                    if(!is_array($chart)) { $chart->{$graph['graph_function_name']}($graph['graph_arg']); }

                    $end = '';
                    if(!isset($graph['no-col'])){

                        $html .= "<div class='col-md-".$graph['graph_size']." col-sm-".$graph['graph_mobile_size']."'>";

                        $end = '</div>';
                    }

                    $html .= "<div class='box box-".$graph['graph_color']." slideInUp  animated'>";
                    $html .= "<div class='box-header with-border'>";
                    $html .= "<h3 class='box-title'>";
                    $html .= "<i class='fa fa-bar-chart-o fa-fw'></i> ".$graph['graph_display_name'].$graph['graph_addition_name']."</h3>";

                    $html .= "<div class='box-tools pull-right'>";
                    $html .= "<button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-plus'></i>";
                    $html .= "</button></div></div>";
                    $html .= "<div class='box-body'>";
                    $html .= "<div id='".$graph['graph_id']."' style='min-width: 280px; height: ".$graph['graph_inner_size']."px; margin: 0 auto'>";

                    $html .= "</div></div></div>";

                    $html .= "$end \n";

                    print(nl2br($html));

                    include dirname(__DIR__)."/graph/".$graph['graph_location'];
                    $html = '';
                }

            }
            

        }
        
        
    }
}