<?php
namespace Crm4u\Forms\search;

use Crm4u\Controller\FormController;
use Crm4u\Import\risk;
use Crm4u\Import\shift;
use Crm4u\Controller\Route;

global $loader;

$filter = $_loadArgs;

if(isset($filter['filters']) && is_array($filter['filters'])) {

    $form = new FormController();


    function div_filters($filter)
    {

        $dfilters = array(
            'risk',
            'shift',
            'payment',
            'user',
            'country',
            'department'
        );
        $i = 0;
        foreach ($filter['filters'] as $value) {

            if (in_array($value, $dfilters)) {

                $i++;
            }

        }
        return $i;
    }

    $col_md = div_filters($filter);

    $col_md = 12 / $col_md;
    ?>

    <div class="col-md-12">

        <div class="box box-primary <?php echo $filter['option']['collapse'] ?>">
            <div class="box-header with-border">
                <h3 class="box-title">Filter Box</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div><!-- /.box-tools -->
            </div>
            <div class="box-body">
                <div class="well well-sm" style="overflow: hidden;">
                    <form id="filter_form">
                        <div class="col-md-12 col-xs-12">
                            <?php if (in_array('tran_cid', $filter['filters'])) { ?>
                                <div class="col-md-6 col-sm-12">
                                    <label>By Tran id or customer id:</label>
                                    <div>
                                        <div class="form-group">
                                            <input class="form-control" type="search" name="text" id="search"
                                                   placeholder="start to type...">
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">

                                            <div class="radio">
                                                <label class="search-choice">
                                                    <input type="radio" class="type" name="type" id="optionsRadios1"
                                                           value="cid" checked="">
                                                    Customer #
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label class="search-choice">
                                                    <input type="radio" class="type" name="type" id="optionsRadios2"
                                                           value="tran_id" checked="">
                                                    Transaction #
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            <?php } ?>

                            <?php if (in_array('date', $filter['filters'])) { ?>
                                <div class="col-md-6 col-sm-12">
                                    <label>By date:</label>
                                    <div class="form-group">
                                        <div id="reportrange" class="pull-right"
                                             style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                            <span></span> <b class="caret"></b>
                                        </div>
                                        <input type="hidden" id="startdate" name="startdate" value="">
                                        <input type="hidden" id="enddate" name="enddate" value="">
                                        </br>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div>
                                <?php if (in_array('user', $filter['filters'])) { ?>

                                    <div class="col-md-<?php echo $col_md ?> col-sm-12">
                                        <label>By Employee name:</label>
                                        <div class="form-group">
                                            <select class="form-control smart-select" id="emp_select" name="emp_id">
                                                <option value="no-select"></option>
                                                \n
                                                <?php
                                                Route::get('select', 'Models', 'users', array('user'));
                                                ?>
                                            </select>
                                        </div>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $('.smart-select').select2();
                                            });

                                        </script>
                                    </div>
                                <?php } ?>

                                <?php if (in_array('payment', $filter['filters'])) { ?>
                                    <div class="col-md-<?php echo $col_md ?> col-sm-12">
                                        <label>By Payment Method:</label>
                                        <div class="form-group">
                                            <select class="form-control" id="payment_search" name="payment">
                                                <option value="">Payment Method</option>
                                                <option value="credit card">Credit Card</option>
                                                <option value="wire">Wire</option>
                                                <option value="paypal">PayPal</option>
                                                <option value="cashu">cashU</option>
                                                <option value="webmoney">Webmoney</option>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php if (in_array('desk', $filter['filters'])) { ?>

                                    <div class="col-md-<?php echo $col_md ?> col-sm-12">
                                        <label>By Desk:</label>
                                        <div class="form-group">
                                            <select class="form-control" id="desk" name="desk">
                                                <option></option>
                                                <?php Route::get('select', 'Models', 'desks', array('desk')); ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>


                                <?php if (in_array('shift', $filter['filters'])) { ?>

                                    <div class="col-md-<?php echo $col_md ?> col-sm-12">
                                        <label>By Shift:</label>
                                        <div class="form-group">
                                            <select class="form-control" id="shift" name="shift">
                                                <option></option>
                                                <?php

                                                $shift = new shift();
                                                print $form->selected_option($shift->shifts['shifts'], 'shift_name');
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php if (in_array('risk', $filter['filters'])) { ?>
                                    <div class="col-md-<?php echo $col_md ?> col-sm-12">
                                        <label>By Risk Status:</label>
                                        <div class="form-group">
                                            <select class="form-control" id="risk" name="risk">
                                                <option></option>
                                                <?php
                                                //risk list import
                                                $risk = new risk();
                                                $risk->import(0);
                                                print $form->selected_option($risk->risks['risks'], 'risk_name');

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php if (in_array('country', $filter['filters'])) { ?>
                                    <div class="col-md-<?php echo $col_md ?> col-sm-12">
                                        <label>By country:</label>
                                        <div class="form-group">
                                            <select class="form-control" id="country" name="country">
                                                <option value="">country</option>
                                                <?php

                                                Route::get('select', 'Models', 'countries', array('country'));
                                                //                                     $country = new country();
                                                //                                     $country->import(0);
                                                //                                     print $form->selected_option($country->countries['countries'],'country_name');

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php if (in_array('verification', $filter['filters'])) { ?>
                                    <div class="col-md-12 col-sm-12">
                                        <label>By Verification status:</label>
                                        <div class="form-group center-filter">
                                            <div class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-default filter-admin-btn">
                                                    <input type="checkbox" autocomplete="off" value="true" class="filter" name="full"
                                                           id="filter-full"> Full
                                                </label>
                                                <label class="btn btn-default filter-admin-btn">
                                                    <input type="checkbox" autocomplete="off" value="true" name="partial"
                                                           class="filter" id="filter-partial"> Partial
                                                </label>
                                                <label class="btn btn-default filter-admin-btn">
                                                    <input type="checkbox" autocomplete="off" value="true" name="none"
                                                           class="filter" id="filter-none">None
                                                </label>
                                                <label class="btn btn-default filter-admin-btn">
                                                    <input type="checkbox" autocomplete="off" value="ftd" name="only_ftd" id="is_ftd">
                                                    Only FTD
                                                </label>
                                            </div>
                                            <?php if (in_array('ftd', $filter['filters'])) { ?>
                                                <div class="btn-group" data-toggle="buttons">

                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>

                <?php if (!in_array('button', $filter['option'])) { ?>
                    <div>
                        <div class="form-group">
                            <button type="button" id="searchBTN" class="btn btn-info">Filter</button>
                            <button type="button" id="resetFilter" class="btn btn-danger" style="display: none">Reset filter</button>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-md-12">
                    <?php
                    function getFilterList($get)
                    {

                        if (isset($_GET['search'])) {

                            unset($get['search']);
                            unset($get['filter']);
                            unset($get['type']);

                            $status = array('full', 'partial', 'none');

                            $filters = '<b>Filters Enabled: </b>';

                            foreach ($get as $key => $value) {

                                $key = (in_array($key, $status)) ? "Verified" : $key;
                                $value = ($value == 1 || $value == "true") ? "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>" : $value;


                                $filters .= "<span class='filter-label label label-info'>" . $key . ": " . $value . "</span>\n\n";
                            }


                            return $filters;
                        }
                    }

                    echo getFilterList($_GET);
                    ?>
                </div>
            </div>

        </div>

    </div>


    <script type="text/javascript">
        $(function () {

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            cb(moment().subtract(29, 'days'), moment());

            $('#reportrange').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
                }
            }, cb);
            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                $startdate = picker.startDate.format('YYYY-MM-DD');
                $enddate = picker.endDate.format('YYYY-MM-DD');

                $("#startdate").val($startdate);
                $("#enddate").val($enddate);
            });
        });


    </script>
    <?php } ?>