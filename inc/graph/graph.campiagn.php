<?php 
include 'inc/graph/graph.format.php';

if(!is_array($chart)) { $chart->campiagnMonitor(); }


?>   
<div class="box box-info"><!-- .collapsed-box for make it close By default-->
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="fa fa-bar-chart-o fa-fw"></i> campiagn Explain by amount
                </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div><!-- /.box-tools -->
                        </div>
                            <div class="box-body">
                <div id="explaincampiagn" style="min-width: 90%; height: 400px; margin: 0 auto"></div>
                            </div>
                        </div>
<!--
<script>$

(function () {
    // Build the chart
    $('#explaincampiagn').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Amount',
            data: [
                <?php foreach($chart->chartResult as $key => $value) { 
        
                echo "{ name: '".$value['campaign_name']."(".$value['amount']."$)', y: ".$value['amount']." },";
} ?>
            ]
            
        }]
    });
});
</script>
-->
<script>
$(function () {
    $('#explaincampiagn').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: [{
            categories: [<?php foreach($chart->chartResult as $key => $value) { 
        
                echo "'".$value['campaign_name']."',";
} 
                ?>], 
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: null,
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'FTD From Campaign',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Deposit From Campaign',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} $',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 120,
            verticalAlign: 'top',
            y: 100,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Deposit ',
            type: 'column',
            yAxis: 1,
            data: [<?php 
                    foreach($chart->chartResult as $key => $count){
                        
                        print round($count['amount'],1).",";
                    }
                ?>], 
            tooltip: { 
                valueSuffix: '$'
            }

        }, {
            name: 'FTD ',
            type: 'line',
            data: [<?php 
                    $i = 0;
                    foreach($chart->chartResult as $key => $count){
                        
                        print $count['ftd'].",";
                        $i++;
                    }
                ?>],
            tooltip: {
                valueSuffix: null
            }
        }]});
});

</script>