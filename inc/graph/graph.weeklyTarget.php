<?php 
include_once dirname(__DIR__).'/graph/graph.format.php';

if(!is_array($chart)) { $chart->weeklyTarget(); }


?>   
<div class="col-md-12">
<ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Average Amount per Deposit</b> <a class="pull-right"><?php echo $chart->chartResult['average_amount'] ?> USD</a>
                </li>
                </li>
                <li class="list-group-item">
                  <b>Average Deposit Count Per a month</b> <a class="pull-right"><?php echo $chart->chartResult['average_countPM']?></a>
                </li>
                <li class="list-group-item">
                  <b>Average Deposit Count Per a week</b> <a class="pull-right"><?php echo $chart->chartResult['average_countPW']?></a>
                </li>
              </ul>
</div>
