
<script>$

(function () {
    // Build the chart
    $('#<?php echo $graph['graph_id'] ?>').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        colors: [
            '#67BCDB',
            '#78ab58',
            '#ca605d',
            '#9b7ed4',
            '#bf5076',
            '#aebf50',
            '#6eaac1',
            '#aeacb1',
            '#57a5ad',
            '#53a06b'],
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                }
            }
        },
        series: [{
            name: 'Amount',
            data: [
                <?php foreach($chart->chartResult as $key => $value) { 
        
                echo "{ name: '".$value['name']."', y: ".$value['percentage_amount']." },";
} ?>
            ]
            
        }]
    });
});
</script>
