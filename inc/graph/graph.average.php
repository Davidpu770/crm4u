
       <div class="box box-warning"><!-- .collapsed-box for make it close By default-->
            <div class="box-header with-border">
        <h3 class="box-title">
            <i class="fa fa-bar-chart-o fa-fw"></i> Average Deposit Per day
        </h3>
    <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
        </button>
    </div><!-- /.box-tools -->
        </div>
            <div class="box-body">
                <div id="average_deposit" style="min-width: 90%; height: 400px; margin: 0 auto"></div>
            </div>
    </div>
<script>
$(function () {
    $('#average_deposit').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rainfall (mm)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'London',
            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

        }]
    });
});
</script>