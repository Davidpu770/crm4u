
<script>   
$(function () {
    $('#<?php echo $graph['graph_id'] ?>').highcharts({
        title: {
            text: ''
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true, 
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: { 
            categories: [<?php

                if(isset($chart->chartResult['data'])){
                    foreach($chart->chartResult['data'] as $key => $value){
                        print "'" . $value['month'] . "',";
                    }
                }
            ?>]},
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value} $',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Deposit Amount',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'FTD Count',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format:null,
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
            valueSuffix: ' $'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            area: {
                fillOpacity: 0.5
                }
            
        },
        series: [ {
            name: 'Deposit',
            type: 'area',
            yAxis: 0,
            data: [<?php
                    if(isset($chart->chartResult['data'])) {
                        foreach($chart->chartResult['data'] as $key => $value){

                            print $value['amount'].",";
                        }
                    }

            ?>]
        }, {
            name: 'Confirm Deposit',
            type: 'area',
            yAxis: 0,
            data: [<?php
                if(isset($chart->chartResult['data'])) {
                    foreach ($chart->chartResult['data'] as $key => $value) {

                        print $value['confirm_amount'] . ",";
                    }
                }
            ?>]
        }, {
            name: 'FTD',
            type: 'line',
            yAxis: 1,
            data: [<?php
                if(isset($chart->chartResult['data'])) {
                    foreach ($chart->chartResult['data'] as $key => $value) {

                        print $value['ftd'] . ",";
                    }
                }
            ?>],
            tooltip: {
                valueSuffix: null
            }
        }, {
            name: 'Target',
            type: 'spline',
            marker: {
                enabled: false
            },
            yAxis: 1,
            data: [<?php
                if(isset($chart->chartResult['data'])) {
                    foreach ($chart->chartResult['data'] as $key => $value) {

                        print $value['target'] . ",";
                    }
                }
            ?>],
            tooltip: {
                valueSuffix: null
            }
        }, {
            name: 'Customer Count',
            type: 'line',
            yAxis: 1,
            data: [<?php
                if(isset($chart->chartResult['data'])) {
                    foreach ($chart->chartResult['data'] as $key => $value) {

                        print $value['countCustomers'] . ",";
                    }
                }
                ?>],
            tooltip: {
                valueSuffix: null
            }
        } ]
    });
});
</script>