<?php 
include_once dirname(__DIR__).'/graph/graph.format.php';

if(!is_array($chart)) { $chart->weekFilter(); }

?>
       <div class="box box-warning"><!-- .collapsed-box for make it close By default-->
            <div class="box-header with-border">
        <h3 class="box-title">
            <i class="fa fa-bar-chart-o fa-fw"></i> Deposit By Day
        </h3>
    <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
        </button>
    </div><!-- /.box-tools -->
        </div>
            <div class="box-body">
<div id="deposit_byweek" style="min-width: 90%; height: 400px; margin: 0 auto"></div>
            </div>
    </div>

<script>
$("document").ready(function(){ 
    $('#deposit_byweek').highcharts({
        chart: {
            type: 'line',
            zoomType: 'x'
        }, 
        title: {
            text: ''
        },
        yAxis: {
            title: {
                text: 'DOLLARS($)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        xAxis: {
            categories: [<?php 
                $i = 0;
                foreach($chart->chartResult['date'] as $key => $date){
                    
                    print "'".$date."',";
                    $i++;
                }   
            ?>]
        },
        legend: {
            enabled: true
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false,
                    states: {
                        hover: {
                            enabled: false,
                            radius: 3
                        }
                    }
                }
            }
        }, 
        series:[
            <?php
            $i = 1;
            foreach($chart->chartResult['brand_name'] as $key => $brand){
                
                echo "{name: '".$brand."',\r\n";
                echo "data: [";
                
                foreach($chart->chartResult['data'] as $key => $amount){
                    if($amount['brand'] == $brand){
                        echo $amount['amount'].",";
                    } else {
                        echo "0,";
                    }
                }
                
                echo "]}";
                if($i < count($chart->chartResult['brand_name'])){
                    echo ",";
                }
                echo "\r\n"; 
                $i++;
            }
            ?>
        ] 
    });
});
</script>