<script>
    
    $('#<?php echo $graph['graph_id'] ?>').highcharts({
        chart: { 
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: ""
        },
        xAxis: {
            categories: [<?php
                    foreach($chart->chartResult as $key => $value){
                        
                      echo "'$value[name]',";  
                    }

                    ?>]
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'Deposit\'\s'
            }
        }, {
            title: {
                text: 'Confirm  Deposit\'\s'
            },
            opposite: true
        }],
        legend: {
            align: 'right',
            shadow: false
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0
            }
        },
        series: [<?php if($_SESSION['priv'] > 1){?>
            {
            name: 'Target',
            color: 'rgba(165,170,217,1)',
            borderRadiusTopLeft: 4,
            borderRadiusTopRight: 4,
            data: <?php
                echo "[";
                
                foreach($chart->chartResult as $key => $value){

                    echo "$value[target],";
                }

                    echo "]";
            ?>,
                tooltip: {
                valuePrefix: '$'},
            pointPadding: 0
            
        },
            <?php } ?>{
            name: 'Deposit\'\s',
            color: 'rgba(126,86,134,.9)',
            borderRadiusTopLeft: 2,
            borderRadiusTopRight: 2,
            data: <?php
                echo "[";
                foreach($chart->chartResult as $key => $value){
                    echo "$value[amount],";
                }

                    echo "]";
            ?>,
                tooltip: {
                valuePrefix: '$'},
            pointPadding: 0.1
        },{
            name: 'Confirm Deposit',
            color: 'rgba(248,161,63,1)',
            borderRadiusTopLeft: 2,
            borderRadiusTopRight: 2,
            data : <?php
                 echo "[";
                foreach($chart->chartResult as $key => $value){
                    echo "$value[confirm_amount],";
                }
                    echo "]";   
            ?>,
                tooltip: {
                valuePrefix: '$'},
            pointPadding: 0.2
        }]
    });
</script>