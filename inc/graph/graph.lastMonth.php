
<script>

    $(function () {
        $('#<?php echo $graph['graph_id'] ?>').highcharts({
            chart: {
                type: 'column',
                height: '300'
            },
            title: {
                text: ''
            },
            xAxis: {
                type: 'category',
                lineWidth: 0,
                minorGridLineWidth: 0,
                lineColor: 'transparent',
                labels: {
                    enabled: false,
                    rotation: -45,
                    style: {
                        fontSize: '16px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                },
                minorTickLength: 0,
                tickLength: 0,
                reversed: true
            },
            yAxis: {
                min: 0,
                lineWidth: 0,
                minorGridLineWidth: 0,

                minorTickLength: 0,
                tickLength: 0,
                title: {
                    text: 'Deposits'
                },

            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Deposits: <b>{point.y:.1f} $</b>'
            },
            series: [{
                name: 'Deposits',
                color: '#67BCDB',
                 borderRadiusTopLeft: 7,
            borderRadiusTopRight: 7,
                data: [<?php 
                        $i = 1;
                        foreach($chart->chartResult as $key => $value) {            
                            if($i > 1){                    
                                echo ",";   
                            }   
                            print "['".$key."',".$value."]";     
                            $i++;
                            
                            //stop after 5 months
                            if($i > 5){
                                break;
                            }
                        }
                    ?>],
                dataLabels: {
                    enabled: true,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y} $', // one decimal
                    y: 5, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
                
            }]
        });
    });
    
</script>