<?php

include 'inc/graph/graph.format.php';

if(!is_array($chart)) { $chart->currencyChart(); }

?>

    <div class="box box-success slideInUp animated">
        <div class="box-header with-border ">
            <h3 class="box-title"><i class="fa fa-bar-chart-o fa-fw"></i> Amount By Currency</h3>
            <div class="box-tools pull-right"> 
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box-tools -->
        </div>
        <div class="box-body">
            <div id="container" style="min-width: 150px; height: 300px; margin: 0 auto">
            
            </div>
        </div>
    </div>
<script>

        $('#container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            colors: ['#67BCDB', '#78ab58', '#ca605d'],
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                type: 'pie',
                name: 'Of all Deposit',
                data: [<?php
                    $i = 1;
                    foreach($chart->chartResult as $key => $value){

                        if($i > 1){
                            echo ",";
                        }

                        echo "['".$key."',".$value."]";
                        $i++;
                    }
                    ?>]
            }]
        });
</script>