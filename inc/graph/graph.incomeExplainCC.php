
<script>$

    (function () {
        // Build the chart
        $('#<?php echo $graph['graph_id'] ?>').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'Amount',
                data: [
                    <?php foreach($chart->chartResult as $key => $value) {

                    echo "{ name: '".$value['name']."', y: ".$value['percentage_amount']." },";
                } ?>
                ]

            }]
        });
    });
</script>
