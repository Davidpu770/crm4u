
<script>
$(function () {
    $.getJSON('inc/graph/graph.depositcount.inc.php'), function(data){
        
        
    }
    $('#<?php echo $graph['graph_id'] ?>').highcharts({
        chart: {
            zoomType: 'xy',
            marginBottom: 120,
            reflow: false,
            style: {
                position: 'absolute'
            }
        },
        credit: {
            enabled: false
        }, 
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: {
                categories: [<?php
                        if(isset($chart->chartResult['data'])){
                            foreach($chart->chartResult['data'] as $key => $value){

                                echo "'$value[date]',";
                            }

                        }


                    ?>]
        },
        yAxis: [{ // Primary yAxis
            labels: {
                format: null,
                type: 'logarithmic',
                max: 200,
                
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            maxZoom: 0.1,
            title: {
                text: 'Deposit Count',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Deposit Amount',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} $',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 120,
            verticalAlign: 'top',
            y: 100,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Deposit Target',
            color: 'rgba(165,170,217,1)',
            type: 'column',
            yAxis: 1,
            data: [<?php
                if(isset($chart->chartResult['data'])) {
                    foreach ($chart->chartResult['data'] as $key => $count) {

                        print round($count['target'], 1) . ",";
                    }
                }
                ?>],
            pointPadding: 0.2,
            borderRadiusTopLeft: 4,
            borderRadiusTopRight: 4,
            tooltip: { 
                valueSuffix: '$'
            }

        },{
            name: 'Deposit Amount',
            type: 'column',
            yAxis: 1,
            data: [<?php
                if(isset($chart->chartResult['data'])){
                    foreach($chart->chartResult['data'] as $key => $count){
                        
                        if($count['amount'] < 0){
                            
                            print "'less then zero',";
                        } else {
                            print round($count['amount'],1).",";
                        }
                        
                    }
                }
                ?>], 
            pointPadding: 0.24,
            borderRadiusTopLeft: 4,
            borderRadiusTopRight: 4,
            tooltip: { 
                valueSuffix: '$'
            }

        }, {
            name: 'Fully status',
            type: 'column',
            yAxis: 1,
            data: [<?php
                if(isset($chart->chartResult['data'])) {
                    foreach ($chart->chartResult['data'] as $key => $count) {

                        if ($count['status'] < 0) {

                            print "'less then zero',";
                        } else {
                            print round($count['status'], 1) . ",";
                        }

                    }
                }
                ?>], 
            pointPadding: 0.35,
             
            borderRadiusTopLeft: 4,
            borderRadiusTopRight: 4,
            tooltip: { 
                valueSuffix: '$'
            }

        }, {
            name: 'Deposit count target',
            type: 'spline',
            marker: {
                enabled: false
            },
            yAxis: 0,
            data: [<?php
                if(isset($chart->chartResult['data'])) {
                    foreach ($chart->chartResult['data'] as $key => $count) {

                        print round($count['countTarget'], 1) . ",";
                    }
                }
                ?>], 
            tooltip: { 
                valueSuffix: null
            }

        }, {
            name: 'Deposit Count',
            type: 'line',
            yAxis: 0,
            data: [<?php
                if(isset($chart->chartResult['data'])) {
                    $i = 0;
                    foreach ($chart->chartResult['data'] as $key => $count) {

                        print $count['count'] . ",";
                        $i++;
                    }
                }
                ?>],
            tooltip: {
                valueSuffix: null
            }
        }]});
});

</script>