<?php

namespace Crm4u\Mod\Shift;

class Box {
}
?>


    <div class="col-md-4 col-sm-12 slideInUp animated">

        <div class="info-box bg-blue supportBox">
            <span class="info-box-icon supportBox"><i class="ion ion-person"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Online Users</span>
                <span class="info-box-number" ><span id="online_users">0</span></span>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-12 slideInUp animated">

        <div class="info-box bg-green supportBox">
            <span class="info-box-icon supportBox"><i class="ion ion-ios-calculator-outline"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Total Shifts</span>
                <span class="info-box-number" ><span id="total_shifts">0</span></span>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-12 slideInUp animated">

        <div class="info-box bg-red supportBox">
            <span class="info-box-icon supportBox"><i class="ion ion-ios-time-outline"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Total Hours</span>
                <span class="info-box-number" ><span id="total_hours">0</span></span>
            </div>
        </div>
    </div>