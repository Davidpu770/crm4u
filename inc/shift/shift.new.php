<?php

namespace Crm4u\Mod\Shift\NewShift;

use Crm4u\Controller\Route;

?>
<div class="modal fade" id="new-shift" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New shift</h4>
            </div>
            <form method="post" id="site_config_form">
                <input type="hidden" value="insert" id="form_type">
                <div class="box-body">
                    <div class="form-group">
                        <label for="emp_name">Employee Name: </label></br>
                        <div class="col-md-12 fix-select">
                            <select class="form-control smart-select" id="emp_id" name="emp_id">
                                <option value="no-select"></option>
                                <?php
                                Route::get('select', 'Models', 'users', array('user'));
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="date">started: </label>
                        <div class="input-group">
                            <input type="text" id="startdate" name="startdate" class="form-control" value="">
                            <label class="input-group-addon btn" for="startdate">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Ended: </label>
                        <div class="input-group">
                            <input type="text" id="enddate" name="enddate" class="form-control" value="">
                            <label class="input-group-addon btn" for="enddate">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">

                    $(function () {

                        $('#startdate').datetimepicker();

                        $('#enddate').datetimepicker({
                            useCurrent: false //Important! See issue #1075
                        });
                        $("#startdate").on("dp.change", function (e) {
                            $('#enddate').data("DateTimePicker").minDate(e.date);
                        });
                        $("#enddate").on("dp.change", function (e) {
                            $('#startdate').data("DateTimePicker").maxDate(e.date);
                        });
                    });
                </script>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" id="submit_config" name="submit_config">Add Shift</button>
                </div>
            </form>

        </div>
    </div>
</div>
