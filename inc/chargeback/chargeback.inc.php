<?php
namespace Crm4u\ChargeBack;


use Crm4u\Controller\checks;
use Crm4u\Controller\ImportController;
use Crm4u\Controller\notification;
use Crm4u\Controller\SwalController;
use Crm4u\Controller\Route;
use Crm4u\SQL\filter;


class ChargeBack extends filter {
    
    //assign
    public $statistic = array();
    
    //-->cunstruct
    function __construct(){

        parent::__construct();
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
           && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $this->ajaxHandler();
        }
        
    }
    
    //-->new chargeback
    function insert(){
        
        global $loader;
        
        //assign checks class
        $checks = new checks();
        
            //-->1.empty field
            if(in_array("",$_POST)){
                
                
                new SwalController("Empty field","One or more field are empty","error");

            } 
            
            //-->2.undefined employee
            if($checks->employeeCheck()){
                
                new SwalController("No employee selected","You need to select an employee first","error");
            }
            
            //-->3.no currency
            if($checks->currencyCheck()){
                
                new SwalController("No currency select","You need to select currency first","error");
            }
        
        //turn $_POST to an array.
        foreach ($_POST as $key => $value) {
                
            $data[$key] = ("'".$value."'");
                
        } 
        
        //unset do post
        unset($data['do']);
        unset($_POST['do']);
        
        //date and time:
        $date = date('Y-m-d H:i:s');
        
        //user who inserted the row
        session_start();
        $update = $_SESSION['firstname']." ".$_SESSION['lastname'];
        
        //tran id - ?
        $tran_id = $data['tran_id'];
        
        //make $data as string
        $data = implode(",",$data);
        
        //month of deposit
        $month = date('M');
        $year = date('y');
        
        //brand
        $brand = $_SESSION['brand'];
        
        
        $sql = "INSERT INTO chargeback";
        
        $sql .= "(";
        $sql .= $colmun = "type, customer_firstname, customer_lastname, firstname, lastname, tran_id, payment_method, clearedby, customer_id, currency, amount, date, update_by, month, year, brand";
        
        $sql .= ")";
        
        $sql .= " VALUES ('chargeback', $data, '$date', '$update', '$month', '$year', '$brand')";
        
        //inject query
        $loader->mysqli->query($sql);
        
        
        //put it on goal
        $this->insertIntoGoal('Charge Back','full', $tran_id);
        
    }
    
    function insertIntoGoal($type, $status, $tran_id){
        
        global $loader;
        
        //select it for insert to goal
        $sql = "SELECT * FROM chargeback WHERE tran_id = ".$tran_id;
        
        //fetch_assoc()
        $result = $loader->mysqli->query($sql) or die(new SwalController("You have an error","There was an error while trying to insert this chargeback","error"));
        
        $row = $result->fetch_assoc();
        
        //type and amount decleretion
        $amount = ($type == "Charge Back" ? "-".$row['amount'] : $row['amount']);
        
//        //status
//        $status = $this->status;

        //array it
        $employee_detail = array('emp_fisrtname'      => $row['firstname'],
                                 'emp_lastname'       => $row['lastname'],
                                 );
        
        $customer_detail = array('customer_firstname' => $row['customer_firstname'], 
                                 'customer_lastname'  => $row['customer_lastname'],
                                 'cid'                => $row['customer_id']
                                 );
        
        $charge_detail   = array('tran_id'            => $row['tran_id'],            
                                 'payment_method'     => $row['payment_method'], 
                                 'amount'             => $amount,
                                 'currency'           => $row['currency']
                                 );

        $other           = array('update_by'          => $_SESSION['firstname']." ".$_SESSION['lastname'],
                                 'month'              => date('M'),
                                 'year'               => date('y'),
                                 'brand'              => $row['brand'],
                                 'status'             => $status
                                 );

        $employee_detail_row = implode("','",$employee_detail);
        $customer_detail_row = implode("','",$customer_detail);
        $charge_detail_row   = implode("','",$charge_detail);
        $other_detail_row    = implode("','",$other);

        $date = date('Y-m-d H:i:s');
        
        //and insert into goal
        $sql = "INSERT INTO goal";
        
        //Colmun
        $sql .= "(customer_firstname, customer_lastname, customer_id, firstname, lastname, tran_id, payment_method, amount, currency, type, FTD, date, update_by, month, year, brand, status)";

        //value
        $sql .= " VALUES (";
        
        $sql .= "'".$customer_detail_row."',";
        $sql .= "'".$employee_detail_row."',";
        $sql .= "'".$charge_detail_row.  "',";
        
        $sql .= "'$type','0',";
        
        $sql .= "'$date',";
        
        $sql .= "'".$other_detail_row."'";
        
        $sql .= ")";
            
        
        $loader->mysqli->query($sql);
        
        /** --notification-- **/
        $update = $_SESSION['firstname']." ".$_SESSION['lastname'];

        //depositor
        $depositor = array(
            'name'      => $_POST['customer_firstname']." ".$_POST['customer_lastname'],
            'amount'    => $_POST['amount'],
            'currency'  => $_POST['currency'],
            'type'      => $_POST['type'],
            'update_by' => $update,
            'brand'     => $row['brand'],
            'tran_id'   => $_POST['tran_id']
                          );
            
        //1. title
        $title = "New ".$depositor['type']." - ".$depositor['amount']." ".$depositor['currency'];

        //2. body:
        $body  = $depositor['name']. " Has made a new ".$depositor['type'];
        $body .= " in the amount of ".$depositor['amount']." ".$depositor['currency'];

        //3. particepent:
        $participants = array('All');

        //4. type:
        $type = "cb";
        
        //5. tags
        $tags = array(array('key' => 'priv',
                            'relation' => '=',
                            'value'    => 'Administrator'),

                      array('operator' => 'OR'),

                      array('key' => 'name',
                            'relation' => '=',
                            'value' => $depositor['name'])
                     );

        //6. execute notification
        new notification($depositor, $title, $body, $participants, $type);
        
    }
    
    //-->change dispute
    function changeDisputeStatus($dispute, $id){
        
        global $loader;
        
        switch($dispute) {
            case 1:
                $result = "Waiting response";
                $date_sent = date('Y-m-d H:i:s');
                break;
            case 0:
                $result = "Pending";
                $date_sent = "No dispute sent";
                break;
        }
        
        $sql = "UPDATE chargeback ";
        $sql .= "SET dispute_sent = '".$dispute."', date_sent = '".$date_sent."', result = '".$result."' ";
        $sql .= "WHERE ID = '".$id."'";
        
        //inject query
        $loader->mysqli->query($sql);
        
        $sent = array($date_sent,$result);
        print_r(json_encode($sent));
    }
    
    //-->import statistic
    function statisticImport(){
        
        global $loader;
        
        $this->table = "chargeback";

        $columns = array(
            "chargeback.id",
            filter::currencyFilter(array('all','cb'),'chargeback', 'chargeback'),
            filter::countRow(array('win','lose','open')),

        );

        $parameters = array(
            "WHERE" => "0 = 0",
            $this->dateFilter(0),
            $this->userFilter(1),
            $this->typeFilter(1),
            $this->statusFilter(0),
            $this->brandFilter(0),
            $this->countriesFilter(1)
        );

        $result = ImportController::import('chargeback', $columns, $parameters);

        $chargeback = $result['data'][0];

        $this->statistic = array('sopen'   => $chargeback['USDopen'] + $chargeback['EURopen'] + $chargeback['GBPopen'],
                                   'slose'  => $chargeback['USDlose'] + $chargeback['EURlose'] + $chargeback['GBPlose'],
                                   'swin'  => $chargeback['USDWin'] + $chargeback['EURWin'] + $chargeback['GBPWin'],
                                   'cwin'   => $chargeback['winCount'],
                                   'close'  => $chargeback['loseCount'],
                                   'copen'  => $chargeback['openCount']
                                  );
    
    }
    
    //-->import data from db
    function import(){
        
        global $loader;
        
        $this->table = 'chargeback';
        
        $sql = "SELECT * ";
        
        //Filters 
        /**
        * the Filter array build as: filterName => argoment (default 1,0)
        * to allow filter just remove the "//";
        **/
        $filters = array("userFilter"   => 1,
                         "dateFilter"   => 0,
                         "typeFilter"   => 1,
                         "statusFilter" => 0,
                         "brandFilter"  => 0,
                         "countriesFilter" => 1
                        );

        $sql .= " FROM $this->table";
        
        $sql .= $this->fixWhere($sql);
        
       
        //filters
        foreach($filters as $filter => $arg){
            
            $sql .= $this->$filter($arg);
        }

        $this->result = $loader->mysqli->query($sql);
        
        while($row = $this->result->fetch_assoc()){
            
            //readable date format
            $date = explode(" ", $row['date']);
            $new_date = date('Y/m/d',strtotime($date[0]));
            $new_time = date('H:i', strtotime($date[1]));
            
            $this->chargebacks[] = array(
                                   'case_status'     => $row['closed'],
                                   'ID'              => $row['id'],
                                   'date'            => $new_date,
                                   'time'            => $new_time,
                                   'payment_method'  => $row['payment_method'],
                                   'tran_id'         => $row['tran_id'],
                                   'cid'             => $row['customer_id'],
                                   'customer_name'   => $row['customer_firstname']." ".$row['customer_lastname'],
                                   'rep_name'        => $row['firstname']." ".$row['lastname'],
                                   'credit_company'  => $row['clearedby'],
                                   'amount'          => $row['amount'],
                                   'currency'        => $row['currency'],
                                   'dispute'         => $row['dispute_sent'], 
                                   'date_sent'       => $row['date_sent'],
                                   'status'          => $row['result']);
        }
        
        
  
        
    }
    
    //-->generate table
    
    //-->close case
    function closeCase(){
        
        global $loader;
        
        $charge_id = mysqli_real_escape_string($loader->mysqli,$_POST['charge_id']);
        $result    = mysqli_real_escape_string($loader->mysqli,$_POST['result']);
        
        //note as closed
        $sql = "UPDATE chargeback SET closed = '1'";
        
        //set result
        $sql .= ", result      = '".$result."'";
        
        //to tran_id
        $sql .= "WHERE ID = '".$charge_id."'";
        
        $loader->mysqli->query($sql);
        
        new SwalController("Case closed successfuly","","success");
    }
    
    //-->get ajax
    function ajaxHandler(){
        
        if(isset($_POST['do']) && $_POST['do'] == 'charge'){
            
            $this->insert();
        }
        if(isset($_POST['charge_id'])){
            
            $this->closeCase();
        }
        if(isset($_POST['dispute_sent'])){
            
            $dispute_sent = $_POST['dispute_sent'];
            $charge_id = $_POST['id'];
        
            $this->changeDisputeStatus($dispute_sent, $charge_id);
        }
    }
}
$test = new chargeback();