<?php
namespace Crm4u\Forms\Chargeback\close_case;
?>
<div class="modal fade" id="closecase-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Close Case</h4>
              </div>
              <div class="modal-body">
                  <p>You are about to close this case.<p>
                  <p>Did we?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                  <button type="button" id="lost" data-value="released" class="btn btn-danger pull-left">Lost</button>
                <button type="button" id="won" data-value="chargeback" class="btn btn-success pull-left">Won</button>
              </div>
            </div>
    </div>
</div>
<script>
    
    function updateResult(){
        $.ajax({
                type: "POST",
                data: mydata,
                url: 'inc/chargeback/chargeback.inc.php',
                datatype: 'text',
                success: function(res){
                    
                    $res = $.parseJSON(res);
                    if($res['flag'] == 0){
                        swal({title: $res['header'],
                              text: $res['message'],
                              type: $res['type']
                             },function(){
                        location.reload();  
                        });
                        
                    }
                }
            });
    }
    
    $(document).ready(function(){
        
        //lost
        $("#lost").click(function(){
            
            charge_id = $(".selected").attr("data-value");
            console.log(charge_id);
            mydata = "charge_id="+charge_id+"&result=lost";
            
            
            updateResult();
            
        });
        
        //win
        //lost
        $("#won").click(function(){
            
            charge_id = $(".selected").attr("data-value");
            console.log(charge_id);
            mydata = "charge_id="+charge_id+"&result=won";
            
            
            updateResult();
            
        })
    });

</script>

 