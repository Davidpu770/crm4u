<?php
namespace Crm4u\Pages\Partial\New_chargeback;

use Crm4u\Controller\FormController;
use Crm4u\Import\employeeImport;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Controller\Route;

global $loader;


?>
<div class="modal fade" id="new-deposit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New Chargeback
                </h4>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <form method="post" id="new_chargeback_form">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="emp_name">Employee Name: </label></br>
                                <div class="col-md-12 fix-select">
                                    <select class="form-control smart-select" id="emp_id" name="emp_id">
                                        <option value="no-select"></option>
                                        <?php
                                        Route::get('select', 'Models', 'users', array('user'));
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="type">Type:</label>
                                    <select class="form-control" name="type" id="type">

                                        <?php
                                        $parms = array(
                                            'selected'    => 'Charge back',
                                            'values' => array(
                                                'Charge back' => array('value' => 'Chargeback', 'data-value' => 'Withdrawal')
                                            )
                                        );

                                        foreach ($parms['values'] as $parm => $value){

                                            $selected = '';

                                            if($parms['selected'] == $value['value']){

                                                $selected = "selected='selected'";

                                            }
                                            echo "<option value='{$value['value']}' data-value='{$value['data-value']}' $selected>$parm</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tran_id">Transaction-Id:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="tran_id" name="tran_id" autocomplete="off" placeholder="Enter Tran-ID" >

                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default tran_check"><i class="fa fa-check"></i> Check ID</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="deposit_cid">Customer ID:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="deposit_cid" placeholder="Enter Customer ID" name="customer_id">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default cid_check"><i class="fa fa-check"></i> Check ID</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cust_name">Customer Name:</label>
                                    <input type="text" class="form-control cust_name" id="cust_name" placeholder="Enter Customer Name" name="customer_name" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="payment_method">Payment Method:</label>
                                    <select class="form-control" name="payment_method" id="payment_method">
                                        <option>Credit Card</option>
                                        <option>Wire</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="deposit">Amount:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="deposit" placeholder="Enter Deposit" name="amount" pattern="[\-]?[0-9,.]*" required>
                                        <span class="input-group-btn">
                            <select class="form-control" name="currency" id="currencey">
                                <option value="no-option"></option>
                                <option value="USD">USD</option>
                                <option value="EUR">EUR</option>i
                                <option value="GBP">GBP</option>
                                <option value="BTC">BTC</option>
                            </select>
                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" id="submitDeposit" name="deposit_submit">Insert Deposit</button>
                </form>
            </div>
            <div class="overlay" style="display: none">
                <i class="fa fa-spinner fa-spin"></i>
            </div>
            <script type="text/javascript" src="/public/js/validator.js"></script>
            <script>
                $(document).ready(function () {

                    $("#new_chargeback_form").submitForm({
                        'table_name' : 'chargeback',
                        'action'     : 'insert',
                        'is_simple'  : false,
                        'refresh_table_name' : 'open_chargeback_table'
                    });
                });
            </script>
        </div>
    </div>
</div>
