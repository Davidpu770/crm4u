<?php
namespace Crm4u\Pages\Partial\Withdrawal_table ;

//risk list import

global $loader;



?>
<div class="col-md-12 withdrawals">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Withdrawals Table
                    <span class="hidden-sm hidden-xs label label-primary">Withdrawals: <span id="all_withdrawals"></span> USD</span>
                    <span class="hidden-sm hidden-xs label label-success">Charge Backs: <span id="all_chargeback"></span> USD</span>
                </h3>
            </div>
            <div class="box-body">

                <div class="table table-responsive">
                    <table id="Withdrawal_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><i class="fa fa-clock-o"></i> date</th>
                                <th title="Payment Method"><i class="fa fa-credit-card"></i> P-Method</th>
                                <th title="Credit Company"><i class="fa fa-credit-card-alt"></i> Credit Company</th>
                                <th>Tran ID</th>
                                <th title="Customer ID"><i class="fa fa-bullhorn"></i> CID</th>
                                <th>Customer Name</th>
                                <th>Country name</th>
                                <th>Rep name</th>
                                <th><i class="fa fa-usd"></i> Amount</th>
                                <th><i class="fa fa-usd"></i> IN USD</th>
                                <th><i class="fa fa-money"></i> currency</th>
                                <th><i class="fa fa-check-square-o"></i> FTD</th>
                                <th><i class="fa fa-clock-o"></i> Verification Date</th>
                                <th>Campaign name</th>
                                <th ><i class=" fa fa-edit"></i> Status</th>
                                <th><i class='fa fa-dot-circle-o'></i> Risk</th>
                                <th >Notes</th>
                                <th >Options</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            <script>
        $(document).ready(function() {


            $("#Withdrawal_table").deleteRow({
                'table_name' : 'goal',
                'pk' : 'tran_id',
                'refresh_table_name' : 'Withdrawal_table'
            });


            editRow(Withdrawal_table, function () {
                $("#edit").modal();
                $(".overlay").css("display","");

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: "/import/Withdrawal/json/"+$row_id,
                    success: function(res){
                        res = res['data'][0];
                        $("select#payment_method_edit").val(res['payment_method']);
                        $("input[type=text]#tran_id_edit").val(res['tran_id']);
                        $("input[type=text]#deposit_cid_edit").val(res['customer_id']);
                        $("input[type=text]#cust_name_edit").val(res['cname']);
                        $("input[type=text]#deposit_edit").val(res['amount']);
                        $("select#currencey_edit").val(res['currency']);
                        $("input[type=text]#date").val(res['date']);
                        $("select#emp_name_edit").val(res['ename']);
                        if(res['ftdValue'] == "1") {
                            $("#ftd_edit").prop('checked', true);
                        } else {
                            $("#ftd_edit").prop('checked', false);
                        }
                        $(".overlay").css("display","none");
                    }
                });

            });

            changeStatus('#Withdrawal_table','status','goal','tran_id');
            $("#searchBTN").on('click', function () {

                $form = $("#filter_form").serialize();
                $table.ajax.reload(function(json) {

                    $.each(json.totals.current_month, function (k, v) {
                        console.log(k + ":" + v);
                        $("#" +k).html(v.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "1,"));
                    });
                });

                $("#resetFilter").css("display","");
            });

            $("#resetFilter").on('click', function () {

                $(':input','#filter_form')
                    .not(':button, :submit, :reset, :checkbox')
                    .val('')
                    .removeAttr('checked')
                    .removeAttr('selected');

                $form = $("#filter_form").serialize();
                $table.ajax.reload();

                $(this).css("display","none");

            });

            $form = '';

            $table = $('#Withdrawal_table').DataTable({
                ajax : {
                    "url": 'import/Withdrawal/table',
                    "type": "GET",
                    data:  function ( d ) {
                        return $form
                    }
                },
                "initComplete": function(settings, json) {

                $.each(json.totals.current_month, function (k, v) {
                    $("#" +k).html(v.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "1,"));
                });
            },
                order: [[ 3, "desc" ]],
                rowId: 'tran_id',
                "columns": [

                    {
                        "title": "date",
                        "class" : "hideOverflow",

                        "data": "date"
                    },
                    {
                        "class" : "hideOverflow",
                        "data": "payment_method"},
                    {
                        "visible": false,
                        "class" : "hideOverflow",
                        "data": "clearedby"},
                    {"data": "tran_id"},
                    {"data": "customer_id"},
                    {
                        "class" : "hideOverflow",
                        "data": "cname"}, ///customer name @todo
                    {
                        "visible": false,
                        "class" : "hideOverflow",
                        "data": "country"},
                    {"data": "ename"}, //emp name
                    {"data": "amount"},
                    {
                        "visible": false,
                        "data": "inUSD"}, //in_usd
                    {"data": "currency"},
                    {"data": "ftd"}, //make font awesome
                    {
                        "visible": false,
                        "data": "verification_date"},
                    {
                        "visible": false,
                        "data": "campaign"},
                    {"data": "status"},
                    {
                        "data": "risk",
                        "visible": false},
                    {
                        "class": "comment",
                        "type": "textarea",
                        "data": "comment"},
                    {
                        "class" : "hideOverflow",
                        "data": null,
                        "defaultContent": DataTableButton([$edit_btn,$delete_btn])
                    }


                ],
                dom: 'Bfrtip',
                "buttons": [
                    {extend: 'colvis', text: 'Columns'}

                ],

                stateSave: true
            });
    } );
            </script>
            </div>
        </div>
    </div>
