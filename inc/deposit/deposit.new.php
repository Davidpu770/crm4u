<?php
namespace Crm4u\Forms\Deposit\insert;

use Crm4u\Controller\FormController;
use Crm4u\Import\employeeImport;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Controller\Route;

global $loader;

$deposit_type = (strpos($_SERVER['REQUEST_URI'],"deposits")) ? 'deposit' : 'Withdrawal';

?>
<div class="modal fade" id="new-deposit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New <?php echo $deposit_type ?>
                </h4>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <form method="post" id="new_deposit_form">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="emp_name">Employee Name: </label></br>
                                <div class="col-md-12 fix-select">
                                    <select class="form-control smart-select" id="emp_id" name="emp_id">
                                        <option value="no-select"></option>
                                        <?php
                                        Route::get('select', 'Models', 'users', array('user'));
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="split" class="checkbox-inline">
                                    <input type="checkbox" id="split" name="split"><strong>Split</strong>
                                </label>
                            </div>
                            <div id="splitEmployee" class="col=md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control" id="split_emp_name">
                                            <option value="no-select"></option>
                                            <?php
                                            Route::get('select', 'Models', 'users', array('user'));
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="sr-only" for="percentage">%</label>
                                        <div class="input-group">
                                            <input type="text" name="percentage" id="percentage" class="form-control">
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="type">Type:</label>
                                        <select class="form-control" name="type" id="type">

                                            <?php
                                            $parms = array(
                                                'selected'    => $deposit_type,
                                                'values' => array(
                                                    'Deposit'     => array('value' => 'deposit',    'data-value' => 'CustomerDeposits'),
                                                    'Withdrawals' => array('value' => 'Withdrawal', 'data-value' => 'Withdrawal'),
                                                    'Charge back' => array('value' => 'Chargeback', 'data-value' => 'Withdrawal')
                                                )
                                            );

                                                foreach ($parms['values'] as $parm => $value){

                                                    $selected = '';

                                                    if($parms['selected'] == $value['value']){

                                                        $selected = "selected='selected'";

                                                    }
                                                    echo "<option value='{$value['value']}' data-value='{$value['data-value']}' $selected>$parm</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tran_id">Transaction-Id:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="tran_id" name="tran_id" autocomplete="off" placeholder="Enter Tran-ID" >

                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default tran_check"><i class="fa fa-check"></i> Check ID</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="deposit_cid">Customer ID:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="deposit_cid" placeholder="Enter Customer ID" name="customer_id">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default cid_check"><i class="fa fa-check"></i> Check ID</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cust_name">Customer Name:</label>
                                    <input type="text" class="form-control cust_name" id="cust_name" placeholder="Enter Customer Name" name="customer_name" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="payment_method">Payment Method:</label>
                                    <select class="form-control" name="payment_method" id="payment_method">
                                        <option>Credit Card</option>
                                        <option>Wire</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="deposit">Amount:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="deposit" placeholder="Enter Deposit" name="amount" pattern="[\-]?[0-9,.]*" required>
                                        <span class="input-group-btn">
                            <select class="form-control" name="currency" id="currencey">
                                <option value="no-option"></option>
                                <option value="USD">USD</option>
                                <option value="EUR">EUR</option>i
                                <option value="GBP">GBP</option>
                                <option value="BTC">BTC</option>
                            </select>
                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="payment_method">Campaign:</label>
                                    <select class="form-control" name="campaign">
                                        <?php Route::get('select', 'Models', 'campaign', array('campaign')); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="deposit">Country:</label>
                                        <select class="form-control" name="country">
                                            <?php Route::get('select', 'Models', 'countries', array('country')); ?>
                                        </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="ftd_bonus" name="ftd_bonus"><strong id="depositor_name"><?php echo str_replace("0","",$loader->site->ftd_bonus) ?>% To FTD</strong>
                                        <input type="hidden" id="depositor_name_hidden" value=""/>
                                        <input type="hidden" id="ftd_bonus_per" value="<?php echo $loader->site->ftd_bonus ?>">
                                    </label>
                                    <div id="error"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div id="ftd_div">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="ftd" name="ftd"><strong>FTD</strong>
                                        </label>
                                    </div>
                                    <input type="hidden" value="allCharge" id="clearedby" name="clearedby">
                                    <div id="error"></div>
                                </div>
                            </div>
                        </div>
                        <?php if(CheckAccess::isSuperAdmin()){?>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="silent" name="silent"><strong id="depositor_name">Silent Deposit</strong>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" id="submitDeposit" name="deposit_submit">Insert Deposit</button>
                </form>
            </div>
            <div class="overlay" style="display: none">
                <i class="fa fa-spinner fa-spin"></i>
            </div>
            <script type="text/javascript" src="/public/js/validator.js"></script>
            <?php
                echo "<script type='text/javascript' src='/public/js/insert_deposit.js'></script>";
            ?>
        </div>
    </div>
</div>
