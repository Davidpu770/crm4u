<?php
namespace Crm4u\Pages\Partial\PendingTable;
/**
 * Created by PhpStorm.
 * User: David
 * Date: 18/01/2017
 * Time: 13:22
 */
?>
<div class="col-md-12 withdrawals">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Withdrawals Table</h3>
        </div>
        <div class="box-body">
            <div class="col-md-12 total">
                <span class="label label-info total-label"><h4>USD <?php echo $deposit->totals['with'] + $deposit->totals['cb']; ?>.00</h4></span>
            </div>
            <div class="table table-responsive">
                <table id="chargeback_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><i class="fa fa-clock-o"></i> date</th>
                        <th>Type</th>
                        <th title="Payment Method"><i class="fa fa-credit-card"></i> P-Method</th>
                        <th>Tran ID</th>
                        <th title="Customer ID"><i class="fa fa-bullhorn"></i> CID</th>
                        <th>Customer Name</th>
                        <th>Rep name</th>
                        <th><i class="fa fa-usd"></i> Amount</th>
                        <th><i class="fa fa-money"></i> currency</th>
                        <th><i class=" fa fa-edit"></i> Status</th>
                        <th>Notes</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($deposit->withdrawal_data as $key => $withdrawal_data) { ?>
                        <tr data-toggle="tooltip" title="Update by: <?php echo $withdrawal_data['update_by'] ?>" data-placement="bottom"
                            data-value="<?php echo $withdrawal_data['ID']?>">
                            <td><?php echo $withdrawal_data['date']." ".$withdrawal_data['time']; ?></td>
                            <td><?php echo $withdrawal_data['type']; ?></td>
                            <td><?php echo $withdrawal_data['payment_method']; ?></td>

                            <?php //transaction id
                            echo "<td>";

                            $tran_id = $withdrawal_data['tran_id'];
                            $cid =  $withdrawal_data['customer_id'];

                            if($brand->brand_format['withdrawal_page']){

                                $tran_page = $brand->brand_format['withdrawal_page'];

                                $tran_page = str_replace('%TRAN_ID%', $tran_id, $tran_page);

                                echo "<a target='_blank' href='".$tran_page."'>".$tran_id."</a>";

                            } else {

                                echo $tran_id;

                            }

                            echo "</td>";

                            //customer id


                            echo "<td>";

                            if($brand->brand_format['customer_page']){

                                $custumer_page = $brand->brand_format['customer_page'];
                                $custumer_page = str_replace('%CID%', $cid, $custumer_page);

                                echo "<a target='_blank' href='".$custumer_page."'>".$cid."</a>";

                            } else {

                                echo $cid;

                            }

                            echo "</td>";
                            ?>
                            <td class="hideOverflow" title="<?php echo $withdrawal_data['customer_name'] ?>"><?php echo $withdrawal_data['customer_name'] ?></td>
                            <td><?php echo $withdrawal_data['emp_name'] ?></td>
                            <td><?php echo $withdrawal_data['amount'];?></td>
                            <td><?php echo $withdrawal_data['currency'];?></td>
                            <td><?php if($user->priv['verify_deposits'] == 1) {?>
                                    <select name="verifed_select" style="margin-bottom: 0px;float: left;width:90px" class="form-control verifed_select">
                                        <?php
                                        $values = array('full','partial','none','released');

                                        foreach($values as $key => $value){
                                            $selected = '';
                                            if($withdrawal_data['status'] == $value){
                                                $selected = "Selected='selected'";
                                            }
                                            echo $option = "<option value='$value' $selected>".ucfirst($value)."</option>";
                                        }

                                        ?>
                                    </select>
                                    <input id="deposit_id" name="deposit_id" type="hidden" value="<?php echo $withdrawal_data['ID']; ?>" />
                                <?php } else {
                                    $color = array("none"     => "danger",
                                        "partial"  => "warning",
                                        "full"     => "success",
                                        "Full"     => "success",
                                        "released" => "danger");

                                    echo "<h4><span class='status-label label label-".ucfirst($color[$withdrawal_data['status']])." label-mini'>".$withdrawal_data['status']."</span></h4>";
                                }?>
                            </td>
                            <td>
                                <form method="post" class="comment_form">
                                        <textarea name="deposit_comment" class="deposit_comment form-control" rows="1" cols="15">
                                        <?php echo $withdrawal_data['comment']; ?>
                                        </textarea>
                                    <input id="deposit_comment_id" name="deposit_id" type="hidden" value="<?php echo $withdrawal_data['ID']; ?>" />
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>

            <script>
                $(document).ready(function() {
                    var table_w =$('#chargeback_table').DataTable();
                    $("#chargeback_table tbody").on("click", "tr", function(){
                        if ( $(this).hasClass('selected') ) {
                            $(this).removeClass('selected');
                            $("#edit-button_w").attr("disabled", "disabled");
                            $("#delete-button_w").attr("disabled", "disabled");
                        }
                        else {
                            table_w.$('tr.selected').removeClass('selected');
                            $(this).addClass('selected');

                            if($(".selected").length){
                                $("#edit-button_w").removeAttr("disabled");
                                $("#delete-button_w").removeAttr("disabled");

                            }
                        }
                        $("#edit-button_w").click(function(){
                            selected_id = $(".selected").attr('data-value');

                        });
                    });
                } );
            </script>
            <?php if($user->priv > 1) { ?>
                <button class="btn btn-info edit-deposit-btn" id="edit-button_w" data-target="#edit" data-toggle="modal" title="Edit Deposit" disabled>
                    <i class="fa fa-pencil"></i> Edit
                </button>
                <button class="btn btn-danger edit-deposit-btn" id="delete-button_w"title="Delete Deposit" disabled>
                    <i class="fa fa-trash-o"></i> Delete
                </button>
            <?php } ?>
        </div>
    </div>
</div>

