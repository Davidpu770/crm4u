<?php

namespace Crm4u\Pages\Partial\deposit;

use Crm4u\Middleware\CheckAccess;

global $loader;


?>
<script src="/public/js/jquery.filthypillow.min.js"></script>
<link src="/public/css/jquery.filthypillow.css">

<script>
$the_table = 'goal';
</script>

    
<?php

$loader->_load('Crm4u\\Forms\\Deposit\\Edit');

if($loader->user->priv > 1){

    $loader->_load('Crm4u\\Forms\\Deposit\\insert');
    $loader->_load('Crm4u\\Forms\\Deposit\\currency');
    
    }

    $filter = array(
    'option'  => array(
        'collapse' => ""),
    'filters' => array(
        'user',
        'payment',
        'tran_cid',
        'verification',
        'ftd',
        'date',
        'risk',
        'desk'
        )
);

$loader->_load('Crm4u\\Pages\\Partial\\searchBox',$filter);

if(CheckAccess::isAdmin()){

    $loader->_load('Crm4u\\Pages\\Partial\\new_deposit_table');
}

$loader->_load('Crm4u\\Pages\\Partial\\deposit_table');

?>
 <script>

    $(document).ready(function() {
        $("#new_deposit").click(function(){
           $("input[type=text]").val("");
        });

        $(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
  
});
    </script>

<?php echo (!CheckAccess::isAdmin()) ? '</div>' : ''; ?>


<div class='row mt'></div>

