<?php
namespace Crm4u\Pages\Partial\Close_chargeback;

$url = '/import/chargeback/table?is_simple=false';

if(isset($_loadArgs) && !is_null($_loadArgs)){

    foreach($_loadArgs as $key => $value){

        $parameters[] = $key."=".$value;
    }

    $url = $url . "&". implode('&',$parameters);
}

global $loader;

?>
<div class="table table-responsive">
    <table id="close_chargeback_table" class="table table-striped table-bordered table-advance table-hover">
        <thead>
        <tr>
            <th>Date</th>
            <th> Payment Method</th>
            <th>Tran ID</th>
            <th title="Customer ID">CID</th>
            <th>Customer Name</th>
            <th>Rep name</th>
            <th>Credit Company</th>
            <th>Amount</th>
            <th>currency</th>
            <th >Result</th>
            <th> Options</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function() {

        $table = $("#close_chargeback_table").DataTable({

            "bLengthChange": false,
            "ajax": {
                'url': '<?php echo $url ?>',
                'type': "GET"
            },
            rowId: 'id',
            "columns": [
                {"data": "date"},
                {"data": "payment_method"},
                {"data": "tran_id"},
                {"data": "customer_id"},
                {"data": "customer_name"},
                {"data": "employee_name"},
                {"data": "clearedby"},
                {"data": "amount"},
                {"data": "currency"},
                {"data": "result"},
                {
                    "class": "hideOverflow",
                    "data": null,
                    "defaultContent": DataTableButton([$edit_btn, $delete_btn])
                }
            ]

        });
    });
</script>