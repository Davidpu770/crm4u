
<div class="col-md-12 new-deposit-table">
    <div class="box box-success" >
        <div class="box-header with-border">
            <h3 class="box-title">Pending Withdrawal Table
            </h3>
        </div>
        <div class="box-body">
            <div class="table table-responsive">
                <table id="new_deposit_table" class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="fa fa-clock-o"></i> date</th>
                        <th title="Payment Method"><i class="fa fa-credit-card"></i> P-Method</th>
                        <th title="Credit Company"><i class="fa fa-credit-card-alt"></i> Credit Company</th>
                        <th>Tran ID</th>
                        <th title="Customer ID"><i class="fa fa-bullhorn"></i> CID</th>
                        <th><i class="fa fa-usd"></i> Amount</th>
                        <th><i class="fa fa-money"></i> currency</th>
                        <th>Approve</th>
                    </tr>
                    </thead>
                </table>
                <script>

                    $(document).ready(function () {



                    });

                    $('#new_deposit_table').DataTable( {
                        "ajax": '/autodeposit/Withdrawal',
                        "language": {
                            "loadingRecords": "No more deposit for now...",
                        },
                        "columns": [
                            { "data": "date" },
                            { "data": "paymentMethod" },
                            { "data": "credit_company" },
                            { "data": "id" },
                            { "data": "cid" },
                            { "data": "amount" },
                            { "data": "currency" },
                            { "data": "approve"}
                        ],
                        "initComplete": function(settings, json) {
                            $(".approve").click(function(){

                                $("input[type=text]#tran_id").val($(this).attr('data-deposit'));
                                $(".tran_check").click();
                            });
                        }
                    } );

                </script>
            </div>
        </div>
    </div>
</div>