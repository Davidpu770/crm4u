<?php
namespace Crm4u\Pages\Partial\deposit_table;

use Crm4u\Controller\FormController;
use Crm4u\Middleware\CheckAccess;

global $loader;


?>

<div class="col-md-12 deposit-table">
    <div class="box box-success" >
        <div class="box-header with-border">
            <h3 class="box-title">Deposit Table
                <span class="hidden-sm hidden-xs label label-primary">Total: <span id="all_deposits"></span> USD</span>
                <span class="hidden-sm hidden-xs label label-success">verified: <span id="total_conf"></span> USD</span>
                <span class="hidden-sm hidden-xs label label-danger">unverified: <span id="total_unconf"></span> USD</span>

            </h3>
            <?php


            if (CheckAccess::isAuthorized(2)) {
                FormController::tableButton(array('new_deposit','currency','export'));
            ?>
        </div>
        <?php } ?>


        <div class="box-body">
            <div class="col-md-12 total">
                <span class="label label-info total-label hidden-lg hidden-md"><h4>USD <?php echo number_format($loader->deposit->totals['deposit']); ?>.00</h4></span>
            </div>
            <div class="table table-responsive">
                <table id="deposit_table" class="table table-striped table-bordered table-advance table-hover" width="100%">
                    <thead>
                        <tr>
                            <th><i class="fa fa-clock-o"></i> date</th>
                            <th title="Payment Method"><i class="fa fa-credit-card"></i> P-Method</th>
                            <th title="Credit Company"><i class="fa fa-credit-card-alt"></i> Credit Company</th>
                            <th>Tran ID</th>
                            <th title="Customer ID"><i class="fa fa-bullhorn"></i> CID</th>
                            <th>Customer Name</th>
                            <th>Country name</th>
                            <th>Rep name</th>
                            <th><i class="fa fa-usd"></i> Amount</th>
                            <th><i class="fa fa-usd"></i> IN USD</th>
                            <th><i class="fa fa-money"></i> currency</th>
                            <th><i class="fa fa-check-square-o"></i> FTD</th>
                            <th><i class="fa fa-clock-o"></i> Verification Date</th>
                            <th>Campaign name</th>
                            <th ><i class=" fa fa-edit"></i> Status</th>
                            <th><i class='fa fa-dot-circle-o'></i> Risk</th>
                            <th >Notes</th>
                            <th >Options</th>
                        </tr>
                    </thead>
                    <tbody id="deposit_list">
                    </tbody>
                </table>
            </div>
<script>

                $(document).ready(function() {


                    $form = '';

                    $("#deposit_table").deleteRow({
                        'table_name' : 'goal',
                        'pk' : 'id',
                        'refresh_table_name' : 'deposit_table',
                        'simple' : null
                    });


                    var row_id = $(this).parent().closest('tr').attr('id');

                    changeStatus('#deposit_table','status','goal','id');
                    changeStatus('#deposit_table','risk','goal','id');

                    $("#searchBTN").on('click', function () {

                        $form = $("#filter_form").serialize();

                        $table.ajax.reload(function (json) {

                            $.each(json.totals.current_month, function (k, v) {
                                $("#" +k).html(v.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "1,"));
                            });
                        });

                        $("#resetFilter").css("display","");
                    });

                    $("#resetFilter").on('click', function () {

                        $(':input','#filter_form')
                            .not(':button, :submit, :reset, :checkbox')
                            .val('')
                            .removeAttr('checked')
                            .removeAttr('selected');

                        $form = $("#filter_form").serialize();
                        $table.ajax.reload();

                        $(this).css("display","none");

                    });



                    $table = $('#deposit_table').DataTable({
                        ajax : {
                            "url": 'import/deposit/table',
                            "type": "GET",
                            data:  function ( d ) {
                                return $form
                            }
                        },
                        order: [[ 3, "desc" ]],
                        rowId: 'id',
                        "fnRowCallback": function (nRow, Data) {

                            (Data['is_new'] === true) ? $(nRow).addClass('success') : false;
                            $(nRow).attr('data-toggle','tooltip');
                            $(nRow).attr('title','Upadte by: ' + Data['update_by']);
                        },
                        dom: 'Bfrtip',
                        "buttons": [
                            {extend: 'excel', text: "export", className: "btn btn-info"},
                            {extend: 'colvis', text: 'Columns'}

                        ],
                        stateSave: true,
                        "initComplete": function(settings, json) {

                            $.each(json.totals.current_month, function (k, v) {
                                $("#" +k).html(v.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "1,"));
                            });
                        },
                        "columns": [

                            {
                                "title": "date",
                                "class" : "hideOverflow",
                                "data": "date"
                            },
                            {
                                "class" : "hideOverflow",
                                "data": "payment_method"},
                            {
                                "visible": false,
                                "class" : "hideOverflow",
                                "data": "clearedby"
                            },
                            {
                                "data": "tran_id"
                            },
                            {
                                "data": "customer_id"
                            },
                            {
                                "class" : "hideOverflow",
                                "data": "cname"}, ///customer name @todo
                            {
                                "visible": false,
                                "class" : "hideOverflow",
                                "data": "country"
                            },
                            {
                                "data": "ename"
                            }, //emp name
                            {
                                "data": "amount"
                            },
                            {
                                "visible": false,
                                "data": "inUSD"
                            }, //in_usd
                            {
                                "data": "currency"
                            },
                            {
                                "data": "ftd"
                            },
                            {
                                "visible": false,
                                "data": "verification_date"},
                            {
                                "visible": false,
                                "data": "campaign"
                            },
                            {
                                "data": "status"
                            },
                            {
                                "data": "risk"
                            },
                            {
                                "class": "comment",
                                "type": "textarea",
                                "data": "comment"},
                            {
                                "class" : "hideOverflow",
                                "data": null,
                                "defaultContent": DataTableButton([$edit_btn,$delete_btn])
                            }


                        ]
                    });

                    $("a.dt-button.buttons-collection").addClass('btn btn-danger');


                    editableRow($('.comment'),'goal','id');
                    exportTable('#export','goal',"#filter_form");

                });

            </script>
        </div>
    </div>
</div>

