<?php
namespace Crm4u\Pages\Partial\Open_chargeback;

$url = '/import/chargeback/table?is_simple=false';

if(isset($_loadArgs) && !is_null($_loadArgs)){

    foreach($_loadArgs as $key => $value){

        $parameters[] = $key."=".$value;
    }

    $url = $url . "&". implode('&',$parameters);
}

global $loader;

?>

<div class="table table-responsive">
    <table id="open_chargeback_table" class="table table-striped table-bordered table-advance table-hover">
        <thead>
        <tr>
            <th>Date</th>
            <th title="Payment Method"> Payment Method</th>
            <th>Tran ID</th>
            <th title="Customer ID">CID</th>
            <th>Customer Name</th>
            <th>Rep name</th>
            <th>Credit Company</th>
            <th>Amount</th>
            <th>currency</th>
            <th>Dispute Sent</th>
            <th>Date sent</th>
            <th>status</th>
            <th>Options</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <button type="button" class="btn btn-info"
            data-target="#new-deposit" data-toggle="modal"
            title="Adding New Deposit">
        <i class="fa fa-plus"></i><span class="hidden-xs hidden-xs-inline"> New Chargeback </span>
    </button>
</div>
<script>
    $(document).ready(function() {

        var table = $("#open_chargeback_table");

        $table = table.DataTable({

            "bLengthChange": false,
            "ajax": {
                'url': '<?php echo $url ?>',
                'type': "GET"
            },
            rowId: 'id',
            "columns": [
                {"data": "date"},
                {"data": "payment_method"},
                {"data": "tran_id"},
                {"data": "customer_id"},
                {"data": "customer_name"},
                {"data": "employee_name"},
                {"data": "clearedby"},
                {"data": "amount"},
                {"data": "currency"},
                {"data": "dispute_sent"},
                {"data": "date_sent"},
                {"data": "status"},
                {
                    "class": "hideOverflow",
                    "data": null,
                    "defaultContent": DataTableButton([$edit_btn, $delete_btn])
                }
            ]

        });

        table.changeStatus({
            'table_name' : 'send_dispute',
            'status_row' : 'dispute_sent',
            'pk'         : 'id',
            'class_name' : 'send_dispute',
            'is_simple'  : false,
            'refresh_table_name' : "open_chargeback_table"
        });
    });
</script>