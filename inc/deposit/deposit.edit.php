<?php
namespace Crm4u\Forms\Deposit\Edit;

use Crm4u\Middleware\CheckAccess;
use Crm4u\Controller\Route;

global $loader;

$deposit_type = (strpos($_SERVER['REQUEST_URI'],"deposits")) ? 'deposit' : 'Withdrawal';

if(CheckAccess::isAuthorized(2)) {

    ?>

    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"> Edit Deposit</h4>
                </div>
                <div class="box-body">
                    <div class="container-fluid">
                        <form method="post" id="edit_deposit_form">
                            <input type="hidden" name="id" value="" id="id">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="emp_name">Employee Name: </label>
                                    <select class="form-control" id="emp_name_edit" name="emp_id" data-placeholder="Select an Employee">
                                        <option value="no-select"></option>\n
                                        <?php
                                        Route::get('select', 'Models', 'users', array('user'));
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="date">date:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" id="date" name="date">
                                            <script>
                                                var $fp = $("#date");
                                                $("#date").filthypillow();

                                                $("#date").on( "focus", function( ) {
                                                    $fp.filthypillow( "show" );
                                                } );

                                                $fp.on( "fp:save", function( e, dateObj ) {
                                                    $fp.val( dateObj.format( "YYYY-MM-DD hh:mm:00" ) );
                                                    $fp.filthypillow( "hide" );
                                                    var month = dateObj.format( "MMM" );

                                                });
                                            </script>
                                        </div><!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tran_id">Transaction-Id:</label>
                                        <input type="text" class="form-control" id="tran_id_edit" name="tran_id" autocomplete="off" placeholder="Enter Tran-ID">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="deposit_cid">Customer ID:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="deposit_cid_edit" placeholder="Enter Customer ID" name="customer_id">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default cid_check"><i class="fa fa-check"></i> Check ID</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cust_name">Customer Name:</label>
                                        <input type="text" class="form-control" id="cust_name_edit" placeholder="Enter Customer Name" name="customer_name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="payment_method">Payment Method: <small id="payment_method_label"></small></label>
                                        <select class="form-control" name="payment_method" id="payment_method_edit">
                                            <option>Credit Card</option>
                                            <option>Wire</option>
                                            <option>PayPal</option>
                                            <option>cashU</option>
                                            <option>Webmoney</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="deposit">Amount:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="deposit_edit" placeholder="Enter Deposit" name="amount" pattern="[\-]?[0-9,.]*" required>
                                            <span class="input-group-btn">
                                            <select class="form-control" name="currency" id="currencey_edit">
                                                <option value="no-option"></option>
                                                <option value="USD">USD</option>
                                                <option value="EUR">EUR</option>
                                                <option value="GBP">GBP</option>
                                                <option value="BTC">BTC</option>
                                            </select>
                                        </span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="comment">Comment:</label>
                                        <textarea type="text" class="form-control" id="comment" name="comment"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div id="ftd_div_edit">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="ftd_edit" name="ftd"><strong>FTD</strong>
                                            </label>
                                        </div>
                                        <div id="error"></div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" id="updateDeposit" name="deposit_submit">Update Deposit</button>
                </div>
                </form>
                <div class="overlay" style="display: none">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
            </div>
            <script>
                $(document).ready(function () {



                    submitForm('#edit_deposit_form','goal','update', false, '#tran_id_edit', 'tran_id');

                    editRow(<?php echo $deposit_type ?>_table, function () {
                        $("#edit").modal();
                        $(".overlay").css("display","");

                        $.ajax({
                            type: "GET",
                            dataType: "json",
                            url: "/import/<?php echo $deposit_type ?>/json/"+$row_id,
                            success: function(res){
                                res = res['data'][0];

                                $.each(res, function (k, v) {
                                    $("input[name='"+k+"'").val(v);
                                });

                                $("select#currencey_edit").val(res['currency']);
                                $("select#emp_name_edit").val(res['emp_id']);

                                $("#comment").val(res['comment']);

                                if(res['ftdValue'] == "1") {
                                    $("#ftd_edit").prop('checked', true);
                                } else {
                                    $("#ftd_edit").prop('checked', false);
                                }
                                $(".overlay").css("display","none");
                            }
                        });

                    });
                });

            </script>
        </div>
    </div>
<?php } ?>