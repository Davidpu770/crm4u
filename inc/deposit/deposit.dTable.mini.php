<?php
namespace Crm4u\Pages\Partial\mini_dTable;

global $loader;

use Crm4u\Controller\FormController;

$url = '/import/deposit/table?limit=5';

if(isset($_loadArgs) && !is_null($_loadArgs)){

    foreach($_loadArgs as $key => $value){

        $parameters[] = $key."=".$value;
    }

    $url = $url . "&". implode('&',$parameters);
}

?>


<div class="deposit-table">
    <div class="box box-success" >
        <div class="box-header with-border">
            <h3 class="box-title">Last Deposit Table</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div><!-- /.box-tools -->
        </div>

        <div class="box-body">
            <div class="table-responsive" >
                <table id="deposit_table" class="table no-margin">
                    <thead>
                    <tr>
                        <th>date</th>
                        <th title="Payment Method">Paymeny Method</th>
                        <th>Status</th>
                        <th>Tran #</th>
                        <th title="Customer ID"> CID</th>
                        <th>Rep name</th>
                        <th>Amount</th>
                        <th>currency</th>
                    </tr>
                    </thead>
                    <tbody id="deposit_list">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    changeStatus('#deposit_table','status','goal','tran_id');
    $('#deposit_table').DataTable({
        ajax : {
            "url": '<?php print $url?>',
            "type": "GET"
        },
        rowId: 'tran_id',
        "columns": [

            {
                "title": "date",
                "class" : "hideOverflow",
                "data": "date"
            },
            {
                "class" : "hideOverflow",
                "data": "payment_method"
            },
            {
                "data": "status"
            },
            {
                "data": "tran_id"
            },
            {
                "data": "customer_id"
            },
            {
                "data": "ename"
            }, //emp name
            {
                "data": "amount"
            },
            {
                "data": "currency"
            }
            ],
        "bFilter": false,
        "bLengthChange": false,
        "bPaginate": false
    });
</script>