<?php
namespace Crm4u\Pages\Partial\Risk;

use Crm4u\Controller\BrandController;
use Crm4u\Import\brand;
use Crm4u\Middleware\Fetch;
use Crm4u\SQL\filter;


class statistic  {

    public $count = array();

    public $amount = array();

    function __construct(){

        global $loader;

        $this->summery = self::count($loader->mysqli);

    }

    function count($mysqli){

        $finance = brand::import_finance(BrandController::current_brand());

        $filter = new filter();

        $cols = array(
            'COUNT(CASE WHEN goal.risk = "high" THEN goal.risk END) THRC',
            "COUNT(CASE WHEN goal.risk_status = 'chargeback' THEN goal.risk_status END) TCBC",
            "COUNT(CASE WHEN goal.risk_status = 'released' THEN goal.risk_status END) TRC ",
        );

        $parameters = array(
            $filter->userFilter(1),
            $filter->dateFilter(0),
            $filter->typeFilter(1),
            $filter->statusFilter(0),
            $filter->brandFilter(0),
            $filter->countriesFilter(1)
        );

        $result = new Fetch('goal',$cols, $parameters);

        $count = array(
            'TCTM' => $finance->current_month->total_deposit_count,
            'THRC' => $result->result->THRC,
            'TCBC' => $result->result->TCBC,
            'TRC'  => $result->result->TRC,

            'TATM' => $finance->current_month->all_deposits,
            'THRA' => $finance->current_month->total_high_risk,
            'TCBA' => $finance->current_month->all_chargeback,
            'TRA'  => $finance->current_month->all_withdrawals
        );

        return $count;
    }

    function box($amount, $count, $color, $icon, $tcba = 0, $thra = 0){

        global $global;

        return array(
            'value' => array(
                $amount, $count
            ),
            'color' => $color,
            'icon'  => $icon,
            'par'   => ($tcba > 0) ? $global->fixRound($par * 100, 1)  : 0
        );
    }

}

$user = new statistic();

//todo: do it with class OOP
//date: 14-09-2016


$statistic = array(

    'Total Amount this month'    => $user->box($user->summery['TATM'], $user->summery['TCTM'],'green','ion ion-card'),

    'Total HR amount'            => $user->box($user->summery['THRA'], $user->summery['THRC'],'yellow','ion ion-thumbsdown'),

    'Total CB amount'            => $user->box($user->summery['TCBA'], $user->summery['TCBC'],'red','ion ion-backspace-outline',$user->summery['TCBA'], $user->summery['THRA']),

    'Total released amount'      => $user->box($user->summery['TRA'], $user->summery['TRC'],'purple','ion-ios-upload-outline',$user->summery['THRA'] * $user->summery['TRA']),
);

?>


        <div class="box-body">

                <?php
                foreach($statistic as $key => $value) {
                    $box = "";
                    $box .= "<div class='col-md-3 col-sm-12'>";
                    $box .= "<div class='info-box bg-" . $value['color'] . " supportBox'>";
                    $box .= "<span class='info-box-icon'><i class='" . $value['icon'] . "' style='line-height: 2'></i></span>";
                    $box .= "<div class='info-box-content'>";
                    $box .= "<span class='info-box-text'>" . $key . "</span>";
                    $box .= "<span class='info-box-number'>" . number_format($value['value'][0]) . " USD</span>";


                    if ($value['par'] > 0) {

                        $box .= "<div class='progress'>";
                        $box .= "<div class='progress-bar' style='width: " . $value['par'] . "%'></div>";
                        $box .= "</div><span class='progress-description'>";
                        $box .= $value['par'] . "% This month";
                        $box .= "</span>";
                    }

                    $box .= "<hr>";
                    $box .= "<span class='info-box-number'>".$value['value'][1]." Deposits</span>";
                    $box .= "</div></div></div>";

                    print $box;
                }
                ?>
        </div>

