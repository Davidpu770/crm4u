<?php
namespace Crm4u\Pages\Partial\risk_dTable;

//risk list import

global $loader;

use Crm4u\Controller\FormController;
use Crm4u\Middleware\CheckAccess;

?>

<script>$the_table = 'goal'</script>
<div class="col-md-12 deposit-table">
        <div class="box box-success" >
            <div class="box-header with-border">
                <h3 class="box-title">High risk table
                    <span class="hidden-sm hidden-xs label label-primary">Total: <span id="total_high_risk"></span> USD</span>
                </h3>
                <?php
                if (CheckAccess::isAuthorized(2)) {

                    FormController::tableButton(array('export'));
                ?>
            </div>
            <?php } ?>

                <div class="box-body"> 
                    <table id="risk_table" class="table table-striped table-bordered table-advance table-hover">
                        <thead>
                        <tr>
                            <th><i class="fa fa-clock-o"></i> date</th>
                            <th title="Payment Method"><i class="fa fa-credit-card"></i> P-Method</th>
                            <th title="Credit Company"><i class="fa fa-credit-card-alt"></i> Credit Company</th>
                            <th>Tran ID</th>
                            <th title="Customer ID"><i class="fa fa-bullhorn"></i> CID</th>
                            <th>Customer Name</th>
                            <th>Country name</th>
                            <th>Rep name</th>
                            <th><i class="fa fa-usd"></i> Amount</th>
                            <th><i class="fa fa-usd"></i> IN USD</th>
                            <th><i class="fa fa-money"></i> currency</th>
                            <th><i class="fa fa-check-square-o"></i> FTD</th>
                            <th><i class="fa fa-clock-o"></i> Verification Date</th>
                            <th>Campaign name</th>
                            <th ><i class=" fa fa-edit"></i> Status</th>
                            <th><i class='fa fa-dot-circle-o'></i> Risk</th>
                            <th >Notes</th>
                            <th >Options</th>
                        </tr>
                        </thead>
                    </table>
                <script>
                    $(document).ready(function() {
                        $table = $('#risk_table').DataTable({
                            ajax : {
                                "url": 'import/deposit/table?risk=high&date=all',
                                "type": "GET"
                            },
                            order: [[ 3, "desc" ]],
                            rowId: 'tran_id',
                            "fnRowCallback": function (nRow, Data) {

                                (Data['is_new'] === true) ? $(nRow).addClass('success') : false;
                            },
                            dom: 'Bfrtip',
                            "buttons": [
                                {extend: 'excel', text: "export", className: "btn btn-info"},
                                {extend: 'colvis', text: 'Columns'}

                            ],
                            stateSave: true,
                            "initComplete": function(settings, json) {

                                $.each(json.totals.current_month, function (k, v) {
                                    $("#" +k).html(v.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "1,"));
                                });
                            },
                            "columns": [

                                {
                                    "title": "date",
                                    "class" : "hideOverflow",
                                    "data": "date"
                                },
                                {
                                    "class" : "hideOverflow",
                                    "data": "payment_method"},
                                {
                                    "visible": false,
                                    "class" : "hideOverflow",
                                    "data": "clearedby"
                                },
                                {
                                    "data": "tran_id"
                                },
                                {
                                    "data": "customer_id"
                                },
                                {
                                    "class" : "hideOverflow",
                                    "data": "cname"}, ///customer name @todo
                                {
                                    "visible": false,
                                    "class" : "hideOverflow",
                                    "data": "country"
                                },
                                {
                                    "data": "ename"
                                }, //emp name
                                {
                                    "data": "amount"
                                },
                                {
                                    "visible": false,
                                    "data": "inUSD"
                                }, //in_usd
                                {
                                    "data": "currency"
                                },
                                {
                                    "data": "ftd"
                                },
                                {
                                    "visible": false,
                                    "data": "verification_date"},
                                {
                                    "visible": false,
                                    "data": "campaign"
                                },
                                {
                                    "data": "status"
                                },
                                {
                                    "data": "risk"
                                },
                                {
                                    "class": "comment",
                                    "type": "textarea",
                                    "data": "comment"},
                                {
                                    "class" : "hideOverflow",
                                    "data": null,
                                    "defaultContent": DataTableButton([$delete_btn])
                                }


                            ]
                        });
                    });
                </script>
            </div>
        </div>
    </div>