<div class="modal fade" id="released-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Release deposit</h4>
              </div>
              <div class="modal-body">

                  <div class="form-group">
                      <label for="release-amount-text">Release Amount: </label>
                      <input class="form-control" type="text" id="release-amount-text">

                  </div>
                  <div class="form-group">
                      <label for="release-amount-text">Status after case close: </label>
                      <select class="form-control" id="release-status-select">
                          <?php
                          $values = array('full','partial','none','released');
                          $selected = '';
                          foreach($values as $key => $value){
                              if($value == 'released'){
                                  $selected = "Selected='selected'";
                              }
                              echo "<option value='$value' $selected>".ucfirst($value)."</option>";
                          }
                          ?>
                      </select>

                  </div>
                  <div class="form-group">
                      <small><label>For deposit #<span id="tran_id"></span></label></small>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                  <button type="button" id="release" data-value="released" class="btn btn-primary">Release</button>
                <button type="button" id="chargeback" data-value="chargeback" class="btn btn-danger">Chargeback</button>
              </div>
            </div>
    </div>
</div>

 