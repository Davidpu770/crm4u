<?php
namespace Crm4u\Forms\Deposit\currency;


?>
<div class="modal fade" id="currency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-info">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Change currency</h4>
            </div>
            <form method="post" id="change-currency">
            <div class="box-body">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group">
                    <label for="eur">EUR value:
                        <input type="text" id="eur" class="form-control" name="eur" value="">
                    </label>
                </div>

                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="btc">BTC value:
                                <input type="text" id="btc" class="form-control" name="btc" value="">
                            </label>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                    <label for="gbp">GBP value:
                        <input type="text" id="gbp" class="form-control" name="gbp" value="">
                    </label>
                </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-left">
                    <button type="button" id="cget" class="btn btn-success">Live Currency</button>
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit" id="submit_currency">Change Currency</button>
                </form>
            </div>
        </div>
    </div>
    <script>

        $(document).ready(function () {

                $.getJSON('/currency/get',function (res) {

                    console.log(res);
                    $("#eur").val(res['eur']);
                    $("#gbp").val(res['gbp']);
                    $("#btc").val(res['btc']);
                });
        });

        $("#cget").click(function (){

            $.getJSON('/import/currency?live=true', function (data) {


                $("#eur").val(data['EUR']);
                $("#gbp").val(data['GBP']);
                $("#btc").val(data['BTC']);
            });
        });



        $("#change-currency").submitForm({
            'action'     : 'currency',
            'table_name' : 'change',
            'is_simple'  : false
        });

    </script>
</div>

    
        

