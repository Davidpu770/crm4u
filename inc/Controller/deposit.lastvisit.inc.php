<?php
namespace Crm4u\Controller;

  //set local time
    date_default_timezone_set("Asia/Jerusalem"); 

class visit{
    
    static function set_brand_session(){
        
    //set brand array
        global $loader;

        if(!isset($_SESSION['visit_deposit']) && is_array($loader->all_brand)){

            foreach($loader->all_brand['data'] as $key => $value){

                $_SESSION['visit_deposit'][$value['id']] = 0;
                
            }
        }
        
    }
    
    
    /**
    * @var $cBrand for current brand
    *
    **/
    
    function set_visit_time($cBrand){
        
        $_SESSION['visit_deposit'][$cBrand] = date('Y-m-d H:i:s');
        
    }
}