<?php
namespace Crm4u\Controller;

class currency {

    /**
     * @var action
     */
    protected $action;

    /**
     * @var eur value
     */
    protected $eur;

    /*
     * @var gbp value
     */
    protected $gbp;


    function __construct($type, $params){

        $this->action = $params[0];

        switch ($this->action) {

            case "change":

                $this->eur = $_POST['eur'];
                $this->gbp = $_POST['gbp'];
                $this->btc = $_POST['btc'];
        }

        $converter =  json_decode(file_get_contents(dirname(__DIR__).'/../public/js/currenceyConvertor.json',true));

        $method = $this->action;

        if(method_exists(self::class,$method)){

            self::$method($converter);

        } else {

            echo "method {$method} is not exist";
        }

    }

    function get($converter){

        print_r(
            json_encode(
                array(
                    'eur' => $converter->EUR,
                    'gbp' => $converter->GBP,
                    'btc' => $converter->BTC
                )
            )
        );
    }
    
    function change($converter){

        $converter->EUR = (float) $this->eur;
        
        $converter->GBP = (float) $this->gbp;

        $converter->BTC = (float) $this->btc;

        $converter->USD = 1;

        $newConverter = json_encode($converter);
            
        file_put_contents(dirname(__DIR__).'/../public/js/currenceyConvertor.json', $newConverter);

        new SwalController('success','Currency Changed successfuly','success');
        LogController::insert('Currency Changed');
    }
    
    
}