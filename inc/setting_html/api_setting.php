<?php
global $loader;
?>

<div class="api">
    <form method="post" id="api_form">
        <div class="form-group">
            <label>API Address:</label>
            <input type="text" name="api_address" class="form-control" value="<?php echo $loader->brand->api_address?>">
        </div>
        <div class="form-group">
            <label>API Transaction page: <small>(Use %TRAN_ID% to replace transaction id)</small></label>
            <input type="text" name="transaction_page" class="form-control" value="<?php echo $loader->brand->transaction_page?>">
        </div>

        <div class="form-group">
            <label>API Customer page: <small>(Use %CID% to replace customer id)</small></label>
            <input type="text" name="customer_page" class="form-control" value="<?php echo $loader->brand->customer_page?>">
        </div>

        <div class="form-group">
            <label>API Login: </label>
            <input type="text" name="api_username" class="form-control" autocomplete="off" value="<?php echo $loader->brand->api_username?>">
        </div>

        <div class="form-group">
            <label>API Password:</label>
            <input type="password" name="api_password" class="form-control" autocomplete="off" value="<?php echo $loader->brand->api_password?>">
        </div>

        <div class="form-group">
            <button class="btn btn-success" type="submit">Apply</button>
        </div>
    </form>
    <script>

        submitForm("#api_form","edit","brand/<?php echo \Crm4u\Controller\BrandController::current_brand() ?>");
    </script>
</div>