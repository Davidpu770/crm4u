<?php
namespace Crm4u\Pages\Partial\ip_address_table;

?>
<div class="modal fade" id="new-IP-address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add ip to list</h4>
            </div>
            <form method="post" action="insert/ipaddress_list" id="new_ip_address_form">
            <div class="box-body">
                    <div class="form-group">
                        <label>IP-Address</label>
                        <input type="text" name="ip_address" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="flag" required>
                            <option value="1">Allowed</option>
                            <option value="0">Blocked</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Notes</label>
                        <textarea cols="3" rows="3" class="form-control" name="notes"></textarea>
                    </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-success" id="submit"><i class="fa fa-plus"></i> Add IP</button>
            </div>
        </form>
        </div>
    </div>
</div>



<div class="col-md-12">
            <table class="table no-margin" id="ip_list" width="100%">
                <thead>
                    <tr>
                        <th>ip-address</th>
                        <th>update by</th>
                        <th>status</th>
                        <th>notes</th>
                        <th>options</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        <button type="button" class="btn btn-info" id="new_ip-address" data-target="#new-IP-address" data-toggle="modal"title="Adding New IP">
            <i class="fa fa-plus"></i> Add IP
        </button>
    </div>
    <script>
        $(document).ready(function() {

            submitForm("#new_ip_address_form", "ipaddress_list", "insert");

            $table = $('#ip_list').DataTable({

                ajax : 'import/ipaddress_list',
                rowId: 'ID',
                columns: [
                    {"data": "ip_address"},
                    {"data": "update_by"},
                    {"data": "flag"},
                    {"data": "notes"},
                    {
                        "data": null,
                        "defaultContent": $delete_btn
                    }
                ]
            });
        });

        deleteRow(ip_list, 'ipaddress_list', 'ID');
    </script>
</div>