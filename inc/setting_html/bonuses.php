
<div class="modal fade" id="new-bonus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add new Bonus</h4>
            </div>
            <form id="bonus-form" method="post">

                <div class="box-body">
                    <div class="form-group">
                        <label>Bonus type</label>
                        <select name="bonus_type" id='bonus_type' class="form-control">
                            <option value="amount" id="amount">Amount</option>
                            <option value="ftd" id="ftd">FTD</option>
                        </select>
                    </div>

                    <div class="form-group" id='ftd_amount_div' style="display: none">
                        <label>FTD Amount: </label>
                        <input type="number" name="ftd_amount" value="0" id='ftd_amount' class="form-control" disabled="disabled">
                    </div>

                    <div class="form-group" id="strip_max_div">
                        <label>Max Amount: <small>(Leave empty for infinite)</small></label>
                        <input type="number" name="strip_max" id="strip_max" class="form-control">
                    </div>

                    <div class="form-group" id="strip_max_div">
                        <label><input type="checkbox" name="infinite"> To infinity</label>

                    </div>

                    <div class="form-group">
                        <label>Promotion type</label>
                        <select name="promotion_type" id='promotion_type' class="form-control">
                            <option value="percentage" id="percentage">Percentage</option>
                            <option value="fix"        id="fix">Fix Price</option>
                        </select>
                    </div>

                    <div class="form-group" id="strip_percentage_div">
                        <label>Deposit Percentage: </label>
                        <input type="number" name="strip_percentage" id="strip_percentage" class="form-control" >
                    </div>

                    <div class="form-group" id="strip_fix_price_div" style="display: none">
                        <label>Fixed Price: <small> (Bonus to salary)</small> </label>
                        <input type="number" name="strip_fix_price" id="strip_fix_price" class="form-control" >
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" id="submit-desk" name="submit"><i class="fa fa-plus"></i> Add Bonus role</button>
                </div>
            </form>

        </div>
    </div>
</div>
<div class="box">
    <div class="box-header">
        Bonuses strip level
    </div>
    <div class="box-body">

        <div class="col-md-12">
            <h4><strong>Amount Table</strong></h4>
            <table class="table table-striped" id="bonus_amount_table" width="100%">
                <thead>
                    <th>Strip amount</th>
                    <th>Percentage</th>
                    <th>options</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="col-md-12">
            <h4><strong>FTD Table</strong></h4>
            <table class="table table-striped" id="bonus_ftd_table" width="100%">
                <thead>
                    <th>FTD Count</th>
                    <th>Promotion Type</th>
                    <th>Percentage</th>
                    <th>Fix Price</th>
                    <th>options</th>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>

    </div>
    <div class="box-footer">
        <button type="button" class="btn btn-info" id="new_bonus" data-target="#new-bonus" data-toggle="modal"title="Adding New Strip">
            <i class="fa fa-plus"></i> New strip
        </button>
    </div>
    <script>


        submitForm("#bonus-form","bonuses","insert",false);

        $("#bonus_type").change(function () {

            if($("#ftd").is(':selected')){
//todo make it oop
                $("#ftd_amount_div").css("display","");
                $("#ftd_amount").removeAttr("disabled");

                $("#strip_max_div").css("display","none");
                $("#strip_min_div").css("display","none");

                $("#strip_max").attr("disabled","disabled");
                $("#strip_min").attr("disabled","disabled");
            } else {

                $("#ftd_amount_div").css("display","none");
                $("#ftd_amount").attr("disabled","disabled");

                $("#strip_max_div").css("display","");
                $("#strip_min_div").css("display","");

                $("#strip_max").removeAttr("disabled");
                $("#strip_min").removeAttr("disabled");
            }

        });

        $("#promotion_type").change(function () {

            if($("#fix").is(':selected')){
//todo make it oop
                $("#strip_fix_price_div").css("display","");
                $("#strip_fix_price").removeAttr("disabled");

                $("#strip_percentage_div").css("display","none");

                $("#strip_percentage").attr("disabled","disabled");
            } else {

                $("#strip_fix_price_div").css("display","none");
                $("#strip_fix_price").attr("disabled","disabled");

                $("#strip_percentage_div").css("display","");
                $("#strip_percentage").removeAttr("disabled");
            }

        });



        $(document).ready(function() {

           $("#bonus_amount_table").DataTable({
                "ajax" : '/import/bonuses?bonusType=amount',
                rowId: 'id',
                "columns": [
                    {"data": "strip_max"},
                    {"data": "strip_percentage"},
                    {
                        "data": null,
                        "defaultContent": $delete_btn
                    }
                ]

            });

            $("#bonus_ftd_table").DataTable({
                "ajax" : '/import/bonuses?bonusType=ftd',
                rowId: 'id',
                "columns": [
                    {"data": "ftd_amount"},
                    {"data": "promotion_type"},
                    {"data": "strip_percentage"},
                    {"data": "strip_fix_price"},
                    {
                        "data": null,
                        "defaultContent": $delete_btn
                    }
                ]

            });

            deleteRow(bonus_table,'bonuses', 'id');
        })
    </script>
</div>