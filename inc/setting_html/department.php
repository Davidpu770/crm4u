<?php
use Crm4u\Controller\Route;
?>
<div class="modal fade" id="new-department" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add new desk</h4>
            </div>
            <form id="department-form" method="post" action="insert/desk">
                <div class="box-body">
                    <div class="form-group">
                        <label>Department name</label>
                        <input type="text" name="department_name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Department Target</label>
                        <input type="text" name="department_target" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Desk Parent</label>
                        <select class="form-control smart-select" name="desk_parent">
                            <option value="no-select"></option>
                            <?php
                            Route::get('select', 'Models', 'desks', array('desk'));
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Department Manager</label>
                        <select class="form-control smart-select" id="emp_id" name="department_manager">
                            <option value="no-select"></option>
                            <?php
                            Route::get('select', 'Models', 'users', array('user'));
                            ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" id="submit-desk" name="submit"><i class="fa fa-plus"></i> Add Department</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div>
    <div class="table-responsive">
        <table id="department_table" class="table" width="100%">
            <thead>
            <tr>
                <th>#</th>
                <th>Department name</th>
                <th>Department Target</th>
                <th>Desk Parent</th>
                <th># User in department</th>
                <th>Department Manager</th>
                <th>Options</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <button type="button" class="btn btn-info" id="new_campaign" data-target="#new-department" data-toggle="modal"title="Adding New Desk">
        <i class="fa fa-plus"></i> Add Department
    </button>
</div>
<script>

    $(document).ready(function() {


        submitForm("#department-form","departments","insert");

        $table = $("#department_table").DataTable({
            "ajax" : '/import/department',
            rowId: 'id',
            "columns": [
                {"data": "id"},
                {"data": "department_name"},
                {"data": "department_target"},
                {"data": "desk_name"},
                {"data": "user_in_department"},
                {"data": "department_manager"},
                {
                    "data": null,
                    "defaultContent": DataTableButton([$edit_btn,$delete_btn])
                }
            ]

        });
        editRow(department_table, function () {
            window.location.href = "departments/" + $row_id;
        });
        deleteRow(department_table,'departments', 'id');

    });
</script>