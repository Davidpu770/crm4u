<?php
    use Crm4u\Controller\Route;

    $selected_department = $_loadArgs[0];
?>
<form method="post" id="department_form">
    <div class="form-group">
        <label>Department name</label>
        <input type="text" name="department_name" class="form-control" value="<?php echo $selected_department->department_name ?>" required>
    </div>

    <div class="form-group">
        <label>Department Manager</label>
        <select class="form-control" name="department_manager">
            <?php
            Route::get('select', 'Models', 'users', array('user',array('selected', $selected_department->department_manager_id)));
            ?>
        </select>
    </div>

    <div class="form-group">
        <label>Desk Parent</label>
        <select class="form-control smart-select" name="desk_parent">
            <?php
            Route::get('select', 'Models', 'desks', array('desk',array('selected', $selected_department->desk_parent)));
            ?>
        </select>
    </div>

    <div class="form-group">
        <label>Target Type</label>
        <select class="form-control smart-select">
            <option>Static</option>
            <option>Dynamic</option>
        </select>
    </div>
    <div class="form-group text-center">
        <button class="btn btn-success" type="submit">Apply</button>
    </div>
</form>

<script>
    submitForm("#department_form","edit","departments/<?php echo $selected_department->id?>");
</script>