<div class="modal fade" id="new-desk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add new desk</h4>
            </div>
            <form id="desk-form" method="post" action="insert/desk">
                <div class="box-body">
                    <div class="form-group">
                        <label>Desk name</label>
                        <input type="text" name="desk_name" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Desk target</label>
                        <input type="text" name="desk_target" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" id="submit-desk" name="submit"><i class="fa fa-plus"></i> Add Desk</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div>
    <div class="table-responsive">
        <table id="Desk_table" class="table" width="100%">
            <thead>
            <tr>
                <th>#</th>
                <th>Desk name</th>
                <th>User in desk</th>
                <th>Created at</th>
                <th>Options</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <button type="button" class="btn btn-info" id="new_desk" data-target="#new-desk" data-toggle="modal"title="Adding New Desk">
        <i class="fa fa-plus"></i> Add Desk
    </button>
</div>
<script>

    $(document).ready(function() {


        submitForm("#desk-form","desk","insert");




    $table = $("#Desk_table").DataTable({
        "ajax" : 'import/desks',
        rowId: 'id',
        "columns": [
            {"data": "id"},
            {"data": "desk_name"},
            {"data": "userSum"},
            {"data": "created_at"},
            {
                "data": null,
                "defaultContent": DataTableButton([$edit_btn,$delete_btn])
            }
        ]

    });
        editRow(Desk_table, function () {
            window.location.href = "desks/" + $row_id;
        });

    deleteRow(Desk_table,'desk', 'id');

    });
</script>