<?php
/**
 * Copyright (c) 2017. All copyright reserved to David Pugatsch
 */

use Crm4u\Controller\Route;

$selected_desk = $_loadArgs[0];
?>
<form method="post" id="desk_form">
    <div class="form-group">
        <label>Desk name</label>
        <input type="text" name="desk_name" class="form-control" value="<?php echo $selected_desk->desk_name ?>" required>
    </div>

    <div class="form-group">
        <label>Desk Manager</label>
        <select class="form-control" name="desk_manager">
            <?php
            Route::get('select', 'Models', 'users', array('user',array('selected',$selected_desk->desk_manager_id)));
            ?>
        </select>
    </div>

    <div class="form-group">
        <label>Target Type</label>
        <select class="form-control smart-select">
            <option>Static</option>
            <option>Dynamic</option>
        </select>
    </div>
    <div class="form-group text-center">
        <button class="btn btn-success" type="submit">Apply</button>
    </div>
</form>

<script>
    submitForm("#desk_form","edit","desks/<?php echo $selected_desk->id?>");
</script>