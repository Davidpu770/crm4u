<div class="modal fade" id="new-campaign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="box box-warning">
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add new desk</h4>
            </div>
            <form id="campaign-form" method="post" action="insert/desk">
                <div class="box-body">
                    <div class="form-group">
                        <label>Campaign name</label>
                        <input type="text" name="campaign_name" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Campaign type</label>
                        <select name="type" class="form-control">
                            <option value="CPA">CPA</option>
                            <option value="CPL">CPL</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" id="submit-desk" name="submit"><i class="fa fa-plus"></i> Add Desk</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div>
    <div class="table-responsive">
        <table id="campaign_table" class="table" width="100%">
            <thead>
            <tr>
                <th>#</th>
                <th>Campaign name</th>
                <th>Type</th>
                <th>Created at</th>
                <th>Options</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <button type="button" class="btn btn-info" id="new_campaign" data-target="#new-campaign" data-toggle="modal"title="Adding New Desk">
        <i class="fa fa-plus"></i> Add Campaign
    </button>
</div>
<script>

    $(document).ready(function() {


        submitForm("#campaign-form","campaign","insert");




        $table = $("#campaign_table").DataTable({
            "ajax" : '/import/campaign',
            rowId: 'id',
            "columns": [
                {"data": "campaign_id"},
                {"data": "campaign_name"},
                {"data": "type"},
                {"data": "created_at"},
                {
                    "data": null,
                    "defaultContent": $delete_btn
                }
            ]

        });

        deleteRow(campaign_table,'campaign', 'id');

    });
</script>