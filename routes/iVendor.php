<?php

namespace Crm4u\Controller;

interface iVendor {

    const VENDORS = [

        //["vendor name","package name","src"]
        'js' => array(
            ['components','jquery','jquery.js'],
            ['moment','moment','min/moment.min.js'],
            ['components','bootstrap','js/bootstrap.min.js'],
            ['eonasdan', 'bootstrap-datetimepicker','build/js/bootstrap-datetimepicker.min.js'],
            ['almasaeed2010','adminlte','dist/js/app.js'],
            ['components','jqueryui','jquery-ui.js'],
            ['infinety','alerts','src/libraries/sweetalert.js'],
            ['datatables','datatables','media/js/jquery.dataTables.min.js'],
            ['datatables','datatables','media/js/dataTables.bootstrap.js'],
            ['drmonty','datatables-buttons','js/dataTables.buttons.js'],
            ['drmonty','datatables-buttons','js/buttons.bootstrap.js'],
            ['drmonty','datatables-buttons','js/buttons.html5.js'],
            ['drmonty','datatables-buttons','js/buttons.colVis.js'],
            ['stuk','jszip','dist/jszip.min.js'],
            ['pnikolov','bootstrap-daterangepicker','js/daterangepicker.min.js'],
            ['mrohnstock','bootstrap-multiselect','js/bootstrap-multiselect.js'],
            ['select2','select2','dist/js/select2.min.js'],
        ),

        'css' => array(
            ['components','bootstrap','css/bootstrap.min.css'],
            ['eonasdan', 'bootstrap-datetimepicker','build/css/bootstrap-datetimepicker.min.css'],
            ['almasaeed2010','adminlte','dist/css/AdminLTE.min.css'],
            ['components','font-awesome','css/font-awesome.min.css'],
            ['driftyco','ionicons','css/ionicons.min.css'],
            ['components','jqueryui','themes/base/jquery-ui.min.css'],
            ['infinety','alerts','src/libraries/sweetalert.css'],
            ['drmonty','datatables-buttons','css/buttons.bootstrap.css'],
            ['drmonty','datatables-buttons','css/buttons.dataTables.css'],
            ['datatables','datatables','media/css/dataTables.bootstrap.min.css'],
            ['pnikolov','bootstrap-daterangepicker','css/daterangepicker.min.css'],
            ['mrohnstock','bootstrap-multiselect','css/bootstrap-multiselect.css'],
            ['select2','select2','dist/css/select2.min.css'],


        )
    ];

}