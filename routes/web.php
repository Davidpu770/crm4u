<?php
//**
//* Created by PhpStorm.
//* User: David
//* Date: 15\\01\\2017
//* Time: 16:09
//**

namespace Crm4u\Controller;

use Crm4u\Import\user;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Models\Settings;
use Crm4u\SQL\Database;

require_once __DIR__."/iVendor.php";
require_once __DIR__."/iProvidor.php";
require_once __DIR__."/iController.php";

class Route implements iVendor, iProvider
{


    public $db, $mysqli, $class;

    public $loader;


    public $vendor;

    public $user;

    public $brand, $all_brand;

    public $site;


    function __construct(){

        $this->vendor = new \stdClass();

        $this->requires(self::VENDORS);
        //Database
        self::_load('Crm4u\\SQL\\Database');

        $this->db = Database::getInstance();
        $this->mysqli = $this->db->getConnection();

        $this->onLoad();

    }


    function onLoad(){

        $this->_autoload();

        if(CheckAccess::isOnline(false, false)){

            //Import connected user
            $this->user = user::import_user(CheckAccess::connected_user());

            //Import brand and array of all brands (if they are) @todo check with only one brand exist
            $this->brand = BrandController::brand_import($_SESSION['brand']);
            $this->all_brand = BrandController::all_brand();

        }

        //Import site configurations

        $site = new Settings('asArray');
        $this->site = $site->result;

    }

    function head($page_level, $title, $show_header = true, $redirect = true){

        $params = array(
            'page_level' => $page_level,
            'title'       => $title,
            'show_header' => $show_header,
            'redirect'    => $redirect);

        (CheckAccess::isOnline()) ? LogController::insert("Load page ({$title})") : false;

        self::_load('Crm4u\\Pages\\Partial\\Header', $params);


    }

    function requires($vendors){

        foreach ($vendors as $vendor =>  $package) {

            if($vendor == 'js'){

                $this->vendor->js[] = $package;
            }

            if($vendor == 'css'){

                $this->vendor->css[] = $package;
            }


        }
    }

    function footer(){


        self::_load('Crm4u\\Pages\\Partial\\Footer');

    }

    function _load($class_name, $_loadArgs = null){

        return include_once dirname(__DIR__)."/".self::Provider[$class_name];
    }

    function import($class_name){

        //inside \\inc


        if(array_key_exists($class_name, self::Provider)){

            require_once  dirname(__DIR__).self::Provider[$class_name];
        }

    }

    function _autoload() {

        spl_autoload_register(function($class) {

            self::import($class);
        });

    }

    static function get($type, $class, $callback, $params = null){

        $_autoload = "Crm4u\\";

        $call = $_autoload;

        if($class){

            $call .= $class;
        }




        if($callback && strpos($callback,'@')){

            $callback = explode("@",$callback);

            $call .= "\\".$callback[0];
            $call::$callback[1]($params);



        } else {

            if($callback){

                $call .= "\\".$callback;
            }

            if(class_exists($call)){

                new $call($type, $params);
            } else {

                Route::view('404');
            }


        }

    }

    static function view($page, $params = null){

        require dirname(__DIR__) . "/public/view/blade.{$page}.php";
    }

}
$loader = new Route();

if(isset($page_level, $title)){

    $loader->head($page_level, $title);
}

