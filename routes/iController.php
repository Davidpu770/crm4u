<?php

namespace Crm4u\Controller;

interface iController{

    static function add();

    static function edit();

    function delete();
}