<?php

namespace Crm4u\Controller;

interface iProvider{

    const Provider = array(


        //import
        'Crm4u\\Import\\user'                     => '/inc/import/user.php',
        'Crm4u\\Import\\brand'                    => '/inc/import/brand.php',
        'Crm4u\\Import\\risk'                     => '/inc/import/risk.php',
        'Crm4u\\Import\\shift'                    => '/inc/import/shift.php',
        'Crm4u\\Import\\graph'                    => '/inc/import/graph.php',
        'Crm4u\\Import\\months'                   => '/inc/import/months.php',
        'Crm4u\\Import\\department'               => '/inc/import/department.php',

        //controllers

        //-->CRUD
        'Crm4u\\Controller\\InsertController'     => '/app/Http/Controllers/InsertController.inc.php',
        'Crm4u\\Controller\\ImportController'     => '/app/Http/Controllers/ImportController.inc.php',
        'Crm4u\\Controller\\DeleteController'     => '/app/Http/Controllers/DeleteController.inc.php',
        'Crm4u\\Controller\\UpdateController'     => '/app/Http/Controllers/UpdateController.inc.php',


        //-->system
        'Crm4u\\Controller\\FormController'       => '/app/Http/Controllers/FormController.inc.php',
        'Crm4u\\Controller\\ApiController'       => '/app/Http/Controllers/ApiController.inc.php',
        'Crm4u\\Controller\\UserController'       => '/app/Http/Controllers/UserController.inc.php',
        'Crm4u\\Controller\\PasswordController'   => '/app/Http/Controllers/PasswordController.inc.php',
        'Crm4u\\Controller\\ExportController'     => '/app/Http/Controllers/ExportController.inc.php',
        'Crm4u\\Controller\\NotificationController'=> '/app/Http/Controllers/NotificationController.inc.php',
        'Crm4u\\Controller\\BrandController'      => '/app/Http/Controllers/BrandController.inc.php',
        'Crm4u\\Controller\\CronController'       => '/app/Http/Controllers/CronController.inc.php',
        'Crm4u\\Controller\\MailController'       => '/app/Http/Controllers/MailController.inc.php',
        'Crm4u\\Controller\\ReportController'     => '/app/Http/Controllers/ReportController.inc.php',
        'Crm4u\\Controller\\gitController'        => '/app/Http/Controllers/gitController.inc.php',
        'Crm4u\\Controller\\LogController'        => '/app/Http/Controllers/LogController.inc.php',
        'Crm4u\\Controller\\CurrencyController'   => '/app/Http/Controllers/CurrencyController.inc.php',
        'Crm4u\\Controller\\ChargebackController' => '/app/Http/Controllers/ChargebackController.inc.php',

        //-->Shift Mod
        'Crm4u\\Controller\\ShiftController'      => '/app/Http/Controllers/ShiftController.inc.php',


        //-->Binary Option Mod
        'Crm4u\\Controller\\DepositController'    => '/app/Http/Controllers/DepositController.inc.php',
        'Crm4u\\Controller\\AutoDeposit'          => '/mod/auto-deposit/autodeposit.import.php',


        //-->Bonuses Mod
        'Crm4u\\Controller\\BonusController'     => '/app/Http/Controllers/BonusController.inc.php',


        //-->other
        'Crm4u\\Controller\\visit'                => '/inc/Controller/deposit.lastvisit.inc.php',
        'Crm4u\\Controller\\TitleController'      => '/app/Http/Controllers/TitleController.inc.php',
        'Crm4u\\Controller\\SwalController'       => '/app/Http/Controllers/SwalController.inc.php',
        'Crm4u\\Controller\\notification'         => '/inc/Controller/new.notification.php',
        'Crm4u\\Controller\\currency'             => '/inc/Controller/changeCurrencey.inc.php',
        'Crm4u\\Controller\\ExternalApiController'=> '/app/Http/Controllers/ExternalApiController.inc.php',

        //Models
        'Crm4u\\Models\\iModels'                  => '/app/Models/iModels.php',
        'Crm4u\\Models\\Settings'                 => '/app/Models/settings.php',
        'Crm4u\\Models\\desks'                    => '/app/Models/desks.php',
        'Crm4u\\Models\\ipaddress_list'           => '/app/Models/ipaddress_list.php',
        'Crm4u\\Models\\countries'                => '/app/Models/countries.php',
        'Crm4u\\Models\\brands'                   => '/app/Models/brands.php',
        'Crm4u\\Models\\users'                    => '/app/Models/users.php',
        'Crm4u\\Models\\notification'             => '/app/Models/notification.php',
        'Crm4u\\Models\\task'                     => '/app/Models/task.php',
        'Crm4u\\Models\\chargeback'               => '/app/Models/chargeback.php',
        'Crm4u\\Models\\deposit'                  => '/app/Models/deposit.php',
        'Crm4u\\Models\\Withdrawal'               => '/app/Models/withdrawal.php',
        'Crm4u\\Models\\Converter'                => '/app/Models/converter.php',
        'Crm4u\\Models\\Shift'                    => '/app/Models/shift.php',
        'Crm4u\\Models\\Totals'                   => '/app/Models/totals.php',
        'Crm4u\\Models\\Chart'                    => '/app/Models/chart.php',
        'Crm4u\\Models\\risk'                     => '/app/Models/risk.php',
        'Crm4u\\Models\\Leaderboard'              => '/app/Models/leaderboard.php',
        'Crm4u\\Models\\campaign'                 => '/app/Models/campaign.php',
        'Crm4u\\Models\\department'               => '/app/Models/department.php',
        'Crm4u\\Models\\bonuses'                  => '/app/Models/bonus.php',
        'Crm4u\\Models\\customer'                 => '/app/Models/customer.php',
        'Crm4u\\Models\\logs'                     => '/app/Models/log.php',
        'Crm4u\\Models\\currency'                 => '/app/Models/currency.php',
        'Crm4u\\Models\\pages'                    => '/app/Models/pages.php',
        'Crm4u\\Models\\cron'                     => '/app/Models/cron.php',


        //Middleware
        'Crm4u\\Middleware\\CheckAccess'          => '/app/Http/Middleware/CheckAccess.inc.php',
        'Crm4u\\Middleware\\Printer'              => '/app/Http/Middleware/Printer.inc.php',
        'Crm4u\\Middleware\\Fetch'                => '/app/Http/Middleware/Fetch.inc.php',

        //autodeposit
        'Crm4u\\Autodeposit\\import'              => '/mod/auto-deposit/autodeposit.import.php',

        //APIs
        'Crm4u\\Mods\\API\\optionlift'             => '/mod/API/module.optionlift.php',
        'Crm4u\\Mods\\API\\SpotOption'             => '/mod/API/SpotOption/module.spotoption.php',
        'Crm4u\\Mods\\API\\SpotModule'             => '/mod/API/SpotOption/iModule.php',
        'Crm4u\\Mods\\API\\hellomarkets'           => '/mod/API/module.hellomarkets.php',


        //Mods
        'Crm4u\\Mod\\Shift\\Box'                 => '/inc/shift/shift.boxes.php',
        'Crm4u\\Mod\\Shift\\NewShift'            => '/inc/shift/shift.new.php',


        //security
        'Crm4u\\Security\\blockAccess'            => '/app/Http/Middleware/BlockAccess.inc.php',
        'Crm4u\\Security\\IP'                     => '/inc/security/ip_address.inc.php',
        'Crm4u\\Security\\password'               => '/inc/security/resetPassword.inc.php',
        'Crm4u\\Security\\CheckLogin'             => '/app/Http/Middleware/CheckLogin.inc.php',


        //SQL
        'Crm4u\\SQL\\filter'                      => '/app/Database/filter.php',
        'Crm4u\\SQL\\Database'                    => '/app/Database/Database.php',
        'Crm4u\\SQL\\Query'                       => '/app/Database/query.php',

        //Deposits
        'Crm4u\\Deposits\\charts'                 => '/inc/charts.inc.php',
        'Crm4u\\Deposits\\getStatistic'           => '/inc/users/users.statistic.php',
        'Crm4u\\ChargeBack\\ChargeBack'           => '/inc/chargeback/chargeback.inc.php',

        //Pages
        'Crm4u\\Pages\\iPages'                     => 'routes/iPages.php',
        'Crm4u\\Pages\\Partial\\Header'            => 'public/view/layout/header.php',
        'Crm4u\\Pages\\Partial\\Footer'            => 'public/view/layout/footer.php',
        'Crm4u\\Pages\\Partial\\searchBox'         => '/inc/search.php',
        'Crm4u\\Pages\\Partial\\deposit_table'     => '/inc/deposit/deposit.dTable.php',
        'Crm4u\\Pages\\Partial\\new_deposit_table' => '/inc/deposit/deposit.nTable.php',
        'Crm4u\\Pages\\Partial\\new_withdrawal_table' => '/inc/deposit/deposit.nwTable.php',
        'Crm4u\\Pages\\Partial\\withdrawal_table'  => '/inc/deposit/deposit.wTable.php',
        'Crm4u\\Pages\\Partial\\risk_dTable'       => '/inc/risk/risk.dTable.php',
        'Crm4u\\Pages\\Partial\\risk_wTable'       => '/inc/risk/risk.wTable.php',
        'Crm4u\\Pages\\Partial\\Brand_settings'    => '/inc/setting_html/brand_setting.php',
        'Crm4u\\Pages\\Partial\\allowed_ip'        => '/inc/setting_html/ip_address.php',
        'Crm4u\\Pages\\Partial\\Support\\Boxes'    => '/inc/support/support.boxes.php',
        'Crm4u\\Pages\\Partial\\Support\\Risk'     => '/inc/support/support.risk.php',
        'Crm4u\\Pages\\Partial\\Notifications'     => '/inc/notification.php',
        'Crm4u\\Pages\\Partial\\tasks'             => '/inc/tasks.php',
        'Crm4u\\Pages\\Partial\\deposit'           => '/inc/deposit/deposit.main.php',
        'Crm4u\\Pages\\Partial\\withdrawals'       => '/inc/deposit/withdrawals.main.php',
        'Crm4u\\Pages\\Partial\\risk'              => '/inc/risk/risk.main.php',
        'Crm4u\\Pages\\Partial\\mini_dTable'       => '/inc/deposit/deposit.dTable.mini.php',
        'Crm4u\\Pages\\Partial\\Risk\\statistic'   => '/inc/risk/risk.statistic.php',
        'Crm4u\\Pages\\Partial\\Location'          => '/inc/setting_html/locations.php',
        'Crm4u\\Pages\\Partial\\Campaigns'         => '/inc/setting_html/campaigns.php',
        'Crm4u\\Pages\\Partial\\Department'           => '/inc/setting_html/department.php',
        'Crm4u\\Pages\\Partial\\api_settings'         => '/inc/setting_html/api_setting.php',
        'Crm4u\\Pages\\Partial\\departmentPreferences'=> '/inc/setting_html/department.preferences.php',
        'Crm4u\\Pages\\Partial\\deskPreferences'      => '/inc/setting_html/desk.preferences.php',
        'Crm4u\\Pages\\Partial\\bonuses'              => '/inc/setting_html/bonuses.php',
        'Crm4u\\Pages\\Partial\\Open_chargeback'      => '/inc/deposit/table.open_chargeback.php',
        'Crm4u\\Pages\\Partial\\Close_chargeback'      => '/inc/deposit/table.close_chargeback.php',
        'Crm4u\\Pages\\Partial\\New_chargeback'      => '/inc/deposit/form.new_chargeback.php',


        'Crm4u\\Pages\\Statistic\\UserPage'        => '/public/view/statistic/blade.users.php',
        'Crm4u\\Pages\\Statistic\\Accounting'      => '/public/view/statistic/blade.accounting.php',
        'Crm4u\\Pages\\Statistic\\Campaign'        => '/public/view/statistic/blade.campaign.php',

        'Crm4u\\Pages\\testPage'                   => '/fixcampaign.php',
        'Crm4u\\Pages\\404'                   => '/public/view/blade.404.php',

        //implements
        'Crm4u\\Cron\\iJobs'                       => '/app/Http/Controllers/iJobs.php',
        'Crm4u\\Export\\iExport'                   => '/app/Models/iExport.php',

        //Layouts
        'Crm4u\\Layout\\info'                     => '/public/view/layout/header.inc.php',

        //graph
        'Crm4u\\Graph\\WeeklyTarget'              =>'/inc/graph/graph.weeklyTarget.php',

        //Forms
        'Crm4u\\Forms\\Chargeback\\close_case'     => '/inc/chargeback/chargeback.close.php',
        'Crm4u\\Forms\\Users\\Dulicate_user'       => '/inc/users/users.duplicate.php',
        'Crm4u\\Forms\\Users\\target'              => '/inc/users/users.target.php',
        'Crm4u\\Forms\\Users\\reset_password'      => '/inc/users/users.resetPassword.php',
        'Crm4u\\Forms\\Deposit\\Edit'              => '/inc/deposit/deposit.edit.php',
        'Crm4u\\Forms\\Deposit\\insert'            => '/inc/deposit/deposit.new.php',
        'Crm4u\\Forms\\Deposit\\currency'          => '/inc/currency.php',
        'Crm4u\\Forms\\Users\\new_user'            => '/inc/users/users.new.php',
    );
}