<?php
/**
 * Copyright (c) 2017. All copyright reserved to David Pugatsch
 */

namespace Crm4u\Controller;
use AltoRouter;
use Crm4u\Controller\Route;
use Crm4u\Models\pages;

require_once dirname(__DIR__) . "/routes/web.php";
require_once dirname(__DIR__) . "/vendor/autoload.php";



$router = new AltoRouter();


$pages = new pages('asArray', array());

foreach ($pages->result['data'] as $key => $value){

    $router->map($value['method'], "/".$value['name'], function() use ($value){

        Route::view($value['page_name']);
    });

}

$router->addRoutes(
    array(


        array('POST','/git/[:action]', function($action) {

            Route::get('post','Controller','gitController',array($action));
        }),

        /**
         * LOGIN PAGE
         */
        array('POST','/login', function() {
            Route::get('json', 'Security', 'CheckLogin');
        }),

        array('GET','/login', function() {
            require 'public/view/login/blade.login.php';
        }),

        /**
         * DASHBOARD PAGE
         */
        array('GET','/', function() {

            Route::view('homepage');
        }),

        /**
         * USERS PAGE
         */

        array('GET','/users/[i:id]', function($id) {

            Route::view('users', array($id));
        }),

        array('POST','/users/[i:id]/edit', function($id) {

            Route::get('post','Controller','UserController@edit', array($id, $_POST));
        }),

        /**
         * DEPARTMENT PAGE
         */
        array('GET','/departments/[i:id]', function($id) {

            Route::view('departments', array($id));
        }),

        array('POST','/departments/[i:id]/edit', function($id) {

            Route::get('post','Controller','FormController@edit', array('departments', $id, $_POST));
        }),

        /**
         * DESK PAGE
         */
        array('GET','/desks/[i:id]', function($id) {

            Route::view('desks', array($id));
        }),

        array('POST','/desks/[i:id]/edit', function($id) {

            Route::get('post','Controller','FormController@edit', array('desk', $id, $_POST));
        }),

        /**
         * TEST NOTIFICATION PAGE
         */
        array('GET','/testNotification', function() {

            Route::view('notificationSend');
        }),

        array('POST','/send/notification', function() {

            Route::get('post','Controller','NotificationController@manualSend', array($_POST['title'], $_POST['message'], $_POST['emp_id'],'deposit'));
        }),

        /**
         * BRAND PAGE
         */
        array('POST','/brand/[i:id]/edit', function($id) {

            Route::get('post','Controller','BrandController@edit', array($id, $_POST));
        }),

        /**
         * STATISTIC PAGE
         */
        array('GET','/statistic/worker', function() {

            Route::get('get','Pages','Statistic\UserPage');
        }),

        array('GET','/statistic/accounting', function() {

            Route::get('get','Pages','Statistic\Accounting');
        }),

        array('GET','/statistic/campaign', function() {

            Route::get('get','Pages','Statistic\Campaign');
        }),

        array('GET','/autodeposit', function() {

            Route::get('json', 'Controller', 'AutoDeposit');
        }),

        array('GET','/autodeposit/[:module]', function($module) {

            Route::get('json', 'Controller', 'AutoDeposit', array($module));
        }),

        array('GET','/autodeposit/cron', function() {

            require dirname(__DIR__) . '/../mod/auto-deposit/autodeposit.cron.php';
        }),

        array('GET','/logout', function() {

            Route::get('json', 'Security', 'CheckLogin@logout');
        }),

        array('GET','/crm_api/[:action]/[:id]', function($action, $id) {

            Route::get('json','Controller','ExternalApiController', array($id, $action, $_GET));
        }),

        array('GET|POST','/currency/[:action]', function($action) {

            Route::get('json','Controller', 'currency', array($action, $_POST));
        }),


        //Import form DB
        array('GET','/import/[:action]', function($action) {

            Route::get('json', 'Models', $action, array($action));
        }),

        //Import from DB with spasific format
        array('GET','/import/[:action]/[:format]', function($action, $format) {

            Route::get($format, 'Models', $action, array($action, $format));
        }),

        //Import form DB with ID parameter
        array('GET','/import/[:action]/[:format]/[i:id]', function($action, $format, $id) {

            Route::get($format, 'Models', $action, array($action, $format, $id));
        }),

        //Insert to DB
        array('POST','/insert/[:action]', function($action) {

            Route::get('post','Controller','InsertController', array($action, $_POST));
        }),

        //update DB
        array('POST','/update/[:action]/[:pk]/[i:id]', function($action, $pk, $id) {

            Route::get('post','Controller','UpdateController', array($action, $pk, $id, $_POST));
        }),

        //Delete from DB
        array('POST','/delete/[:table]/[i:id]/[:pk]', function($table, $id, $pk) {

            Route::get('post','Controller', 'DeleteController', array($table, $id, $pk, $_POST));
        }),

        //change brand
        array('GET','/brand/change/[:brand]', function($brand) {

            Route::get('get', 'Controller', 'BrandController@changeBrand', array($brand));
        }),


        //Exporting
        array('GET','/export/[:table]', function($table){

            Route::get('get','Controller','ExportController',array($table, $_GET));
        }),

        //CRON JOB
        array('GET','/cron/[:password]', function($password) {

            Route::get('get', 'Controller', 'CronController@init', array($password));
        }),

        //TESTING
        array('GET','/test', function() {

            Route::get('get', 'Pages', 'testPage');
        }),

    )
);


// match current request url
$match = $router->match();


// call closure or throw 404 status
if( $match && is_callable( $match['target'] ) ) {
    call_user_func_array( $match['target'], $match['params'] );
} else {
    // no route was matched
    Route::get('get', 'Pages', '404');
}
