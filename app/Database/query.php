<?php

namespace Crm4u\SQL;

class Query {

    static function init($sql){

        global $loader;

        $db = \Crm4u\SQL\Database::getInstance();
        $mysqli = $db->getConnection();

        return $mysqli->query($sql);

    }
}