<?php

namespace Crm4u\SQL;

interface iFilters {

    const FILTERS = array(
        'type'         => '',
        'text'         => '',
        'startdate'    => '',
        'enddate'      => '',
        'date'         => '',
        'all_time'     => '',
        'payment'      => '',
        'risk'         => '',
        'risk_status'  => '',
        'shift'        => '',
        'emp_name'     => '',
        'main_brand'   => '',
        'brand_filter' => '',
        'brand'        => '',
        'user_id'      => '',
        'emp_id'       => '',
        'search'       => '',
        'desk'         => '',
        'full'         => '',
        'partial'      => '',
        'none'         => '',
        'only_ftd'     => '',
        'limit'        => '',
        'department'   => '',
        'deskParent'   => '',
        'bonusType'    => '',
    );

}