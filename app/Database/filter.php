<?php

namespace Crm4u\SQL;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\FormController;
use Crm4u\Controller\Route;
use Crm4u\Import\months;
use Crm4u\Import\risk;
use Crm4u\Import\user;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Models\Converter;

//load file
require_once __DIR__ . "/iFilters.php";

class filter implements iFilters {

    /**
    * The table name
    * @var table(string)
    * @default "goal"
    **/
    public $table;

    
    /**
    * @var bool (1,0)
    * if has error
    * @default 0
    **/
    public $hasError = 0;
    
    
    
    
    /**
    * @var string
    * the error message
    **/
    public $error = '';


    /**
     * @var array
     * the requested filter
     */
    protected $filters = array();


    /**
     * @var array
     * the requested session array
     */
    protected $session = array();

    /**
     * @var array
     * the convertor as json array
     */
    protected $convertor = array();

    /**
    * cunstruct
    * This cunstuvet the table name
    **/


    function __construct($table = 'goal'){

        $this->table = $table;

        $this->filters = new \stdClass();
        $this->session = new \stdClass();

        //import currency
        $this->convertor = json_decode(file_get_contents(dirname(__DIR__) . "/../public/js/currenceyConvertor.json"), TRUE);

        if(!isset($_SESSION)){
            session_start();
        }

        //get an array with all filters
        $this->getFilters();
    }


    function getFilters() {

        foreach (self::FILTERS as $key => $value) {

            if(key_exists($key, $_GET) && !empty($_GET[$key])) {
                $this->filters->$key = $_GET[$key];

            }

            if(key_exists($key, $_SESSION)) {

                $this->session->$key = $_SESSION[$key];
            }
        }

    }
    
    
    
    
    /**
    * @conditions_function add a spacific query to
    * the function. 
    * @example brand filter add: " AND brand="{the selected brand}" ".
    **/
    
    /**
    * @conditions_function
    * if its not the main brand then import info only
    * from the current brand. 
    * $brand = the current brand; $_SESSION['main_brand'] = the flag (1,0).
    * @int $filter if $filter(arg) is 1 the condition are always be true;
    **/
    function brandFilter($filter){

        if((isset($this->session->main_brand) && $this->session->main_brand == 0) || $filter == 1 || (isset($this->filters->brand_filter) && $this->filters->brand_filter == "on")) {
            
            $filter = " AND ".$this->table.".brand = '".BrandController::current_brand(). "'";
            return $filter; 
        }
        
    }
    
    
     
    
    /**
    * @conditions_function
    * filter by date
    * $month = the month in "M" format; $_GET['month'] = the month in get
    * @int $filter if $filter(arg) is 1 the condition are always be true;
    **/
    function dateFilter($filter, $period = 'month', $date_column = 'date') {
    
        global $loader;

        $month = date('M');
        $year = date('y');

        if(isset($this->filters->date)){

            return null;
        }

        if($filter > 0 && !isset($this->filters->date) && !(isset($this->filters->all_time))) {

            if(isset($_GET['month'])){

                //include month list

                $months = new months();

                if(in_array($_GET['month'], $months->months)) {           
                   $month = $_GET['month'];   
                }
            }
            if(isset($_GET['year'])){
                $year = mysqli_real_escape_string($loader->mysqli,$_GET['year']);
            }

            $filter = self::datePeriodFilter($period, $date_column);

            if((isset($this->filters->startdate) && $this->filters->startdate != '') && (isset($this->filters->enddate) && $this->filters->enddate != '')){

                $startdate = mysqli_real_escape_string($loader->mysqli,$this->filters->startdate)." 00:00:00";
                $enddate   = mysqli_real_escape_string($loader->mysqli,$this->filters->enddate)  ." 23:59:59";

                $filter = " AND ".$this->table.".".$date_column." BETWEEN '".$startdate."' AND '".$enddate."'";


            }

            return $filter;
        }
    }

    function datePeriodFilter($period, $date_column){

        switch ($period){

            case 'year':
                return " AND {$this->table}.{$date_column} BETWEEN '".date('Y-01-01')."' AND '".date('Y-12-31')."'";

            case 'month':
                return " AND {$this->table}.{$date_column} BETWEEN '".date('Y-m-01 00:00:00')."' AND '".date('Y-m-31 23:59:59')."'";

            case 'day':
                return " AND {$this->table}.{$date_column} BETWEEN '".date('Y-m-d 00:00:00')."' AND '".date('Y-m-d 23:59:59')."'";

            case 'week':
                return " AND {$this->table}.{$date_column} BETWEEN '".date("Y-m-d", strtotime('monday this week'))."' AND '".date("Y-m-d", strtotime('Saturday this week'))."'";
        }
    }
     
    
    function onlineFilter($filter){

        if($filter == 1){

            $filter = " AND {$this->table}.active = 1";

            return $filter;
        }
    }
    /**
 * @conditions_function
 * filter by type
 * $type = cid or tranid
 * @int $filter if $filter(arg) is 1 the condition are always be true;
 **/
    function typeFilter($filter){

        global $loader;

            if($filter == 1 && isset($this->filters->type)){

                if($this->filters->type == "tran_id" && !empty($this->filters->text)){

                    $tran_id = $this->filters->text;
                    $filter = " AND ".$this->table.".tran_id LIKE '%$tran_id%'";

                    return $filter;
                }

                if($this->filters->type == "cid" && !empty($this->filters->text)){

                    $cid = mysqli_real_escape_string($loader->mysqli, $this->filters->text);
                    $filter = " AND ".$this->table.".customer_id LIKE '%$cid%'";

                    return $filter;
                }



            }

    }

    function bonusFilter($filter){

        if($filter == 1 && isset($this->filters->bonusType)){

            if($this->filters->bonusType == "amount" || $this->filters->bonusType == "ftd"){


                $filter = " AND ".$this->table.".bonus_type = '{$this->filters->bonusType}'";

                return $filter;
            }

        }

    }


    /**
     * this function filter deposit result by department
     * @param $filter
     * @param $department_id called when no $_GET is set and is value is not false
     */
    function departmentFilter($table_name, $filter, $department_id = false){

        if($filter == 1){
            $filter = " AND ".$table_name.".department = ";

            if(isset($this->filters->department)){

                 $filter .= $this->filters->department;
                return $filter;
            }

            if($department_id && !isset($_GET['department'])){

                $filter .= $department_id;
                return $filter;
            }
        }

        return false;
    }

    /**
     * @param $campaign
     * @return string
     *
     * filter by Campaign
     */
    function campaignFilter($campaign){

        global $loader;

            $campaign = mysqli_real_escape_string($loader->mysqli, $campaign);
            $filter = " AND goal.campaign = '".$campaign."'";

            return $filter;
    }

    public function __set($name, $value)
    {
        $this->filters->$name = $value;
    }


    /**
     * @conditions_function
     * filter by shift
     * $shift = morning, night and etc.
     * @int $filter if $filter(arg) is 1 the condition are always be true;
     * @return mixed
    **/
    function shiftFilter($filter, $table_name = 'users', $shift_id = false){

        if($filter == 1){

            $filter = " AND {$table_name}.shift = ";

            if(isset($this->filters->shift)){

                $filter .= $this->filters->shift;
                return $filter;
            }

            if($shift_id && !isset($_GET['shift'])) {

                $filter .= $shift_id;

                return $filter;
            }

        }

        return false;
    }

    /**
     * @conditions_function
     * filter by desk
     * $shift = morning, night and etc.
     * @int $filter if $filter(arg) is 1 the condition are always be true;
     **/
    function deskFilter($filter, $table_name = 'users', $desk_id = false){

            if($filter == 1){

                $filter = " AND {$table_name}.location = ";

                if(isset($this->filters->desk)){

                    $filter .= $this->filters->desk;
                    return $filter;
                }

                if($desk_id && !isset($_GET['desk'])){

                    $filter .= $desk_id;

                    return $filter;
                }
            }

            return false;

        }


    
    
    function countriesFilter($filter){

        global $loader;
        
        $country = '';
        
        if($filter == 1 && isset($_GET['country'])){
            
            $country = mysqli_real_escape_string($loader->mysqli, $_GET['country']);
            $filter = " AND country = '".$country."'";
            
            return $filter;
        }
    }
    
    
    
    /**
    * @conditions_function
    * if the user is simple user witout
    * permission.
    * @int $_SESSION['priv'] = the priv(int).
    * @int $filter if $filter(arg) is 1 the condition are always be true;
    **/
    function userFilter($filter, $user_column  = 'emp_id'){

        global $loader;

        $filter = false;

        if(strpos($_SERVER['REQUEST_URI'],"users")){

            $user = explode("/",$_SERVER['REQUEST_URI']);
            $emp_id = $user[2];
            $filter = true;

        } else {

            if(isset($this->filters->emp_id) && $this->filters->emp_id != "no-select"){

                $emp_id = $this->filters->emp_id;
                $filter = true;
            }

            if(CheckAccess::isOnline(false, false) && !CheckAccess::isAuthorized(2)) {

                $emp_id = $loader->user->user_key_id;
                $filter = true;
            }


        }

        if($filter){

            $filter  =  " AND ".$this->table.".".$user_column." = ".$emp_id;

            return $filter;
        }

    }


    /**
     * @param $filter
     */
    function waitingFilter($filter){

        if($filter == 1){

            $filter = " AND waiting_verification = 1";
            return $filter;
        }

    }

    /**
     * Filter by withdrawal/deposit
     */

    function depositTypeFilter($type){

        $filter = " AND type='{$type}'";
        return $filter;

    }
    
    function limitFilter(){

        if(isset($this->filters->limit) && $this->filters->limit > 0){

            $filter = " LIMIT {$this->filters->limit}";

        } else {

            $filter = " LIMIT 500";
        }

        return $filter;
    }
    
    /**
    * @conditions_function
    * limit result
    * $limit = how much result to show
    * @var $limit
    * @default disable
    **/
    function limitResult($limit){
    
        if(isset($limit) && $limit > 0) {
                
                $filter = " LIMIT ".$limit;
                return $filter; 
            }  
        
    }

    static function Sum($is_case_when = false, $case_when = false, $sql_column, $row_name){

        $filter = "SUM( ";



        if($is_case_when){

            $filter .= $case_when;
        } else {

            $filter .= $sql_column." ";
        }


        $filter .= ") ";

        $filter .= $row_name;

        return $filter;
    }


    static function case_when($case_when_column, $case_when_statment, $case_when_then, $case_when_else = false, $is_in_sum = false){

        $filter = "CASE WHEN (".$case_when_column." ".$case_when_statment.") THEN '".$case_when_then."'";

        if($case_when_else) {

            $filter .= " ELSE '".$case_when_else."'";

        } else {

            $filter .= " ELSE ".$case_when_column;
        }

        $filter .= " END ";

        $name = explode(".",$case_when_column);

        if(!$is_in_sum){

            $filter .= $name[1];
        }


        return $filter;
    }
    
    
    
    /**
    * @conditions_function
    * only ftd deposits
    * @require $ftd
    * @var $ftd (bool) default: null option: 1 or 0;
    * @default disable
    **/
    function ftdFilter($filter){

        if($filter == 1 || isset($this->filters->only_ftd)){

                $filter = "AND ".$this->table.".ftd = 1";

                return $filter;
        }
    }
    
    function riskFilter($filter){
        
        global $loader;

        $filters = '';

        if($filter == 1 && isset($this->filters->risk)){
            
            $filters = " AND {$this->table}.risk = '{$this->filters->risk}' ";

        }
        
        if($filter == 1 && isset($this->filters->risk_status)){

            $filters .= " AND {$this->table}.risk_status = '{$this->filters->risk_status}' ";
        
        }

        return $filters;
        
    }

    function deskParentFilter($filter){

        global $loader;

        $filter = '';

        if($filter == 1 || isset($this->filters->deskParent)) {

            $desk_parent =  mysqli_real_escape_string($loader->mysqli, $_GET['deskParent']);

            $filter .= " AND ".$this->table.".desk_parent = {$desk_parent} ";
        }
        return $filter;
    }

    function paymentFilter($filter){

        global $loader;

        $filter = '';


        if($filter == 1 || isset($this->filters->payment)) {

            $payment =  mysqli_real_escape_string($loader->mysqli, $_GET['payment']);

            $filter .= " AND ".$this->table.".payment_method = '".$payment."' ";
        }
        return $filter;
    }
    
    
    /**
    * @conditions_function
    * only ftd deposits
    * @require $ftd
    * @var $ftd (bool) default: null option: 1 or 0;
    * @default disable
    **/
    function statusFilter($filter){

        global $loader;

        //reset
        $filter = "";

        //get status


        $status = array(
            'full'    => (isset($this->filters->full))    ? true : false,
            'partial' => (isset($this->filters->partial)) ? true : false,
            'none'    => (isset($this->filters->none))    ? true : false,
        );

        if(is_array($status) && in_array(true, $status)) {

            $filter = " AND (";

                $i = 0;
                foreach($status as $key => $value){

                    if($status[$key]){
                        if(count(array_filter($status)) > 1 && $i > 0){

                            $filter .= " OR ";
                        }

                        $filter .= " ".$this->table.".status = '".$key."'";
                        $i++;


                    }
                }
                //print $filter;
            $filter .= ")";
            }
            return $filter;
        }
    
    
    
    /**
    * @var $silent_status is "regular" or "silent"
    * @var $only_for default is: 0 nobody can see. 1 just admin. 2 everyone.
    * @var $filter is 1 or 0;
    **/
    function silentFilter($sql, $filter, $silent_status){
    
    	$inner_join = $this->innerJoin("goal", "users");
    	
    	if(strpos($sql, $inner_join)){	
    	
	    	$ave_silent_status = array("regular","silent");
	    
	    	if($filter == 1 && isset($filter) && in_array($silent_status, $ave_silent_status)){
	    		
	    		$filter = " AND users.silent_emp = '". $silent_status . "'";
	    		
	    		return $filter;
	    	}
	    } 
	    
    }
    
    
    
    /**
    * @query_function is a short way to add a big query
    * each time. 
    **/
    
    
    /**
    * @query_function
    * import deposit based on currency filter
    * @str $filter;
    **/
    static function currencyFilter(

        $convert = array(
            'all',
            'confirm',
            'with_cb',
            'risk',
            'ftd'
        ),
        $type = 'deposit',$table = 'goal'
    ){
        
        $currencies = array('USD','EUR','GBP','BTC');
        


        $filter = "";
        /**
        * all deposit
        * converted
        **/
        
        if(in_array('all',$convert)){

            $i = 0;
            foreach($currencies as $currency){

                if($i >= 1){

                    $filter .= ", ";
                }
            $filter  .= "SUM(CASE WHEN $table.type = '$type' AND $table.currency = '$currency'";
            $filter  .= " THEN $table.amount ELSE '0' END) * ".Converter::$currency()." as $currency";

            $i++;
            }
        }
        
        
        
        /**
        * confirm deposit of current month
        * converted
        **/
        if(in_array('confirm',$convert)){
            
            foreach($currencies as $currency){
                
                $filter .= ", SUM(CASE WHEN ($table.status = 'full' AND $table.currency = '$currency')";
                $filter .= " OR ($table.status = 'released' AND $table.currency = '$currency') AND";
                $filter .= " waiting_verification IS NULL";
                $filter .= " THEN $table.amount ELSE '0' END) * ".Converter::$currency();
                $filter .= " as conf".$currency."deposits";
            }

        }
        
        
        
        
        /**
        * withdrawals and cb
        * converted
        **/
        if(in_array('with_cb',$convert)){
            
            foreach($currencies as $currency){
            
                $filter .= ", SUM(CASE WHEN ($table.type = 'withdrawal')";
                $filter .= " AND $table.currency = '$currency' THEN $table.amount";
                $filter .= " ELSE 0 END) * ".Converter::$currency()." as ".$currency."Withdrawals";
                
                $filter .= ", SUM(CASE WHEN $table.type = 'Charge Back'";
                $filter .= " AND $table.currency = '$currency' THEN $table.amount";
                $filter .= " ELSE 0 END) * ".Converter::$currency()."  as ".$currency."chargeBack";
            }
        }
        
        /**
        * win lose open on cb
        * converted
        **/
        if(in_array('cb',$convert)){

            foreach($currencies as $currency){

                $filter .= ", SUM(CASE WHEN ($table.closed = '0') AND $table.currency = '$currency' THEN $table.amount ELSE 0 END) * ".Converter::$currency()." as ".$currency."open";

                $filter .= ", SUM(CASE WHEN ($table.result = 'won') AND $table.currency = '$currency' THEN $table.amount ELSE 0 END) * ".Converter::$currency()." as ".$currency."Win";

                $filter .= ", SUM(CASE WHEN ($table.result = 'lost') AND $table.currency = '$currency' THEN $table.amount ELSE 0 END) * ".Converter::$currency()." as ".$currency."lose";
            }
        }

        /**
         * Risk amount
         * converted
         */
        if(in_array('risk',$convert)){

            $risk = new risk();
            $risk->import(0);

            foreach($currencies as $currency){

                foreach ($risk->risks['risks'] as $level){

                    $filter  .= ", SUM(CASE WHEN $table.risk = '".$level['risk_name']."' AND $table.currency = '$currency'";
                    $filter  .= " THEN $table.amount ELSE '0' END) * ".Converter::$currency()." as ".$level['risk_name']."_".$currency." ";

                }
            }

        }

        if(in_array('ftd',$convert)){

            foreach($currencies as $currency){

                $filter .= ", SUM(CASE WHEN ($table.ftd = '1') AND $table.currency = '$currency' THEN $table.amount ELSE 0 END) * ".Converter::$currency()." as ".$currency."ftd";
            }
        }

        
        return $filter;
    }
    
    
    
    
    /**
    * @query_function
    * count deposit based on filter
    * @str $filter;
    *
    **/
    function countRow($count = array('all','confirm','not_confirm','cb','wd')){
    
        $filter = "";
        if(in_array('confirm',$count)){
            $filter  .= " COUNT(CASE WHEN goal.type= 'deposit' AND goal.status = 'full' THEN goal.amount END) as sumconfdeposits";
        }
        
        if(in_array('not_confirm',$count)){
            $filter .= ", COUNT(CASE WHEN goal.type= 'deposit' AND goal.status = 'none' OR goal.status = 'partial' THEN goal.amount END) as notverifed";
            
        }
        
        if(in_array('all',$count)){    
            $filter .= ", COUNT(CASE WHEN goal.type = 'deposit' THEN goal.amount END)
        - COUNT(CASE WHEN goal.type= 'deposit' AND goal.status = 'released' THEN goal.amount END)as sumdeposits";
            
        }
        
        if(in_array('cb',$count)){    
            $filter .= ", COUNT(CASE WHEN goal.type = 'Charge Back' THEN goal.amount END) as sumCharge";
            
        }
        
        if(in_array('wd',$count)){
            $filter .= ", COUNT(CASE WHEN goal.type = 'withdrawal' THEN goal.amount END) as sumWithd";
        
        }
        
        if(in_array('win',$count)){
            
            $filter .=" COUNT(CASE WHEN $this->table.result = 'won' THEN $this->table.result END) winCount";
            
        }
        
        if(in_array('lose',$count)){
            
            $filter .=", COUNT(CASE WHEN $this->table.result = 'lost' THEN $this->table.result END) loseCount";
            
        }
        
        if(in_array('open',$count)){
            
            $filter .=", COUNT(CASE WHEN $this->table.closed = '0' THEN $this->table.result END) openCount";
            
        }
        
        return $filter;
    }
    
    /**
    * @query_function
    * count ftd based on filter
    * @str $filter;
    **/
    static function countFTD(){
        
        $filter = " COUNT(CASE WHEN goal.ftd = 1 THEN goal.ftd END) as countftd";
        
        return $filter;
    }

    /**
     * @query_function
     * count ftd based on filter
     * @str $filter;
     * @todo @array $req; the requested count
     **/
    static function countType($type){

        $filter = " COUNT(CASE WHEN goal.type = '{$type}' THEN goal.type END) as count{$type}";

        return $filter;
    }

    static function countConfirm($is_approve = true){

        if($is_approve){

            $filter = " COUNT(CASE WHEN goal.status = 'full' AND goal.type = 'Deposit' THEN goal.status END) as countConfirmFull";
        } else {

            $filter = " COUNT(CASE WHEN goal.status = 'partial' or goal.status = 'none' THEN goal.status END) as countConfirmNone";
        }


        return $filter;
    }


    /**
     * @query_function
     * count customers based on filter
     * @str $filter;
     * @todo @array $req; the requested count
     **/
    function countCustomers(){

        $filter = " COUNT(DISTINCT goal.customer_id) as countCustomers";

        return $filter;
    }


    
      /**
    * @query_function
    * add where if missing
    * @str $html;
    * Exepet $sql string.
    **/
    function fixWhere($sql){
        
        if(!strpos($sql, "WHERE")){
            
            return $html = " WHERE 1=1 ";
            
        }
        
    }
    
    
     /**
    * @query_function
    * Inner join tables
    * @str $filter;
    * Exepet $main_table as the main and $join_table, both as string.
    **/
    function innerJoin($main_table, $join_table){
        
        if($join_table == "users") {
            
            $filter  = " INNER JOIN ".$join_table." ON ";

            $filter .=  $main_table.".emp_id = ".$join_table.".user_key_id ";

            $filter .= " AND ".$main_table.".brand = "    .$join_table.".brand";
        }
        
        if($join_table == "goal") {
            
            $filter  = " INNER JOIN ".$join_table." ON ";

            $filter .=         $main_table.".firstname = ".$join_table.".firstname ";

            $filter .= " AND ".$main_table.".lastname = " .$join_table.".lastname";

            $filter .= " AND ".$main_table.".brand = "    .$join_table.".brand";
        }
        
        if($join_table == "brands"){
            
            $filter  = " INNER JOIN ".$join_table." ON ";
            
            $filter .= $main_table.".brand = ".$join_table.".brand_name";
            
        }
        
        return $filter;
    }
    
    /**
    * @query_function
    * left join tables
    * @str $filter;
    * Exepet $main_table as the main and $join_table, both as string.
    **/
    function leftJoin($main_table, $join_table){
        
        if($join_table == "users") {
            
            $filter  = " LEFT JOIN ".$join_table." ON ";

            $filter .=         $main_table.".firstname = ".$join_table.".firstname ";

            $filter .= " AND ".$main_table.".lastname = " .$join_table.".lastname";

            $filter .= " AND ".$main_table.".brand = "    .$join_table.".brand";
        }
        
        if($join_table == "goal") {
            
            $filter  = " LEFT JOIN ".$join_table." ON ";

            $filter .=         $main_table.".firstname = ".$join_table.".firstname ";

            $filter .= " AND ".$main_table.".lastname = " .$join_table.".lastname";

            $filter .= " AND ".$main_table.".brand = "    .$join_table.".brand";
        }
        
        if($join_table == "brands"){
            
            $filter  = " LEFT JOIN ".$join_table." ON ";
            
            $filter .= $main_table.".brand = ".$join_table.".brand_name";
            
        }
        
        return $filter;
    }
    
    /**
    * @query_function
    * right join tables
    * @str $filter;
    * Exepet $main_table as the main and $join_table, both as string.
    **/
    function rightJoin($main_table, $join_table){
        
        if($join_table == "users") {
            
            $filter  = " RIGHT JOIN ".$join_table." ON ";

            $filter .=         $main_table.".firstname = ".$join_table.".firstname ";

            $filter .= " AND ".$main_table.".lastname = " .$join_table.".lastname";

            $filter .= " AND ".$main_table.".brand = "    .$join_table.".brand";
        }
        
        if($join_table == "goal") {
            
            $filter  = " RIGHT JOIN ".$join_table." ON ";

            $filter .=         $main_table.".firstname = ".$join_table.".firstname ";

            $filter .= " AND ".$main_table.".lastname = " .$join_table.".lastname";

            $filter .= " AND ".$main_table.".brand = "    .$join_table.".brand";
        }
        
        if($join_table == "brands"){
            
            $filter  = " RIGHT JOIN ".$join_table." ON ";
            
            $filter .= $main_table.".brand = ".$join_table.".brand_name";
            
        }
        
        return $filter;
    }
    
    
     
    /**
    * @error_handler
    * this is a message trowen when erorr
    * @array $message
    **/
    function error($message){
        
        $html  = "<div class='callout callout-danger'>";
        $html .= "<h4>".$message['title']."</h4>";
        $html .= "<p>".$message['message']."</p>";
        $html .= "</div>";
        
        
        //enable error
        $this->hasError = 1;
        $this->error = $html;
    }
}