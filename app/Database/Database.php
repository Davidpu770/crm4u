<?php
namespace Crm4u\SQL;

include_once dirname(__FILE__) . "/settings.php";


class Database {
    
    private $_connection;
    private static $_instance;

    public $db, $mysqli;
    
    /**
     * get an instance of the database
     * $return Database
     */
    public static function getInstance($settings = '') {

        if(!self::$_instance){

            if(!isset($settings) || $settings == ''){

                $settings = new settings('localhost','crm4u','root','');
            }

            self::$_instance = new self($settings);
        }

        return self::$_instance;
    }
    /**
     * Constructor
     */
    public function __construct($settings) {
        
        $this->_connection = new \mysqli($settings->db_address, $settings->username, $settings->password, $settings->db_name);

        if (mysqli_connect_error()) {
          trigger_error('Failed to connect to MySQL: ' . mysqli_connect_error(), E_USER_ERROR);
        }
    }
    
    private function __clone() {
        
    }
    
    public function getConnection(){
        return $this->_connection;
    }

}