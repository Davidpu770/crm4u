<?php
namespace Crm4u\SQL;


class settings{

    //the address of the phone DB center default: localhost
    public $db_address;
    
    //the name of the phone DB default: brand
    public $db_name;
    
    //the username for the db connection default: root
    public $username;
    
    //the password for the db connection default: blank
    public $password;
    
    function __construct($db_address = 'localhost',$db_name = 'brand',$username = 'root',$password = ''){
        
        $this->db_address = $db_address;
        
        $this->db_name = $db_name;
        
        $this->username = $username;
        
        $this->password = $password;
        
    }
}