<?php
namespace Crm4u\Middleware;

use Crm4u\Controller\ImportController;

class Fetch {

    public $result;


    function __construct($sql_table, $sql_columns = array(),$sql_options = array())
    {

        $fetch = new \stdClass();

        $results = ImportController::import($sql_table, $sql_columns,$sql_options);

        If(count($results['data']) === 0) {

            return false;
        }

        if(count($results['data']) > 1){

            foreach ($results['data'] as $key => $value) {

                $fetch->$key = $value;

            }

        } else {

            foreach ($results['data'][0] as $key => $value) {

                $fetch->$key = $value;

            }
        }


        $this->result = $fetch;
    }
}