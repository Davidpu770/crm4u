<?php
/**
 * Copyright (c) 2017. All copyright reserved to David Pugatsch
 */

namespace Crm4u\Security;

use Crm4u\Controller\ImportController;

class blockAccess {

    //@feature
    // block by ip - done;
    // block by priv - done;
    // block by mandatory block - done;
    // block for constriction - done;
    // $page_level needed - done;
    // error message return by demand - done

    protected $pageLevel;

    protected $status = 'offline';

    /**
     * blockAccess constructor.
     * @param $level
     */
    function __construct($level, $loginStatus = 'offlide', $user){

        global $_SITE;

        //everyone
        $this->level          = $level;
        $this->status         = $loginStatus;
        $this->priv           = '0';
        $this->user           = $user;

        //block ip
        $this->ip_address     = $_SERVER['REMOTE_ADDR'];
        $this->whiteListOnly  = $_SITE['whiteListOnly'];

        //mandatory
        $this->mandatory      = $_SITE['mandatory_block'];

        //constraction
        $this->inConstriction = $_SITE['inConstraction'];

    }


    /**
     * @return mixed
     */
    function blockIP($show_error = false){

        $white = self::allowedIPlist();
        $black = self::blockedIPlist();

        $whiteListOnly = $this->whiteListOnly;


        if(!is_null($black) && in_array($this->ip_address, $black)) {

            return self::showError($show_error,'Access Denied');
        }

        if(($whiteListOnly) && !in_array($this->ip_address, $white)) {

            return self::showError($show_error,'Access Denied');
        }

    }

    /**
     * @param bool $show_error
     */
    function blockPriv($show_error = false){

        if($this->status  == 'online' && $this->level > $this->user->priv){

            return self::showError($show_error, "(9) Access Deined");
        }
    }

    function blockMandatory($show_error){

        if($this->mandatory){

            return self::showError($show_error, "Contact Support");
        }
    }


    /**
     * @param $show_error
     * @param $error
     */

    function inConstriction($show_error){

        if($this->inConstriction && import\user){

            return self::showError($show_error, "(11) Under Constraction");
        }

    }
    function showError($show_error, $error){

        if($show_error){

            $message  = "<div class='callout callout-danger'>";
            $message .= "<h4>You have an Error.</h4>";
            $message .= "<p>You don't have enough permission to see this page.</p>";
            $message .= "<p>If you think you do please contact administrator.</p>";
            $message .= "<p>". $error;
            $message .= "</div>";

            print $message;

        }
        die();
    }

    /**
     * @return array
     */
    function allowedIPlist(){

        $allowed_ip = ImportController::import('ipaddress_list', array('flag','ip_address'),array('WHERE' => 'flag = 1'));

        return $allowed_ip['data'];
    }

    /**
     * @return array
     */
    function blockedIPlist(){

        $blocked_ip = ImportController::import('ipaddress_list', array('flag','ip_address'),array('WHERE' => 'flag = 0'));

        return $blocked_ip['data'];

    }

    function before_header(){

        self::blockIP(0);
        self::blockMandatory(0);

    }

    function after_header(){

        self::blockPriv(1);
        self::inConstriction(1);
        //--slideInDown animation
        $_SESSION['slideInDown'] = 'slideInDown';
        if($this->status == 'online'){
            $_SESSION['slideInDown'] = '';

        }
    }
}