<?php
namespace Crm4u\Security;

use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Controller\InsertController;
use Crm4u\Controller\UpdateController;
use Crm4u\Controller\LogController;


class CheckLogin {

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    protected $loginType;


    function __construct() {

        //first check what method of login we use
        $this->loginType = $_POST['type'];

        //check if there is empty field
        FormController::empty_field();

        //then try to log in
        $user = $this->loginType."Login";

        $this->connect($this->$user());

    }


    /**
     * User is login via the form
     */
    function userLogin(){

        $this->username = $_POST['username'];
        $this->password = self::salt_password($_POST['password']);

        $response = ImportController::import(
            'users',
            array('*'),
            array(
                "WHERE" => "username = '{$this->username}'",
                "AND" => "password = '{$this->password}'"
            )
        );
        FormController::Debug($response);
        return $response;
    }

    /**
     * user is login via google plugin
     */
    function googleLogin(){

        $this->email = $_POST['email'];

        $response = ImportController::import(
            'users',
            array('*'),
            array(
                "WHERE" => "email = '{$this->email}'"
            )
        );

        return $response;
    }



    public function connect($user){

        global $loader;

        if(count($user['data'][0]) > 0){

            //start session:
            $_SESSION = array(
                'session'    => 1,
                'login'      => 1,
                'username'   => $user['data'][0]['username'],
                'priv'       => $user['data'][0]['priv'],
                'firstname'  => $user['data'][0]['firstname'],
                'lastname'   => $user['data'][0]['lastname'],
                'user_id'    => $user['data'][0]['user_key_id'],
                'last_login' => $user['data'][0]['last_login'],
                'location'   => $user['data'][0]['location']
            );

            //set brand deposit visit time
            $this->setBrandArray($loader->all_brand);

            if($user['data'][0]['multiple_brand'] == 1){
                $_SESSION['multiple'] = 1;
            }

            //update the last login value:
            date_default_timezone_set("Asia/Jerusalem");
            $date = date('Y-m-d H:i:s');


            //And insert it to DB (table last_log):
            $ip = $_SERVER['REMOTE_ADDR'];
            $user_id = $user['data'][0]['user_key_id'];




            //set brand
            $_SESSION['brand'] = ($user['data'][0]['main_brand'] != "") ? $user['data'][0]['main_brand'] : $_SESSION['brand'] = $user['data'][0]['brand'];

            $login_log = array(
                'date' => FormController::getDate(),
                'ip_address' => $ip,
                'user_key_id' => $user_id
            );

            InsertController::insert('login_log',$login_log);
            UpdateController::update('users',array('last_login' => $date),array(' WHERE' => 'user_key_id = '.$user_id));
            LogController::insert('Login');

        }else{

            //Send Back to AJAX request DENY to go on
            header('HTTP/1.1 401 Unauthorized');
        }
    }

    function setBrandArray($all_brands){

//        foreach($all_brands as $key => $value){
//
//            $_SESSION['visit_deposit'][$value['brand_name']] = 0;
//
//        }
        //return;
    }

    protected static function salt_password($password){

        return crypt($password, '$1$sAles$');
    }

    function protect_username(){

        if(!$this->username || $this->username == "") {

            return true;
        }
    }

    static function logout(){

        global $loader;

        LogController::insert('Logout');

        $_SESSION = array();
        $_SESSION['login'] = 0;

        header("location: ../");
    }
}
