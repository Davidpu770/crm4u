<?php

namespace Crm4u\Middleware;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\SwalController;

/**
 * Class CheckAccess
 * @package Crm4u\Middleware
 */
class CheckAccess
{

    /**
     * @param bool $show_error
     * @return bool
     * check if user is online
     */
    static function isOnline($show_error = false, $redirect = false){

        if(!isset($_SESSION)) { session_start(); }

        if(isset($_SESSION['login']) && $_SESSION['login'] != 0) {


            return true;
        } else {

            if($show_error){

                new SwalController("error","you are offline, please sign in again","error");
                die();
            }

            if($redirect){

                (self::inLoginPage()) ? null : header("location: /login");
            }

        }
    }

    /**
     * @return bool
     * check if user is super admin
     */
    static function isSuperAdmin(){

        if(self::isOnline() && isset($_SESSION['priv']) && $_SESSION['priv'] == 4){

            return true;
        }
    }

    /**
     * @return bool
     * check if user is admin
     */
    static function isAdmin(){

        if(self::isOnline() && $_SESSION['priv'] >= 3){

            return true;
        }
    }

    static function inLoginPage(){

        if(strpos($_SERVER['REQUEST_URI'], "login")){
            return true;
        }
    }


    /**
     * @param $auth_level
     * @param bool $show_error
     * @return bool
     * check if user is Authorized
     */
    static function isAuthorized($auth_level, $show_error = false){

        if(self::isOnline(false, false) && $_SESSION['priv'] >= $auth_level){

            return true;
        } else {

            if($show_error){

                new SwalController("error","you are not authorized to view this page", "error");
                die();
            }
            return false;
        }
    }

    /**
     * This function used for accessing the DB for one session using server access (access only for read)
     */
    function ServerApiAccess(){

        if(!CheckAccess::isOnline(false, false)){

            
        }
    }


    static function hasApi(){

        $brand = BrandController::brand_import();

        return ($brand->has_api == 1) ? true : false;
    }

    static function connected_user(){


        return (self::isOnline()) ? $_SESSION['user_id'] : false;
    }
}