<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 07/06/2017
 * Time: 20:18
 */
namespace Crm4u\Middleware;


class Printer {

    public $result;

    function __construct($type, $data, $params = null)
    {
        if(method_exists(self::class, $type)){

            self::$type($data, $params);
        }
    }

    function json($data){
        print_r(json_encode($data));
    }

    /*
     * $params needed = 1.table name 2. id of selected item
     */
    function select($data, $params) {

        $select = '';
        $selected = '';

        foreach ($data['data'] as $key => $value){

            if(isset($params[1]) && is_array($params[1]) && isset($params[1][1])){

                $selected = ($data['data'][$key]['id'] == $params[1][1]) ? "selected" : "";
            }

            $select .= "<option value='".$value['id']."' $selected>".$value[$params[0].'_name']."</option>\n";
        }

        print $select;
        return true;
    }

    function asArray($data){

        $this->result = new \StdClass();

        foreach (isset($data['data']) ? $data['data'] : $data as $key => $value) {

            $this->result->$key = $value;
        }

        return $this->result = $data;
    }

    function variable($data){

        return $data;
    }
    function table($data){

        print_r(json_encode($data));
    }

}