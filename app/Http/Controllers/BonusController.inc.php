<?php

namespace Crm4u\Controller;

use Crm4u\Import\user;
use Crm4u\Models\bonuses;

class BonusController implements iController {

    function delete()
    {

    }
    static function add()
    {
        $strip = self::assignValue($_POST);

        InsertController::insert('bonuses', $strip, true);
    }

    static function edit()
    {

    }

    static function boot()
    {

    }

    static function calculate($emp_id, $type){

        //1. get total of given id
        $user  = user::import_user($emp_id);
        $total = user::import_finance($emp_id);

        //2. find which role to apply
        $roles = array(
            'user'         => bonuses::import($type, 'users'      ,$user->user_key_id),
            'department'   => bonuses::import($type, 'departments',$user->department),
            'desk'         => bonuses::import($type, 'desks'      ,$user->location)
        );

        //3. calculate
        foreach($roles as $object => $role){

            foreach ($role['data'] as $strip => $value){

                if($total->current_month->all_deposits < $value['strip_max']){

                    return $total->current_month->all_deposits * (sprintf("%02d", $value['strip_percentage']) / 100);
                }
            }
        }
    }

    static function generateReport(){


    }

    static function assignValue($strip){

        //get data
        $referral = explode('/',$_SERVER['HTTP_REFERER']);

        $strip['object_type'] = $referral[3];
        $strip['object_id']   = $referral[4];

        $strip['infinite'] = (isset($strip['infinite']) && $strip['infinite'] == 'on') ? 1 : 0;

        return $strip;
    }
}