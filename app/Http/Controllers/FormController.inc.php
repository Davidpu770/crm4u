<?php

namespace Crm4u\Controller;

use Crm4u\Middleware\Fetch;

class FormController {

    /**
     * return true if given value exist in given table;
     * @param $value
     * @param $table
     */
    public static function value_exist($value, $table){

        $result = ImportController::import($table, array('*'), array('WHERE' => "id = {$value}"));

        return (count($result['data']) > 0) ? true : false;
    }

    public static function empty_field(){

        if(in_array("", $_POST)){

            new SwalController("Empty field","One or more field are empty","error");
            die();
        }
    }

    public static function spanConvert($text, $color){


       return "<span class='status-label label label-{$color}'>{$text}</span>";
    }

    public static function selectConvert($options, $class_name, $selected_option){

        $select = "<select class='form-control $class_name' name='{$class_name}'>";

        $select .=  self::selected_option($options, 0, $selected_option);

        $select .= "</select>";

        return $select;
    }


    /**
     * Global update function
     * Require table name and unique id
     * @param $params
     */
    static function edit($params){

        unset($_POST['simple']);

        foreach ($_POST as $key => $value){

            $value = ($value == "on") ? 1 : $value;
            $colSet[$key] = $value;
        }

        $parameters = array('WHERE' => "id = {$params[1]}");

        UpdateController::update($params[0], $colSet, $parameters);

    }

    public static function textareaConvert($class, $data_url, $data_name, $data){

        return "<a class='$class' data-url='$data_url' data-name='$data_name'>$data</a>";
    }

    public static function checkConvert($item)
    {

        if ($item == 1) {

            return "<i class='fa fa-check ftd-check' aria-hidden='true'></i>";
        } else {

            return null;

        }

    }

    public static function linkConvert($link, $replace, $data){

        if(!filter_var($link, FILTER_VALIDATE_URL)){

            return $data;
        } else {

            $link = str_replace('%'.$replace."%", $data, $link);

            return "<a target='_blank' href='{$link}'>$data</a>";
        }

    }

    /**
     * this function check if employee was selected on the form
     */
    public static function employeeCheck(){

        if($_POST['emp_id'] == 'no-select'){

            new SwalController("No employee selected","You need to select an employee first","error");
            die();
        }
    }


    /**
     * @return mixed
     * this function is called when no data is printed and DataTables require some result in json format
     * @since 24.07.2017
     */
    public static function EmptyData(){

        return print_r(json_encode(array('data' => array())));
    }

    public static function is_split(){

        return (isset($_POST['split'])) ? true : false;
    }


    public static function ValueExists($sql_table, $row = array(), $sql_column, $show_error = false){


        $i = 0;

        foreach ($sql_column as $key => $value){

            if($i == 0){

                $parameters['WHERE'] = $key . "=". $value;
            } else {

                $parameters['AND'] = $key . "=". $value;
            }
            $i++;
        }

        $result = ImportController::import($sql_table, array($row), $parameters);



        if(count($result['data']) > 0){

            if($show_error){

                new SwalController('You have an error','You are trying to setup a new '.$sql_table." But this value already exist",'error');
                die();
            }

            return true;

        }

    }

    /**
     * this function check if currency was selected on the form
     */
    public static function currencyCheck(){

        if($_POST['currency'] == "no-option"){

            new SwalController("No currency select","You need to select currency first","error");
        }
    }


    /**
     * this function check if ftd exist already
     */
    public static function ftdCheck($cid){

        $result = ImportController::import('goal',array('ftd'),array('WHERE' => "customer_id = '{$cid}'","AND" => "ftd = 1"));

        if(count($result['data']) > 0 && isset($_POST['ftd']) && $_POST['ftd'] == 1){

            new SwalController("FTD already exist","You trying to Insert a FTD to an exist user","error");
        }

    }

    /**
     * this function check if transaction exist
     * @param $check_type bool true = if tran exist false = if tran not exist
     */
    public static function tranCheck($tran_id, $type, $check_type = true, $split_deposit = false){

        $columns = array('tran_id');

        $parameters = array(
            'WHERE' => "brand = ".BrandController::current_brand(),
            "AND" => "goal. id = '{$tran_id}'",
            "AND " => "goal.type = '{$type}'"
        );

        $result = ImportController::import('goal', $columns, $parameters);

        if(!$split_deposit){

            if (count($result['data']) > 0 && $check_type) {

                new SwalController("Transaction Exist", "This Transaction exist already.", "error");
                die();
            } elseif (count($result['data']) === 0 && !$check_type) {

                new SwalController("Transaction not Exist", "This Transaction are not exist.", "error");
                die();
            }

        }



        return true;
    }

    public static function transactionExist($tran_id, $type, $check_type = true, $split_deposit = false){

        $columns = array('tran_id');

        $parameters = array(
            'WHERE' => "brand = ".BrandController::current_brand(),
            "AND" => "goal.tran_id = '{$tran_id}'",
            "AND " => "goal.type = '{$type}'"
        );

        $result = ImportController::import('goal', $columns, $parameters);

        if(!$split_deposit){

            if (count($result['data']) > 0 && $check_type) {

                new SwalController("Transaction Exist", "This Transaction exist already.", "error");
                die();
            } elseif (count($result['data']) === 0 && !$check_type) {

                new SwalController("Transaction not Exist", "This Transaction are not exist.", "error");
                die();
            }

        }

    }

    /**
     * @param array $option
     * @param string $arg
     * @param string $value
     * @return string
     * return the selected option form DB of <select>
     */
    public static function selected_option($option = array(),$arg = '',$value = ''){

        $html = '';
        $i = 1;
        foreach($option as $key => $val){

            $val = (($arg !== 0 && isset($arg)) ? $val[$arg] : $val);


            $selected = '';

            if($value == $val){

                $selected = "selected='selected'";

            }
            if(is_null($arg)){
                $html .= "<option value='$i' $selected>$key</option>";
                $i++;
                continue;
            }
            $html .="<option value='$val' $selected >$val</option>";

        }
        return $html;
    }

    /**
     * this function check if transaction from same customer was on full at least once
     */
    static function checkVerification($cid){

        $columns    = array("tran_id");

        $parameters = array(
            "WHERE" => "status = 'full'",
            'AND' => "customer_id = '{$cid}'");

        $result = ImportController::import('goal', $columns, $parameters);


        if(count($result['data']) > 0) {

            return true;
        }
    }

    public static function hashPassword($password){

        $pass = new password();

        return $pass->salt_password($password);

    }

    static function tableButton($btn_list = array('export','new_deposit','currency')){

        $html = '<div class="pull-right">';

            $html .= '<div class="btn-group deposit-admin-btn">';

                if(in_array('export',$btn_list)){

                    $html .= '<button class="btn btn-success deposit-admin-btn export" id="export" data-value="all" title="Export Deposit Table">';
                        $html .= '<i class="fa fa-desktop"></i><span class="hidden-xs hidden-xs-inline"> Export</span>';
                    $html .= '</button>';

                }

                if(in_array('new_deposit',$btn_list)){

                    $html .= '<button type="button" class="btn btn-info deposit-admin-btn" id="new_deposit" data-target="#new-deposit" data-toggle="modal"title="Adding New Deposit">';
                        $html .= '<i class="fa fa-plus"></i><span class="hidden-xs hidden-xs-inline"> New Deposit</span>';
                    $html .= '</button>';

                }

                if(in_array('currency',$btn_list)){

                    $html .= '<button class="btn btn-danger deposit-admin-btn" data-target="#currency" data-toggle="modal" title="Change Currency Value">';
                        $html .= '<i class="fa fa-gbp"></i> <span class="hidden-xs hidden-xs-inline"> Currency</span>';
                    $html .= '</button>';

                }

            $html .= '</div>';

        $html .= '</div>';

        echo $html;
    }

    static function strongPasswordCheck($password){

        if(!preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[0-9]).*$#", $password)) {

            new SwalController('Password are not strong', 'Password are not strong', 'error');
            die();
        }
    }

    static function fixRound($sum,$zeros){

        return ($sum === 0) ? 0 : round($sum, $zeros);
    }

    /**
     * @param $sql_value (array)
     * @return mixed
     * Convert the given array to SQL string
     */
    static function convertArrayValuesToString($sql_value){


        foreach ($sql_value as $key => $value) {

            if(is_string($value)){

                $value = "'".$value."'";

                $convertedArray[$key] = $value;
            } else {

                $convertedArray[$key] = $value;
            }

        }

        return $convertedArray;
    }

    public static function scriptConvert($scriptName, $scriptArg){

        echo "<script>$scriptName($scriptArg)</script>";
    }

    public static function Debug($var = false){

        echo "<pre>";
        var_dump($var);
        echo "</pre>";
    }

    /**
     * @param $sql_value (array)
     * @return mixed
     * Convert the given string to SQL string
     */
    static function convertValuesToString($sql_value){

        if(is_string($sql_value)){

            return "'{$sql_value}'";
        } else {

            return $sql_value;
        }


    }

    static function removeTags($deposit, $value){


        return str_replace("'","",$deposit[$value]);
    }

    static function existInArray($array, $value){

        foreach ($array as $item){

            if(in_array($value, $item)){

                return true;
            }
        }
    }


    static function getMonth(){

        return date('M');
    }


    static function getYear(){

        return date('y');
    }

    static function getDate(){

        return date('Y-m-d H:i:s');
    }
}