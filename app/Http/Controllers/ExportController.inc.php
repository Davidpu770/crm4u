<?php
namespace Crm4u\Controller;


use Crm4u\Export\iExport;
use Crm4u\Import\user;
use Crm4u\SQL\filter;
use PHPExcel;

class ExportController implements iExport {

    function __construct($type, $params)
    {
        $tables = array(
            'goal' => iExport::goal,
            'users',
            'shift' => iExport::shift
        );

        $sql_table = $params[0];

        $filter     = new filter();

        $cols       = $tables[$sql_table]['cols'];

        $filter->table = (isset($tables[$sql_table]['filter_table'])) ? $tables[$sql_table]['filter_table'] : $filter->table;


        if(isset($tables[$sql_table]['join'])) {

            foreach ($tables[$sql_table]['join'] as $key => $parameter) {

                $parameters['INNER JOIN'] = $key;

                $i = 0;
                foreach ($parameter as $key => $item) {

                    if ($i === 0) {

                        $parameters['ON'] = $parameters['INNER JOIN'] . "." . $key . " = " . $sql_table . "." . $item;
                    } else {

                        $parameters['AND '] = $parameters['INNER JOIN'] . "." . $key . " = " . $sql_table . "." . $item;
                    }
                    $i++;

                }
            }
        }

        $parameters['WHERE'] = '0 = 0';

        if(isset($tables[$sql_table]['filter'])) {

            foreach ($tables[$sql_table]['filter'] as $key => $parameter){

                if(is_array($parameter)){

                    $parameters[] = $filter->$key($parameter[0], $parameter[1]);
                } else {

                    $parameters[] = $filter->$key($parameter);
                }


            }
        }

        $data = ImportController::import($sql_table, $cols, $parameters);

        if(count($data['data']) > 0){

           ExportController::export($data['data'], BrandController::brand_name()."_".FormController::getDate().".xls", false);
        } else {

            new SwalController('No deposit found','There is no deposit to export','error');
        }

    }

    static function export($data, $filename, $showSuccessMessage = true){

        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();
        $user = user::ConvertID(user::current_user());

        // Set document properties
        $objPHPExcel->getProperties()
            ->setCreator("{$user}")
            ->setLastModifiedBy("{$user}")
            ->setTitle("Export on ".FormController::getDate())
            ->setSubject('All Deposits')
            ->setDescription("Exported from crm4u")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("result file");

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);

        //letters array
        $l = range('A', 'Z');

        $i = 0;
        foreach ($data[0] as $key => $value){
            $objPHPExcel->getActiveSheet()->setCellValue($l[$i]."1", strtoupper($key));
            $objPHPExcel->getActiveSheet()->getColumnDimension($l[$i])->setWidth(20);

            $i++;
            $last_letter = $l[$i];
        }

        // Insert data to cells
        $i = 2;
        foreach($data as $key => $value) {
            $j = 1;
            $startCell = 0;
            foreach ($value as $item){
                $objPHPExcel->getActiveSheet()->setCellValue($l[$startCell].$i, $item);
                $j++;
                $startCell++;
            }
            $i++;
        }

        // Set fonts
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$last_letter.'1')->getFont()->setBold(true);

        // Set page orientation and size
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename);
        header('Cache-Control: max-age=0');

        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

        if($showSuccessMessage){

            new SwalController('Exported success','File exported successfuly','success');
        }
    }
}
