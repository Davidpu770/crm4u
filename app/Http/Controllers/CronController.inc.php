<?php
namespace Crm4u\Controller;

use Crm4u\Cron\iJobs;
use Crm4u\Models\cron;

class CronController {



    static function init(){

        $jobs = new cron('asArray', array());

        foreach ($jobs->result['data'] as $key => $JOB){


            $class = 'Crm4u\\Controller\\'.$JOB['class_name'];

            if(class_exists($class)){

                if((strtotime(FormController::getDate()) * 1000 - (strtotime($JOB['updated'])) * 1000) > $JOB['run_every']) {

                    $job = $JOB['method_name'];

                    $class::$job();

                    UpdateController::update('cron_jobs',array('updated' => FormController::getDate()), array('WHERE' => '0=0', 'AND' => 'id = ' . $JOB['id']), null, null, false);

                }



            } else {

                new SwalController('error','method not exist','error');
            }

        }
    }


}