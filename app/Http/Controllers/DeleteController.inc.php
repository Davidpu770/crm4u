<?php

namespace Crm4u\Controller;


use Crm4u\Middleware\CheckAccess;

class DeleteController {

    protected $loader;

    function __construct($type, $params)
    {
        global $loader;

        $this->loader = $loader;

        LogController::insert("Row deleted (table: {$params[0]})");

        self::delete($params[0], $params[1], $params[2]);
    }

    function delete($sql_table, $sql_row_id, $sql_primary_key = 'id'){

        CheckAccess::isAuthorized('2', true);

        $sql = "DELETE FROM ".$sql_table." WHERE ".$sql_primary_key." = '".$sql_row_id."'";

        if(!$this->loader->mysqli->query($sql)){

            new SwalController('You have an error',$this->loader->mysqli->error,'error');

        } else {

            new SwalController('Deleted!','Row has been deleted.','success');
        }
    }
}