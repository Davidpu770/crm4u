<?php
namespace Crm4u\Controller;

use Crm4u\Middleware\CheckAccess;
use Crm4u\Middleware\Fetch;
use Crm4u\SQL\filter;

class BrandController extends filter{


    /**
     * @return array
     * get all brand exist in array order
     */
    static function all_brand(){

        $all_brand = ImportController::import('brands',array('*'));

        return $all_brand;
    }

    /**
     * @return mixed
     * This function get the current brand in integer
     */
    static function current_brand(){

        return (CheckAccess::isOnline(false, false)) ? $_SESSION['brand'] : false;
    }

    /**
     * @return mixed
     * This function get the current brand in string
     */
    static function brand_name()
    {

        if(CheckAccess::isOnline(false, false)){

            $result = new Fetch('brands', array('brand_name'), array('WHERE' => "id = '" . self::current_brand() . "'"));
            return $result->result->brand_name;
        }

    }


    /**
     * @return Fetch|\stdClass
     * import current brand
     */
    static function brand_import(){


        $brand = new Fetch('brands',array('*'),array('WHERE' => "id = '".self::current_brand()."'"));
        $brand = $brand->result;

        return $brand;

    }

    static function has_api(){

        $result = new Fetch('brands',array('has_api'),array('WHERE' => "id = '".self::current_brand()."'"));

        return ((int) $result->result->has_api === 0) ? false : true;

    }

    /**
     * @return mixed
     * get platform issuer
     */
    static function platform_issuer(){

        if(CheckAccess::isOnline(false, false)){

            $result = new Fetch('brands',array('platform_issuer'),array('WHERE' => "id = '".self::current_brand()."'"));
            return $result->result->platform_issuer;
        }
    }

    /**
     * @return \stdClass
     * get API details
     */
    static function apiDetails()
    {

        $result = new Fetch('brands', array('api_username', 'api_password', 'api_address'), array('WHERE' => "id = '" . self::current_brand() . "'"));
        return $result->result;
    }

    /**
     * @param $params
     * change the brand when multiple is allowed
     */
    function changeBrand($params){

        global $loader;

        if(CheckAccess::isAuthorized(2) || $loader->user->multiple_brand){

            $_SESSION['brand'] = $params[0];
        }

        return self::returnToPage();



    }

    static function edit($params){

        unset($_POST['simple']);

        foreach ($_POST as $key => $value){

            $value = ($value == "on") ? 1 : $value;
            $colSet[$key] = $value;
        }

        $parameters = array('WHERE' => "id = {$params[0]}");

        UpdateController::update('brands',$colSet, $parameters);

    }


    /**
     * return to last page when changing brand
     */
    function returnToPage(){

        if(isset($_SERVER['HTTP_REFERER'])) {

            header("Location: ".$_SERVER['HTTP_REFERER']);

        } else {

            header("Location: /");

        }
    }
}
