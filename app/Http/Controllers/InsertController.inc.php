<?php

namespace Crm4u\Controller;
use Crm4u\Import\user;
use Crm4u\Middleware\CheckAccess;

/**
 * Class InsertController
 * @package Crm4u\Controller
 * @todo checks
 */
class InsertController {

    public $loader;


    protected $simpleForm;

    function __construct($type, $params)
    {

        global $loader;

        $this->loader = $loader;

        CheckAccess::isOnline(true);

        $this->simpleForm = ($_POST['simple'] == 'false') ? false : true;

        unset($_POST['simple']);

        if (!$this->simpleForm) {

            $this->{$params[0]}();
        } else {
            LogController::insert("Row inserted (table: {$params[0]})");
            $this->insert($params[0], $_POST);
        }

    }

    function goal(){

        DepositController::add();
    }

    function users(){

        UserController::add();
    }

    function shift(){

        ShiftController::add();
    }

    function bonuses(){

        BonusController::add();
    }

    function chargeback(){

        $_POST['emp_id'] = user::current_user();
        $this->insert('chargeback', $_POST);
    }

    function boot($sql_table, $sql_value){

        $this->insert($sql_table, $sql_value);
    }


    /**
     * @param string $sql_table
     * @param array $sql_value
     * @param bool $showSuccessMessage
     * @return SwalController
     */
    static function insert($sql_table, $sql_value = array(), $showSuccessMessage = true){

        global $loader;

            $sql  = "INSERT INTO ". $sql_table. "(".implode(",",array_keys($sql_value)).")";
            $sql .= " VALUES (".implode(", ",FormController::convertArrayValuesToString($sql_value)).")";

            if(!$loader->mysqli->query($sql)){

                return new SwalController('You have an error',$loader->mysqli->error,'error');

            } elseif($showSuccessMessage) {

                return new SwalController('Everything is ok','Data successfully saved','success');
            }

    }

    /**
     * checklist
     *
     * 1. if user is online
     * 2. if user is allowed to update
     * 3. if value need to be unique
     * 4. if required value is missing
     */
}