<?php
namespace Crm4u\Controller;

use Crm4u\Import\brand;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Controller\PasswordController as Password;

class UserController implements iController {

    static function add() //TODO
    {

        $newUser = self::assignValue($_POST);

        FormController::empty_field();
        FormController::strongPasswordCheck($_POST['password']);

        UserController::check_ukid($newUser['user_key_id']);
        UserController::user_max_limit();
        UserController::check_brand($newUser['brand'], $newUser['user_key_id']);

        //insert into DB:
        InsertController::insert('users', $newUser);

    }

    static function assignValue($newUser){

        $newUser['firstname'] = FormController::removeTags($newUser, 'firstname');
        $newUser['lastname']  = FormController::removeTags($newUser, 'lastname');
        $newUser['password']  = Password::salt_password($newUser['password']);

        $newUser['brand']     = BrandController::current_brand();


        return $newUser;
    }

    static function boot(){

        $_POST = array(
            'firstname'       => 'self',
            'lastname'        => 'deposit',
            'username'        => 'self',
            'email'           => 'self@deposit',
            'password'        => 'd1d2d3D4D5D6D4D3D2D4',
            'brand'           => BrandController::current_brand(),
            'main_brand'      => BrandController::current_brand(),
            'goal_deposit'    => '0',
            'goal_ftd'        => '0',
            'department'      => 'Self',
            'priv'            => '0',
            'user_key_id'     => '1'
        );

        self::add();
    }

    static function edit()
    {

        unset($_POST['simple']);

        foreach ($_POST as $key => $value){

            $value = ($value == "on") ? 1 : $value;
            $colSet[$key] = $value;
        }

        $parameters = array('WHERE' => "user_key_id = ".$_POST['user_key_id']);

        UpdateController::update('users',$colSet, $parameters);
    }

    static function updatePassword($id){


        CheckAccess::isOnline();
        $password = $_POST['password'];
        $rePassword = $_POST['repassword'];

        if(PasswordController::CheckPassword($password, $rePassword, true)){

            $password = array('password' => PasswordController::salt_password($password));

            $parameters = array('WHERE' => "user_key_id = $id");

            UpdateController::update('users',$password, $parameters);
        }
    }


    function delete()
    {

        $user = explode('/',$_SERVER['REQUEST_URI']);

        CheckAccess::isOnline();

        $parameters = array('WHERE' => "user_key_id = {$user[3]}");

        UpdateController::update('users', array('disabled' => 1), $parameters);
    }


    /**
     * Check if ukid is empty nor exist
     **/
    static function check_ukid($ukid){

        $result = ImportController::import('users',array('user_key_id'),array('WHERE' => 'user_key_id ='.$ukid));

        //is empty
        if(is_null($ukid) || $ukid == ''){

            return new SwalController("You have an error","User key id cannot be empty","error");
        }

        if(count($result['data']) > 0){

            return new SwalController("UKID # exist","The user key ID # already exist","error", true);
        }
    }

    static function main_brand_no_in_list($main_brand, $brand){

        if($main_brand != $brand){

            new SwalController("you have an error","Main Brand are not listed on brand list","error");

        }
    }


    /**
     * @return number of user allowed
     */
    static function user_max_limit(){

        global $loader;

        if(self::usersCount() > $loader->site->user_max_limit){

            new SwalController("user max limit","You have reach your max limit users","error");
        }
    }

    static function usersCount(){

        $result = ImportController::import('users', array('COUNT(users.id) users'));

        return (count($result['data'][0]) > 1) ? $result['data'][0]['users'] : false;
    }

    static function check_brand($brand, $ukid){

        if(is_null($brand)){

            return new SwalController('you have an error','You need to pick at least one brand','error');
        }

        $brands = ImportController::import('users',array('brand'), array('WHERE' => 'user_key_id = '.$ukid));


        if(FormController::existInArray($brands['data'], $brand)){

            return new SwalController('you have an error',"The user is exist in {$brand}",'error');
        }
    }
}