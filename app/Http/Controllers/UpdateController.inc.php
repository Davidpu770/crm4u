<?php

namespace Crm4u\Controller;

use Crm4u\Middleware\CheckAccess;
use Crm4u\SQL\filter;
use Crm4u\SQL\Query;


class UpdateController {

    function __construct($type, $params)
    {
        global $loader;

        $this->loader = $loader;

        CheckAccess::isOnline(true);

        $this->simpleForm = ($_POST['simple'] == 'false') ? false : true;

        unset($_POST['simple']);

        if (!$this->simpleForm) {

            if(method_exists($this, $params[0])) {
                $this->$params[0]($params);
            } else {

                new SwalController('error','You have an error, the method you are trying to use is not exist','error');
            }
        } else {

            $this->update($params[0], $_POST, array(), $params[1], $params[2]);
        }
    }


    static function update($sql_table, $sql_update = array(), $sql_options = array(), $pk = null, $default_options = null, $showSuccessMessage = true){

        $isOnline = CheckAccess::isOnline(false);

        $isAuthorized = CheckAccess::isAuthorized(2, false);

        global $loader;


        $sql = "UPDATE ".$sql_table." SET ";

        $i = 0;
        foreach ($sql_update as $key => $value) {

            $sql .= ($i > 0) ? ", " : " ";
            $sql .= $key. " = ".FormController::convertValuesToString($value);

            $i++;
        }

        if(count($sql_options) > 0) {

            foreach ($sql_options as $key => $value) {

                $sql .= " $key $value";
            }
        } else {

            $sql .= " WHERE $pk = $default_options";
        }

        $result = Query::init($sql);


        if(!$result && $showSuccessMessage){

            new SwalController('You have an error',$loader->mysqli->error,'error');

        } elseif($showSuccessMessage) {

            new SwalController('Everything is ok','Data successfully saved','success');
        }
    }

    function password($params = null){

        UserController::updatePassword($params[2]);
    }

    function currency($params = null){


    }

    function send_dispute($params = null){

        ChargebackController::send_dispute($params);
    }

    function goal(){

        DepositController::edit();
    }
}