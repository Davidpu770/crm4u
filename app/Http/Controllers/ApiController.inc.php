<?php

namespace Crm4u\Controller;
use Crm4u\Import\brand;
use Crm4u\Import\user;

/**
 * Class ApiController
 * @package Crm4u\Controller
 * example usage: ApiController::init('Withdrawal','FILTER[id]',4344','view')
 */
class ApiController{

    public static function init($module, $filter, $filterVal, $command, $extra){

        if(BrandController::has_api()){

            $api_class = '\Crm4u\Mods\API\\'.BrandController::platform_issuer();

            $api = new $api_class;

            return $api->get_request($module, $filter, $filterVal, $command, $extra, true);
        }

    }
}

