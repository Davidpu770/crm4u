<?php
/**
 * Copyright (c) 2017. All copyright reserved to David Pugatsch
 */

namespace Crm4u\Controller;

use Crm4u\Import\user;

/**
 * Class ShiftController
 * @package Crm4u\Controller
 *
 */
class ShiftController
{

    /**
     * Add new shift
     * 1. check if value exist
     * 2. insert new shift
     * 3. send notification
     */
    static function add(){

        FormController::ValueExists('shift','started',array('uid' => user::current_user(),'active' => 1), true);

        LogController::insert('Start Shift');

        InsertController::insert('shift', $_POST);

        NotificationController::send('New shift started','You started your shift on: '.FormController::getDate(),'deposit', NotificationController::CurrentUserFilter());

    }

    static function getActiveShift()
    {

        $parameters = array(
            'WHERE' => 'uid = '.user::current_user(),
            'AND'   => 'ISNULL(ended)'
        );

        $result = ImportController::import('shift', array('*'),$parameters);

        if(count($result['data']) > 0) {

            return $result['data'][0];
        }

        return false;
    }

    static function hasActiveShift(){

        $parameters = array(
            'WHERE' => 'uid = '.user::current_user(),
            'AND'   => 'ISNULL(ended)'
        );


        $result = ImportController::import('shift', array('started'),$parameters);

        if(count($result['data']) > 0){

            return "disabled";
        }
    }

    public static function toolbar(){

        global $loader;

        if($loader->site->timeWatchMod){

            $shift = ShiftController::getActiveShift();

            $toolbar ="<li>";
            $toolbar .="<div class=''>";
            $toolbar .= "</li>";

            $toolbar .= "<li>";
            $toolbar .= "<div class=><p class='shiftWatch watch'>Clocked out</p></div>";

            $toolbar .= "</li>";

            $toolbar .= "<li>";
            $toolbar .= "<input type='hidden' id='shiftTime' value='".$shift['started']."'/>";
            $toolbar .= "<input type='hidden' id='shift_id' value='".$shift['id']."'/>";
            $toolbar .= "<input type='hidden' id='playerID' value=''/>";
            $toolbar .= "<input type='button' id='startShift' class='btn btn-success btn-timewatch' value='Clock In' ".ShiftController::hasActiveShift().">";
            $toolbar .= "<input type='button' id='stopShift' class='btn btn-danger btn-timewatch' value='Clock Out' >";
            $toolbar .= "</li>";

            echo $toolbar;


            echo "<script> 
                    startShift('#startShift','uid=" . user::current_user() . "');        
                 </script>";

//            FormController::scriptConvert('startShift',"'#startShift', 'uid=".user::current_user()."&playerID='+$('#playerID').val()");
            FormController::scriptConvert('stopShift',"'#stopShift',$('#shift_id').val() ,'ended=GETDATE()'");

        }

    }

//Jobs

    static function StopShift(){

        $col = array('id','uid','started');

        $parameters = array('WHERE' => 'active = 1');

        $result = ImportController::import('shift',$col,$parameters);

        if(count($result['data']) > 0){

            foreach ($result['data'] as $key => $value){

                //get time since connected in seconds
                $timeSinceConnected = strtotime('now') - strtotime($value['started']);

                //send a remainder after 9 hour
                if($timeSinceConnected >= 32400 && $timeSinceConnected < 43200){
                    NotificationController::send('Please do not forget to clock out','You are online for more then 9 hours','cb', NotificationController::UserFilter($value['uid']));
                }

                //close shift after 12 hour
                elseif(strtotime('now') - strtotime($value['started']) >= 43200){

                    UpdateController::update('shift', array('active' => 0, 'auto_close' => 1), array('WHERE' => "id = {$value['id']}"), null, null, false);
                    NotificationController::send('You were clocked out','You was connected for more then 12 hour','cb', NotificationController::UserFilter($value['uid']));
                }
            }
        }
    }

}