<?php
namespace Crm4u\Controller;

use stdClass;

header("Content-type: text/json");
/**
 * Class autoComplete
 * @package Crm4u\Controller
 * this class get module and customer data from api
 * need "has_api" to be true
 */
class ExternalApiController {

    /**
     * @var stdClass
     */
    protected $api;

    /**
     * @var string
     */
    protected $apiClass;

    /**
     * @var string
     */
    protected $loader = \stdClass::class;

    /*
     * @var stdClass
     */
    protected $brand;

    /*
     * @var array
     */
    protected $params = array();


    /**
     * autoComplete constructor.
     */
    function __construct($type, $params){

        global $loader;

        $this->loader = $loader;

        $this->params = new stdClass();

        $this->params->id = $params[0];
        $this->params->action = $params[1];
        $this->params->get = $params[2];

        $this->api = new stdClass();

        $this->apiClass = self::checkApi();

        $this->import($this->api->has_api);
    }

    /**
     * @return bool
     */
    function checkApi()
    {
        //import brand;
        if (BrandController::has_api()) {

            $this->api->has_api = true;
            $this->api->platform_issuer = $this->loader->brand->platform_issuer;

            $api_class = '\Crm4u\Mods\API\\'.$this->api->platform_issuer;

             return new $api_class;

        } else {

            $this->api->has_api = false;

            return false;
        }
    }

    /**
     * @param bool $with_api
     */
    function import($with_api = false){

        $filter = "FILTER[id]";

        if($this->params->id == 'all'){

            $filter = "";
        }

        //import tran
        if($with_api) {
            //transaction:
           print_r(json_encode(self::callAPI($this->params->action, $filter, $this->params->id)));
        }

    }

    /*
     * @param string $module
     * @param string $filter
     * @param string $filterVal
     * @return json array
     */
    function callAPI($module, $filter, $filterVal){

        return $this->apiClass->get_request($module, $filter, $filterVal, 'view');
    }
}