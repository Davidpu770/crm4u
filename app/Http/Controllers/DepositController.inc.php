<?php

namespace Crm4u\Controller;

use Crm4u\Import\user;

class DepositController implements iController {

    static function add()
    {

        $silent = (isset($_POST['silent'])) ? DepositController::silentDeposit($_POST['silent']) : false;
        $deposit = self::assignValue($_POST);

        //checks
        FormController::empty_field();
        FormController::employeeCheck();
        FormController::currencyCheck();
        FormController::ftdCheck($deposit['customer_id']);


        $split = FormController::is_split();
        $split_deposit = (isset($deposit['split_deposit'])) ? true : false;

        FormController::transactionExist($deposit['tran_id'], $deposit['type'], true, $split_deposit);

        if($split_deposit){

            unset($deposit['split_deposit']);
        }

        //Insert Deposit
        InsertController::insert('goal', $deposit);
        LogController::insert('Add Deposit', $deposit['customer_id']);

        //Update last time updated on brands
        UpdateController::update('brands',array("updated" => FormController::getDate()),array("WHERE" => "id = ".BrandController::current_brand()),null,null,false);

        //if is split
        ($split) ? self::split($_POST['split_emp_name'], $deposit['tran_id'], $deposit['type'], $_POST['percentage']) : false;

        //import user that made the deposit
        $finance = user::import_finance($deposit['emp_id']);

        $title = "New {$deposit['type']} - ".user::ConvertID($deposit['emp_id']);

        //check if its FTD

        $ftd_body     = user::ConvertID($deposit['emp_id'])." Made a new FTD for {$deposit['amount']} {$deposit['currency']} he now has ".$finance->current_month->total_ftd." FTDS";
        $deposit_body = user::ConvertID($deposit['emp_id'])." Made a new {$deposit['type']} for {$deposit['amount']} {$deposit['currency']} he now has ". ($finance->current_month->all_deposits + $finance->current_month->all_withdrawals). " USD in total";

        $body = (isset($deposit['ftd']) && $deposit['ftd'] == 1) ? $ftd_body : $deposit_body;


        if(!$silent) {

            NotificationController::send($title, $body, 'deposit');
            NotificationController::add($title, $body, 'deposit');
        }

    }

    static function edit()
    {

        $form = $_POST;

        $form['ftd'] = (isset($form['ftd'])) ? 1 : 0;

        $parameters = array('WHERE' => "id = {$form['id']}");

        UpdateController::update('goal', $form, $parameters);

    }

    function delete()
    {
        // TODO: Implement delete() method.
    }

    static function split($split_emp_id, $tran_id, $type, $percentage){

        $cols = array(
            'tran_id',
            'date',
            'payment_method',
            'clearedby',
            'customer_id',
            'customer_name',
            'emp_id',
            'amount',
            'currency',
            'ftd',
            'type'
        );

        $deposit = ImportController::import('goal',$cols,array('WHERE' => "tran_id = {$tran_id}", 'AND' => "type = '{$type}'"), true);

        //split
        $percentage = array(
            'main' => $deposit['data'][0]['amount'] * (100 - $percentage) / 100,
            'split' => $deposit['data'][0]['amount'] * $percentage / 100
        );

        //edit main deposit
        UpdateController::update('goal',array("amount" => $percentage['main']),array("WHERE" => "tran_id = {$tran_id}"));

        //insert new deposit
        $deposit['data'][0]['amount'] = ($deposit['data'][0]['type'] != 'deposit') ? $percentage['split'] * -1 : $percentage['split'];
        $deposit['data'][0]['emp_id'] = $split_emp_id;
        $deposit['data'][0]['split_deposit']  = true;

        $_POST = $deposit['data'][0];
        DepositController::add();
    }

    function boot(){}

    static function silentDeposit($silent_status){

        if($silent_status == "on"){

            return true;
        }

        return false;
    }

    static function assignValue($deposit){

        global $loader;


        /**
         * EDIT
         */

        $deposit['clearedby']     = (empty($deposit['clearedby'])) ? $loader->site->default_clearedby : $deposit['clearedby'];
        $deposit['campaign']      = (empty($deposit['campaign'])) ? $loader->site->default_campaign : $deposit['campaign'];
        $deposit['country']       = (empty($deposit['country'])) ? $loader->site->default_country : $deposit['country'];
        $deposit['ftd']           = (isset($deposit['ftd']) && $deposit['ftd'] == 'on') ? 1 : 0;
        $deposit['customer_name'] = FormController::removeTags($deposit, 'customer_name');
        $deposit['amount']        = ($deposit['type'] != 'deposit') ? $deposit['amount'] * -1 : $deposit['amount'];

        /**
         * CREATE
         */

        //default
        $deposit['status']        =  (FormController::checkVerification($deposit['customer_id'])) ? 'partial' : $loader->site->default_status;
        $deposit['risk']          =  $loader->site->default_risk;
        $deposit['risk_status']   =  $loader->site->default_risk_status;

        //user & brand
        $deposit['update_by']     =  user::current_user();
        $deposit['brand']         =  BrandController::current_brand();

        //date
        $deposit['date']          =  FormController::getDate();
        $deposit['month']         =  FormController::getMonth();
        $deposit['year']          =  FormController::getYear();

        /**
         * DELETE
         */

        if(empty($deposit['percentage'])) {

            unset($deposit['percentage']);
            unset($_POST['percentage']);
        }

        if(isset($deposit['split'])){

            unset($deposit['split_emp_name']);
            unset($deposit['percentage']);
        }
        unset($deposit['silent']);

        return $deposit;

    }
}