<?php

namespace Crm4u\Controller;

use Crm4u\Middleware\Printer;
use Crm4u\Models\currency;

class CurrencyController {

    static function cronUpdate(){

        $converter = self::getLiveCurrency();

        self::change($converter->result, false);


    }

    static function getLiveCurrency(){

       return new currency('asArray', array());
    }

    static function change($converter, $showSuccessMessage = true){

        $converter->result['USD'] = 1;

        $jsonConverter = json_encode($converter->result);

        file_put_contents(dirname(__DIR__).'/../../public/js/currenceyConvertor.json', $jsonConverter);

        if($showSuccessMessage) {

            new SwalController('success', 'Currency Changed successfuly', 'success');

        }
    }
}