<?php
namespace Crm4u\Controller;

/**
 * Class Password
 * can check password strong
 * can salt password
 * @package Crm4u\Controller
 */
class PasswordController
{

    static function CheckPassword($Password, $rePassword = null, $reset = false)
    {

        if ($reset) {

            PasswordController::passwordEmpty($Password, $rePassword);
            PasswordController::compPassword($Password, $rePassword);
        }

        FormController::strongPasswordCheck($Password);

        return true;
    }

    static function passwordEmpty($password, $repassword)
    {

        if ($password == '' || $repassword == '') {

            new SwalController("Empty Password", "Password field cannot be empty.", "error");
            die();
        }
    }

    static function compPassword($password, $repassword)
    {

        if ($password != $repassword) {

            new SwalController("Password not match", "The passwords are unmatched", "error");
            die();
        }
    }

    static function salt_password($password)
    {

        $password = crypt($password, '$1$sAles$');

        return $password;
    }
}