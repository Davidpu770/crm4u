<?php
/**
 * Copyright (c) 2017. All copyright reserved to David Pugatsch
 */

namespace Crm4u\Controller;

class TitleController {
    
    /*
    *the brand according to session
    *@var string
    */
    protected $brand_name;
    
    /*
    *the title need to be set in the header of the page
    *@var string
    */
    public $title;

    
    function __construct($brand_name = 'Undefined', $title = 'Undefined'){
        
        $this->brand_name = $brand_name;
        $this->title      = $title;
    }
    
    function set_title(){
        
        print "<title>".$this->title." | ".$this->brand_name."</title>"; 
            
    }
    
} 
 