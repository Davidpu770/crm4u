<?php

namespace Crm4u\Controller;

use Crm4u\Import\user;

class gitController{

    public $scriptFile;

    function __construct($type, $params)
    {

        $this->scriptFile = '/pull.sh';

        $this->$params[0]();

    }

    function update()
    {
        $contents = file_get_contents(dirname(__DIR__)."/../..".$this->scriptFile);

        LogController::insert('Git Pulled');

        $log = self::exec($contents);

        new SwalController('success',$log, 'success');

    }

    static function exec($cmd){

        return shell_exec($cmd." 2>&1");

    }

    static function getVersion(){

        $commitHash = trim(exec('git log --pretty="%h" -n1 HEAD'));

        return $commitHash;
    }

    static function checkUpdate(){

        $logger = 'git remote -v update';

         return self::exec($logger);
    }
}