<?php

namespace Crm4u\Controller;

use PHPMailer;

class MailController {

   static function send($addressList = array(), $subject = '', $body = '', $altBody = ''){

       $mail = new PHPMailer();

       $mail->isSMTP();

       $mail->Host = 'smtp.gmail.com';

       $mail->Port = 587;

       $mail->SMTPSecure = 'tls';

       $mail->SMTPAuth = true;

       $mail->isHTML(true);

       $mail->Username = "david.poga@gmail.com";

       $mail->Password = "wfhntvngfdobhley";



       $mail->setFrom('notification@crm4u.me', 'CRM4U Team'); //will change to db send mail from

       foreach ($addressList as $key => $address) {

           $mail->addAddress($address);
       }

       $mail->Subject  = $subject;
       $mail->Body     = $body;
       $mail->AltBody  = $altBody;

       if (!$mail->send()) {
           FormController::Debug("Mailer Error: " . $mail->ErrorInfo);
       }
   }

   static function Templater($title, $body, $teamplate_name = 'simple'){

       $email_template_location = dirname(__DIR__)."/../../public/view/email/{$teamplate_name}.html";

       $email = file_get_contents($email_template_location);

       $email = str_replace("%TITLE%", $title, $email);
       $email = str_replace("%BODY%", $body, $email);

       return $email;

   }

}