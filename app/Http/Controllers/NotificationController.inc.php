<?php
namespace Crm4u\Controller;

use Crm4u\Import\user;

/**
 * Class NotificationController
 * @package Crm4u\Controller
 *
 */
class NotificationController {

    /**
     * This function send notification
     * @param string $title
     * @param string $body
     * @param string $type
     * @param array $participants
     * @param array $url
     *
     */
    static function send($title, $body, $type, $filters = null, $participants = array('All'), $url = array()){

        global $loader;

        //1. -->Notification Title
        $title = array(

            "en" => $title

        );

        //2. -->Notification Body
        $body = array(

            "en" => $body

        );

        $tags = array();

        $type = $type."_icon";
        //4. -->Notification values
        $notification = array(
            'app_id'             => $loader->site->onesignal_id,
            'tags'               => $tags,
            'included_segments'  => $participants,

            'headings'           => $title,
            'contents'           => $body,
            'chrome_web_icon'    => $loader->site->$type,
            'url'                => $url
        );

        $filter = (!is_null($filters) && is_array($filters)) ? array('filters' => $filters) : null;

        $notification = (is_array($filter)) ? array_merge($notification, $filter) : $notification;

        //5. -->encode to json format
        $notification = json_encode($notification);

//        6. -->start sending to onesignal server
        $ch = curl_init();

        //7. -->connect to onesignal api
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");

        //8. -->Authorization
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Authorization: Basic '.$loader->site->autorization
            )
        );

        //9. -->set few other values
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $notification);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //10. execute to server
        curl_exec($ch);

        //11. close connection
        curl_close($ch);
    }

    static function manualSend($params){

        FormController::empty_field();
        NotificationController::send($params[0], $params[1], $params[3], NotificationController::UserFilter($params[2]));
        new SwalController('Sent...','Notification succssefuly sent','success');
    }

    static function CurrentUserFilter(){

        return array(array("field" => 'tag','key' => 'uid', 'relation' => '=', 'value' => user::current_user()));
    }

    static function UserFilter($uid){

        if($uid != "false"){

            return array(array("field" => 'tag','key' => 'uid', 'relation' => '=', 'value' => $uid));
        }

    }

    static function add($title, $body, $type){

            $values = array(
                'action'     => $type,
                'head'       => $title,
                'content'    => $body,
                'created'    => FormController::getDate(),
                'created_by' => user::current_user(),
                'brand'      => BrandController::current_brand()
            );

            InsertController::insert('notification', $values, false);
    }
    
}