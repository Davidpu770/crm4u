<?php

namespace Crm4u\Controller;

use Crm4u\Import\brand;

/**
 * Class ReportController
 * @package Crm4u\Controller
 * @todo add option to chose how often report will be generated
 */
class ReportController {

    static function DailyReport(){

        global $loader;

        $brandTotals = new \stdClass();

        foreach ($loader->all_brand['data'] as $brand){

            $brandName = $brand['brand_name'];

            $brandTotals->$brandName = brand::import_finance($brand['id'], 'day');

            $subject = "Your Deposit Daily total for ".FormController::getDate();

            $body  = "<h1>The total deposit for today:</h1> \n\n";
            $body .= "<p>Total Deposits - {$brandTotals->$brandName->current_month->all_deposits} USD</p> \n\n";
            $body .= "<p>Total Withdrawal - {$brandTotals->$brandName->current_month->all_withdrawals} USD</p> \n\n";
            $body .= "<p>Your confirm deposit for today - {$brandTotals->$brandName->current_month->total_conf} USD</p> \n\n";

            MailController::send(array('david.poga@gmail.com'),$subject, MailController::Templater($subject, $body));
        }
    }
}