<?php
/**
 * Copyright (c) 2017. All copyright reserved to David Pugatsch
 */

namespace Crm4u\Controller;

class SwalController{

    //the error message title;
    public $title;

    //the error message
    public $message;

    //the type are error; but it can be warning if set
    public $type;

    function __construct($title = '', $message = '', $type = '', $die_message = false){

        //the error message title;
        $this->title = $title;

        //the error message
        $this->message = $message;

        //the header are error; but it can be warning if set
        $this->type = $type;

        //run the error
        $this->printError($die_message);

    }

    function printError($die_message){

        $message = array(
            "flag"    => 0,
            "header"  => $this->title,
            "message" => $this->message,
            "type"    => $this->type
        );

        print_r(json_encode($message));

        ($die_message) ? die() : false;
    }
}