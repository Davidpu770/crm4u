<?php

namespace Crm4u\Controller;

use Crm4u\Middleware\CheckAccess;
use Crm4u\SQL\filter;
use Crm4u\SQL\Query;


class ImportController extends filter {

    static function import($sql_table, $sql_columns = array(),$sql_options = array(), $show_query = false){

        $isOnline = CheckAccess::isOnline(false, false);

        $isAuthorized = CheckAccess::isAuthorized(2, false);


        global $loader;


            $sql = "SELECT ".implode(",", $sql_columns)." FROM ".$sql_table;

            if(count($sql_options) > 0) {

                foreach ($sql_options as $key => $value) {

                    $sql .= (!is_int($key)) ?  " $key $value" : " $value";
                }
            }

        $items = array(
            'data' => array(),
            'status' => 'not_initialize',
            'count' => 0
        );

        $result = Query::init($sql);

            if($show_query){

                echo "<pre>";
                echo $sql;
                echo "</pre>";
            }


            if(!$result){

                new SwalController('You have an error',$loader->mysqli->error,'error');

            } else {

                $items['status'] = 'no_result';
                $items['count'] = 0;

                if ($result->field_count > 0) {

                    while ($row = $result->fetch_assoc()) {

                        foreach ($row as $key => $value) {

                            $item[$key] = $value;
                        }

                        $items['data'][] = $item;
                        $items['status'] = 'success';
                        $items['count'] = count($items['data']);
                    }
                }


                return $items;
            }
    }
}