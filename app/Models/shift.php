<?php

namespace Crm4u\Models;

use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Fetch;
use Crm4u\Middleware\Printer;
use Crm4u\SQL\filter;

class Shift
{
    public $result;

    function __construct($type)
    {

        $only_online = (isset($type) && $type == 'online') ? 1 : 0;

        $filter = new filter();

        $filter->table = 'shift';

        $col = array(
            'shift.id',
            "CONCAT(users.firstname,' ',users.lastname) name",
            "desk.desk_name",
            'shift.active',
            'shift.started',
            'shift.ended',
            'TIMESTAMPDIFF(hour, started, ended) total_hour');

        $parameters = array(

            "INNER JOIN" => "users",
            "ON"         => 'users.user_key_id = shift.uid',

            "INNER JOIN " => "desk",
            "ON "         => 'users.location = desk.id',

            "WHERE"      => '0=0',
            $filter->userFilter(0, 'uid'),
            $filter->deskFilter(0),
            $filter->dateFilter(1,'month', 'started'),
            $filter->onlineFilter($only_online),
            );



        $shifts = ImportController::import('shift', $col, $parameters);



        $col = array(
            'SUM(TIMESTAMPDIFF(hour, started, ended)) total_hours',
            'COUNT(shift.id) total_shifts',
            'SUM('.filter::case_when('shift.active',' = 1','+1', 0, true).") online_users",
        );

        $shifts['Total'] = new Fetch('shift',$col,$parameters);

        new Printer('json', $shifts);

    }

}