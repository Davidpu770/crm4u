<?php
/**
 * Copyright (c) 2017. All copyright reserved to David Pugatsch
 */

namespace Crm4u\Models;

use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Printer;

class campaign
{
    function __construct($type, $params)
    {

        $campaign = ImportController::import('campaign',array('*'));

        return new Printer($type, $campaign, $params);
    }

}