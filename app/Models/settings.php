<?php

namespace Crm4u\Models;

use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Printer;

class Settings
{
    public $result;

    function __construct($type)
    {

        switch ($type){

            case "asArray":

                $result = new Printer($type, ImportController::import('site_config_data',array('*'),array('ORDER BY' => 'site_config_data.group_set ASC')));

                $this->result = new \stdClass();

                foreach ($result->result['data'] as $key => $value){

                    $this->result->{$value['path']} = $value['value'];
                }
                break;

            case "withGroup";

                $group_set = ImportController::import('site_config_data',array('group_set'), array('GROUP BY' => 'group_set'));


                foreach ($group_set['data'] as $key => $value){

                    $group_settings = ImportController::import('site_config_data', array('*'), array('WHERE' => '0=0', 'AND' => "group_set = '{$value['group_set']}'"));
                    $this->result->group[$value['group_set']] = $group_settings['data'];

                }

            default:
                new Printer($type, ImportController::import('site_config_data',array('*'),array('ORDER BY' => 'site_config_data.group_set ASC')));
        }


    }

}