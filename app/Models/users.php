<?php

namespace Crm4u\Models;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Printer;
use Crm4u\SQL\filter;

class users
{
    function __construct($type, $params)
    {

        $columns = array(
            'CONCAT(firstname," ",lastname) as user_name',
            'user_key_id id',
            'email',
            'departments.department_name department',
            'last_login',
            'username',
            filter::case_when('desk.desk_name' ,'IS NULL','No desk')
        );

        $parameters = array(
            'LEFT JOIN' => 'desk',
            'ON'        => 'desk.id = users.location',
            "INNER JOIN" => 'departments',
            "ON "      => 'departments.id = users.department',
            "WHERE"     => "brand = '".BrandController::current_brand()."'",
            "AND"       => "disabled = 0"

        );

        if(isset($params[1])){

            if(!is_array($params[1])) {

                $parameters['AND'] = 'user_key_id = '.$params[1];
            }

        }


        $users = ImportController::import('users', $columns, $parameters);



        return new Printer($type, $users, $params);
    }

}