<?php

namespace Crm4u\Models;

class Converter {

    static function USD() {

        return 1;
    }

    static function GBP() {

        $convertor = json_decode(file_get_contents(dirname(__DIR__) . "/../public/js/currenceyConvertor.json"), TRUE);

        return $convertor['GBP'];
    }

    static function EUR() {

        $convertor = json_decode(file_get_contents(dirname(__DIR__) . "/../public/js/currenceyConvertor.json"), TRUE);
        return $convertor['EUR'];
    }

    static function BTC() {

        $convertor = json_decode(file_get_contents(dirname(__DIR__) . "/../public/js/currenceyConvertor.json"), TRUE);
        return $convertor['BTC'];
    }
}
