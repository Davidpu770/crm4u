<?php

namespace Crm4u\Models;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Controller\SwalController;
use Crm4u\Middleware\Printer;
use Crm4u\SQL\filter;

class customer implements iModels {

    function __construct($type, $params)
    {

        if(BrandController::has_api()){

             $result['flag'] = 2;

            return new Printer('json',$result);
        }


        $filter = new filter();


        $filter->__set('type','cid');

        $filter->__set('text',$params[2]);


        $cols = array(
            'customer_name name',
            'currency',
            'campaign',
            'country'
        );

        $parameters = array(
            'WHERE' => '0 = 0',
            $filter->typeFilter(1),
            $filter->limitResult(1)
        );

        $result = ImportController::import('goal',$cols, $parameters);



        if($result['count'] > 0){

            return new Printer('json',$result['data'][0]);

        } else {

            return new SwalController('Customer not exist','Don\'t forget to add FTD', 'info');
        }
    }

}