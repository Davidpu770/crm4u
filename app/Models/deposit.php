<?php

namespace Crm4u\Models;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\ImportController;
use Crm4u\Middleware\CheckAccess;
use Crm4u\Middleware\Printer;
use Crm4u\SQL\filter;
use Crm4u\Controller\FormController;

class deposit
{
    /**
     * deposit constructor.
     * @param $type
     * @param $params
     * $param[0] = type (Deposit/Withdrawals)
     * $param[1] = Format (table/Json)
     * $param[2] = Tran # (for specific transaction)
     */
    function __construct($type, $params)
    {

        $this->table = 'goal';

        $filter = new filter();

        $columns = array(
            'goal.id',
            'goal.date',
            'goal.tran_id',
            'goal.payment_method',
            'goal.clearedby',
            "goal.customer_id",
            "goal.customer_name",
            filter::currencyFilter(array('all')),
            'goal.customer_name cname',
            'goal.country',
            'CONCAT(ename.firstname," ",ename.lastname) ename',
            'CONCAT(update_by.firstname," ",update_by.lastname) update_by',
            'goal.emp_id',
            'goal.amount',
            'goal.currency',
            'goal.ftd',
            'goal.verification_date',
            'goal.campaign',
            'goal.status',
            'goal.risk',
            'goal.comment',
            'COUNT(tran_id) totalCount'
        );

        $join = array(
            'JOIN' => 'users ename',
            'ON' => 'ename.user_key_id = goal.emp_id'
        );

        $join1 = array(
            'JOIN ' => 'users update_by',
            'ON '   => 'update_by.user_key_id = goal.update_by'
        );


        if (isset($params[2]) && is_numeric($params[2]) && FormController::tranCheck($params[2], $params[0], false)) {
            $parameters['WHERE'] = 'goal.id = ' . $params[2];

        } else {

            $parameters = array(

                'WHERE' => '0 = 0',
                $filter->dateFilter(1),
                $filter->userFilter(1),
                $filter->typeFilter(1),
                $filter->ftdFilter(0),
                $filter->shiftFilter(0),
                $filter->statusFilter(0),
                $filter->brandFilter(1),
                $filter->riskFilter(1),
                $filter->countriesFilter(1),
                $filter->deskFilter(0),
                $filter->paymentFilter(1),
                $filter->depositTypeFilter('Deposit'),
                $filter->departmentFilter('ename',1),
                $filter->deskFilter(1, 'ename'),


            );
        }


        $parameters = array_merge($join, $join1, $parameters);

        $parameters['GROUP BY'] = 'goal.id';
        $parameters['ORDER BY'] = 'goal.tran_id DESC';

        $parameters[]= $filter->limitFilter();

        $response = ImportController::import('goal', $columns, $parameters);



        //Date in Readable format
        if (isset($params[1]) && method_exists(self::class, $params[1])) {

            $deposits = $this->{$params[1]}($response);
        }

        $deposits['iTotalRecords'] = count($response['data']);
        $deposits['iTotalDisplayRecords'] = count($response['data']);

        return new Printer($type, $deposits, $params);
    }

    function table($response)
    {

        global $loader;

        $status_color = array(
            "none" => "danger",
            "partial" => "warning",
            "full" => "success",
            "released" => "info"
        );


        $risk_color = array('low' => 'info', 'high' => 'danger');

        $status_option = array('full', 'partial', 'none', 'released');
        $risk_option = array('low', 'high');

        if (count($response['data']) > 0) {
            foreach ($response['data'] as $key => $value) {

                $new_date = date('d/m - H:i', strtotime($value['date']));

                $deposits['data'][] = array(
                    'id'   => $value['id'],
                    'date' => $new_date,
                    'tran_id' => FormController::linkConvert($loader->brand->transaction_page, 'TRAN_ID', $value['tran_id']),
                    'customer_id' => FormController::linkConvert($loader->brand->customer_page, 'CID', $value['customer_id']),
                    'payment_method' => $value['payment_method'],
                    'inUSD' => $value['USD'] + $value['EUR'] + $value['GBP'] + $value['BTC'],
                    'cname' => $value['cname'],
                    'ename' => $value['ename'],
                    'update_by' => $value['update_by'],
                    'emp_id'  => $value['emp_id'],
                    'amount' => $value['amount'],
                    'country' => $value['country'],
                    'currency' => $value['currency'],
                    'verification_date' => $value['verification_date'],
                    'clearedby' => $value['clearedby'],
                    'campaign' => $value['campaign'],
                    'risk' => (CheckAccess::isAdmin()) ? FormController::selectConvert($risk_option, 'risk', $value['risk']) : FormController::spanConvert($value['risk'], $risk_color[$value['risk']]),
                    'comment' => FormController::textareaConvert('comment', '/update/goal', 'comment', $value['comment']),
                    'ftdValue' => $value['ftd'],
                    'ftd' => FormController::checkConvert($value['ftd']),
                    'status' => (CheckAccess::isAdmin()) ? FormController::selectConvert($status_option, 'status', $value['status']) : FormController::spanConvert($value['status'], $status_color[$value['status']]),
                    'is_new' => self::newDeposit($value['date'])
                );
            }

            $deposits['totals'] = Totals::get();

            return $deposits;
        } else {

            $deposits['data'] = [];

            return $deposits;
        }

    }

    function newDeposit($depositDate){

        if(is_string($_SESSION['visit_deposit'][BrandController::current_brand()])) {

            if($depositDate > $_SESSION['last_login']){

                return true;
            }
        }
        return false;
    }

    function json($response)
    {
        foreach ($response['data'] as $key => $value) {

            $deposits['data'][] = array(
                'id'   => $value['id'],
                'date' => $value['date'],
                'tran_id' => $value['tran_id'],
                'customer_id' => $value['customer_id'],
                'customer_name' => $value['customer_name'],
                'payment_method' => $value['payment_method'],
                'inUSD' => $value['USD'] + $value['EUR'] + $value['GBP'] + $value['BTC'],
                'cname' => $value['cname'],
                'ename' => $value['ename'],
                'emp_id'  => $value['emp_id'],
                'amount' => $value['amount'],
                'country' => $value['country'],
                'currency' => $value['currency'],
                'verification_date' => $value['verification_date'],
                'clearedby' => $value['clearedby'],
                'campaign' => $value['campaign'],
                'risk' => $value['risk'],
                'comment' => $value['comment'],
                'ftdValue' => $value['ftd'],
                'ftd' => $value['ftd'],
                'status' => $value['status']);
        }

        $deposits['totals'] = Totals::get();
        return $deposits;
    }

}