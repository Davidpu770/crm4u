<?php

namespace Crm4u\Models;

use Crm4u\Controller\BrandController;
use Crm4u\Controller\FormController;
use Crm4u\Import\brand;
use Crm4u\Middleware\Printer;

class Totals implements iModels
{
    function __construct($type, $params)
    {
        $totals = brand::import_finance(BrandController::current_brand());

        return new Printer($type, $totals, $params);
    }

    static function get(){

        return brand::import_finance(BrandController::current_brand());
    }
}