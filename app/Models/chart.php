<?php

namespace Crm4u\Models;

use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Printer;
use Crm4u\SQL\filter;

class Chart implements iModels{

    protected $filters;

    function __construct($type, $params)
    {

        $this->filters = new filter();
        $this->explainIncome('users');
    }


    /**
     * @param array $cols
     * @param array $parameters
     */
    function import($sql_table, $cols, $parameters){

        $result = ImportController::import($sql_table, $cols, $parameters);

       new Printer('json',$result['data']);

    }

    function explainIncome($type){

        $cols = array(
            ($type == 'users') ? "CONCAT(users.firstname,' ',users.lastname) name" : $filter->table.".".$type." name",
            'goal.month',
            'goal.year',
            filter::Sum(false,false,'goal.amount','s_amount'),
        );

        $join = array(
            'INNER JOIN' => 'users',
            'ON'         => 'goal.emp_id = users.user_key_id',
            'AND'        => 'goal.brand  = users.brand'
        );

        $parameters = array(
            $this->filters->userFilter(0),
            $this->filters->dateFilter(1),
            $this->filters->typeFilter(1),
            $this->filters->shiftFilter(0),
            $this->filters->statusFilter(0),
            $this->filters->brandFilter(0),
            $this->filters->countriesFilter(1),
            "GROUP BY"  => ($type == "users") ? "goal.emp_id" : "goal.".$type
        );

        $parameters = array_merge($join, $parameters);

        $this->import('goal',$cols, $parameters);
    }




}