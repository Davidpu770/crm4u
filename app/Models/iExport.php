<?php

namespace Crm4u\Export;

use Crm4u\Controller\FormController;
use Crm4u\Controller\UserController;
use Crm4u\Import\user;
use Crm4u\Models\users;

interface iExport{

    const goal = array(

        'cols' => array(
            'tran_id',
            'type',
            'payment_method',
            'date',
            'customer_name',
            'currency',
            'amount',
            'customer_id',
            "CONCAT(users.firstname,' ',users.lastname) emp_name",
            'comment',
            'ftd',
            'status',
            'waiting_verification',
            'verification_date'
        ),

        'filter' => array(
            'dateFilter'        => 1,
            'userFilter'        => 1,
            'typeFilter'        => 1,
            'ftdFilter'         => 0,
            'shiftFilter'       => 0,
            'statusFilter'      => 0,
            'brandFilter'       => 1,
            'riskFilter'        => 1,
            'countriesFilter'   => 1,
            'deskFilter'        => 0,
            'paymentFilter'     => 1,
        ),

        'join' => array(
            'users' => array('user_key_id' => 'emp_id','brand' => 'brand')
        )

    );

    const shift = array(

        'cols' => array(
            "CONCAT(users.firstname,' ',users.lastname) emp_name",
            'started',
            'ended',
            'TIMESTAMPDIFF(hour, started, ended) total_hours',
        ),

        'join' => array(
            'users' => array('user_key_id' => 'uid')
        ),

        'filter_table' => 'shift',

        'filter' => array(
            'userFilter'    => array(1, 'uid'),
            'deskFilter'    => 0,
            'dateFilter'    => array(1, 'month', 'started')
        )

    );
}