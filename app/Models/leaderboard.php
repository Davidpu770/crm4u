<?php

namespace Crm4u\Models;

use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Import\user;
use Crm4u\Middleware\Printer;
use Crm4u\SQL\filter;

class Leaderboard implements iModels {

    /**
     * @var filter
     */
    public $filter;

    function __construct($type, $params)
    {

        $this->filter = new filter();

        $cols = array(
            'CONCAT(users.firstname," ",users.lastname) ename',
            'emp_id',
            $this->filter->currencyFilter(),
            $this->filter->countFtd(),
        );

        $join = array(
            'JOIN' => 'users',
            'ON' => 'users.user_key_id = goal.emp_id'
        );

        $parameters = array(
            'WHERE' => '0 = 0',
            $this->filter->dateFilter(1, 'week'),
            $this->filter->userFilter(1),
            'GROUP BY' => 'ename',
            'ORDER BY' => 'USD DESC ',

        );

        $result = ImportController::import('goal', $cols, array_merge($join, $parameters));

        $i = 1;

        if(count($result['data']) > 0){

            foreach ($result['data'] as $value) {

                $total = $value['USD'] + $value['EUR'] + $value['GBP'] + $value['BTC'];

                $userData = user::import_finance($value['emp_id']);

                $users['data'][] = array(
                    'rank'    => $i,
                    'name'    => $value['ename'],
                    'total'   => (int) $total,
                    'count'   => (int) $value['countftd'],
                    'total_m' => (int) $userData->current_month->all_deposits,
                    'count_m' => (int) $userData->current_month->total_ftd
                );


                $i++;

            }

        } else {

            $users['data'] = array();
        }


        $users['leaders']['monthly'] = $this->getLeader('month');
        $users['leaders']['weekly'] = $this->getLeader('week');

        if(isset($users) && is_array($users)){

            return new Printer($type, $users, $params);

        } else {

            return FormController::EmptyData();
        }

    }

    function getLeader($period){

        $this->filter->table = 'goal';

        $table = "( SELECT ".
            $this->filter->currencyFilter(array('all')).
            ",".
            " CONCAT(users.firstname,' ',users.lastname) ename, date".
            " FROM goal".
            " INNER JOIN users ON users.user_key_id = goal.emp_id".
            " WHERE 0 = 0".
            $this->filter->dateFilter(1).
            " GROUP BY tran_id".
            ") t";

        $cols = array(
            't.ename',
            'SUM(t.USD + t.GBP + t.EUR) total'
        );

        $join = array(
            'LEFT JOIN' => 'users',
            'ON' => 'users.user_key_id = goal.emp_id'
        );

        $this->filter->table = 't';

        $parameters = array(
            'WHERE' => '0 = 0',
            $this->filter->dateFilter(1,$period),
            'GROUP BY' => 'ename',
            'ORDER BY' => 'total DESC',
            'LIMIT'    => '1'
        );

        $result = ImportController::import($table, $cols, $parameters);

        return (count($result['data']) === 0) ? false : $result['data']['0']['ename'];
    }

}