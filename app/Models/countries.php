<?php

namespace Crm4u\Models;

use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Printer;

class countries
{
    function __construct($type, $params)
    {

        $countries = ImportController::import('countries',array('*'));

        return new Printer($type, $countries, $params);
    }

}