<?php

namespace Crm4u\Models;

use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Printer;

class brands
{
    function __construct($type, $params)
    {

        $brands = ImportController::import('brands',array('*'));

        return new Printer($type, $brands, $params);
    }

}