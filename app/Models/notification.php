<?php

namespace Crm4u\Models;

use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Printer;

class notification implements iModels
{
    function __construct($type, $params)
    {

        $notification = ImportController::import('notification',array('id, created, head, content, action'),array('ORDER BY' => 'created DESC', 'LIMIT' => 5));

        return new Printer($type, $notification, $params);
    }

}