<?php

namespace Crm4u\Models;

use Crm4u\Controller\BrandController;
use Crm4u\Import\brand;


class task implements iModels {

    function __construct($type, $params)
    {
        $notify_data = new \stdClass();
        $notify_data->notify = new \stdClass();


        $deposit_target = brand::import_finance(BrandController::current_brand());

        $notify_data->notify->message = array(
            self::DepositTarget($deposit_target),
            self::VerifiedTarget($deposit_target)
        );
        return print_r(json_encode($notify_data->notify));
    }


    static function VerifiedTarget($notification){

        global $loader;

        if($loader->user->goal_deposit > 0) {

            $tillTarget = $loader->user->goal_deposit - $notification->current_month->all_deposits;
            $par = round($notification->current_month->all_deposits / $loader->user->goal_deposit * 100,1);

            $html  = "<li>";
            $html .= "<a href='#'>";
            $html .= "<h3>";
            $html .= $tillTarget."$/".$loader->user->goal_deposit."$ To reach target";
            $html .= "<small class='pull-right'>".$par."%</small>";
            $html .= "</h3>";
            $html .= "<div class='progress xs'>";
            $html .= "<div class='progress-bar progress-bar-blue' style='width: ".$par."%'";
            $html .= "role='progressbar' aria-valuenow='".$par."' aria-valuemin='0' aria-valuemax='100'>";
            $html .= "<span class='sr-only'>".$par."% Complete</span>";
            $html .= "</div>";
            $html .= "</div>";
            $html .= "</a>";
            $html .= "</li>";

            return $html;
        }

    }


    static function DepositTarget($notification){

        if($notification->current_month->total_confirm_count >= 0) {

            $unverified = $notification->current_month->total_deposit_count - $notification->current_month->total_confirm_count ."/".$notification->current_month->total_deposit_count;

            $html  = "<li>";
            $html .= "<a href='#'>";
            $html .= "<h3>";
            $html .= ($notification->current_month->total_deposit_count > 0) ? $unverified." Deposits not Verified" : "All deposit verified!";
            $html .= "<small class='pull-right'>".$notification->current_month->par_conf."%</small>";
            $html .= "</h3>";
            $html .= "<div class='progress xs'>";
            $html .= "<div class='progress-bar progress-bar-aqua' style='width: ".$notification->current_month->par_conf."%'";
            $html .= "role='progressbar' aria-valuenow='".$notification->current_month->par_conf."' aria-valuemin='0' aria-valuemax='100'>";
            $html .= "<span class='sr-only'>".$notification->current_month->par_conf."% Complete</span>";
            $html .= "</div>";
            $html .= "</div>";
            $html .= "</a>";
            $html .= "</li>";

            return $html;
        }

    }

}