<?php
/**
 * Copyright (c) 2017. All copyright reserved to David Pugatsch
 */

namespace Crm4u\Models;

use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Printer;
use Crm4u\SQL\filter;

class bonuses
{
    function __construct($type, $params)
    {

        $filter = new filter();

        $filter->table = 'bonuses';

        $referral = explode('/',$_SERVER['HTTP_REFERER']);

        $parameters = array(
            'WHERE' => "object_type = '{$referral[3]}'",
            'AND'   => "object_id   = {$referral[4]}",
            $filter->bonusFilter(1),

        );

        $bonus = ImportController::import('bonuses',array('*'), $parameters);

        return new Printer($type, $bonus, $params);
    }

    static function import($type, $object_type, $object_id){

        $cols = array('strip_min','strip_max','strip_percentage');

        $parameters = array(
            'WHERE' => "object_type = '{$object_type}'",
            'AND'   => "object_id   = {$object_id}",
            'AND '  => "bonus_type  = '{$type}'"
        );

        $bonus = ImportController::import('bonuses',$cols, $parameters);

        return $bonus;
    }

}