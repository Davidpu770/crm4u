<?php

namespace Crm4u\Models;

use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Controller\InsertController;
use Crm4u\Middleware\Printer;
use Crm4u\Middleware\Fetch;
use Crm4u\SQL\filter;

class Desks
{
    function __construct($type, $params)
    {
        $locations = ImportController::import('desk', array('*'));

        if(count($locations['data']) == 0){

            $this->boot();
        }

        foreach ($locations['data'] as $key => $value ){

            $userSum = ImportController::import(
                'users',
                array('location',
                    'SUM(location) as userSum'
                ),
                array(
//                    'INNER JOIN' => 'goal',
//                    'ON' => 'users.user_key_id = goal.emp_id',
//                    "AND" => 'users.firstname = goal.firstname',
//                    "AND" => 'users.lastname = goal.lastname',
//                    "AND" => 'users.brand = goal.brand',
                    'WHERE' => 'location = '.$value['id'],
                    'GROUP BY' => 'location')
            );

            $locations['data'][$key]['userSum'] = (count($userSum['data']) > 0) ? (int) $userSum['data'][0]['userSum'] : 0;

        }

        return new Printer($type, $locations, $params);
    }

    static function import($desk_id){

        $cols = array(
            'desk.id',
            'desk_name',
            'desk_target goal_deposit',
            'desk_target goal_ftd', //todo ftd
            'concat(users.firstname, " ",users.lastname) desk_manager',
            'desk_manager desk_manager_id'
        );

        $join = array(
            'LEFT JOIN' => 'users',
            'ON' => 'users.user_key_id = desk.desk_manager'
        );

        $parameters = array('WHERE' => "desk.id = {$desk_id}");

        $result = new Fetch('desk',$cols,array_merge($join, $parameters));

        return $result->result;

    }

    static function import_finance($desk_id){

        global $loader;

        $filter = new filter();

        $columns = array(
            filter::currencyFilter(array('all','confirm','with_cb')),
            filter::countFTD()
        );

        $join = array(
            'LEFT JOIN' => 'users',
            'ON' => 'users.user_key_id = goal.emp_id'
        );


        $parameters = array(
            "WHERE"  => "0=0",
            $filter->deskFilter(1, 'users', $desk_id)
        );

        $parameters_extra = array(
            "AND"    => "month = '".FormController::getMonth()."'",
            "AND "   => "year =  '".FormController::getYear()."'"
        );

        $current_month = new Fetch('goal', $columns, array_merge($join, $parameters, $parameters_extra));
        $all_time      = new Fetch('goal', $columns, array_merge($join, $parameters));

        $finance  = new \stdClass();

        $finance->current_month               = new \stdClass();
        $finance->all_time                    = new \stdClass();

        $finance->current_month->all_deposits    = $current_month->result->USD + $current_month->result->EUR + $current_month->result->GBP;
        $finance->current_month->total_conf      = $current_month->result->confUSDdeposits + $current_month->result->confEURdeposits + $current_month->result->confGBPdeposits;
        $finance->current_month->total_ftd       = $current_month->result->countftd;
        $finance->current_month->all_withdrawals = $current_month->result->USDWithdrawals + $current_month->result->EURWithdrawals + $current_month->result->GBPWithdrawals;


        $finance->all_time->total_deposits    = $all_time->result->USD + $all_time->result->EUR + $all_time->result->GBP;
        $finance->all_time->ave_income        = $finance->all_time->total_deposits / $loader->user->exist_period;

        return $finance;
    }

    function boot(){

        $default_desk = array(
            'desk_name' => "Main Desk",
            'desk_target' => '300000'
        );

        $insert = new InsertController(array(false,false,false), true);
        $insert->boot('desk',$default_desk);
    }
}