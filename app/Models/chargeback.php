<?php

namespace Crm4u\Models;

use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Printer;
use Crm4u\SQL\filter;

class chargeback implements iModels{

    function __construct($type, $params)
    {
        $filter = new filter();
        $filter->table = 'chargeback';

        $cols = array('chargeback.*',"CONCAT(users.firstname,' ',users.lastname) employee_name");

        $join = array(
            'LEFT JOIN' => 'users',
            'ON' => 'users.user_key_id = chargeback.emp_id'
        );

        $parameters = array();

        $response = ImportController::import($filter->table, $cols, array_merge($join, $parameters));

        //Date in Readable format
        if (isset($params[1]) && method_exists(self::class, $params[1])) {

            $chargebacks = $this->$params[1]($response);
        }

        return new Printer($type, $chargebacks, $params);
    }

    function table($response){

        $dispute = array('No','Yes');

        foreach ($response['data'] as $key => $chargeback) {

            $response['data'][$key]['dispute_sent'] = FormController::selectConvert($dispute, 'send_dispute', self::disputeStatusConvert($response['data'][$key]['dispute_sent']));
        }

        return $response;
    }

    static function disputeStatusConvert($dispute){


        switch ($dispute) {
            case 1:
                return "Yes";

            case 0:
                return "No";
        }
    }
}