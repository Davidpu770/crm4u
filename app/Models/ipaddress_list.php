<?php

namespace Crm4u\Models;

use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Printer;

class ipaddress_list
{
    function __construct($type)
    {

        $ip = ImportController::import('ipaddress_list',array('*'));

        foreach ($ip['data'] as $key => $value){

            switch ($ip['data'][$key]['flag']){

                case "1": $ip['data'][$key]['flag'] = "Blocked";
                case "0": $ip['data'][$key]['flag'] = "Allowed";
            }
        }

        return new Printer($type, $ip);
    }

}