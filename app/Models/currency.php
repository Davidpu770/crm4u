<?php

namespace Crm4u\Models;

use Crm4u\Controller\FormController;
use Crm4u\Middleware\Printer;


class currency implements iModels {

    /**
     * @var Printer;
     */

    public $result;

    /**
     * currency constructor.
     * @param $type
     * @param $params
     */
    function __construct($type, $params)
    {

        $currencies = array(
            'base' => array('EUR','GBP'),
            'symbols' => array('USD')
        );

        $currency = self::get_currencies($currencies);

        return $this->result = new Printer($type, $currency, $params);

    }

    function get_currencies($currencies){

        foreach ($currencies['base'] as $key => $value){

            $get = json_decode(file_get_contents("http://api.fixer.io/latest?symbols=".$currencies['symbols'][0]."&base=".$value), true);

            $updated_value[$get['base']] = round($get['rates']['USD'], 2);
        }

        $updated_value['BTC'] = $this->getBTC();

        return $updated_value;
    }

    function getBTC(){

        $get = json_decode(file_get_contents("https://blockchain.info/ticker"));

        return $get->USD->last;

    }
}