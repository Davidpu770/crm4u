<?php

namespace Crm4u\Models;

use Crm4u\Controller\ImportController;
use Crm4u\Middleware\Printer;
use Crm4u\SQL\filter;

class logs implements iModels {

    function __construct($type, $params)
    {

        $filter = new filter();
        $filter->table = 'logs';

        $cols = array(
            "CONCAT(users.firstname,' ',users.lastname) employee_name",
            "goal.customer_name customer_name",
            "logs.*"
        );

        $join = array(
            'LEFT JOIN' => 'users',
            'ON' => 'logs.employee_id = users.user_key_id'
        );

        $join1 = array(
            'LEFt JOIN' => 'goal',
            'On' => 'goal.customer_id = logs.customer_id AND goal.emp_id = logs.employee_id'
        );

        $parameters = array(
            'WHERE' => '0=0',
            'ORDER BY' => 'logs.id DESC',
            'LIMIT' => '500'
        );


        $log = ImportController::import('logs',$cols,array_merge($join, $join1, $parameters));

        return new Printer($type, $log, $params);
    }
}