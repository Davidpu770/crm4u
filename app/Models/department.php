<?php
/**
 * Copyright (c) 2017. All copyright reserved to David Pugatsch
 */

namespace Crm4u\Models;

use Crm4u\Controller\FormController;
use Crm4u\Controller\ImportController;
use Crm4u\Import\user;
use Crm4u\Middleware\Fetch;
use Crm4u\Middleware\Printer;
use Crm4u\SQL\filter;

class department
{
    function __construct($type, $params)
    {

        $cols = array(
            'departments.id',
            'department_name',
            'department_target',
            'desk.desk_name desk_name',
            'COUNT(CASE WHEN count.department = departments.id THEN 1 END) user_in_department',
            'CONCAT(manager.firstname, " ",manager.lastname) department_manager',
            'department_manager department_manager_id'
        );

        $join = array(
            'JOIN' => 'users manager',
            'ON' => 'manager.user_key_id = departments.department_manager',


            'left Join' => 'users count',
            'On'   => 'count.department = departments.id',

            'left join' => 'desk',
            'on'   => 'departments.desk_parent = desk.id'
        );

        $filter = new filter();

        $filter->table = 'departments';

        $parameters = array(
            'WHERE'    => '0 = 0',
            $filter->deskParentFilter(1),
            'GROUP BY' => 'departments.id'
        );

        $department = ImportController::import('departments',$cols, array_merge($join, $parameters));

        return new Printer($type, $department, $params);
    }

    static function import($department_id){

        $cols = array(
            'departments.id',
            'department_name',
            'department_target goal_deposit',
            'department_ftd_target goal_ftd',
            'desk.desk_name desk_parent_name',
            'concat(users.firstname, " ",users.lastname) department_manager',
            'department_manager department_manager_id'
        );

        $join = array(
            'JOIN' => 'users',
            'ON' => 'users.user_key_id = departments.department_manager'
        );

        $join1 = array(
            'LEFT JOIN ' => 'desk',
            'ON ' => 'desk.id = departments.desk_parent'
        );

        $parameters = array(
            'WHERE' => "departments.id = {$department_id}"
        );

        $result = new Fetch('departments',$cols,array_merge($join,$join1, $parameters));

        return $result->result;
    }

    static function import_finance($department_id)
    {
        global $loader;

        $filter = new filter();

        $columns = array(
            filter::currencyFilter(array('all','confirm','with_cb')),
            filter::countFTD()
        );

        $join = array(
            'LEFT JOIN' => 'users',
            'ON' => 'users.user_key_id = goal.emp_id'
        );


        $parameters = array(
            "WHERE"  => "0=0",
            $filter->departmentFilter('users',1, $department_id)
            );



        $parameters_extra = array(
            "AND"    => "month = '".FormController::getMonth()."'",
            "AND "   => "year =  '".FormController::getYear()."'"
        );

        $current_month = new Fetch('goal', $columns, array_merge($join, $parameters, $parameters_extra));
        $all_time      = new Fetch('goal', $columns, array_merge($join, $parameters));

        $finance  = new \stdClass();

        $finance->current_month               = new \stdClass();
        $finance->all_time                    = new \stdClass();

        $finance->current_month->all_deposits    = $current_month->result->USD + $current_month->result->EUR + $current_month->result->GBP;
        $finance->current_month->total_conf      = $current_month->result->confUSDdeposits + $current_month->result->confEURdeposits + $current_month->result->confGBPdeposits;
        $finance->current_month->total_ftd       = $current_month->result->countftd;
        $finance->current_month->all_withdrawals = $current_month->result->USDWithdrawals + $current_month->result->EURWithdrawals + $current_month->result->GBPWithdrawals;


        $finance->all_time->total_deposits    = $all_time->result->USD + $all_time->result->EUR + $all_time->result->GBP;
        $finance->all_time->ave_income        = $finance->all_time->total_deposits / $loader->user->exist_period;

        return $finance;
    }



    static function convertID($id){

        $result = ImportController::import('departments',array('department_name'),array('WHERE' => "id = {$id}"));

        return (count($result['data']) > 0) ? $result['data'][0]['department_name'] : false;
    }
}